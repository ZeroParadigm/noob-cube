//texture		SH_TEXTURE8;				/// render target texture
//float		SH_TIME;					/// application time
//float4		SH_SUN_POS;					/// sun position (in world space)
//float4x4	SH_MAT_COMBINED;			/// world * view * projection matrix
//float4x4	SH_MAT_PROJECTION;			/// projection matrix
//float4		SH_RENDER_TARGET_PARAMS;	/// (1.0f/rt_width, 1.0f/rt_height, rt_width, rt_height)
//
///// number of samples divided by 2
//#define NSAMPLES_2 8
///// real number of samples
//#define NSAMPLES (NSAMPLES_2*2)
///// radial glow step
//#define STEP (1.0f/NSAMPLES)
///// texcoord scale factor
//#define SCALE_FACTOR -2.5f
//
//
//sampler2D g_samSrcColor =
//sampler_state
//{
//    Texture = <SH_TEXTURE8>;
//    AddressU = Clamp;
//    AddressV = Clamp;
//    MinFilter = Point;
//    MagFilter = Linear;
//    MipFilter = Linear;
//};
//
//struct VS2PS
//{
//  float4 vPos : POSITION;
//  float4 vTexCoord0[NSAMPLES_2] : TEXCOORD0;
//};
//
//
///// scale texcoords
//float2 NTexcoord( float2 Texcoord, int iIdx, float3 c )
//{
//	/// calculate scale factor
//	float Scale = sqrt(iIdx) * c.z + 1.0;
//	/// perform texcoords scaling
//    return ( Texcoord.xy - c.xy ) * Scale + c.xy;
//}
//
///// vertex shader
//VS2PS RenderVS(float4 vPos : POSITION, 
//               float2 vTexCoord0 : TEXCOORD0, 
//               float2 vTexCoord1 : TEXCOORD1)
//{
//  VS2PS output;
//  
//  /// transform to screen space [-1, 1]
//  output.vPos.xy = 2.0f * vPos.xy * SH_RENDER_TARGET_PARAMS.xy - 1.0f;
//  output.vPos.y *= -1;
//  output.vPos.zw = float2(0.5f, 1.0f);
//
//  /// transform sun position
//  float4 sunPos = mul(SH_SUN_POS, SH_MAT_COMBINED); 
//  /// determine sun position in screen space and texcoord scale factor
//  float3 ts = float3((sunPos.x / sunPos.w) * 0.5f + 0.5f, 
//                     (-sunPos.y / sunPos.w) * 0.5f + 0.5f, 
//                     SCALE_FACTOR / (NSAMPLES));
//
//  /// calculate n = N_SAMPLES different mappings for sun radial glow
//  int j=0;
//  for (int i=0; i<NSAMPLES_2;i++)
//  {
//      output.vTexCoord0[i].xy = NTexcoord(vTexCoord0.xy, j, ts);
//      j++;
//      output.vTexCoord0[i].zw = NTexcoord(vTexCoord0.xy, j, ts);
//      j++;
//  }
//  
//  return output;
//}
//
//
///// pixel shader
//float4 RenderPS( float4 vTexCoord0[NSAMPLES_2] : TEXCOORD0 ) : COLOR0
//{
//	float4 col = 0;
//	float lum = 1.0f;
//	
//	/// sample radial glow buffer with different mappings and decreasing luminance
//	for (int i=0; i<NSAMPLES_2;i++)
//	{
//		col += tex2D(g_samSrcColor, vTexCoord0[i].xy) * lum;
//		lum -= STEP;
//		col += tex2D(g_samSrcColor, vTexCoord0[i].zw) * lum;
//		lum -= STEP;
//	}
//	return col * 0.25f;
//}
//
//technique PostProcess
//{
//    pass p0
//    {
//        VertexShader = compile vs_2_0 RenderVS();
//        PixelShader  = compile ps_2_0 RenderPS();
//        ZEnable = false;
//    }
//}
