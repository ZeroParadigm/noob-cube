//texture		SH_TEXTURE8;				/// render target texture
//texture		SH_TEXTURE9;				/// sun radial glow mask
//float		SH_TIME;					/// application time
//float4		SH_SUN_POS;					/// sun position (in world space)
//float4x4	SH_MAT_COMBINED;			/// world * view * projection matrix
//float4x4	SH_MAT_PROJECTION;			/// projection matrix
//float4		SH_RENDER_TARGET_PARAMS;	/// (1.0f/rt_width, 1.0f/rt_height, rt_width, rt_height)
//
//
//sampler2D g_samSrcColor =
//sampler_state
//{
//    Texture = <SH_TEXTURE8>;
//    AddressU = Clamp;
//    AddressV = Clamp;
//    MinFilter = Point;
//    MagFilter = Linear;
//    MipFilter = Linear;
//};
//sampler2D g_samMaskColor = sampler_state
//{
//    Texture = <SH_TEXTURE9>;
//    AddressU = Clamp;
//    AddressV = Clamp;
//    MinFilter = Point;
//    MagFilter = Linear;
//    MipFilter = Linear;
//};
//
//
//struct VS2PS
//{
//  float4 vPos : POSITION;
//  float4 vTexCoord0 : TEXCOORD0;
//};
//
//  
///// vertex shader
//VS2PS RenderVS(	float4 vPos : POSITION, 
//				float2 vTexCoord0 : TEXCOORD0, 
//				float2 vTexCoord1 : TEXCOORD1)
//{
//  VS2PS output;
//  
//  /// transform to screen space [-1, 1]
//  output.vPos.xy = 2.0f * vPos.xy * SH_RENDER_TARGET_PARAMS.xy - 1.0f;
//  output.vPos.y *= -1;
//  output.vPos.zw = float2(0.5f, 1.0f);
//  
//  /// transform sun position to screen space [-1, 1]
//  float4 sunPos = mul(SH_SUN_POS, SH_MAT_COMBINED); 
//  float2 ts = float2(sunPos.x / sunPos.w, -sunPos.y / sunPos.w);
//  
//  /// texture coordinates for source render target
//  output.vTexCoord0.xy = vTexCoord0.xy;
//  /// use sun position in screen space to determine texture coordinates for glow mask
//  output.vTexCoord0.zw = vTexCoord0.xy - 0.5f * ts.xy;
//  
//  return output;
//}
//
///// pixel shader
//float4 RenderPS( float4 vTexCoord0 : TEXCOORD0 ) : COLOR0
//{
//	/// sample render target
//	float4 diff1 = tex2D( g_samSrcColor, vTexCoord0.xy );
//	/// compute pixel luminance
//	float luminance = dot( diff1.xyz, 1.0f/3 );
//	/// adjust lighting contrast
//	diff1 *= luminance;
//	/// sample glow mask
//    float4 diff2 = tex2D( g_samMaskColor, vTexCoord0.zw );
//    /// combine render target and the mask
//	return diff1 * diff2;
//}
//
//
//technique PostProcess
//{
//    pass p0
//    {
//        VertexShader = compile vs_2_0 RenderVS();
//        PixelShader  = compile ps_2_0 RenderPS();
//        ZEnable = false;
//    }
//}