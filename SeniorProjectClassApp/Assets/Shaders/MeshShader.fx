struct material
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float specPower;
};

struct light
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float3 posW;
	float range;
};

uniform extern float4x4	gWVP;
uniform extern float4x4	gWorld;
uniform extern float4x4	gWorldInv;

uniform extern float3	gEyePos;

uniform extern float	gHeightScale;	//bump

uniform extern material	gMtrl;	
uniform extern light	gLight;

uniform extern texture	gTex;
uniform extern texture	gNorm;
uniform extern texture	gSpecular;
uniform extern texture	gIllumination;
uniform extern texture	gBump;

uniform extern bool		gDifFlag;
uniform extern bool		gBmpFlag;
uniform extern bool		gNrmFlag;
uniform extern bool		gSpcFlag;
uniform extern bool		gIllFlag;

/* ~~~~~~~~~~ Texture Sampler States ~~~~~~~~~~ */
sampler TexS = sampler_state
{
	Texture  = <gTex>;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MinFilter = Anisotropic;
	MaxAnisotropy = 8;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler NormalS = sampler_state
{
	Texture  = <gNorm>;
	Filter   = MIN_MAG_MIP_LINEAR;
	//MinFilter = POINT;
	//MagFilter = POINT;
	//MipFilter = POINT;
	AddressU = WRAP;
	AddressV = WRAP;
};

sampler SpecularS = sampler_state
{
	Texture		= <gSpecular>;
	Filter		= MIN_MAG_MIP_LINEAR;
	AddressU	= WRAP;
	AddressV	= WRAP;
};

sampler IlluminationS = sampler_state
{
	Texture		= <gIllumination>;
	Filter		= MIN_MAG_MIP_LINEAR;
	AddressU	= WRAP;
	AddressV	= WRAP;
};

sampler BumpS = sampler_state
{
	Texture		= <gBump>;
	filter		= MIN_MAG_MIP_LINEAR;
	AddressU	= WRAP;
	AddressV	= WRAP;
};


/* ~~~~~~~~~~ Data Structures ~~~~~~~~~~ */
struct VS_NormalIn
{
	float4 posL		: POSITION0;
	float3 normal	: NORMAL0;
	float3 binormal : BINORMAL0;
	float3 tangent	: TANGENT0;
	float2 tex0		: TEXCOORD0;
};

struct VS_NormalOut
{
	float4 posH		: POSITION0;
	float3 posW		: TEXCOORD0;
	float2 tex0		: TEXCOORD1;
	float3 eyeDir	: TEXCOORD2;
	float3 lightDir : TEXCOORD3;
};


/* ~~~~~~~~~~ Pass One - Uses normal map to texture object ~~~~~~~~~~ */
VS_NormalOut NormalVS(VS_NormalIn input)
{
	VS_NormalOut output = (VS_NormalOut)0;

	//Transform to homogeneous clip space
	output.posH = mul(input.posL, gWVP);

	float3 posW = mul(input.posL, gWorld);
	output.posW = posW;

	//pass texture coord on
	output.tex0 = input.tex0;

	// new way for bump
	float3x3 wtts;
	wtts[0] = mul(normalize(input.tangent), gWorld);
	wtts[1] = mul(normalize(input.binormal), gWorld);
	wtts[2] = mul(normalize(input.normal), gWorld);

	wtts = transpose(wtts);

	//~~~~

	/* old way without bump
	// Tangent-Binormal-Normal
	float3x3 tts;
	tts[0] = input.tangent;
	tts[1] = input.binormal;
	tts[2] = input.normal;

	// To Tangent Space
	tts = transpose(tts);
	*/

	//new way for bump

	output.lightDir = mul(normalize(gLight.posW - output.posW), wtts);
	output.eyeDir = mul(normalize(gLight.posW - output.posW), wtts);

	//~~~~


	/* Old way without bump
	float3 lightDir = mul(gLight.posW, gWorldInv);
	lightDir = normalize(lightDir - input.posL);
	output.lightDir = mul(lightDir, tts);
	
	float3 eyeDir = mul(gEyePos, gWorldInv);
	eyeDir = normalize(eyeDir - input.posL);
	output.eyeDir = mul(eyeDir, tts);
	*/

	return output;
}

float4 NormalPS(VS_NormalOut input) : COLOR
{
	//renormalize light and eye direction
	float3 eyeDir = normalize(input.eyeDir);
	float3 lightDir = normalize(input.lightDir);

	float3 toEye = normalize(gEyePos - input.posW);

	//calculate parallax info
	float height = tex2D(BumpS, input.tex0).r;
	height = gHeightScale * (2.0f * height - 1.0f);
	float2 displacedTex = input.tex0 + height * eyeDir.yx;


	//calculate color, normal, spec highlight and illumination with new texture coords
	float3 color = tex2D(TexS, displacedTex).xyz;
	float3 norm = normalize(2.0f * tex2D(NormalS, displacedTex) - 1.0f);
	float3 illuminationColor = tex2D(IlluminationS, displacedTex).xyz;
	float3 specValue = tex2D(SpecularS, displacedTex).xyz; 


	//calculate falloff
	float dist = distance(input.posW, gLight.posW);
	dist = min(gLight.range, dist);
	float power = 1.0f - (dist/gLight.range);


	//calculate diffuse lighting
	float shade = dot(norm, lightDir);
	power *= shade;


	//calculate specular lighting
	//float3 r = 2.0f * dot(lightDir, norm) * norm - lightDir;
	float3 r = reflect(lightDir, norm);
	float t;
	if(gSpcFlag)
		t = pow(max(dot(r, eyeDir), 0.0f), gMtrl.specPower) * specValue;
	else
		t = pow(max(dot(r, eyeDir), 0.0f), gMtrl.specPower);

	float s = max(dot(lightDir, norm), 0.0f);

	
	//float t = pow(max(dot(r, toEye), 0.0f), gMtrl.specPower) * specValue;
	float3 spec = t * (gMtrl.spec * gLight.spec).rbg;
	float3 ambient = (gMtrl.ambient * gLight.ambient).rgb;
	float3 diffuse = s * (gMtrl.diffuse * gLight.diffuse).rgb;

	//color = ((color + diffuse) + spec) * power + ((color +ambient) * 0.4);
	//color = (((((ambient + diffuse + color) + spec) * power) + ((ambient + diffuse + color) * 0.5f))) + (illuminationColor * 1.0f);

	if(gIllFlag)
		color = (((ambient + diffuse) * color + spec) * power) + (((ambient + diffuse) * color + spec) * 0.9f) + (illuminationColor * 1.0f);
	else
		color = (((ambient + diffuse) * color + spec) * power) + (((ambient + diffuse) * color + spec) * 0.9f);

	return float4(color, 1.0f);



	/* old way without using bump
	//normalize eye and light direction
	float3 eyeDir = normalize(input.eyeDir);
	float3 lightDir = normalize(input.lightDir);

	float3 lightVec = lightDir;

	float3 toEye = normalize(gEyePos - input.posW);

	//float3 norm = normalize((tex2D(NormalS, input.tex0).xyz - 0.0) * 2.0f);
	float3 norm = normalize(2.0f * tex2D(NormalS, input.tex0) - 0.0f);
	//float3 norm = 2.0f * tex2D(NormalS, input.tex0) - 1.0f;
	
	//light
	float dist = distance(input.posW, gLight.posW);
	dist = min(gLight.range, dist);
	float power = 1.0f-(dist/gLight.range);
	
	float shade = dot(norm, lightDir);
	power *= shade;

	
	//float3 r = 2.0f * dot(lightDir, norm) * norm - lightDir;	//old
	float3 r = reflect(lightVec, norm);
	//r = 0.0f;

	//float t = pow(abs(r), gMtrl.specPower); //highlights from specular power
	float4 SpecColor = tex2D(SpecularS, input.tex0); 
	float t = pow(max(dot(r, toEye), 0.0f), gMtrl.specPower) * SpecColor;
	//float t = pow(abs(r), tex2D(SpecularS, input.tex0).r); //highlights from specular map

	float s = max(dot(r, norm), 0.0f);

	//if(s >= 0.0f)
		//t = 0.0f;

	//float3 spec = gMtrl.specAttr * max(pow(s, gMtrl.shininess),0);	//old
	float3 spec = t * (gMtrl.spec * gLight.spec).rgb;		//new
	//float3 spec = (t * (gMtrl.spec * gMtrl.spec).rgb) * tex2D(SpecularS, input.tex0);
	float3 diffuse = s * (gMtrl.diffuse * gLight.diffuse).rgb;
	float3 ambient = gMtrl.ambient * gLight.ambient;

	float4 texColor = tex2D(TexS, input.tex0);
	float4 illuminationColor = tex2D(IlluminationS, input.tex0);

	
	float3 color = ((((ambient + diffuse) * texColor.rgb + spec) * power) + (((ambient + diffuse) * texColor.rgb + spec) * 0.8f)) + (illuminationColor.rgb * 0.5f);
	//float3 color = ((((ambient + diffuse) * texColor.rgb + spec) * power) + (illuminationColor.rgb * 0.0f));
	//float3 color = (((ambient + diffuse) * texColor.rgb + spec) * power);
	//float3 color = ((ambient * texColor.rgb + spec) * power);

	//float3 color = (((ambient + 0) * texColor.rgb + spec) * power);
	//if(power <= 0)
		//color = ((ambient + diffuse) * texColor.rgb + spec);
	//float3 color = (ambient + ((diffuse + spec) / A)) * texColor;
	
	//color = ((color + gLightColor) + spec) * power + gAmbientAttn;
	//color = (((gMtrl.diffuse * color.rgb + spec) * power) + 0.01f) + (gAmbientAttn * color.rgb * power);
	//color = (((gMtrl.diffuse * color.rgb + spec) * power) + 0.01f) + (color.rgb * power);


	//float t = pow(s, 0.5f);
	//float4 ambient = gMtrl.ambient * gLight.ambient;
	//float4 diffuse = gMtrl.diffuse * gLight.diffuse * 1.0;
	//float4 specular = t * (gMtrl.spec * gLight.spec);

	//color = (ambient + (diffuse * color) + spec);

	//color = (((gMtrl.diffuse * color) + spec) * power) + (gAmbientAttn * color * power);


	//spot = spotlight coefficient... same as "power" of the light
	//ambient = material ambient * light ambient
	//diffuse = material diffuse * light diffuse * diffuse intensity
	//specular = t * material spec * light spec  //t = in my lightshadow.fx file
	//color = spot * (ambient + diffuse * texturecol.rgb + specular);

	
	
	return float4(color, 1.0f);
	*/
}


/* ~~~~~~~~~~ Normal Mapping Technique ~~~~~~~~~~ */
technique NormalTech
{
	pass p0
	{
		vertexShader = compile vs_3_0 NormalVS();
		pixelShader  = compile ps_3_0 NormalPS();
	}
}