//=============================================================================
// PhongDirLtTex.fx by Frank Luna (C) 2004 All Rights Reserved.
//
// Phong directional light & texture.
//=============================================================================

struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float  specPower;
};

struct DirLight
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float3 dirW;  
};



uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern DirLight gLight;
uniform extern float3   gEyePosW;
uniform extern texture  gTex;
uniform extern int	gWireframe;
uniform extern texture  gPostTex;

//Switches
uniform extern bool     gTextured;


sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
   	AddressV  = WRAP;
};

sampler PostTexS = sampler_state
{
	Texture = <gPostTex>;
	MinFilter = Anisotropic;
	MaxAnisotropy = 8;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
   	AddressV  = WRAP;

};
 
struct PostOutputVS
{
	float2 tex0    : TEXCOORD0;
    float4 posH	   : POSITION0;
};

struct OutputVS
{
    float4 posH    : POSITION0;
    float3 normalW : TEXCOORD0;
    float3 toEyeW  : TEXCOORD1;
    float2 tex0    : TEXCOORD2;
    float3 color   : COLOR0;
};

struct VS_INPUT
{
	float4 Position		: POSITION0;
	float3 Normal		: NORMAL0;
	float2 TexCoord		: TEXCOORD0;
	float4 Color		: COLOR0;
};

struct VS_OUTPUT
{
	float4 Position		: POSITION0;
	float3 Normal		: NORMAL0;
	float2 TexCoord		: TEXCOORD0;
	float4 Color		: COLOR0;
	float3 toEyeW		: TEXCOORD1;
};

OutputVS PhongDirLtTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{

    // Zero out our output.
	OutputVS outVS = (OutputVS)0;
	
	// Transform normal to world space.
	outVS.normalW = mul(float4(normalL, 0.0f), gWorldInvTrans).xyz;
	
	// Transform vertex position to world space.
	float3 posW  = mul(float4(posL, 1.0f), gWorld).xyz;
	
	// Compute the unit vector from the vertex to the eye.
	outVS.toEyeW = gEyePosW - posW;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	
	// Pass on texture coordinates to be interpolated in rasterization.
	outVS.tex0 = tex0;

	// Done--return the output.
    	return outVS;
}

float4 PhongDirLtTexPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	normalW = normalize(normalW);
	toEyeW  = normalize(toEyeW);
	
	// Light vector is opposite the direction of the light.
	float3 lightVecW = -gLight.dirW;
	
	// Compute the reflection vector.
	float3 r = reflect(-lightVecW, normalW);
	
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, toEyeW), 0.0f), gMtrl.specPower);
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(lightVecW, normalW), 0.0f);
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 spec = t*(gMtrl.spec*gLight.spec).rgb;
	float3 diffuse = s*(gMtrl.diffuse*gLight.diffuse).rgb;
	float3 ambient = gMtrl.ambient*gLight.ambient;
	
	float4 texColor;
	if(gTextured)
	{
		texColor = tex2D(TexS, tex0);
	}else
	{
		texColor = float4(1.0, 1.0, 1.0, 1.0);
	}
	// Combine the color from lighting with the texture color.
	float3 color = (ambient + diffuse)*texColor.rgb + spec;
		
	// Sum all the terms together and copy over the diffuse alpha.
    return float4(color, gMtrl.diffuse.a*texColor.a);
}

VS_OUTPUT PhongDirLtTexLineVS(VS_INPUT Input)
{
	VS_OUTPUT Output;

	Output.Position = mul(Input.Position, gWVP);
	//Output.Position = Input.Position;
	Output.Color = Input.Color;
	Output.TexCoord = Input.TexCoord;
	//Output.Normal = mul(float4(Input.Normal, 0.0f), gWorldInvTrans).xyz;
	Output.Normal = Input.Normal;
	Output.toEyeW = gEyePosW - Input.Position;

	return Output;

 //   // Zero out our output.
	//OutputVS outVS = (OutputVS)0;
	//
	//// Transform normal to world space.
	//outVS.normalW = mul(float4(normalL, 0.0f), gWorldInvTrans).xyz;
	//
	//// Transform vertex position to world space.
	//float3 posW  = mul(float4(posL, 1.0f), gWorld).xyz;
	//
	//// Compute the unit vector from the vertex to the eye.
	//outVS.toEyeW = gEyePosW - posW;
	//
	//// Transform to homogeneous clip space.
	//outVS.posH = mul(float4(posL, 1.0f), gWVP);
	//
	//// Pass on texture coordinates to be interpolated in rasterization.
	//outVS.tex0 = tex0;

	//outVS.color = color;

	//// Done--return the output.
 //   return outVS;
}

float4 PhongDirLtTexLinePS(VS_OUTPUT Input) : COLOR
{
	//// Interpolated normals can become unnormal--so normalize.
	//Input.Normal = normalize(Input.Normal);
	//Input.toEyeW  = normalize(Input.toEyeW);
	//
	//// Light vector is opposite the direction of the light.
	//float3 lightVecW = -gLight.dirW;
	//
	//// Compute the reflection vector.
	//float3 r = reflect(-lightVecW, Input.Normal);
	//
	//// Determine how much (if any) specular light makes it into the eye.
	//float t  = pow(max(dot(r, Input.toEyeW), 0.0f), gMtrl.specPower);
	//
	//// Determine the diffuse light intensity that strikes the vertex.
	//float s = max(dot(lightVecW, Input.Normal), 0.0f);
	//
	//// Compute the ambient, diffuse and specular terms separatly. 
	//float3 spec = t*(gMtrl.spec*gLight.spec).rgb;
	//float3 diffuse = s*(gMtrl.diffuse*gLight.diffuse).rgb;
	//float3 ambient = gMtrl.ambient*gLight.ambient;
	//
	//// Get the texture color.
	//float3 texColor = Input.Color;
	//
	//// Combine the color from lighting with the texture color.
	//float3 finalColor = (ambient + diffuse)*texColor.rgb + spec;
	//	
	//// Sum all the terms together and copy over the diffuse alpha.
 //   //return float4(finalColor, gMtrl.diffuse.a*texColor.a);


	float4 pixel = tex2D(TexS, Input.TexCoord.xy) * Input.Color;
	//pixel = saturate(pixel);

	return pixel;
}



PostOutputVS PostVS(float3 posL : POSITION0, float2 tex0: TEXCOORD0)
{
     // Zero out our output.
	PostOutputVS outVS = (PostOutputVS)0;
    outVS.posH = mul(float4(posL,1.0), gWVP);
	outVS.tex0 = tex0;
    return outVS;
}

float4 PostPS(float2 tex0 : TEXCOORD0) : COLOR
{
	float4 texColor = tex2D(PostTexS, tex0);
    return texColor;
}


technique PhongDirLtTexTechFilled
{

    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PhongDirLtTexVS();
        pixelShader  = compile ps_3_0 PhongDirLtTexPS();

    }
}

technique PhongDirLtTexTechWireframe
{

    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PhongDirLtTexVS();
        pixelShader  = compile ps_3_0 PhongDirLtTexPS();
	
	FillMode = Wireframe;
	CullMode = None;
    }
}

technique PhoongDirLtTexLineTech
{
	pass P0
	{
		vertexShader = compile vs_3_0 PhongDirLtTexLineVS();
		pixelShader	 = compile ps_3_0 PhongDirLtTexLinePS();
	}
}

technique PostProcess
{
	pass P0
	{
		vertexShader = compile vs_3_0 PostVS();
        pixelShader  = compile ps_3_0 PostPS();
	}
}