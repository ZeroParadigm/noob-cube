texture g_txSrcColor;
texture g_txSrcNormal;
texture g_txSrcPosition;
texture g_txSrcVelocity;

texture g_txSceneColor;
texture g_txSceneNormal;
texture g_txScenePosition;
texture g_txSceneVelocity;

sampler2D g_samSrcColor =
sampler_state
{
	Texture = <g_txSrcColor>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcNormal =
sampler_state
{
	Texture = <g_txSrcNormal>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcPosition =
sampler_state
{
	Texture = <g_txSrcPosition>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcVelocity =
sampler_state
{
	Texture = <g_txSrcVelocity>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};

sampler2D g_samSceneColor = sampler_state
{
	Texture = <g_txSceneColor>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSceneNormal = sampler_state
{
	Texture = <g_txSceneNormal>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samScenePosition = sampler_state
{
	Texture = <g_txScenePosition>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSceneVelocity = sampler_state
{
	Texture = <g_txSceneVelocity>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};


float4 OverlayColor = float4(0.0f, 0.0f, 0.0f, 0);


//-----------------------------------------------------------------------------
// Pixel Shader: HorizontalBlur
// Desc: Blurs the image horizontally
//-----------------------------------------------------------------------------
float4 PostProcessPS(float2 Tex : TEXCOORD0) : COLOR0
{
	//float4 Color = 0;

	//for (int i = 0; i < g_cKernelSize; i++)
	//{
	//	Color += tex2D(g_samSrcColor, Tex + TexelKernel[i].xy) * BlurWeights[i];
	//}

	//return Color * OverlayColor;

	return tex2D(g_samSrcColor, Tex) * OverlayColor;
}


//-----------------------------------------------------------------------------
// Technique: PostProcess
// Desc: Performs post-processing effect that converts a colored image to
//       black and white.
//-----------------------------------------------------------------------------
technique PostProcess
<
string Parameter0 = "OverlayColor";
float4 Parameter0Def = float4(0.0f, 0.0f, 0.75f, 0);
int Parameter0Size = 4;
string Parameter0Desc = " (vector of 4 floats)";
>
{
	pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 PostProcessPS();
		ZEnable = false;
	}
}
