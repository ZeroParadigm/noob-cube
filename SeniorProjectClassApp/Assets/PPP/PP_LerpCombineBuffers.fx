texture g_txSrcColor;
texture g_txSrcNormal;
texture g_txSrcPosition;
texture g_txSrcVelocity;

texture g_txSceneColor;
texture g_txSceneNormal;
texture g_txScenePosition;
texture g_txSceneVelocity;


float FX_TE = 0.0f;
//float FX_DT = 0.0f;


sampler2D g_samSrcColor =
sampler_state
{
	Texture = <g_txSrcColor>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcNormal =
sampler_state
{
	Texture = <g_txSrcNormal>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcPosition =
sampler_state
{
	Texture = <g_txSrcPosition>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSrcVelocity =
sampler_state
{
	Texture = <g_txSrcVelocity>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};

sampler2D g_samSceneColor = sampler_state
{
	Texture = <g_txSceneColor>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSceneNormal = sampler_state
{
	Texture = <g_txSceneNormal>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samScenePosition = sampler_state
{
	Texture = <g_txScenePosition>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};
sampler2D g_samSceneVelocity = sampler_state
{
	Texture = <g_txSceneVelocity>;
	AddressU = Clamp;
	AddressV = Clamp;
	MinFilter = Point;
	MagFilter = Linear;
	MipFilter = Linear;
};

float4 PostProcessPS(float2 Tex : TEXCOORD0) : COLOR0
{
	float f = 0.5f * (1.0f + sin(FX_TE * 0.003f));
	//float f = 0.5f * (1.0f + sin(FX_TE));
	float4 diff1 = tex2D(g_samSrcColor, Tex);
	float4 diff2 = tex2D(g_samSceneColor, Tex);
	//return lerp(diff1, diff2, f);
	return lerp(diff1, diff2, .5);
}


technique PostProcess
<
string TemporalVar = "FX_TE";
//string TemporalVar = "FX_DT";
>
{
	pass p0
	{
		VertexShader = null;
		PixelShader = compile ps_2_0 PostProcessPS();
		ZEnable = false;
		//        AlphaBlendEnable = false;
		//        SrcBlend = SrcAlpha;
		//        DestBlend = InvSrcAlpha;
	}
}