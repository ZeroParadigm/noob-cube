#include <Windows.h>

#include "SimpleEngine.h"

#include "CommonUtilities.h"
#include "DebugHelp.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{
	//Utils::EnableConsole();
	DebugHelp::EnableLeakDetection();
	DebugHelp::SetBreakPointAlloc(0);

	SimpleEngine engine;

	engine.Init(hInstance, _T("Noob Cube"));

	return engine.Run();
}