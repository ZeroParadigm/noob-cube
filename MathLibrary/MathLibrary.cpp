#include "MathLibrary.h"
#include <stdexcept>
#include <random>
#include <chrono>

namespace DXMathLibrary
{
	const Matrix4 Matrix4::g_Identity(D3DXMATRIX(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1));
	const Quaternion Quaternion::g_Identity(D3DXQUATERNION(0,0,0,1));

	/***********************************************************
		Vector2 Math class that extended the D3D math class
	************************************************************/

	Vector2::Vector2() : D3DXVECTOR2(0.0f, 0.0f) {}

	Vector2::Vector2(D3DXVECTOR2 &vector2)
	{
		x = vector2.x;
		y = vector2.y;
	}

	Vector2::Vector2(const float fx, const float fy)
	{
		x = fx;
		y = fy;
	}

	inline float Vector2::Length()
	{
		return D3DXVec2Length(this);
	}

	inline float Vector2::LengthSqr()
	{
		return D3DXVec2LengthSq(this);
	}

	inline float Vector2::Dot(const Vector2 &rhs)
	{
		return D3DXVec2Dot(this, &rhs);
	}

	inline float Vector2::PerpDot(const Vector2 &rhs)
	{
		return (-y * rhs.x) + (x * rhs.y);
	}

	inline Vector2 Vector2::Normal()
	{
		Vector2 normal(x, y);
		normal.Normalize();
		return normal;
	}

	inline Vector2* Vector2::Normalize()
	{
		return (Vector2*)D3DXVec2Normalize(this, this);
	}


	/***********************************************************
		Vector3 Math class that extended the D3D math class
	************************************************************/
	Vector3::Vector3() : D3DXVECTOR3(0.0f, 0.0f, 0.0f) {}

	Vector3::Vector3(D3DXVECTOR3 &Vector3)
	{
		x = Vector3.x; 
		y = Vector3.y; 
		z = Vector3.z;
	}

	Vector3::Vector3(const float fx, const float fy, const float fz)
	{
		 x = fx; 
		 y = fy; 
		 z = fz;
	}

	Vector3::Vector3(const class Vector4 &v4)
	{
		x = v4.x; 
		y = v4.y; 
		z = v4.z;
	}

	inline void Vector3::TransformCoord(Matrix4 mat)
	{
		D3DXVec3TransformCoord(this, this, &mat);
	}

	inline void Vector3::TransformCoord(Vector3 vec, Matrix4 mat)
	{
		D3DXVec3TransformCoord(this, &vec, &mat);
	}

	inline float Vector3::Length()
	{
		return D3DXVec3Length(this);
	}

	inline float Vector3::LengthSq()
	{ 
		return D3DXVec3LengthSq(this);				
	}

	inline float Vector3::Dot(const Vector3 &b)				
	{ 
		return D3DXVec3Dot(this, &b);
	}

	inline Vector3* Vector3::Normalize()
	{
		return (Vector3*)D3DXVec3Normalize(this,this);
	}

	inline Vector3 Vector3::Cross(const Vector3 &b) const
	{
		Vector3 out;
		D3DXVec3Cross(&out, this, &b);
		return out;
	}

	inline Vector3 Vector3::ProjectOnto(const Vector3& b) const
	{
		Vector3 out;

		float num, den;
		num = D3DXVec3Dot(this, &b);
		den = D3DXVec3Dot(&b, &b);

		out = b * (num/den);
		
		return out;
	}

	inline Vector3 Vector3::ProjectVectorOnPlane(Vector3& planeNormal, Vector3& vector)
	{
		return vector - (vector.Dot(planeNormal) * planeNormal);
	}

	inline Vector3 Vector3::ProjectPointOnPlane(Vector3& planeNormal, const Vector3& planePoint, const Vector3& point)
	{
		float distance;
		Vector3 translation;

		distance = DistanceFromPlane(planeNormal, planePoint, point);

		distance *= -1.0f;

		translation = planeNormal * distance;

		return point + translation;
	}

	inline float Vector3::DistanceFromPlane(Vector3& planeNormal, const Vector3& planePoint, const Vector3& point)
	{
		return planeNormal.Dot(point - planePoint);
	}

	inline bool Vector3::Equal(const Vector3& rhs)
	{
		return Vector3(*this - rhs).LengthSq() < 0.01;
	}


	/***********************************************************
		Vector4 Math class that extended the D3D math class
	************************************************************/
	Vector4::Vector4() : D3DXVECTOR4() {}

	Vector4::Vector4(D3DXVECTOR4 &vec4)
	{
		x = vec4.x;
		y = vec4.y; 
		z = vec4.z; 
		w = vec4.w;
	}

	Vector4::Vector4(const float fx, const float fy, const float fz, const float fw)
	{
		x = fx; 
		y = fy; 
		z = fz; 
		w = fw;
	}

	Vector4::Vector4(const Vector3 &Vector3)
	{
		x = Vector3.x; 
		y = Vector3.y; 
		z = Vector3.z; 
		w = 1.0f;
	}

	inline float Vector4::Length()
	{
		return D3DXVec4Length(this);
	}

	inline float Vector4::LengthSq()
	{
		return D3DXVec4LengthSq(this);
	}

	inline float Vector4::Dot(const Vector4 &b)
	{
		return D3DXVec4Dot(this, &b);
	}

	inline Vector4* Vector4::Normalize()
	{
		return (Vector4*)D3DXVec4Normalize(this,this);
	}

	/***********************************************************
		Quaternion Math class that extended the D3D math class
	************************************************************/
	Quaternion::Quaternion() : D3DXQUATERNION(0.0f, 0.0f, 0.0f, 1.0f) {}
	Quaternion::Quaternion(D3DXQUATERNION &q) : D3DXQUATERNION(q) {}
	Quaternion::Quaternion(const float& fx,
						   const float& fy,
						   const float& fz,
						   const float& fw) : D3DXQUATERNION(fx, fy, fz, fw) {}

	void Quaternion::Normalize() 
	{
		D3DXQuaternionNormalize(this, this);
	}
	void Quaternion::Slerp(const Quaternion &begin, const Quaternion &end, float cooef)
	{
		//performs spherical linear interpolation between begin and end
		//note: set cooef between 0.0f - 1.0f
		D3DXQuaternionSlerp(this, &begin, &end, cooef);
	}

	Vector3 Quaternion::VectorTransform(const Vector3& v)
	{
		//Convert the vector into a quaternion
		Quaternion vectorAsQuat = Quaternion(v.x, v.y, v.z, 0);

		//Transform it
		(*this).Normalize();
		Quaternion inv = (*this);
		//inv.Invert();
		inv.Conjugate();
		inv.Normalize();

		vectorAsQuat = inv * vectorAsQuat * (*this);

		//Return the transformed vector
		return Vector3(vectorAsQuat.x, vectorAsQuat.y, vectorAsQuat.z);
	}

	Vector3 Quaternion::Forward()
	{
		return Vector3( 2 * (x * z + w * y),
						2 * (y * z - w * x),
						1 - 2 * (x * x + y * y));
	}

	Vector3 Quaternion::Up()
	{
		return Vector3( 2 * (x * y - w * z),
						1 - 2 * (x * x + z * z),
						2 * (y * z + w * x));
	}

	Vector3 Quaternion::Right()
	{
		return Vector3( 1 - 2 * (y * y + z * z),
						2 * (x * y + w * z),
						2 * (x * z - w * y));
	}

	void Quaternion::GetAxisAngle(Vector3 &axis, float &angle)const
	{
		D3DXQuaternionToAxisAngle(this, &axis, &angle);
	}

	void Quaternion::BuildUp(const Vector3& forward,
							 const Vector3& up)
	{
		Vector3 u, f, r;
		f = forward;
		u = up;
		r = u.Cross(f);

		w = sqrtf(1.0f + r.x + u.y + f.z) * 0.5f;
		float w_recip = 1.0f / (4.0f * w);
		x = (u.z - f.y) * w_recip;
		y = (f.x - r.z) * w_recip;
		z = (r.y - u.x) * w_recip;
	}

	void Quaternion::BuildRotYawPitchRoll(const float yawRadians,
								const float pitchRadians,
								const float rollRadians)
	{
		D3DXQuaternionRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians);
	}

	void Quaternion::Invert()
	{
		(*this).Normalize();
		D3DXQuaternionInverse(this, this);
	}

	void Quaternion::Conjugate()
	{
		(*this).Normalize();
		D3DXQuaternionConjugate(this, this);
	}

	void Quaternion::Identity()
	{
		*this = Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
	}

	Matrix4 Quaternion::BuildMatrix()
	{
		Matrix4 ret;
		D3DXMatrixRotationQuaternion(&ret, this);
		return ret;
	}

	void Quaternion::BuildAxisAngle(const Vector3 &axis, const float radians)
	{
		D3DXQuaternionRotationAxis(this, &axis, radians);
	}

	void Quaternion::Build(const Matrix4 &matrix)
	{
		//D3DXQuaternionRotationMatrix(this, &matrix);
	}

	inline Quaternion operator * (const Quaternion &a, const Quaternion &b)
	{
		// for rotaitons, this is exactly like concatenating
		// matrices - the new quat represents rot A follwed by rot B.
		Quaternion out;
		D3DXQuaternionMultiply(&out, &a, &b);
		return out;

		//return a * b;
	}

	inline Quaternion operator * (const Quaternion& a, const D3DXQUATERNION &b)
	{
		Quaternion out;
		D3DXQuaternionMultiply(&out, &a, &b);
		return out;
	}

	/***********************************************************
		Matrix4 Math class that extended the D3D math class
	************************************************************/
	Matrix4::Matrix4() : D3DXMATRIX() 
	{
		memcpy(&m, &Matrix4::g_Identity, sizeof(Matrix4::g_Identity));
	}
	Matrix4::Matrix4(D3DXMATRIX &matrix)							
	{ 
		memcpy(&m, &matrix.m, sizeof(matrix.m));			
	}

	inline void Matrix4::SetPosition(Vector3 const &pos)
	{
		m[3][0] = pos.x;
		m[3][1] = pos.y;
		m[3][2] = pos.z;
		m[3][3] = 1.0f;
	}

	inline void Matrix4::SetPosition(Vector4 const &pos)
	{
		m[3][0] = pos.x;
		m[3][1] = pos.y;
		m[3][2] = pos.z;
		m[3][3] = pos.w;
	}

	inline Vector3 Matrix4::GetPosition() const
	{
		return Vector3(m[3][0], m[3][1], m[3][2]);
	}

	inline Vector3 Matrix4::GetDirection() const
	{
		Vector3 g_Forward(0.0f, 0.0f, 1.0f);
		Matrix4 justRot = *this;
		justRot.SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		Vector3 forward = justRot.Xform(g_Forward);
		return forward;
	}

	inline Vector4 Matrix4::Xform(Vector4 &v) const
	{
		Vector4 temp;
		D3DXVec4Transform(&temp, &v, this);
		return temp;
	}

	inline Vector3 Matrix4::Xform(Vector3 &vec) const
	{
		Vector4 temp(vec), out;
		D3DXVec4Transform(&out, &temp, this);
		return Vector3 (out.x, out.y, out.z);
	}

	inline Matrix4 Matrix4::Inverse() const
	{
		Matrix4 out;
		D3DXMatrixInverse(&out, NULL, this);
		return out;
	}

	inline void Matrix4::BuildTranslation(const Vector3 &pos)
	{
		*this = Matrix4::g_Identity;
		m[3][0]  = pos.x; m[3][1] = pos.y; m[3][2] = pos.z;
	}

	inline void Matrix4::BuildTranslation(const float x, const float y, const float z)
	{
		*this = Matrix4::g_Identity;
		m[3][0] = x; m[3][1] = y; m[3][2] = z;
	}

	inline void Matrix4::BuildRotationX(const float radians)					
	{ 
		D3DXMatrixRotationX(this, radians); 
	}

	inline void Matrix4::BuildRotationY(const float radians)					
	{ 
		D3DXMatrixRotationY(this, radians); 
	}

	inline void Matrix4::BuildRotationZ(const float radians)					
	{ 
		D3DXMatrixRotationZ(this, radians); 
	}

	inline void Matrix4::BuildRotationAxis(const Vector3 vec, const float angle)
	{
		D3DXMatrixRotationAxis(this, &vec, angle);
	}

	inline void Matrix4::BuildYawPitchROll(const float yawRadians, const float pitchRadians, const float rollRadians)
	{
		D3DXMatrixRotationYawPitchRoll(this, yawRadians, pitchRadians, rollRadians); 
	}
		
	inline void Matrix4::BuildRotationQuat(const Quaternion &q)
	{
		D3DXMatrixRotationQuaternion(this, &q);
	}

	inline void Matrix4::BuildRotationLookAt(const Vector3 &eye, const Vector3 &at, const Vector3 &up)
	{
		D3DXMatrixLookAtRH(this, &eye, &at, &up);
	}

	inline void Matrix4::BuildScale(const Vector3 scale)
	{
		D3DXMatrixScaling(this, scale.x, scale.y, scale.z);
	}

	inline Matrix4 operator* (const Matrix4 &a, const Matrix4 &b)
	{
		Matrix4 out;
		D3DXMatrixMultiply(&out, &a, &b);
		return out;
	}


	/***********************************************************
						Math Utility Functions
	************************************************************/

	float RandomBinomial(void)
	{
		std::default_random_engine generator;
		generator.seed((unsigned long)std::chrono::high_resolution_clock::now().time_since_epoch().count());
		//std::binomial_distribution<int> distribution(100, 0.5);

		std::uniform_real_distribution<float> distribution(-1.0f, 1.0f);

		//float x1 = (float)distribution(generator) / 100;
		//float x2 = (float)distribution(generator) / 100;

		//return x1 - x2

		return distribution(generator);
	}

	//Custom modulus function for mapping to pi  (Original function by Lio Kogan @ StackOverflow.com)
	float mod(float x, float y)
	{
		if (0.0 == y)
		{
			return x;
		}

		float m = x - y * floor(x / y);

		//Handle boundary cases
		if (y > 0)
		{
			if (m >= y)
			{
				return 0;
			}
			if (m < 0)
			{
				if (y + m == y)
					return 0;
				else
					return y + m;
			}
		}
		else
		{
			if (m <= y)
			{
				return 0;
			}
			if (m > 0)
			{
				if (y + m == y)
					return 0;
				else
					return y + m;
			}
		}
		return m;
	}

	inline float MapToPi(float rotation)	// <ALERT> Re-evaluated by Jesse (12.2.13 jd)
	{
		float angle = mod(rotation + pi, 2 * pi) - pi; 
		return angle;
	}

	Vector3 WorldToLocal(Vector3 obj, Vector3 axis)
	{
		Matrix4 objTransform;
		D3DXMatrixTranslation(&objTransform, obj.x, obj.y, obj.z);
		D3DXMatrixInverse(&objTransform, 0, &objTransform);

		Vector3 result;
		Matrix4 axisZ, worldTransform;

		D3DXMatrixTranslation(&axisZ, axis.x, axis.y, axis.z);

		worldTransform = objTransform * axisZ;

		D3DXVec3TransformCoord(&result, &obj, &worldTransform);

		return result;
	}

	Vector3 TransformBtoA(Vector3 posA, Vector3 posB)
	{
		Vector3 newB;
		Matrix4  invA, b;

		D3DXMatrixTranslation(&invA, posA.x, posA.y, posA.z);
		D3DXMatrixTranslation(&b, posB.x, posB.y, posB.z);
		D3DXMatrixInverse(&invA, 0, &invA);

		b = invA * b;

		D3DXVec3TransformCoord(&newB, &posB, &b);

		return newB;
	}

	void ClampToDomain(const float min, const float max, float& value)
	{
		float result = value;
		result = max(result, min);
		result = min(result, max);
		value = result;
	}

	Vector3 TransformQuaternion(Vector3 vector, Quaternion quat) //<ALERT> Reevaluate - Need to switch transform order? (1-20-14 jd)
	{
		//Convert the vector into a quaternion
		Quaternion vectorAsQuat = Quaternion(vector.x, vector.y, vector.z, 0);

		Quaternion inv = quat;
		inv.Invert();

		//Transform it
		vectorAsQuat = quat * vectorAsQuat * inv;

		//Return transformed vector
		return Vector3(vectorAsQuat.x, vectorAsQuat.y, vectorAsQuat.z);
	}

}