#pragma once
#include <d3dx9.h>
#include <d3d9.h>

#pragma warning(disable : 4275)

//#undef _USRDLL //Force disable of DLL semantics

#ifdef _USRDLL

#ifdef MATHLIBRARY_EXPORTS
#define DLLPORT __declspec(dllexport)
#else
#define DLLPORT __declspec(dllimport)
#endif

#else
#define DLLPORT
#endif

namespace DXMathLibrary
{

#define pi D3DX_PI
#define _epsilon D3DX_16F_EPSILON
	
	class Matrix4;

	class DLLPORT Vector2 : public D3DXVECTOR2
	{
	public:
		Vector2();
		Vector2(D3DXVECTOR2 &vector2);
		Vector2(const float fx,
				const float fy);
		
	public:
		inline float		Length();
		inline float		LengthSqr();
		inline float		Dot(const Vector2 &rhs);
		inline float		PerpDot(const Vector2 &rhs);

		inline Vector2		Normal();
		inline Vector2*		Normalize();

	};

	typedef Vector2 Vec2;

	class DLLPORT Vector3 : public D3DXVECTOR3
	{
	public:
		Vector3();
		Vector3(D3DXVECTOR3 &Vector3);
		Vector3(const float fx, 
				const float fy, 
				const float fz);

		Vector3(const class Vector4 &v4);

	public: 
		inline void			TransformCoord(Matrix4 mat);
		inline void			TransformCoord(Vector3 vec, Matrix4 mat);

		inline float		Length();
		inline float		LengthSq();
		inline float		Dot(const Vector3 &b);

		inline Vector3		*Normalize();
		inline Vector3		Cross(const Vector3 &b) const;

		inline Vector3      ProjectOnto(const Vector3 &b) const;
		inline Vector3		ProjectVectorOnPlane(Vector3& planeNormal, Vector3& vector);
		inline Vector3		ProjectPointOnPlane(Vector3& planeNormal, const Vector3& planePoint, const Vector3& point);

		inline float		DistanceFromPlane(Vector3& planeNormal, const Vector3& planePoint, const Vector3& point);

		inline bool			Equal(const Vector3& rhs);
	};

	typedef Vector3 Vec3;

	class DLLPORT Vector4 : public D3DXVECTOR4
	{
	public:
		Vector4();
		Vector4(D3DXVECTOR4 &vec4);
		Vector4(const float fx, 
				const float fy, 
				const float fz, 
				const float fw);

		Vector4(const Vector3 &Vector3);

	public:
		inline float		Length();
		inline float		LengthSq();
		inline float		Dot(const Vector4 &b);

		inline Vector4		*Normalize();
		//cross product would be D3DXVec3Cross()
	};

	typedef Vector4 Vec4;

	class DLLPORT Quaternion : public D3DXQUATERNION
	{
	public:
		Quaternion();
		Quaternion(D3DXQUATERNION &q);
		Quaternion(const float& fx,
				   const float& fy,
				   const float& fz,
				   const float& fw);

		static const Quaternion g_Identity;

	public:
		void		Normalize();
		void		Slerp(const Quaternion &begin, const Quaternion &end, float cooef);
		void		BuildAxisAngle(const Vector3 &axis, const float radians);
		void		Build(const Matrix4 &matrix);
		void		GetAxisAngle(Vector3 &axis, float &angle) const;
		void		BuildUp(const Vector3& forward,
							const Vector3& up);
		void		BuildRotYawPitchRoll(const float yawRadians,
										 const float pitchRadians,
										 const float rollRadians);
		void		Invert();
		void		Conjugate();
		void		Identity();

		Matrix4		BuildMatrix();

		Vector3		VectorTransform(const Vector3& v);

		Vector3		Forward();
		Vector3		Up();
		Vector3		Right();

	};

	typedef Quaternion Quat;

	DLLPORT inline Quaternion operator * (const Quaternion &a, const Quaternion &b);
	DLLPORT inline Quaternion operator * (const Quaternion &a, const D3DXQUATERNION &b);

	class DLLPORT Matrix4 : public D3DXMATRIX
	{
	public:
		Matrix4();
		Matrix4(D3DXMATRIX &matrix);

		static const Matrix4 g_Identity;

	public:
		inline void		SetPosition(Vector3 const &pos);
		inline void		SetPosition(Vector4 const &pos);
		inline void		BuildTranslation(const Vector3 &pos);
		inline void		BuildTranslation(const float x, const float y, const float z);
		inline void		BuildRotationX(const float radians);
		inline void		BuildRotationY(const float radians);
		inline void		BuildRotationZ(const float radians);
		inline void		BuildRotationAxis(const Vector3 vec, const float angle);
		inline void		BuildRotationQuat(const Quaternion &q);
		inline void		BuildRotationLookAt(const Vector3 &eye, const Vector3 &at, const Vector3 &up);
		inline void		BuildYawPitchROll(const float yawRadians, 
										  const float pitchRadians, 
										  const float rollRadians);
		inline void		BuildScale(const Vector3 scale);

		inline Vector3	GetPosition() const;
		inline Vector3	GetDirection() const;
		inline Vector3	Xform(Vector3 &vec) const;

		inline Vector4	Xform(Vector4 &v) const;

		inline Matrix4	Inverse() const;
	};

	typedef Matrix4 Matrix;

	DLLPORT inline Matrix4 operator* (const Matrix4 &a, const Matrix4 &b);




	/***********************************************************
						Math Utility Functions
	************************************************************/

	//Random number between -1 and 1
	DLLPORT float RandomBinomial(void);

	//Map float rotation to 0 < rotation < 2Pi
	DLLPORT inline float MapToPi(float rotation);

	//Transform world A to local B
	DLLPORT Vector3 WorldToLocal(Vector3 A, Vector3 B);

	//Transform object B into A space
	DLLPORT Vector3 TransformBtoA(Vector3 posA, Vector3 posB);

	//Clamp value to domain
	DLLPORT void ClampToDomain(const float min, const float max, float& value);

	//Transform Quaternion
	DLLPORT Vector3 TransformQuaternion(Vector3 v, Quaternion q);
}