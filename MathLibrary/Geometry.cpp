#include "Geometry.h"

namespace DXMathLibrary
{


	/**
	 * Sphere
	 */

	Sphere::Sphere(void)
	{
	}

	Sphere::Sphere(const Vector3& center)
	{
		m_Center = center;
	}

	Sphere::Sphere(const Vector3& center, const float& radius)
	{
		  m_Center = center; 
		  m_Radius = radius;
	}

	Sphere::~Sphere(void)
	{
	}

	float Sphere::GetSurfaceArea(void)
	{
		return 4 * D3DX_PI * (m_Radius * m_Radius);
	}

	float Sphere::GetVolume(void)
	{
		return (4/3) * D3DX_PI * (m_Radius * m_Radius * m_Radius);
	}

	/**
	 * AABB
	 */

	AABB::AABB(void)
	{
		m_X = 1.0f; 
		m_Y = 1.0f; 
		m_Z = 1.0f;
	}

	AABB::AABB(const Vector3& center)
	{
		m_Center = center; 
		m_X = 1.0f; 
		m_Y = 1.0f; 
		m_Z = 1.0f;
	}

	AABB::AABB(const Vector3& center, const float& x, const float& y, const float& z)
	{
		m_Center = center; 
		m_X = x; 
		m_Y = y; 
		m_Z = z;
	}

	AABB::~AABB(void)
	{
	}

	float AABB::GetSurfaceArea(void)
	{
		//Values are half-extents
		return (4 * m_X) + (4 * m_Y) + (4 * m_Z);
	}

	float AABB::GetVolume(void)
	{
		return (2 * m_X) * (2 * m_Y) * (2 * m_Z);
	}

	float AABB::MinX(void)
	{
		return m_Center.x - m_X;
	}

	float AABB::MaxX(void)
	{
		return m_Center.x + m_X;
	}

	float AABB::MinY(void)
	{
		return m_Center.y - m_Y;
	}

	float AABB::MaxY(void)
	{
		return m_Center.y + m_Y;
	}

	float AABB::MinZ(void)
	{
		return m_Center.z - m_Z;
	}

	float AABB::MaxZ(void)
	{
		return m_Center.z + m_Z;
	}

	/**
	 * Line
	 */

	Line::Line(void)
	{
		m_A = Vector3(0.0f, 0.0f, 0.0f); 
		m_B = Vector3(0.0f, 0.0f, 0.0f);
	}

	Line::Line(const Vector3& a, const Vector3& b)
	{
		m_A = a; 
		m_B = b;
	}

	Line::~Line(void)
	{
	}

	/**
	 * Ray
	 */

	Ray::Ray(void)
	{
	}

	Ray::Ray(const Vector3& a, const Vector3& dir)
	{
		m_A = a;
		m_Direction = dir;
	}

	Ray::Ray(const Line t)
	{
		ComputeRay(t.m_A, t.m_B);
	}

	Ray::~Ray(void)
	{
	}

	void Ray::ComputeRay(const Vector3& a, const Vector3& b)
	{
		Vector3 dir = b - a;

		m_A = a;
		m_Direction = *dir.Normalize();
	}

	Vector3 Ray::GetPointOnRay(void)
	{
		return m_A + m_Direction;
	}


	/** 
	 * Plane
	 */

	Plane::Plane(void)
	{
		m_Normal = Vector3(0.0f, 0.0f, 0.0f); 
		m_Distance = 0.0f;
	}

	Plane::Plane(Vector3 normal, float distance)
	{
		m_Normal = normal; 
		m_Distance = distance;
	}

	Plane::Plane(Vector3 a, Vector3 b, Vector3 c)
	{
		ComputePlane(a, b, c);
	}

	Plane::~Plane(void)
	{
	}

	void Plane::ComputePlane(Vector3 a, Vector3 b, Vector3 c)
	{
		Vector3 ba, ca;

		ba = b-a;
		ca = c-a;

		m_Normal = ba.Cross(ca);
		m_Normal.Normalize();

		m_Distance = m_Normal.Dot(a);
	}

	void Plane::ClosestPoint(Vector3& normal, float& distance)
	{
		//TODO: Implement
	}

	bool Plane::LineIntersect(const Vector3& start, const Vector3& end, Vector3& ip, float& distance)
	{
		//For debugging 
		Plane p = *this;

		Vector3 direction = end - start;
		float dot = m_Normal.Dot(direction);

		if (dot <= _epsilon)
		{
			return false;	//The normal and line are parallel
		}

		float t = (m_Distance - m_Normal.Dot(start)) / dot;

		if ((t < 0.0f) || (t > 1.0f))
		{
			return false;	//No intersection
		}

		ip = start + (t * direction);

		Vector3 toPlane = ip - start;
		distance = toPlane.Length();

		return true;
	}


	/*************INTERSECT******************/
	/*<ALERT> All intersect tests need to be tested in new framework */


	bool IntersectSphereSphere(Sphere a, Sphere b, Vector3& point)
	{
		Vector3 distance = a.m_Center - b.m_Center;

		float distSquared = distance.LengthSq();

		float radiiSquared = a.m_Radius + b.m_Radius;
		radiiSquared *= radiiSquared;

		if (point != NULL)
		{
			point = distance / 2;
		}

		return distSquared <= radiiSquared;
	}

	bool IntersectSphereRay(Sphere s, Ray l, float& t, Vector3& point) // <ALERT> Ericson 179 for optimization
	{
		Vector3 m = l.m_A - s.m_Center;
	
		float b = m.Dot(l.m_Direction);
		float c = m.Dot(m) - s.m_Radius * s.m_Radius;

		//Bail out if ray is outside sphere and pointing away
		if ( c > 0.0f && b > 0.0f)
		{
			return false;
		}

		//Negative discriminant correspondes to ray missing sphere 
		float discr = b*b - c;
		if (discr < 0.0f)
		{
			return false;
		}

		//Ray is intersecting
		t = -b - sqrt(discr);

		//If t is negative the ray started inside the sphere
		if (t < 0.0f)
		{
			t = 0.0f;
		}

		if (point != NULL)
		{
			point = l.m_A + t * l.m_Direction;
		}

		return true;
	}

	bool IntersectSphereAABB(Sphere a, AABB b)
	{
		//Compute squared distance between sphere center and AABB
		float distSq = SqDistPointAABB(a.m_Center, b);

		//Return if distance is less than squared radius
		return distSq <= a.m_Radius * a.m_Radius;
	}

	bool IntersectSpherePlane(Sphere a, Plane b)
	{
		//Make sure plane is normalized
		b.m_Normal.Normalize();

		float distance = a.m_Center.Dot(b.m_Normal) - b.m_Distance;

		return fabsf(distance) <= a.m_Radius;
	}

	void ClosestPointOnLine(Vector3 ref, Line l, float& t, Vector3& point)
	{
		Vector3 lineStart = l.m_A;
		Vector3 lineEnd   = l.m_B;

		Vector3 ab = lineEnd - lineStart;

		//Project c onto ab
		if (t <= 0.0f)
		{
			// c projects outside [a,b] on the a side
			t = 0.0f;
			point = lineStart;
		}
		else
		{
			float denom = ab.Dot(ab);

			if (t >= denom)
			{
				// C projects outside the interval on the b side
				t = 1.0f;
				point = lineEnd;
			}

			else
			{
				t = t / denom;
				point = lineStart + t * ab;
			}
		}
	}

	void ClosestPointOnRay(Vector3 ref, Ray l, float& t, Vector3& point)
	{
		Vector3 lineStart = l.m_A;
		Vector3 lineEnd   = l.GetPointOnRay();

		Vector3 ab = lineEnd - lineStart;

		//Project c onto ab
		if (t <= 0.0f)
		{
			// c projects outside [a,b] on the a side
			t = 0.0f;
			point = lineStart;
		}
		else
		{
			float denom = ab.Dot(ab);

			if (t >= denom)
			{
				// Do not clamp
			}

			else
			{
				t = t / denom;
				point = lineStart + t * ab;
			}
		}
	}

	void ClosestPointOnAABB(Vector3 ref, AABB b, Vector3& point)
	{
		if(point)
		{
			//For each axis, clamp the point to the box
			ClampToDomain(b.MinX(), b.MaxX(), point.x);
			ClampToDomain(b.MinY(), b.MaxY(), point.y);
			ClampToDomain(b.MinZ(), b.MaxZ(), point.z);
		}
	}

	float SqDistPointAABB(Vector3 p, AABB b)
	{
		float distSq = 0.0f;

		//For each axis, count the excess distance

		// X-Axis
		if (p.x < b.MinX())
		{
			distSq += (b.MinX() - p.x) * (b.MinX() - p.x);
		}
		if (p.x > b.MaxX())
		{
			distSq += (p.x - b.MaxX()) * (p.x - b.MaxX());
		}

		// Y-Axis
		if (p.y < b.MinY())
		{
			distSq += (b.MinY() - p.y) * (b.MinY() - p.y);
		}
		if (p.y > b.MaxY())
		{
			distSq += (p.y - b.MaxY()) * (p.y - b.MaxY());
		}

		// Z-Axis
		if (p.z < b.MinZ())
		{
			distSq += (b.MinZ() - p.z) * (b.MinZ() - p.z);
		}
		if (p.z > b.MaxZ())
		{
			distSq += (p.z - b.MaxZ()) * (p.z - b.MaxZ());
		}

		return distSq;
	}


}