/**
 * Geometry - Defined geometry and intersection tests
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "MathLibrary.h"

namespace DXMathLibrary
{
	/*************CLASSES******************/

	/**
	 * Primitive - An abstract base class interface
	 */

	class DLLPORT Primitive
	{
	public:

		virtual float GetSurfaceArea(void) = 0;
		virtual float GetVolume(void) = 0;

	};

	/**
	 * Sphere - A three dimensional sphere
	 */

	class DLLPORT Sphere : public Primitive
	{
	public:

		Vector3 m_Center;
		float   m_Radius;

	public:
		Sphere(void);
		Sphere(const Vector3& center);
		Sphere(const Vector3& center, const float& radius);
		
		~Sphere(void);

		float GetSurfaceArea(void) override final;
		float GetVolume(void) override final;
	};

	/**
	 * AABB - An axis aligned bounding box with length and center
	 *		  X, Y, and Z are half-extents
	 */

	class DLLPORT AABB : public Primitive
	{
	public:

		Vector3 m_Center;

		float m_X;
		float m_Y;
		float m_Z;

	public:
		
		AABB(void);
		AABB(const Vector3& center);												    
		AABB(const Vector3& center, const float& x, const float& y, const float& z);

		~AABB(void);

		float GetSurfaceArea(void) override final;
		float GetVolume(void) override final;

		float MinX(void);
		float MaxX(void);

		float MinY(void);
		float MaxY(void);

		float MinZ(void);
		float MaxZ(void);
	};

	/**
	 * Line - A line with two endpoints
	 */

	class DLLPORT Line
	{
	public:

		Vector3 m_A;
		Vector3 m_B;

	public:

		Line(void);								 
		Line(const Vector3& a, const Vector3& b);

		~Line(void);
	};

	/**
	 * Ray - A ray with a starting point and direction
	 */

	class DLLPORT Ray
	{
	public:

		Vector3 m_A;
		Vector3 m_Direction;

	public:

		Ray(void);
		Ray(const Vector3& a, const Vector3& dir);
		Ray(const Line t);

		~Ray(void);

		void ComputeRay(const Vector3& start, const Vector3& b);

		Vector3 GetPointOnRay(void);
	};

	/**
	 * Plane - A three dimensional plane consisting of normal and distance from origin
	 */

	class DLLPORT Plane
	{
	public:

		Vector3 m_Normal;
		float	m_Distance;

	public:

		Plane(void);
		Plane(Vector3 normal, float distance);
		Plane(Vector3 a, Vector3 b, Vector3 c);
		~Plane(void);

		void ComputePlane(Vector3 a, Vector3 b, Vector3 c);

		/**
		* ClosestPoint
		*
		* Get closest point and distance from reference
		*
		* param[out] Vector3& Normal from plane to reference
		* param[out] float&   Distance from plane to reference
		*/
		void ClosestPoint(Vector3& point, float& distance);

		/**
		 * Line intersect
		 * 
		 * Determines if a line intersects the plane
		 *
		 * param[in]  Vector3 Line start
		 * param[in]  Vector3 Line end
		 * param[out] Vector3 Intersect point
		 * param[out] float   Distance to intersection
		 */
		bool LineIntersect(const Vector3& start, const Vector3& end, Vector3& ip, float& distance);
	};
	


	/*************INTERSECT******************/

	/**
	 * Sphere/Sphere Intersection
	 */
	DLLPORT bool IntersectSphereSphere(Sphere a, Sphere b, Vector3& point);

	/**
	 * Sphere/Line Intersection
	 */
	DLLPORT bool IntersectSphereRay(Sphere s, Ray l, float& t, Vector3& point);

	/**
	 * Sphere/AABB Intersection
	 */
	DLLPORT bool IntersectSphereAABB(Sphere a, AABB b, Vector3& point);

	/**
	 * Sphere/Plane Intersection
	 */
	DLLPORT bool IntersectSpherePlane(Sphere a, Plane b, Vector3& point);


	/*
	 * Closest point on ray to line segment
	 */
	DLLPORT void ClosestPointOnLine(Vector3 ref, Line l,  float& t, Vector3& point);

	/**
	 * Closest point on ray to point
	 */
	DLLPORT void ClosestPointOnRay(Vector3 ref, Ray l, float& t, Vector3& point);

	/**
	 * Closest point on AABB to point
	 */
	DLLPORT void ClosestPointOnAABB(Vector3 ref, AABB b, Vector3& point);

	/**
	 * Distance between point p and AABB b
	 */
	DLLPORT float SqDistPointAABB(Vector3 p, AABB b);

}