#pragma once

class IColliderComponent;
class RigidBodyComponent;
class SphereColliderComponent;
class BoxColliderComponent;

class ContactPoint;

class Observable;

class ContactGenerator
{
private:

	std::vector<IColliderComponent*> m_colliders;
	std::vector<ContactPoint> m_Contacts;

	int m_MaxContacts;

	Observable* m_Observable;

private:

	void SphereToSphere(SphereColliderComponent* a, SphereColliderComponent* b);
	void SphereToBox(SphereColliderComponent* sphere, BoxColliderComponent* box);
	void BoxToBox(BoxColliderComponent* a, BoxColliderComponent* b);

	void Resolve(double dt);

public:

	ContactGenerator();
	~ContactGenerator();

	void Init();

	void Generate(double dt);

	void SetColliders(std::vector<IColliderComponent*> colliders);
};