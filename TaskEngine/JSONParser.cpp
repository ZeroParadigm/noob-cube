#include "EngineCommonStd.h"

#include "JSONParser.h"
#include <iostream>
#include <algorithm>

using namespace std;

namespace Utility
{
	JSONParser::JSONParser(void)
	{
		m_tabCount = 0;
	}


	JSONParser::~JSONParser(void)
	{

	}

	//===== Internal Private Functions ====================
	void JSONParser::Tab()
	{
		for (int t = 0; t < m_tabCount; t++)
			m_outputStream << "    ";
	}

	void JSONParser::ReadFile()
	{
		m_inputStream.seekg(0, ios::end);
		m_inputString.resize((int)m_inputStream.tellg());
		m_inputStream.seekg(0, ios::beg);
		m_inputStream.read(&m_inputString[0], m_inputString.size());
		m_inputStream.close();
	}

	bool JSONParser::ParseFile()
	{
		cout << m_inputString;
		return ParseObject(m_inputString.begin(), m_inputString.end(), RUNNING, m_JSON);
	}

	inline bool NextIsVariable(string::iterator it1, string::iterator it2)
	{
		string temp = ":";
		it1 = find(it1, it2, ':');

		if (it1 == it2) return false;

		it1++;
		it1++;
		it2 = it1;
		it2++;
		return (string(it1, it2) == "\"");
	}

	inline bool NextIsObject(string::iterator it1, string::iterator it2)
	{
		string temp = ":";
		it1 = find(it1, it2, ':');

		if (it1 == it2) return false;

		it1++;
		it1++;
		it2 = it1;
		it2++;
		return (string(it1, it2) == "{");
	}

	inline bool NextIsArray(string::iterator it1, string::iterator it2)
	{
		string temp = ":";
		it1 = find(it1, it2, ':');

		if (it1 == it2) return false;

		it1++;
		it1++;
		it2 = it1;
		it2++;
		return (string(it1, it2) == "[");
	}

	inline bool HasMoreData(string::iterator it1, string::iterator it2)
	{
		it2 = find(it1, it2, '}');

		if (it1 == it2) return false;

		return (it1 != it2);
	}

	bool JSONParser::ParseVariable(string::iterator &it1, string::iterator it2, JSONObject &object)
	{
		string::iterator helper1 = it2;

		it1 = find(it1, it2, '\"');
		helper1 = find(++it1, it2, '\"');
		string name(it1, helper1);

		it1 = find(it1, it2, ':');
		it1++;

		it1 = find(it1, it2, '\"');
		helper1 = find(++it1, it2, '\"');
		string value(it1, helper1);

		string deliminator = "\",";
		it1 = search(it1, it2, deliminator.begin(), deliminator.end());
		it1++;

		object.m_variables.insert(make_pair(name, value));

		return GOOD;
	}

	bool JSONParser::ParseArray(string::iterator &it1, string::iterator it2, JSONObject &object)
	{
		string::iterator temp2;
		JSONArray newArray;
		temp2 = find(it1, it2, '\"');
		newArray.m_name = string(it1, temp2);
		it1 = find(it1, it2, '[');
		temp2 = it2;

		while (HasMoreData(it1, it2))
		{
			temp2 = find(it1, it2, '}');
			JSONObject newObject;
			ParseObject(it1, temp2, ARRAY_OBJECT, newObject);
			it1 = temp2;
			it1 = find(it1, it2, '{');
			newArray.m_values.push_back(newObject);
		}

		object.m_arrays.insert(make_pair(newArray.m_name, newArray));

		return GOOD;
	}

	bool JSONParser::ParseObject(string::iterator it1, string::iterator it2, PARSE_MESSAGE operation, JSONObject &object)
	{
		if (it1 == it2)
			return GOOD;

		if (operation == NEW_OBJECT)
		{
			string::iterator temp2 = it2;
			temp2 = find(it1, it2, '\"');
			object.m_name = string(it1, temp2);
			it1 = find(it1, it2, '{');
			operation = RUNNING;
		}

		while (it1 != it2)
		{
			if (NextIsVariable(it1, it2))
			{
				ParseVariable(it1, it2, object);
				continue;
			}

			if (NextIsArray(it1, it2))
			{
				it1 = find(it1, it2, '"');
				string::iterator temp2 = find(++it1, it2, ']');
				ParseArray(it1, temp2, object);
				continue;
			}

			if (NextIsObject(it1, it2))
			{
				JSONObject newObject;
				it1 = find(it1, it2, '\"');
				string::iterator temp2 = find(++it1, it2, '}');
				ParseObject(it1, temp2, NEW_OBJECT, newObject);
				it1 = temp2;
				object.m_objects.insert(make_pair(newObject.m_name, newObject));
				continue;
			}

			return GOOD;
		}

		return BAD_FILE;
	}

	int JSONParser::Parse()
	{
		ReadFile();

		ParseFile();

		return GOOD;
	}
	//=====================================================

	//===== Input Functions ===============================
	int JSONParser::OpenFile(string filename)
	{
		m_inputStream.open(filename);

		return m_inputStream ? Parse() : NOT_FOUND;;
	}

	JSONObject JSONParser::GetParsedObject()
	{
		return m_JSON;
	}
	//=====================================================

	//===== Output Functions ==============================
	void JSONParser::CreateNewFile(string filename)
	{
		m_outputStream.open(filename, ios::trunc | ios::out);
		m_outputStream << "{\n";
		m_tabCount++;
	}

	void JSONParser::CloseFile()
	{
		m_outputStream << "}\n";
		m_outputStream.close();
	}

	void JSONParser::CreateObject(string name)
	{
		Tab();

		if (name == "NULL")
			m_outputStream << "{\n";
		else
			m_outputStream << "\"" << name << "\": {\n";

		m_tabCount++;
	}

	void JSONParser::CloseObject()
	{
		m_tabCount--;
		Tab();
		m_outputStream << "},\n";
	}

	void JSONParser::CreateVariable(string name, string value)
	{
		Tab();
		m_outputStream << "\"" << name << "\": \"" << value << "\",\n";
	}

	void JSONParser::CreateArray(string name)
	{
		Tab();
		m_tabCount++;
		m_outputStream << "\"" << name << "\": [\n";
	}

	void JSONParser::CloseArray()
	{
		m_tabCount--;
		Tab();
		m_outputStream << "]\n";
	}
	//=====================================================
}