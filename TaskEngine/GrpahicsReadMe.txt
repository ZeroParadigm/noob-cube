========================================================================
    Engine Graphics : Overview
	Author: Anthony Hernandez
========================================================================

The primary function of this Engein's Graphics pipline is to provide a simple
interface to allow drawing of mesh data onto a visual context.

There will be no scene graph or any advanced ordering of drawing.

The graphics pipeline will draw all active Actors who's transform lies in
the current view frustum.

A simple real-time glow effect will be applied literally to the entire screen
buffer.

/////////////////////////////////////////////////////////////////////////////
	TODO List
/////////////////////////////////////////////////////////////////////////////

	- Implement D3D9 capabilities
		- 3D pipeline
		- 2D pipeline
		- Deferred Context
	- Link with Engine component system
	- Link with UI system

/////////////////////////////////////////////////////////////////////////////
	<More info below>
/////////////////////////////////////////////////////////////////////////////

	- D3D11* filter is an example placeholder