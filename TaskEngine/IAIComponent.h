#pragma once
#include "BaseComponent.h"

/**
 * IAIComponent
 *
 * AI Interface component
 */
class IAIComponent : public BaseComponent
{
private:

public:

	IAIComponent(void) : BaseComponent() {}
	IAIComponent(Actor* owner) : BaseComponent(owner) {}

	~IAIComponent(void) {}

};