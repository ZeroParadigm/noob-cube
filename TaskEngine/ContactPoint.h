#pragma once
#include "MemoryDelegate.h"

class ParticleComponent;

class ContactPoint
{
public:

	double calculateSeperatingVelocity() const;

	void ResolveVelocity(double dt);
	void ResolveInterpentration(double dt);

	ParticleComponent* particle[2];
	DXMathLibrary::Vector3 particleMovement[2];

	double restitution;
	double friction;
	double penetration;

	DXMathLibrary::Vector3 contactNormal;
	DXMathLibrary::Vector3 contactPoint;

	void Resolve(double dt);
	void setBodyData(ParticleComponent* one, ParticleComponent* two, double rest, double fric);

	ContactPoint(void);
	~ContactPoint(void);
};