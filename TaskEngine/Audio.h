#pragma once

#include <map>
#include <fmod.hpp>

#define SOUND Audio::Instance()


class Audio
{
private:
	
	FMOD::System*	 m_System;

	FMOD::Channel*	 m_MusicChannel;
	FMOD::Channel*	 m_SoundChannel;

	std::map<const char*, FMOD::Sound*> m_Sounds;

	float m_MasterVolume;
	float m_MusicVolume;
	float m_SoundVolume;

private:

	Audio(void);

public:

	static Audio* Instance()
	{
		static Audio i;
		return &i;
	}
	~Audio(void);

	void Initialize(void);
	void Shutdown(void);

	void PlayMusic(const char* fileName, bool looped);
	void StopMusic(const char* fileName);

	void PlaySample(const char* fileName);

	void LoadSound(const char* fileName, bool looped);

	void LoadMusic(const char* fileName, bool looped);

	void ReleaseSound(const char* fileName);
};
