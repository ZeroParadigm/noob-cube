#pragma once

#include "GameState.h"

class PlayerComponent;

class WindowElement;
class StaticTextElement;
class StaticImageElement;
class ButtonElement;


class MainGameState : public GameState
{
private:

	std::default_random_engine generator;

	float m_TimeElapsed;
	float m_TimeSinceLastWave;

	float m_SecondsBetweenWaves;

	int m_WaveNum;

	PlayerComponent* m_Player;

	WindowElement* GUI_MainWindow;
	StaticTextElement* GUI_WaveNumber;
	StaticImageElement* GUI_TestImage1;
	StaticImageElement* GUI_TestImage2;

public:

	MainGameState();
	~MainGameState();

	void EnterState() override;
	void LeaveState() override;
	void UpdateState(float dt) override;

private:

	void CreateCamera(void);
	void CreateWorld(void);
	void CreatePlayer(void);
	void CreateEnemy(void);

	void StartCamera(void);

	void SpawnWave(int n);

	void CreateGUI();
};