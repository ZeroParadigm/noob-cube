#include "EngineCommonStd.h"
#include "RenderWindow.h"

#include "IRenderDevice.h"

//This extends the basic functionlity of a BaseWindow to bind
//	relavent Procs with a graphics device
LRESULT RenderWindow::HandleMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{
	//Call the atatched device's window proc function
	//	so it can manage any state changes between the window
	//	and device context
	if (m_deviceContext)
	{
		bool noFurtherProcessing = false;
		LRESULT res = m_deviceContext->WndProc(m_hwnd, msg, wParam, lParam);
		if (noFurtherProcessing)
			return res;
	}

	//Call base window functionality such as
	//	window destruction and closing
	__super::HandleMessage(msg, wParam, lParam);

	WARNING_TODO("Add a method to alert device window is closing");
}

int RenderWindow::Init(HINSTANCE appInstance,
					const TCHAR* caption,
					long x, long y,
					long w, long h)
{
	//Initalize member variables
	m_appInstance	= appInstance;

	//Register window class
	Register();

	//Create window
	WinCreateWindowEx(0, caption,
					WS_OVERLAPPEDWINDOW,
					x, y,
					w, h,
					NULL, NULL);
	
	//Return the unique ID for reference purposes
	return m_id;
}

void RenderWindow::AttatchDevice(IRenderDevice* device)
{
	//Check device to see if it's going
	//	to form a stable render context
	if (!device->IsContextStable(this))
	{
		std::wstring output;
		output += L"Failed to create Context between window and device.\n";
		output += L"Device: " + device->DeviceName() + L"\n";
		output += L"Window: " + GetCaption() + L"\n";
		output += L"Check " + CRASH_FILENAME
		output += L" OR Output for more info.";

		ENGINE_FATAL(output);

		PostQuitMessage(0);
	}

	//Nothing bad happen.
	//	Grab a reference to it and move on
	m_deviceContext = device;
}

void RenderWindow::BreakContext()
{
	m_deviceContext = nullptr;
}