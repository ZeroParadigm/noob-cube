#include "EngineCommonStd.h"

#include "Mesh.h"
#include "GraphicsUtility.h"


Mesh::Mesh()
{
	m_Mesh = nullptr;
	m_Texture = nullptr;
	m_fileName = L"noFileName";

	m_UsingMesh = true;

	m_VertexBuffer = nullptr;
	m_IndexBuffer = nullptr;
	m_NumVertices = 0;
	m_NumTriangles = 0;
}

Mesh::Mesh(std::wstring file) : m_fileName(file)
{
	m_Mesh = nullptr;
	m_Texture = nullptr;

	m_UsingMesh = true;

	m_VertexBuffer = nullptr;
	m_IndexBuffer = nullptr;
	m_NumVertices = 0;
	m_NumTriangles = 0;
}

Mesh::~Mesh()
{
}

void Mesh::Load(IDirect3DDevice9* device)
{
	D3DXLoadMeshFromX(m_fileName.c_str(), D3DXMESH_SYSTEMMEM, device, NULL, NULL, NULL, NULL, &m_Mesh);
}

void Mesh::Release()
{
	if (IsUsingMesh())
	{
		if (m_Mesh)
		{
			m_Mesh->Release();
		}
	}
	else
	{
		if (m_VertexBuffer != nullptr)
		{
			m_VertexBuffer->Release();
			m_VertexBuffer = nullptr;
		}
		if (m_IndexBuffer != nullptr)
		{
			m_IndexBuffer->Release();
			m_IndexBuffer = nullptr;
		}
		
		m_NumVertices = 0;
		m_NumTriangles = 0;
	}
}

void Mesh::DrawSubset(int subset)
{
	if (IsUsingMesh())
	{
		m_Mesh->DrawSubset(subset);
	}
	else
	{
		g_d3dDevice->SetVertexDeclaration(VertexPNT::Decl);

		g_d3dDevice->SetStreamSource(0, m_VertexBuffer, 0, sizeof(VertexPNT));
		g_d3dDevice->SetIndices(m_IndexBuffer);
		g_d3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_NumVertices, 0, m_NumTriangles);
	}
}