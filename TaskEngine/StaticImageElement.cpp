#include "EngineCommonStd.h"

#include "StaticImageElement.h"
#include "GraphicsUtility.h"


StaticImageElement::StaticImageElement() : BaseGUIElement()
{
	m_Name = L"StaticImageElement" + GetID();

	m_Image = nullptr;
	m_ColorMask = D3DCOLOR_RGBA(255, 255, 255, 255);
}

StaticImageElement::StaticImageElement(BaseGUIElement* parent) : BaseGUIElement(parent)
{
	m_Name = L"StaticImageElement" + GetID();

	m_Image = nullptr;
	m_ColorMask = D3DCOLOR_RGBA(255, 255, 255, 255);
}

StaticImageElement::~StaticImageElement()
{
}

void StaticImageElement::Draw()
{
	if (m_Image == nullptr)
		return;

	g_d3dSprite->SetTransform(&GetMatrix());

	g_d3dSprite->Draw(GetImage(), NULL, NULL, NULL, m_ColorMask);
}

void StaticImageElement::SetImage(Sprite* image)
{
	m_Image = image;
	m_Width = image->GetWidth();
	m_Height = image->GetHeight();
}

void StaticImageElement::SetColorMask(D3DCOLOR color)
{
	m_ColorMask = color;
}

IDirect3DTexture9* StaticImageElement::GetImage()
{
	return m_Image->GetTexture();
}

Sprite* StaticImageElement::GetImageObj()
{
	return m_Image;
}

D3DCOLOR StaticImageElement::GetColorMask()
{
	return m_ColorMask;
}