/**
 * Actor - Every object in the game world is an actor
 */

#pragma once


class IRendererComponent;
class TransformComponent;
class MeshComponent;
class IAIComponent;
class IGameplayComponent;
class ParticleComponent;
class IColliderComponent;


class Actor
{
	friend class ActorFactory;
	friend class ComponentManager;

private:

	int m_Id;

protected:

	TransformComponent*		m_Transform;
	MeshComponent*			m_Mesh;
	IAIComponent*			m_Ai;
	IRendererComponent*		m_Renderer;
	IGameplayComponent*		m_Gameplay;
	ParticleComponent*		m_Particle;
	IColliderComponent*		m_Collider;

public:

	Actor(void);
	~Actor(void);

	int Id(void){return m_Id;}

	TransformComponent*		Transform()		{ return m_Transform; }
	MeshComponent*			Mesh()			{ return m_Mesh; }
	IAIComponent*			Ai()			{ return m_Ai; }
	IRendererComponent*     Renderer()		{ return m_Renderer; }
	IGameplayComponent*		Gameplay()		{ return m_Gameplay; }
	ParticleComponent*		Particle()		{ return m_Particle; }
	IColliderComponent*		Collider()		{ return m_Collider; }

private:

	void SetId(void);
};