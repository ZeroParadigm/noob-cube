#pragma once

/*
	A managed MemoryPool takes advantage of timed
	defragmentation. Refer to PoolManager.cpp

	Managed pools allows for controlled release
	of memory. Retain, weak etc.

	This would allow for easy state wipe / reset
	EG tell memory to free components of gameobjects
	but keep textures and models for new state loading
	to only worry about component creation.

*/
class PoolManager;

class MemoryPool
{
friend class PoolManager;
private:

	CriticalSection m_cs;

	PoolManager *m_manager;
	int m_poolID;

private:

	unsigned char**		m_rawMemoryPool;	//Array to blocks
	unsigned char*		m_head;				//ptr to next free chunk

	unsigned int		m_chunkSize;		//size of chunk in bytes
	unsigned int		m_chunkCount;		//number of chunks used

	unsigned int		m_blockSize;		//size of block in chunks
	unsigned int		m_blockCount;		//number of blocks used

	bool				m_canGrow;			//MemoryPool can grow
	bool				m_isManaged;		//MemoryPool is managed

#ifdef _DEBUG
	std::wstring m_poolName;
	unsigned long m_peakAlloc, m_numAllocs;
#endif

public:

	MemoryPool();
	~MemoryPool();

	bool Init(int ChunkSize, int InitialSize, bool canGrow, bool isManaged = false);
	void Destroy();

	void* Alloc();
	void Free(void* mem);

	unsigned int GetChunkSize() { return m_chunkSize; }
	unsigned int GetChunkCount() { return m_chunkCount; }
	unsigned int GetAllocatedSize() { return m_chunkSize * m_chunkCount; }

	void SetAllowGrow(bool allowGrow) { m_canGrow = allowGrow; }
	void SetManaged(bool managed);

	//Debugging purposes
	void SetDebugPoolName(const TCHAR* poolName);
	std::wstring GetDebugPoolName();

private:

	void Reset();

	bool GrowMemoryPool();

	void SetNext(unsigned char* blockToChange, unsigned char* newNext);

	unsigned char* AllocateNewBlock();
	unsigned char* GetNext(unsigned char* block);

	MemoryPool(const MemoryPool& copy){} //No copy allowed
};