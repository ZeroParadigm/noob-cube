#include "EngineCommonStd.h"
#include "Observable.h"

Observable::Observable(void)
{
	m_Observer = nullptr;
}

Observable::~Observable(void)
{
	if (m_Observer)
	{
		Detach(m_Observer);
	}

	m_EventHandlers.clear();
}

void Observable::Attach(Observer* observer)
{
	m_Observer = observer;
}

void Observable::Detach(Observer* observer)
{
	m_Observer = nullptr;
}

void Observable::Notify(EVENT event, EventData data)
{
	if (m_Observer)
	{
		m_Observer->Notify(event, data);
	}
}

void Observable::Subscribe(int events)
{
	m_Events = events;

	if (m_Observer)
	{
		m_Observer->Subscribe(this);
	}
}

void Observable::Unsubscribe(void)
{
	if (m_Observer)
	{
		m_Observer->Unsubscribe(this);
	}
}

void Observable::OnEvent(EVENT type, std::function<void (const EventData&)> func)
{
	m_EventHandlers.insert(std::make_pair(type, func));
}

void Observable::ResetEvents()
{
	m_EventHandlers.clear();
}

void Observable::HandleEvent(EVENT type, const EventData& data)
{
	std::map<EVENT, std::function<void(const EventData&)>>::iterator it;

	it = m_EventHandlers.find(type);

	if (it != m_EventHandlers.end())
	{
		it->second(data);
	}
}
