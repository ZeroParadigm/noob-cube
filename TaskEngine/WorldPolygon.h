#pragma once

#include "WorldPlane.h"

class MeshComponent;
class ActorFactory;


class WorldPolygon
{
public:

	std::vector<WorldPlane*>			 m_Faces;
	std::vector<DXMathLibrary::Plane*>	 m_Walls;

	unsigned int m_NumWalls;
	bool	     m_HasWalls;

	float		 m_ScaleX, m_ScaleY, m_ScaleZ;

public:

	WorldPolygon();
	~WorldPolygon();

	//void Draw();

	void CreatePlaneBuffer(MeshComponent* mesh);
	void CreatePyramidBuffer(MeshComponent* mesh);

	void CreateCubeBuffer(MeshComponent* mesh);

	/**
	 * Walls
	 *
	 * Returns whether or not walls are in play
	 *
	 * param[out] Plane* Pointer to the wall array
	 * param[out] int    Number of walls in play
	 */
	bool Walls(std::vector<DXMathLibrary::Plane*>* walls, int& numWalls);
	std::vector<DXMathLibrary::Plane*>* GetWalls(void);

private:

	void ReleaseBuffers();

	void CreateWalls(const DXMathLibrary::Vector3& p0, const DXMathLibrary::Vector3& p1, 
		const DXMathLibrary::Vector3& p2, const DXMathLibrary::Vector3& p3, float scale);
	void DestroyWalls(void);
};