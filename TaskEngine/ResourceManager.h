#pragma once

//#include <map>
//#include <string>
//#include <d3dx9.h>

class Mesh;
class Sound;
class Texture;
class Sprite;

class ResourceManager
{
	friend class SimpleEngine;

protected:

	Mesh*								m_DefaultMesh;
	//Sound*							m_DefaultSound;
	Texture*							m_DefaultTexture;

	std::map<std::wstring, Mesh*>		m_Meshes;
	//std::map<std::string, Sound*>		m_Sounds;
	std::map<std::wstring, Texture*>	m_Textures;
	std::map<std::wstring, Sprite*>		m_Sprites;

	IDirect3DDevice9* e_d3dDevice;

public:

	ResourceManager(IDirect3DDevice9* d3dDevice);
	~ResourceManager();

	bool LoadMesh(std::wstring fileName);
	bool LoadModel(std::wstring folderName);

	bool LoadSound(std::wstring fileName);

	bool LoadTexture(std::wstring fileName);
	bool LoadSprite(std::wstring fileName, unsigned int width, unsigned int height);

	Mesh* GetMesh(std::wstring fileName);
	Mesh* GetEmptyMesh(std::wstring name);
	Mesh* GetModel(std::wstring folderName);

	//Sound* GetSound(std::string fileName);

	Texture* GetTexture(std::wstring fileName);
	Sprite* GetSprite(std::wstring fileName);

	void LoadDefaults();
	void ReleaseResources();

	Mesh* GetDefaultMesh();
	//Sound* GetDefaultSound();
	Texture* GetDefaultTexture();

};