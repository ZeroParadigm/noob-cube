#include "EngineCommonStd.h"

#include "ActorFactory.h"

#include "Actor.h"
#include "ActorManager.h"
#include "ComponentManager.h"
#include "ResourceManager.h"
#include "BulletSystem.h"


ActorFactory::ActorFactory(void)
{
}

ActorFactory::ActorFactory(ActorManager* actorMngr, ComponentManager* componentMngr, ResourceManager* resourceMngr)
	: m_ActorManager(actorMngr), m_ComponentManager(componentMngr), m_ResourceManager(resourceMngr)
{
}

ActorFactory::~ActorFactory(void)
{
}

void ActorFactory::LoadFile(const TCHAR* file)
{
	Tstring fileName(file);
}

void ActorFactory::SetActorTemplate(ActorTemplate templaet)
{
	m_CurrentActorTemplate = templaet;
}

Actor* ActorFactory::AssembleActor()
{
	Actor* newActor = m_ActorManager->CreateActor();

	return newActor;
}

Actor* ActorFactory::CreateBlankActor()
{
	Actor* blankActor = m_ActorManager->CreateActor();

	return blankActor;
}

BaseComponent* ActorFactory::CreateBlankComponent(Actor* owner, ComponentType type)
{
	BaseComponent* blankComponent = m_ComponentManager->CreateComponent(owner, type);

	return blankComponent;
}

Actor* ActorFactory::CreateBasicMeshActor(float x, float y, float z)
{
	Actor* meshActor = m_ActorManager->CreateActor();

	// Add Transform, Mesh

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(meshActor, COMPONENT_TRANSFORM);
	transform->SetPosition(Vector3(x, y, z));
	meshActor->m_Transform = transform;
	//m_ActorManager->TrackActor(meshActor, TRANSFORM);

	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(meshActor, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetDefaultMesh());
	meshActor->m_Mesh = mesh;
	//m_ActorManager->TrackActor(meshActor, MESH);

	return meshActor;
}

Actor* ActorFactory::CreateBasicModelActor(float x, float y, float z, std::wstring folderName)
{
	Actor* meshActor = m_ActorManager->CreateActor();

	// Add Transform, Mesh

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(meshActor, COMPONENT_TRANSFORM);
	transform->SetPosition(Vector3(x, y, z));
	meshActor->m_Transform = transform;
	//m_ActorManager->TrackActor(meshActor, TRANSFORM);

	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(meshActor, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetModel(folderName));
	meshActor->m_Mesh = mesh;
	//m_ActorManager->TrackActor(meshActor, MESH);

	return meshActor;
}

Actor* ActorFactory::CreateBasicSphereActor()
{
	Actor* sphereActor = m_ActorManager->CreateActor();

	// Add Transform, Mesh, Renderer, Physics components

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(sphereActor, COMPONENT_TRANSFORM);
	sphereActor->m_Transform = transform;
	//m_ActorManager->TrackActor(sphereActor, TRANSFORM);

	return sphereActor;
}

Actor* ActorFactory::CreateBasicScriptActor()
{
	Actor* scriptActor = m_ActorManager->CreateActor();

	// Add Script Component



	return scriptActor;
}

Actor* ActorFactory::CreateBasicAIActor()
{
	Actor* aiActor = m_ActorManager->CreateActor();

	// Add Transform, Mesh, Renderer, Physics, AI components

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(aiActor, COMPONENT_TRANSFORM);
	aiActor->m_Transform = transform;
	//m_ActorManager->TrackActor(aiActor, TRANSFORM);

	return aiActor;
}

Actor* ActorFactory::CreateCamera(void)
{
	Actor* camera = m_ActorManager->CreateActor();

	//Add transform, camera, and ai
	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(camera, COMPONENT_TRANSFORM);
	camera->m_Transform = transform;
	CameraComponent* cam = (CameraComponent*)m_ComponentManager->CreateComponent(camera, COMPONENT_CAMERA);
	camera->m_Renderer = cam;
	MovementComponent* movement = (MovementComponent*)m_ComponentManager->CreateComponent(camera, COMPONENT_MOVEMENT);
	camera->m_Ai = movement;
	movement->SetMaxSpeed(400.0f);
	movement->SetMaxAcceleration(Vector3(0.0f, 0.0f, 500.0f));
	movement->SetMaxRotation( pi );
	movement->SetMaxRotationAcceleration( pi / 5.0f );
	movement->SetDeceleration(3);
	movement->SetCameraMode(true);

	m_ActorManager->SetCamera(camera);

	return camera;
}

Actor* ActorFactory::CreatePlayer(void)
{
	CreateGun();

	Actor* player = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_TRANSFORM);
	player->m_Transform = transform;
	
	MovementComponent* movement = (MovementComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_MOVEMENT);
	player->m_Ai = movement;
	movement->SetWeightSeek(3.0f);
	movement->SetMaxAcceleration(Vector3(0.0f, 0.0f, 300.0f));
	movement->SetMaxSpeed(75.0f);
	
	PlayerComponent* playerComp = (PlayerComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_PLAYER);
	player->m_Gameplay = playerComp;
	playerComp->Init();
	playerComp->SetCamera(m_ActorManager->GetCamera());
	playerComp->SetGun(m_ActorManager->GetGun());
	
	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetModel(L"battle_cruiser_1"));
	player->m_Mesh = mesh;

	ParticleComponent* physics = (ParticleComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_PARTICLEPHYSICS);
	player->m_Particle = physics;
	
	SphereColliderComponent* sphere = (SphereColliderComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_SPHERE);
	sphere->SetRadius(5);
	player->m_Collider = sphere;

	
	m_ActorManager->SetPlayer(player);

	return player;
}

Actor* ActorFactory::CreateEnemy(void)
{
	Actor* enemy = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_TRANSFORM);
	enemy->m_Transform = transform;
	
	MovementComponent* movement = (MovementComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_MOVEMENT);
	enemy->m_Ai = movement;
	
	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetModel(L"dark_fighter_1"));
	enemy->m_Mesh = mesh;
	
	EnemyComponent* enemy_comp = (EnemyComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_ENEMY);
	enemy->m_Gameplay = enemy_comp;
	enemy_comp->Init();

	ParticleComponent* physics = (ParticleComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_PARTICLEPHYSICS);
	enemy->m_Particle = physics;
	
	SphereColliderComponent* sphere = (SphereColliderComponent*)m_ComponentManager->CreateComponent(enemy, COMPONENT_SPHERE);
	sphere->SetRadius(5);
	enemy->m_Collider = sphere;

	movement->SetMaxSpeed(50.0f);
	movement->SetMaxAcceleration(Vector3(0, 0, 150.0f));

	movement->SetMaxRotation(4 * pi / 5.0f);
	movement->SetMaxRotationAcceleration(4 * pi / 5.0f);

	return enemy;
}

Actor* ActorFactory::CreateGameWorld(void)
{
	Actor* gameworld = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(gameworld, COMPONENT_TRANSFORM);
	gameworld->m_Transform = transform;
	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(gameworld, COMPONENT_MESH);
	gameworld->m_Mesh = mesh;
	mesh->SetMesh(m_ResourceManager->GetEmptyMesh(L"gameWorld"));
	mesh->GetMesh()->SetTexture(m_ResourceManager->GetDefaultTexture());
	GameWorldComponent* gwc = (GameWorldComponent*)m_ComponentManager->CreateComponent(gameworld, COMPONENT_GAMEWORLD);
	gameworld->m_Gameplay = gwc;

	m_ActorManager->SetWorld(gameworld);

	return gameworld;
}

void ActorFactory::CreateGun()
{
	BulletSystem* gun = new BulletSystem();

	gun->Initialize(this);

	m_ActorManager->SetGun(gun);
}

BulletComponent* ActorFactory::CreateBullet()
{
	Actor* bullet = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(bullet, COMPONENT_TRANSFORM);
	bullet->m_Transform = transform;
	transform->SetScale(Vector3(5.0f,5.0f,5.0f));
	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(bullet, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetMesh(L"Assets/Meshes/Sphere_halfUnit/Sphere_halfUnit.X"));
	mesh->GetMesh()->SetTexture(m_ResourceManager->GetTexture(L"Assets/Textures/Red.jpg"));
	mesh->SetActive(false);
	bullet->m_Mesh = mesh;

	//ParticleComponent* physics = (ParticleComponent*)m_ComponentManager->CreateComponent(bullet, COMPONENT_PARTICLEPHYSICS);
	//bullet->m_Particle = physics;
	SphereColliderComponent* sphere = (SphereColliderComponent*)m_ComponentManager->CreateComponent(bullet, COMPONENT_SPHERE);
	sphere->SetRadius(5.0f);
	sphere->SetActive(false);
	bullet->m_Collider = sphere;

	BulletComponent* b = (BulletComponent*)m_ComponentManager->CreateComponent(bullet, COMPONENT_BULLET);
	b->Init();
	bullet->m_Gameplay = b;

	return b;
}

Actor* ActorFactory::CreateSphereTriggerVolume()
{
	Actor* volume = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_TRANSFORM);

	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetMesh(L"Assets/Meshes/Sphere_halfUnit/Sphere_halfUnit.X"));
	mesh->GetMesh()->SetTexture(m_ResourceManager->GetTexture(L"Assets/Textures/Red.jpg"));

	SphereCC* sphereCC = (SphereCC*)m_ComponentManager->CreateComponent(volume, COMPONENT_SPHERE);
	sphereCC->SetRadius(5);

	TriggerVolumeComponent* trigger = (TriggerVolumeComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_TRIGGERVOLUME);
	trigger->Init();

	return volume;
}

Actor* ActorFactory::CreateBoxTriggerVolume()
{
	Actor* volume = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_TRANSFORM);

	//MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_MESH);
	//mesh->SetMesh(m_ResourceManager->GetMesh(L"Assets/Meshes/Box/Box.X"));
	//mesh->GetMesh()->SetTexture(m_ResourceManager->GetTexture(L"Assets/Textures/Red.jpg"));

	BoxColliderComponent* boxCC = (BoxCC*)m_ComponentManager->CreateComponent(volume, COMPONENT_BOX);

	TriggerVolumeComponent* trigger = (TriggerVolumeComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_TRIGGERVOLUME);
	//TriggerVolumeComponent* trigger = (TriggerVolumeComponent*)m_ComponentManager->CreateComponent(volume, COMPONENT_EDGETRIGGERVOLUME);
	trigger->Init();

	return volume;
}

Actor* ActorFactory::CreateDebugPlayer()
{
	CreateGun();

	Actor* player = m_ActorManager->CreateActor();

	TransformComponent* transform = (TransformComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_TRANSFORM);
	transform->SetScale(Vector3(10, 10, 10));

	MovementComponent* movement = (MovementComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_MOVEMENT);
	movement->SetWeightSeek(3.0f);
	movement->SetMaxAcceleration(Vector3(0.0f, 0.0f, 300.0f));
	movement->SetMaxSpeed(75.0f);
	movement->FaceForward3DOff();
	movement->FacePoint3DOff();

	PlayerComponent* playerComp = (PlayerComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_PLAYER);
	playerComp->Init();
	playerComp->SetCamera(m_ActorManager->GetCamera());
	playerComp->SetGun(m_ActorManager->GetGun());

	MeshComponent* mesh = (MeshComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_MESH);
	mesh->SetMesh(m_ResourceManager->GetMesh(L"Assets/Meshes/Sphere_halfUnit/Sphere_halfUnit.X"));
	mesh->GetMesh()->SetTexture(m_ResourceManager->GetTexture(L"Assets/Textures/Green.jpg"));

	ParticleComponent* physics = (ParticleComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_PARTICLEPHYSICS);

	SphereColliderComponent* sphere = (SphereColliderComponent*)m_ComponentManager->CreateComponent(player, COMPONENT_SPHERE);
	sphere->SetRadius(5);

	m_ActorManager->SetPlayer(player);

	return player;
}

TriggerVolumeComponent* ActorFactory::CreateWorldTriggerVolume(DXMathLibrary::Vector3 position, DXMathLibrary::Vector3 halfExtents)
{
	Actor* volume = CreateBoxTriggerVolume();

	TransformComponent* transform = volume->Transform();
	transform->SetPosition(position);

	TriggerVolumeComponent* trigger = (TriggerVolumeComponent*)volume->Gameplay();
	trigger->SetHalfExtents(halfExtents);
	trigger->SetTriggerType(TRIGGER_ONENTER);
	trigger->SetTriggerCooldown(0.5f);

	return trigger;
}