#pragma once

#include "IGameplayComponent.h"
#include "MathLibrary.h"
#include <functional>

struct EventData;

class WorldPlane;

enum TriggerType
{
	TRIGGER_ONENTER,
	TRIGGER_WHILETOUCHING,
};


class TriggerVolumeComponent : public IGameplayComponent
{
protected:

	std::function<void(const EventData&)> m_TriggerEvent;

private:

	DXMathLibrary::Vector3 m_HalfExtents;

	TriggerType m_TriggerType;

	bool m_Triggered;
	float m_Timer;
	float m_Cooldown;

public:

	TriggerVolumeComponent();
	TriggerVolumeComponent(Actor* owner);
	virtual ~TriggerVolumeComponent();

	void Update(float dt) override;

	virtual void Init();

	void Activate();

	void Deactivate();

	void HandleEvent(const EventData& data);

	void SetTriggerEvent(std::function<void(const EventData&)> eventHandler);

	void SetHalfExtents(DXMathLibrary::Vector3 halfExtents);
	DXMathLibrary::Vector3 GetHalfExtents();

	void SetTriggerType(TriggerType type);
	TriggerType GetTriggerType();

	void SetTriggerCooldown(float t);

};

/**
 * EdgeTriggerVolume
 *
 * Derived class which sets up edge volumes for movement
 */

class EdgeTriggerVolume : public TriggerVolumeComponent
{
private:

	//World planes which this edge connects
	WorldPlane* m_PlaneA;
	WorldPlane* m_PlaneB;

public:

	EdgeTriggerVolume(void);
	EdgeTriggerVolume(Actor* owner);
	~EdgeTriggerVolume(void) override;

	void SetWorldPlanes(WorldPlane* a, WorldPlane* b);

	void Init(void) override;

	void EdgeTesting(const EventData& data);
};