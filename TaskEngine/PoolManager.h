#pragma once

#define POOL_MANAGER DEFINE_SINGLETON_ACCESS(PoolManager)

enum PoolNotifyMessage
{
	NOTIFYPOOL_NULL = 0,
	NOTIFYPOOL_GROWTH,
	NOTIFYPOOL_COUNT,
};

class MemoryPool;

class PoolManager
{
friend class MemoryPool;
LOCK_TO_SINGLETON(PoolManager);
private:

	std::unordered_map<int, MemoryPool*> m_managedPool;
	std::map<std::wstring, MemoryPool*> m_registerdPools;


	unsigned int		m_ManagedPoolSize;

	std::atomic<float>	m_TESinceLastChecksum;
	std::atomic<bool>	m_checksumActive;

	float				m_memChecksumFreq;

	bool				m_defrag;
	bool				m_logging;

private:

	PoolManager* Attach(MemoryPool* poolToManage);
	void Inform(int id, int message, unsigned int package);

	void PerformChecksum();

public:

	//Utility functions
	void Update(float dt, bool force = false);

	int ActivePoolCount();
	MemoryPool* GetPool(std::wstring name);

	int GetPoolAllocSpace(std::wstring name);
	int GetTotalAllocSpace();

	void SetChecksumFrequency(float value);
	void SetDefragging(bool value);
	void SetLogging(bool value);

	//Registration calls
	void RegisterPool(std::wstring name, MemoryPool* MemoryPool);

	void UnRegisterPool(std::wstring name);
	void UnRegisterAllPools();

	//De-allocation calls
	void DestroyAllPools();
	void DestroyPool(std::wstring poolName);

	//Destructive calls
	void ReleasePool(std::wstring name);
	void ReleaseAllPools();

	//Logging
	void Output();
};

