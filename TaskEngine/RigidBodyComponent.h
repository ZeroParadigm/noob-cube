#pragma once
#include "IPhysicsComponent.h"

class RigidBodyComponent : public IPhysicsComponent
{
private:
	
	Vector3 m_acceleration;
	Vector3 m_lastFrameAcceleration;

	Vector3 m_forceAccumaltor;
	Vector3 m_torqueAccumaltor;

	Matrix4 m_inverseInertiaTensor;
	Matrix4 m_inverseInertiaTensorWorld;

	Matrix4 m_transformMatrix;

	double	m_linearDamping;
	double	m_angularDamping;

	double	m_inverseMass;
	double	m_mass;

	double	m_motion;
	bool	m_wakeStatus;
	bool	m_isAwake;
	bool	m_canSleep;

public:

	RigidBodyComponent();
	RigidBodyComponent(Actor *owner);
	~RigidBodyComponent();
};