#pragma once
#include "BaseComponent.h"

class Observable;
struct EventData;

class IColliderComponent : public BaseComponent
{
private:

	Observable* m_Observable;

	bool m_Active;

public:

	IColliderComponent(void);
	IColliderComponent(Actor* owner);
	~IColliderComponent(void);

	void OnCollision(EventData event);

	void SetActive(bool active);

	bool IsActive();
};