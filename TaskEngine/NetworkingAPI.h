#pragma once


class NetworkingCore;

class  NetworkingAPI
{
private:

	NetworkingCore* core;

public:

	NetworkingAPI();
	~NetworkingAPI();

	bool Connect(std::string ipaddr, std::string port);

	bool IsConnected();

	bool Send(char* data, unsigned int size);

	void Disconnect();

	bool CheckPacket(unsigned int packetID);

	char* GetLastPacket(unsigned int packetID);
};