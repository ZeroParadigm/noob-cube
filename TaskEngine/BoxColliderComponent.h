#pragma once
#include "IColliderComponent.h"

class BoxColliderComponent : public IColliderComponent
{
private:

	DXMathLibrary::Vector3 m_halfLengths;

	BoxColliderComponent(void) {}

public:

	BoxColliderComponent(Actor* owner);
	~BoxColliderComponent(void);

	std::pair<DXMathLibrary::Vector3, DXMathLibrary::Vector3> GetAABB();

	void SetHalfLengths(DXMathLibrary::Vector3 halfLengths);
	DXMathLibrary::Vector3 GetHalfLengths();
};

typedef BoxColliderComponent BoxCC;