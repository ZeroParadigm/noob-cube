#pragma once

#include "BaseComponent.h"

/**
 * IRenderer
 *
 * Provides an interface class to specialty rendering components
 */
class IRendererComponent : public BaseComponent
{
private:

public:

	IRendererComponent(void) : BaseComponent(){}
	IRendererComponent(Actor* owner) : BaseComponent(owner) {}

	~IRendererComponent(void) {}

	virtual void Update(float dt) = 0;
};