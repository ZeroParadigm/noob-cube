#pragma once

#include "IGameplayComponent.h"
#include "WorldPolygon.h"

class GameWorldComponent : public IGameplayComponent
{
public:

	WorldPolygon* m_World;

public:

	GameWorldComponent();
	GameWorldComponent(Actor* owner);
	~GameWorldComponent();

	void Update(float dt) override;

	WorldPolygon* GetPolygon(){ return m_World; }

	void MakePlane();
	void MakePyramid();
	void MakeCube();

};