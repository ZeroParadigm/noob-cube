#include "EngineCommonStd.h"
#include "D3D9PostProcessPipeline.h"
#include "D3D9Vertex.h"

#include "MemoryDelegate.h"

#pragma region Effect File Arrays
// Name of the PostProcess .fx files
LPCWSTR g_aszFxFile[] =
{
	L"PP_ColorMonochrome.fx",
	L"PP_ColorInverse.fx",
	L"PP_ColorGBlurH.fx",
	L"PP_ColorGBlurV.fx",
	L"PP_ColorBloomH.fx",
	L"PP_ColorBloomV.fx",
	L"PP_ColorBrightPass.fx",
	L"PP_ColorToneMap.fx",
	L"PP_ColorEdgeDetect.fx",
	L"PP_ColorDownFilter4.fx",
	L"PP_ColorUpFilter4.fx",
	L"PP_ColorCombine.fx",
	L"PP_ColorCombine4.fx",
	L"PP_NormalEdgeDetect.fx",
	L"PP_DofCombine.fx",
	L"PP_NormalMap.fx",
	L"PP_PositionMap.fx",
	L"PP_ColorOverlay.fx",
};


// Description of each PostProcess supported
LPCWSTR g_aszPpDesc[] =
{
	L"[Color] Monochrome",
	L"[Color] Inversion",
	L"[Color] Gaussian Blur Horizontal",
	L"[Color] Gaussian Blur Vertical",
	L"[Color] Bloom Horizontal",
	L"[Color] Bloom Vertical",
	L"[Color] Bright Pass",
	L"[Color] Tone Mapping",
	L"[Color] Edge Detection",
	L"[Color] Down Filter 4x",
	L"[Color] Up Filter 4x",
	L"[Color] Combine",
	L"[Color] Combine 4x",
	L"[Normal] Edge Detection",
	L"DOF Combine",
	L"Normal Map",
	L"Position Map",
	L"Color Overlay",
};
#pragma endregion

#pragma region Private Definitions

void D3D9PostProcessPipeline::UpdateBackbufferDescription()
{
	HRESULT hr;

	//Update surface description
	IDirect3DSurface9* backBuffer;
	hr = m_device->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuffer);
	D3DSURFACE_DESC* backBufferSurfaceDesc = &m_backBufferSurfaceDesc;
	ZeroMemory(backBufferSurfaceDesc, sizeof(D3DSURFACE_DESC));
	if (SUCCEEDED(hr))
	{
		backBuffer->GetDesc(backBufferSurfaceDesc);
		SAFE_RELEASE(backBuffer);
	}
}

HRESULT D3D9PostProcessPipeline::ApplyPostProcessFilter(PP9FilterInstance filterInst, IDirect3DVertexBuffer9* pVB, VertexPP* aQuad, float& fExtentX, float& fExtentY)
{
	HRESULT hr;
	static float tic = 0.0f;
	if (filterInst.m_filter->m_isTemporal)
	{
		tic += 0.08f;
		filterInst.m_filter->m_effect->SetFloat(filterInst.m_filter->m_TemporalVariable, (filterInst.m_filter->m_usesDT ? 0.01667f : tic));
	}
	

	for (int i = 0; i < RT_COUNT; ++i)
		filterInst.m_filter->m_effect->SetTexture(filterInst.m_filter->m_TextureScene[i], m_SceneSave[i]);

	// Initalize any paramaters
	for (int i = 0; i < NUM_PARAMS; ++i)
	{
		if (filterInst.m_filter->m_Param[i])
			filterInst.m_filter->m_effect->SetVector(filterInst.m_filter->m_Param[i], &filterInst.m_Param[i]);
	}

	// Render the quad
	if (SUCCEEDED(m_device->BeginScene()))
	{
		filterInst.m_filter->m_effect->SetTechnique("PostProcess");

		m_device->SetVertexDeclaration(VertexPP::Decl);

		//Draw Quad
		UINT passes, p;
		filterInst.m_filter->m_effect->Begin(&passes, 0);
		for (p = 0; p < passes; ++p)
		{
			bool updatedVB = false;

			if (aQuad[1].texUV0.x != fExtentX)
			{
				aQuad[1].texUV0.x = aQuad[3].texUV0.x = fExtentX;
				updatedVB = true;
			}
			if (aQuad[2].texUV0.y != fExtentY)
			{
				aQuad[2].texUV0.y = aQuad[3].texUV0.y = fExtentY;
				updatedVB = true;
			}

			//Check annotations for extent info

			float scaleX = 1.0f;
			float scaleY = 1.0f;
			D3DXHANDLE pass = filterInst.m_filter->m_effect->GetPass(filterInst.m_filter->m_PPTechnique, p);

			D3DXHANDLE extentScaleX = filterInst.m_filter->m_effect->GetAnnotationByName(pass, "fScaleX");
			if (extentScaleX)
				filterInst.m_filter->m_effect->GetFloat(extentScaleX, &scaleX);

			D3DXHANDLE extentScaleY = filterInst.m_filter->m_effect->GetAnnotationByName(pass, "fScaleY");
			if (extentScaleY)
				filterInst.m_filter->m_effect->GetFloat(extentScaleY, &scaleY);

			//modify quad with new extents, if they are new
			if (scaleX != 1.0f)
			{
				aQuad[1].pos.x = (aQuad[1].pos.x + 0.5f) * scaleX - 0.5f;
				aQuad[3].pos.x = (aQuad[3].pos.x + 0.5f) * scaleX - 0.5f;
				updatedVB = true;
			}
			if (scaleY != 1.0f)
			{
				aQuad[2].pos.y = (aQuad[2].pos.y + 0.5f) * scaleY - 0.5f;
				aQuad[3].pos.y = (aQuad[3].pos.y + 0.5f) * scaleY - 0.5f;
				updatedVB = true;
			}

			if (updatedVB)
			{
				LPVOID VBData;
				if (SUCCEEDED(pVB->Lock(0, 0, &VBData, D3DLOCK_DISCARD)))
				{
					CopyMemory(VBData, aQuad, 4 * sizeof(VertexPP));
					pVB->Unlock();
				}
			}

			fExtentX *= scaleX;
			fExtentY *= scaleY;

			//Set up textures and the render target
			for (int i = 0; i < RT_COUNT; ++i)
			{
				if (m_RTChain[i].m_firstRender)
					filterInst.m_filter->m_effect->SetTexture(filterInst.m_filter->m_TextureSource[i], m_SceneSave[i]);
				else
					filterInst.m_filter->m_effect->SetTexture(filterInst.m_filter->m_TextureSource[i], m_RTChain[i].GetNextSource());
			}

			// set up the new render target
			IDirect3DTexture9* target = m_RTChain[filterInst.m_filter->m_renderTargetChannel].GetNextTarget();
			IDirect3DSurface9* textureSurface;

			hr = target->GetSurfaceLevel(0, &textureSurface);
			if (FAILED(hr))
			{
				ENGINE_ERROR(L"Error setting up new render target.");
				return E_FAIL;
			}

			m_device->SetRenderTarget(0, textureSurface);
			textureSurface->Release();

			// Flag the target since there is output
			m_RTChain[filterInst.m_filter->m_renderTargetChannel].m_firstRender = false;

			// Clear the render target
			m_device->Clear(0, NULL, D3DCLEAR_TARGET, 0x00000000, 1.0f, 0);

			// Render
			filterInst.m_filter->m_effect->BeginPass(p);
			m_device->SetStreamSource(0, pVB, 0, sizeof(VertexPP));
			m_device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
			filterInst.m_filter->m_effect->EndPass();

			// update the next rendertarget index
			m_RTChain[filterInst.m_filter->m_renderTargetChannel].Flip();
		}

		filterInst.m_filter->m_effect->End();

		m_device->EndScene();
	}
	return S_OK;
}

HRESULT D3D9PostProcessPipeline::PostProcess()
{
	HRESULT hr;

	float extentX = 1.0f;
	float extentY = 1.0f;

	// Create quad
	VertexPP quad[4] =
	{
		VertexPP(-0.5f, -0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f),
		VertexPP(m_backBufferSurfaceDesc.Width - 0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f),
		VertexPP(-0.5f, m_backBufferSurfaceDesc.Height - 0.5f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f),
		VertexPP(m_backBufferSurfaceDesc.Width - 0.5f, m_backBufferSurfaceDesc.Height - 0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f)
	};

	// Create vertex buffer
	IDirect3DVertexBuffer9* VB;
	hr = m_device->CreateVertexBuffer(sizeof(VertexPP)* 4,
		D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
		0,
		D3DPOOL_DEFAULT,
		&VB,
		NULL);

	if (FAILED(hr))
	{
		ENGINE_ERROR(L"Failed to create Post Process Vertex Buffer.");
		return E_FAIL;
	}

	// Fill in vertex buffer
	LPVOID VBData;
	if (SUCCEEDED(VB->Lock(0, 0, &VBData, D3DLOCK_DISCARD)))
	{
		CopyMemory(VBData, quad, sizeof(quad));
		VB->Unlock();
	}

	// Clear flags
	for (int i = 0; i < RT_COUNT; ++i)
		m_RTChain[i].m_firstRender = true;

	// Apply filters
	for (auto filter : m_activeFilters)
	{
		ApplyPostProcessFilter(filter, VB, quad, extentX, extentY);
	}


	VB->Release();

	return S_OK;
}

void D3D9PostProcessPipeline::AddFilter(int filterID)
{
	PP9FilterInstance newInst;
	newInst.m_filter = m_availableFilters.at(filterID);
	ZeroMemory(newInst.m_Param, sizeof(newInst.m_Param));
	for (int p = 0; p < NUM_PARAMS; ++p)
		newInst.m_Param[p] = m_availableFilters.at(filterID)->m_paramDef[p];

	m_activeFilters.push_back(newInst);
}


#pragma endregion


#pragma region Public Definitions

void D3D9PostProcessPipeline::Render()
{
	auto cam = m_memory->GetCamera();
	//auto cam = m_memory->GetDebugCamera();

	Matrix4 camView = cam->View();
	std::vector<MeshComponent*> objects = m_memory->GetMeshes();

	HRESULT hr;
	unsigned int pass, p;
	ID3DXMesh* meshObj;
	Matrix4 worldView;

	// Save the render target 0 so it can be retored later
	IDirect3DSurface9* oldRT;
	m_device->GetRenderTarget(0, &oldRT);

	//Render the scene
	if (SUCCEEDED(m_device->BeginScene()))
	{
		// Set the vertex declaration
		V(m_device->SetVertexDeclaration(VertexPNT::Decl));

		// Render the mesh

		//===== Replicatable placement code ==================
		//Matrix4 translate, rotate, scale, mat;

		//translate.BuildTranslation(Vector3(0.0f, 0.0f, 0.0f));
		//rotate.BuildRotationQuat(Quaternion());
		//scale.BuildScale(Vector3(0.5f, 0.5f, 0.5f));
		//Matrix4 meshWorld = scale * rotate * translate;

		//worldView = meshWorld * cam->View();

		//====================================================


		V(m_effect->SetTexture("g_txEnvMap", m_envTex));

		Matrix4 revView = cam->View();
		revView._41 = revView._42 = revView._43 = 0.0f;

		revView = revView.Inverse();

		V(m_effect->SetMatrix("g_mRevView", &revView));


		V(m_effect->SetTechnique(m_TRenderScene));


		//meshObj = m_sceneMesh[m_scene].GetMesh();




		// Draw each mesh in the scene
		V(m_effect->Begin(&pass, 0));
		V(m_effect->BeginPass(0));

		Matrix4 W;

		// Set render targets for this pass
		for (int rt = 0; rt < m_RTUsed; ++rt)
			V(m_device->SetRenderTarget(rt, m_RTTable[0].RenderTarget[rt]));

		for (auto object : objects)
		{
			if (!object->IsActive() /*|| (object->GetMesh()->GetMesh() == nullptr)*/)
				continue;
		//auto object = m_memory->GetPlayer();
			//object->Transform()->SetScale(Vector3(2.0f, 2.0f, 2.0f));
			W = object->GetOwner()->Transform()->GetWorldMatrix();
			W = W * camView;

			V(m_effect->SetMatrix("g_mWorldView", &W));


			//m_sceneMesh
			V(m_effect->SetTexture("g_txScene", object->GetMesh()->GetTexture()));
			//V(m_effect->SetTexture("g_txScene", m_sceneMesh[0].m_textures[0]));
			V(m_effect->CommitChanges());
			object->DrawSubset(0);
			//m_sceneMesh[0].GetMesh()->DrawSubset(0);

		}

		V(m_effect->EndPass());
		V(m_effect->End());







		//auto player = m_memory->GetPlayer();
		//player->Transform()->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		//W = player->Transform()->GetWorldMatrix();
		//Matrix4 W;
		//W *= camView;

		//V(m_effect->SetMatrix("g_mWorldView", &W));

		//V(m_effect->Begin(&pass, 0));

		//for (p = 0; p < pass; ++p)
		//{
		//	// Set the render targets for this pass
		//	for (int rt = 0; rt < m_RTUsed; ++rt)
		//		V(m_device->SetRenderTarget(rt, m_RTTable[p].RenderTarget[rt]));

		//	V(m_effect->BeginPass(p));

		//	// render each subset with its texture
		//	for (DWORD m = 0; m < m_skyBoxMesh.m_numMaterials; ++m)
		//	{
		//		V(m_effect->SetTexture("g_txScene", m_sceneMesh[0].m_textures[m]));
		//		V(m_effect->CommitChanges());
		//		m_sceneMesh[0].GetMesh()->DrawSubset(m);
		//	}

		//	V(m_effect->EndPass());
		//}

		//V(m_effect->End());










		// Render skybox around camera center
		V(m_effect->SetTechnique(m_TRenderSkyBox));
		V(m_device->SetVertexDeclaration(VertexSkyBox::Decl));

		//====================================================
		Matrix4 skytranslate, skyscale;

		skytranslate.BuildTranslation(cam->GetOwner()->Transform()->GetPosition());
		//skytranslate.BuildTranslation(cam->m_debugPosition);
		//skyrotate.BuildRotationQuat(Quaternion());
		skyscale.BuildScale(Vector3(1000.0f, 1000.0f, 1000.0f));
		Matrix4 skyWorld = skyscale * skytranslate;

		worldView = skyWorld * camView;
		//====================================================

		V(m_effect->SetMatrix("g_mWorldView", &worldView));

		meshObj = m_skyBoxMesh.GetMesh();

		V(m_effect->Begin(&pass, 0));

		for (p = 0; p < pass; ++p)
		{
			// Set the render targets for this pass
			for (int rt = 0; rt < m_RTUsed; ++rt)
				V(m_device->SetRenderTarget(rt, m_RTTable[p].RenderTarget[rt]));

			V(m_effect->BeginPass(p));

			// render each subset with its texture
			for (DWORD m = 0; m < m_skyBoxMesh.m_numMaterials; ++m)
			{
				V(m_effect->SetTexture("g_txScene", m_skyBoxMesh.m_textures[m]));
				V(m_effect->CommitChanges());
				V(meshObj->DrawSubset(m));
			}

			V(m_effect->EndPass());
		}

		V(m_effect->End());

		V(m_device->EndScene());
	}

	//Swap chains
	int i;
	for (i = 0; i < RT_COUNT; ++i)
		m_RTChain[i].Flip();

	// reset render targets used EXCEPT RT 0
	for (i = 1; i < m_RTUsed; ++i)
		V(m_device->SetRenderTarget(i, NULL));

	bool performPostProcess = m_enablePost && (m_activeFilters.size() > 0);

	//Perform post-processes
	if (performPostProcess)
		PostProcess();

	// Restore old render target 0 (back buffer)
	V(m_device->SetRenderTarget(0, oldRT));
	SAFE_RELEASE(oldRT);

	// Get the final result image onto the backbuffer
	//--------------------------------------
	//IDirect3DSurface9* backBuffer;
	//m_device->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuffer);
	//D3DSURFACE_DESC d3dBackBufferDesc;
	//backBuffer->GetDesc(&d3dBackBufferDesc);

	//--------------------------------------

	if (SUCCEEDED(m_device->BeginScene()))
	{
		// Render screen sized quad
		VertexPP quad[4] =
		{
			VertexPP(-0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f),
			VertexPP(m_backBufferSurfaceDesc.Width - 0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f),
			VertexPP(-0.5f, m_backBufferSurfaceDesc.Height - 0.5f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f),
			VertexPP(m_backBufferSurfaceDesc.Width - 0.5f, m_backBufferSurfaceDesc.Height - 0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f)
		};

		IDirect3DTexture9* prevTarget;
		prevTarget = (performPostProcess) ? m_RTChain[0].GetPreviousTarget() : m_SceneSave[0];
		
		V(m_device->SetVertexDeclaration(VertexPP::Decl));
		V(m_effect->SetTechnique(m_TRenderNoLight));
		V(m_effect->SetTexture("g_txScene", prevTarget));

		unsigned int passes;
		V(m_effect->Begin(&passes, 0));
		for (p = 0; p < passes; ++p)
		{
			V(m_effect->BeginPass(p));
			V(m_device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, quad, sizeof(VertexPP)));
			V(m_effect->EndPass());
		}

		V(m_effect->End());

		V(m_device->EndScene());
	}
}

HRESULT D3D9PostProcessPipeline::Init(MemoryDelegate* memory, IDirect3DDevice9* device, IDirect3D9* d3d9)
{
	///// Exclusive setup /////
	m_memory = memory;
	m_device = device;

	UpdateBackbufferDescription();

	// Query miltipe RT setting and set pass variables
	D3DCAPS9 caps;
	ZeroMemory(&caps, sizeof(D3DCAPS9));
	m_device->GetDeviceCaps(&caps);
	if (caps.NumSimultaneousRTs > 2)
	{
		// One pass of 3 RTs
		m_passes = 1;
		m_RTUsed = 3;
	}
	else if (caps.NumSimultaneousRTs > 1)
	{
		// Two passes of 2RTs the 2nd pass uses only one
		m_passes = 2;
		m_RTUsed = 2;
	}
	else
	{
		//Three passes of a single RT
		m_passes = 3;
		m_RTUsed = 1;
	}

	// Determine which of D3DFMT_A16B16G15R16F or D3DFMT_A8R8G8B8
	// to use for scene-rendering RTs
	D3DDISPLAYMODE DisplayMode;
	//d3d9
	m_device->GetDisplayMode(0, &DisplayMode);

	if (FAILED(d3d9->CheckDeviceFormat(caps.AdapterOrdinal, caps.DeviceType,
		DisplayMode.Format, D3DUSAGE_RENDERTARGET,
		D3DRTYPE_TEXTURE, D3DFMT_A16B16G16R16F)))
		m_TexFormat = D3DFMT_A8R8G8B8;
	else
		m_TexFormat = D3DFMT_A16B16G16R16F;

	//hold on to d3dObject


	// Read the d3dx effect file

	HRESULT hr;
	LPD3DXBUFFER XLog = 0;
	hr = D3DXCreateEffectFromFileW(m_device, L"Scene.fx", NULL, NULL, NULL, NULL, &m_effect, &XLog);
	if (XLog)
		MessageBox(0, (TCHAR*)XLog->GetBufferPointer(), 0, 0);

	// Initialize the PostProcess objects
	std::vector<std::wstring> effectFiles = Utility::Filesystem::GetDirectoryContents(L"Assets\\PPP", L"*.fx");


	std::wstring filterLoadResults = L"Post Process Plugins available:\n";
	for (auto effectPath : effectFiles)
	{
		PP9Filter* filter = new PP9Filter();

		if (SUCCEEDED(hr = filter->Init(m_device, 0, effectPath.c_str())))
		{
			m_filterCount++;
			m_availableFilters.insert(make_pair(m_filterCount, filter));

			filterLoadResults += std::to_wstring(m_filterCount) + L":\t" + std::wstring(wcsrchr(effectPath.c_str(), L'\\') ? wcsrchr(effectPath.c_str(), L'\\') + 1 : effectPath.c_str()) + L"\n";
		}
		else
		{
			ENGINE_ERROR(L"Error loading: " + effectPath);
			return hr;
		}
	}
	OUTPUT(filterLoadResults);

	// Obtain the technique handles
	switch (m_passes)
	{
	case 1:
		m_TRenderScene = m_effect->GetTechniqueByName("RenderScene");
		m_TRenderEnvMapScene = m_effect->GetTechniqueByName("RenderEnvMapScene");
		m_TRenderSkyBox = m_effect->GetTechniqueByName("RenderSkyBox");
		break;
	case 2:
		m_TRenderScene = m_effect->GetTechniqueByName("RenderSceneTwoPasses");
		m_TRenderEnvMapScene = m_effect->GetTechniqueByName("RenderEnvMapSceneTwoPasses");
		m_TRenderSkyBox = m_effect->GetTechniqueByName("RenderSkyBoxTwoPasses");
		break;
	case 3:
		m_TRenderScene = m_effect->GetTechniqueByName("RenderSceneThreePasses");
		m_TRenderEnvMapScene = m_effect->GetTechniqueByName("RenderEnvMapSceneThreePasses");
		m_TRenderSkyBox = m_effect->GetTechniqueByName("RenderSkyBoxThreePasses");
		break;
	}
	m_TRenderNoLight = m_effect->GetTechniqueByName("RenderNoLight");

	// Load explicit test scene
	//if (FAILED(m_sceneMesh[0].Create(m_device, L"dwarf\\dwarf.x")))
	//	return DXUTERR_MEDIANOTFOUND;
	//m_sceneMesh[0].SetVertexDecl(m_device, VertexPNT::Decl);
	m_sceneMesh[0].SetVertexDecl(m_device, VertexPNT::Decl);

	//if (FAILED(m_sceneMesh[0].Create(m_device, L"battlecruiser.X")))
	//	return DXUTERR_MEDIANOTFOUND;

	if (FAILED(m_sceneMesh[0].Create(m_device, L"Meshes\\battleship_tac_2\\battleship_tac_2.X")))
		return DXUTERR_MEDIANOTFOUND;

	if (FAILED(m_sceneMesh[1].Create(m_device, L"Meshes\\Sun\\Sun.X")))
		return DXUTERR_MEDIANOTFOUND;
	m_sceneMesh[1].SetVertexDecl(m_device, VertexPNT::Decl);

	//if (FAILED(m_sceneMesh[0].Create(m_device, L"misc\\skullocc.x")))
	//	return DXUTERR_MEDIANOTFOUND;
	//m_sceneMesh[0].SetVertexDecl(m_device, VertexPNT::Decl);

	if (FAILED(m_skyBoxMesh.Create(m_device, L"alley_skybox.x")))
		return DXUTERR_MEDIANOTFOUND;
	m_skyBoxMesh.SetVertexDecl(m_device, VertexSkyBox::Decl);


	//Calculate the mesh world matrix - for some reason the demo
	//does this in a complicated manner

	ID3DXMesh* mesh = m_sceneMesh[0].GetMesh();

	LPDIRECT3DVERTEXBUFFER9 VB;
	if (FAILED(mesh->GetVertexBuffer(&VB)))
		return E_FAIL;

	LPVOID VBData;
	if (SUCCEEDED(VB->Lock(0, 0, &VBData, D3DLOCK_READONLY)))
	{
		Vector3 center;
		float radius;
		D3DXComputeBoundingSphere((Vector3*)VBData, mesh->GetNumVertices(),
			D3DXGetFVFVertexSize(mesh->GetFVF()),
			&center, &radius);

		m_meshWorld.BuildTranslation(-center);

		Matrix4 m;
		m.BuildScale(Vector3(1 / radius, 1 / radius, 1 / radius));

		m_meshWorld = m_meshWorld * m;

		VB->Unlock();
	}

	SAFE_RELEASE(VB);

	m_pipelineCreated = true;

	//	L"[Color] Monochrome",
	//	L"[Color] Inversion",
	//	L"[Color] Gaussian Blur Horizontal",
	//	L"[Color] Gaussian Blur Vertical",
	//	L"[Color] Bloom Horizontal",
	//	L"[Color] Bloom Vertical",
	//	L"[Color] Bright Pass",
	//	L"[Color] Tone Mapping",
	//	L"[Color] Edge Detection",
	//	L"[Color] Down Filter 4x",
	//	L"[Color] Up Filter 4x",
	//	L"[Color] Combine",
	//	L"[Color] Combine 4x",
	//	L"[Normal] Edge Detection",
	//	L"DOF Combine",
	//	L"Normal Map",
	//	L"Position Map",
	//	L"Color Overlay",

	//AddFilter(L"[Normal] Edge Detection");

	//AddFilter(L"Normal Map");	
	//AddFilter(L"Normal Map");	
	//AddFilter(L"[Color] Tone Mapping");	
	//AddFilter(L"Color Overlay");						//Filter applies to world space light reflection
	//AddFilter(L"[Color] Monochrome");				//Remove all color from normal map

	//AddFilter(L"[Color] Down Filter 4x");			//down sample 4x

	//AddFilter(L"[Color] Gaussian Blur Horizontal");	//apply vertical gaussion blur
	//AddFilter(L"[Color] Gaussian Blur Vertical");	//apply horizontal gaussion blur

	//AddFilter(L"[Color] Bloom Horizontal");			//apply horizontal bloom
	//AddFilter(L"[Color] Bloom Vertical");			//apply vertical bloom

	//AddFilter(L"[Color] Up Filter 4x");				//combine and streth 4x
	//AddFilter(L"[Color] Combine 4x");


	//Colored Edge detection
	//AddFilter(21);
	//AddFilter(11);
	//AddFilter(8);
	//AddFilter(13);
	//AddFilter(7);
	//AddFilter(2);
	//AddFilter(3);
	//AddFilter(6);
	///////////////////////


	//Bloom
	AddFilter(14);
	AddFilter(4);
	//AddFilter(14);
	AddFilter(7);
	AddFilter(7);
	AddFilter(2);
	AddFilter(3);
	AddFilter(2);
	AddFilter(3);
	AddFilter(15);
	AddFilter(6);
	///////////////////////
		


	// 1:	PP_AddBuffers.fx			AddFilter(1);
	// 2 : PP_ColorBloomH.fx			AddFilter(2);
	// 3 : PP_ColorBloomV.fx			AddFilter(3);
	// 4 : PP_ColorBrightPass.fx		AddFilter(4);
	// 5 : PP_ColorCombine.fx			AddFilter(5);
	// 6 : PP_ColorCombine4.fx			AddFilter(6);
	// 7 : PP_ColorDownFilter4.fx		AddFilter(7);
	// 8 : PP_ColorEdgeDetect.fx		AddFilter(8);
	// 9 : PP_ColorGBlurH.fx			AddFilter(9);
	// 10 : PP_ColorGBlurV.fx			AddFilter(10);
	// 11 : PP_ColorInverse.fx			AddFilter(11);
	// 12 : PP_ColorMonochrome.fx		AddFilter(12);
	// 13 : PP_ColorOverlay.fx			AddFilter(13);
	// 14 : PP_ColorToneMap.fx			AddFilter(14);
	// 15 : PP_ColorUpFilter4.fx		AddFilter(15);
	// 16 : PP_Copy.fx					AddFilter(16);
	// 17 : PP_DofCombine.fx			AddFilter(17);
	// 18 : PP_LerpCombineBuffers.fx	AddFilter(18);
	// 19 : PP_NormalEdgeDetect.fx		AddFilter(19);
	// 20 : PP_NormalMap.fx				AddFilter(20);
	// 21 : PP_PositionMap.fx			AddFilter(21);
}

void D3D9PostProcessPipeline::Destroy()
{
	SAFE_RELEASE(m_effect);

	m_sceneMesh[0].Release();
	m_sceneMesh[1].Release();
	m_skyBoxMesh.Release();

	for (auto filter : m_availableFilters)
	{
		filter.second->Cleanup();
		delete filter.second;
	}

	ZeroMemory(&m_backBufferSurfaceDesc, sizeof(D3DSURFACE_DESC));

	m_pipelineCreated = false;
}

void D3D9PostProcessPipeline::OnLostDevice()
{
	if (m_effect) m_effect->OnLostDevice();

	for (auto filter : m_availableFilters)
		filter.second->OnLostDevice();

	for (int i = 0; i < RT_COUNT; ++i)
	{
		SAFE_RELEASE(m_SceneSave[i]);
		m_RTChain[i].Cleanup();
	}

	m_sceneMesh[0].InvalidateDeviceObjects();
	m_sceneMesh[1].InvalidateDeviceObjects();
	m_skyBoxMesh.InvalidateDeviceObjects();

	for (int p = 0; p < RT_COUNT; ++p)
	{
		for (int rt = 0; rt < RT_COUNT; ++rt)
		{
			SAFE_RELEASE(m_RTTable[p].RenderTarget[rt]);
		}
	}

	SAFE_RELEASE(m_envTex);
}

HRESULT D3D9PostProcessPipeline::OnResetDevice()
{
	HRESULT hr;

	auto cam = m_memory->GetDebugCamera();

	UpdateBackbufferDescription();

	if (m_effect) V_RETURN(m_effect->OnResetDevice());

	for (auto filter : m_availableFilters)
		filter.second->OnResetDevice(m_backBufferSurfaceDesc.Width, m_backBufferSurfaceDesc.Height);

	//g_pEffect->SetMatrix("g_mProj", g_Camera.GetProjMatrix());
	m_effect->SetMatrix("g_mProj", &cam->Projection());

	//g_Camera.SetWindow(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

	//g_SceneMesh[0].RestoreDeviceObjects(pd3dDevice);
	//g_SceneMesh[1].RestoreDeviceObjects(pd3dDevice);
	//g_Skybox.RestoreDeviceObjects(pd3dDevice);
	m_sceneMesh[0].RestoreDeviceObjects(m_device);
	m_sceneMesh[1].RestoreDeviceObjects(m_device);
	m_skyBoxMesh.RestoreDeviceObjects(m_device);

	for (int i = 0; i < RT_COUNT; ++i)
	{
		V_RETURN(hr = m_device->CreateTexture(m_backBufferSurfaceDesc.Width,
			m_backBufferSurfaceDesc.Height,
			1,
			D3DUSAGE_RENDERTARGET,
			m_TexFormat,
			D3DPOOL_DEFAULT,
			&m_SceneSave[i],
			NULL));


		IDirect3DTexture9* RT[2];
		ZeroMemory(RT, sizeof(RT));

		for (int t = 0; t < 2; ++t)
		{
			V_RETURN(m_device->CreateTexture(m_backBufferSurfaceDesc.Width,
				m_backBufferSurfaceDesc.Height,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,
				D3DPOOL_DEFAULT,
				&RT[t],
				NULL));
		}

		m_RTChain[i].Init(RT);
		SAFE_RELEASE(RT[0]);
		SAFE_RELEASE(RT[1]);
	}

	// Create the environment mapping texture
	WCHAR filepath[MAX_PATH];
	Utility::Filesystem::FindAssetFileFromCommonLocs(filepath, MAX_PATH, L"Light Probes\\uffizi_cross.dds");
	if (FAILED(D3DXCreateCubeTextureFromFile(m_device, filepath, &m_envTex)))
		return S_FALSE;

	// Initalize the render target table based on how many simultaneous RTs
	// the card can support
	IDirect3DSurface9* surf;
	switch (m_passes)
	{
	case 1:
		m_SceneSave[0]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[0] = surf;
		m_SceneSave[1]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[1] = surf;
		m_SceneSave[2]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[2] = surf;

		// Pass 1 and 2 not used
		m_RTTable[1].RenderTarget[0] = NULL;
		m_RTTable[1].RenderTarget[1] = NULL;
		m_RTTable[1].RenderTarget[2] = NULL;

		m_RTTable[2].RenderTarget[0] = NULL;
		m_RTTable[2].RenderTarget[1] = NULL;
		m_RTTable[2].RenderTarget[2] = NULL;
		break;
	case 2:
		m_SceneSave[0]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[0] = surf;
		m_SceneSave[1]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[1] = surf;
		m_RTTable[0].RenderTarget[2] = NULL;


		m_SceneSave[2]->GetSurfaceLevel(0, &surf);
		m_RTTable[1].RenderTarget[0] = surf;
		m_RTTable[1].RenderTarget[1] = NULL;
		m_RTTable[1].RenderTarget[2] = NULL;


		m_RTTable[2].RenderTarget[0] = NULL;
		m_RTTable[2].RenderTarget[1] = NULL;
		m_RTTable[2].RenderTarget[2] = NULL;
		break;
	case 3:
		m_SceneSave[0]->GetSurfaceLevel(0, &surf);
		m_RTTable[0].RenderTarget[0] = surf;
		m_RTTable[0].RenderTarget[1] = NULL;
		m_RTTable[0].RenderTarget[2] = NULL;

		m_SceneSave[1]->GetSurfaceLevel(0, &surf);
		m_RTTable[1].RenderTarget[0] = surf;
		m_RTTable[1].RenderTarget[1] = NULL;
		m_RTTable[1].RenderTarget[2] = NULL;

		m_SceneSave[2]->GetSurfaceLevel(0, &surf);
		m_RTTable[2].RenderTarget[0] = surf;
		m_RTTable[2].RenderTarget[1] = NULL;
		m_RTTable[2].RenderTarget[2] = NULL;
		break;
	}

	return S_OK;
}

#pragma endregion