#include "EngineCommonStd.h"

#include "ClosestPointAlgorithms.h"

using namespace DXMathLibrary;

void ClosestPointAlgorithms::ClosestPointAABB(DXMathLibrary::Vector3 position, BoxColliderComponent* aabb, ContactPoint &point)
{
	std::pair<Vector3, Vector3> MinMax = aabb->GetAABB();

	Vector3 Min = MinMax.first;
	Vector3 Max = MinMax.second;

	Vector3 q;

	float v = position.x;
	if (v < Min.x) v = Min.x;
	if (v > Max.x) v = Max.x;
	q.x = v;

	v = position.y;
	if (v < Min.y) v = Min.y;
	if (v > Max.y) v = Max.y;
	q.y = v;

	v = position.z;
	if (v < Min.z) v = Min.z;
	if (v > Max.z) v = Max.z;
	q.z = v;

	point.contactPoint = q;
}