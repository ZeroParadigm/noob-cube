#pragma once

#include <XInput.h>

#pragma comment(lib, "XInput.lib")

class Gamepad
{
private:

	XINPUT_STATE ControllerState;

public:

	Gamepad(int player);
	~Gamepad(void);

	unsigned int m_Players;

	//HANDLE		 m_Thread;
	//unsigned int m_ThreadKillState;

	XINPUT_STATE GetState(void);

	struct Controller
	{
		bool InUse;
		POINT LAnol, RAnol;
		float LTrig, RTrig;
		bool A, B, X, Y;
		bool Start, Select;
		bool LShoulder, RShoulder;
		bool LStick, RStick;
		bool DPAD_UP, DPAD_DOWN, DPAD_LEFT, DPAD_RIGHT;
	};

	Controller current[4];
	Controller previous[4];

	bool IsConnected(void);

	void Init(void);
	void Check(void);
};

extern Gamepad* gGamepad;