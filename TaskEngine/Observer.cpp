#include "EngineCommonStd.h"
#include "Observable.h"

Observer::Observer(void)
{
}

Observer::~Observer(void)
{
	for (auto sub : m_Subscribers)
	{
		Observable* temp = sub;
		sub = nullptr;
	}

	m_Subscribers.clear();
}

Observer* Observer::Instance(void)
{
	static Observer obs;
	return &obs;
}

void Observer::Notify(EVENT type, EventData data)
{
	for (unsigned int x = 0; x < m_Subscribers.size(); ++x)
	{
		if (m_Subscribers[x]->m_Events & type)
		{
			m_Subscribers[x]->HandleEvent(type, data);
		}
	}
}

void Observer::Subscribe(Observable* subscriber)
{
	m_Subscribers.push_back(subscriber);
}

void Observer::Unsubscribe(Observable* subscriber)
{
	std::vector<Observable*>::iterator sub;

	for (sub = m_Subscribers.begin(); sub != m_Subscribers.end(); sub++)
	{
		if (*sub == subscriber)
		{
			m_Subscribers.erase(sub);
			return;
		}
	}
}