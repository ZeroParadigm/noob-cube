/**
 * CriticalSection
 * 
 * Defines a CriticalSection object for threadsafe operations
 *
 * Use ScopedCriticalSection to expose
 */

#pragma once



class CriticalSection : public ENGINE_noncopyable
{
protected:
	mutable CRITICAL_SECTION m_cs;

public:
	CriticalSection(void) {InitializeCriticalSection(&m_cs);}
	~CriticalSection(void){DeleteCriticalSection(&m_cs);}

	void Lock(void)  {EnterCriticalSection(&m_cs);}
	void Unlock(void){LeaveCriticalSection(&m_cs);}

};

class ScopedCriticalSection : public ENGINE_noncopyable
{
private:
	CriticalSection& m_csResource;

public:
	ScopedCriticalSection(CriticalSection& csResource)
		: m_csResource(csResource)
	{
		m_csResource.Lock();
	}

	~ScopedCriticalSection(void)
	{
		m_csResource.Unlock();
	}
};