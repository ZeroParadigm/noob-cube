#pragma once

#include "IGameplayComponent.h"
#include "ScoreSystem.h"

class WorldPlane;
class Actor;
class BulletSystem;

/**
 * PlayerComponent
 *
 * Handles gameplay specific to player
 */
class PlayerComponent : public IGameplayComponent
{
private:

	WorldPlane*		m_CurrentFace;

	Actor*			m_Camera;

	BulletSystem*	m_Gun;

	ScoreSystem		m_ScoreBoard;

	float			m_MovePointDistance;

	float			m_Timer;
	float			m_SpawnTime;
	float			m_ShootTimer;
	float			m_FireRate;

	bool			m_CanShoot;
	bool			m_Active;
	bool			m_Edging;
	
	int				m_Lives;

public:

	PlayerComponent(void);
	PlayerComponent(Actor* owner);
	~PlayerComponent(void);

	void Init();

	/**
	 * Update
	 *
	 * Updates all player systems
	 * @param[in] float time elapsed
	 */
	void Update(float dt) override;

	/**
	 * Spawn
	 *
	 * Places the player back on the current face
	 */
	void Spawn(void);
	/**
	 * Activate
	 *
	 * Register the player for collisions
	 */
	void Activate(void);
	/**
	 * Deactivate
	 *
	 * Unsubscribe for events
	 */
	void Deactivate(void);
	/**
	 * Die
	 *
	 * Deactivate the player and spawn event
	 */
	void Die(void);

	/**
	 * Shoot
	 *
	 * Fire a bullet in the direction
	 * @param[in] POINT Point on unit circle at which bullet should fire
	 */
	void Shoot(POINT p);
	/**
	 * Move
	 *
	 * Move toward position
	 * @param[in] POINT Point on unit circle for arrive target
	 */
	void Move(POINT p);

	/**
	* GetPlane
	*
	* Gets a pointer to the current world plane
	*/
	WorldPlane* GetPlane(void);

	/**
	 * ChangeFace
	 *
	 * Switches movement from one face to another
	 * @param[in] WorldPlane* Next face for movement.  
	 */
	void ChangeFace(WorldPlane* face);

	/**
	 * SetCamera
	 *
	 * Sets a pointer to the camera object.
	 * Needed to calculate movement
	 * @param[in] Actor* Camera actor
	 */
	void SetCamera(Actor* camera);

	/**
	*	SetGun
	*
	*	Sets a pointer to the BulletSystem object.
	*	Needed to fire bullets.
	*	@param[in] BulletSystem* gun object
	*/
	void SetGun(BulletSystem* gun);

	/**
	 * AddScore
	 *
	 * Add points to the player score
	 * @param[in] int Points to be added
	 */
	void AddScore(int points);

	/**
	* GetScore
	*
	* Returns the player's true score
	*/
	int GetScore();

	/**
	* GetDisplayScore
	*
	* Returns the player's display score
	*/
	int GetDisplayScore();

	/**
	 * IsActive
	 *
	 * Checks to see if the player is active
	 */
	bool IsActive(void);

	/**
	* IsEdging
	*
	* Checks to see if the player is currently edging
	*/
	bool IsEdging(void);

	/**
	* SetEdging
	*
	* Sets the player's edging status
	*/
	void SetEdging(bool edging);

protected:

	bool CanShoot(void);

	void CameraTransform(DXMathLibrary::Quaternion q, DXMathLibrary::Vector3& p);

	void HandleCollisionEvent(Actor* first, Actor* second);
};