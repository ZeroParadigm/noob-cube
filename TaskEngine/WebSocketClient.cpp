#include "EngineCommonStd.h"

#include "WebSocketClient.h"
#include "JSON.h"

using namespace std;
using namespace easywsclient;


WebSocketClient::WebSocketClient()
{
	m_ws = nullptr;
	m_ServerAddress = "ws://50.130.9.202:8080/server";
}

WebSocketClient::~WebSocketClient()
{
#ifdef _WIN32
	WSACleanup();
#endif
}

void WebSocketClient::Initialize()
{
#ifdef _WIN32
	int error;

	error = WSAStartup(MAKEWORD(2, 2), &m_WSAData);
	if (error)
	{
		//LOG(L"WSAStartup() failed!");
	}
#endif
}

void WebSocketClient::Shutdown()
{
	if (m_ws || m_ws != nullptr)
		m_ws->close();
}

bool WebSocketClient::Connect()
{
	m_ws = WebSocket::from_url(m_ServerAddress);

	if (m_ws == 0)
	{
		m_ws = nullptr;
		return false;
	}
	else
	{
		return true;
	}
}

void WebSocketClient::Send()
{
}

void WebSocketClient::SendPosition(float x, float y, float z)
{
	// Make the packetID a value
	JSONValue* packetID = new JSONValue(2.);
	// Make the position x, y, z values
	JSONValue* xVal = new JSONValue((double)x);
	JSONValue* yVal = new JSONValue((double)y);
	JSONValue* zVal = new JSONValue((double)z);
	//// Make the position array from the values
	//JSONArray position;
	//position.push_back(xVal);
	//position.push_back(yVal);
	//position.push_back(zVal);
	//// Make the position array a value
	//JSONValue* pos = new JSONValue(position);
	// Tie the packetID and position into an object
	JSONObject object;
	object.insert(pair<wstring, JSONValue*>(L"packetId", packetID));
	object.insert(pair<wstring, JSONValue*>(L"x", xVal));
	object.insert(pair<wstring, JSONValue*>(L"y", yVal));
	object.insert(pair<wstring, JSONValue*>(L"z", zVal));
	// Make the object a value
	JSONValue* val = new JSONValue(object);
	// Stringify the object
	wstring wstr(val->Stringify());
	string sendBuffer(wstr.begin(), wstr.end());
	// Send the JSON
	m_ws->send(sendBuffer);

	if (val)
		delete val;
}

void WebSocketClient::Update(float dt)
{
	if (m_ws || m_ws != nullptr)
	{
		m_ws->poll();
		m_ws->dispatch(m_RecvHandler);
	}
}

easywsclient::WebSocket::readyStateValues WebSocketClient::GetReadyState()
{
	if (m_ws || m_ws != nullptr)
		return m_ws->getReadyState();
	else
		return WebSocket::CLOSED;
}

void WebSocketClient::HandleMessage(std::function<void(const std::string& message)> handler)
{
	m_RecvHandler = handler;
}