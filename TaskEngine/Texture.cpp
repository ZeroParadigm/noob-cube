#include "EngineCommonStd.h"

#include "Texture.h"


Texture::Texture()
{
}

Texture::Texture(std::wstring fileName) : m_FileName(fileName)
{
}

Texture::~Texture()
{
}

void Texture::Load(IDirect3DDevice9* device)
{
	D3DXCreateTextureFromFile(device, m_FileName.c_str(), &m_Texture);
}

void Texture::Release()
{
	SAFE_RELEASE(m_Texture);
}
