#pragma once


class ScoreSystem
{
private:

	int m_CurrentScore;
	float m_DisplayScore;

	float m_GrowthRate;

public:

	ScoreSystem();
	~ScoreSystem();

	void Update(float dt);

	void AddPoints(int points);

	void SetGrowthRate(float pointsPerSecond);

	int GetCurrentScore();
	int GetDisplayScore();
};