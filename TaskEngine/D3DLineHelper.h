#pragma once


class D3DLineHelper
{
private:

	struct LineNode
	{
		bool AA;
		int ID;
		float width;
		float scaleRatio;
		D3DXVECTOR2* vertexList;
		D3DCOLOR color;
		DWORD vertexListCount;
	};

	std::vector<LineNode*> m_LineArray;
	IDirect3DDevice9* m_device9;
	ID3DXLine* m_lineDrawer9;

public:

	D3DLineHelper();
	~D3DLineHelper();

	HRESULT Init(IDirect3DDevice9* device);
	HRESULT Release();

	HRESULT OnResetDevice();
	HRESULT OnLostDevice();

	HRESULT Render();

	HRESULT AddLine(int* pnLineID, 
					D3DXVECTOR2* pVertexList, 
					DWORD dwVertexListCount, 
					D3DCOLOR Color, 
					float fWidth,
					float fScaleRatio, 
					bool bAntiAlias);

	HRESULT AddRect(int* pnLineID, 
					RECT rc, 
					D3DCOLOR Color, 
					float fWidth, 
					float fScaleRatio, 
					bool bAntiAlias);

	HRESULT RemoveLine(int nLineID);
	HRESULT RemoveAllLines();
};