#include "EngineCommonStd.h"

#include "ContactPoint.h"

#include "ParticleComponent.h"
#include "SphereColliderComponent.h"
#include "BoxColliderComponent.h"

#include "Observer.h"

using namespace DXMathLibrary;

ContactPoint::ContactPoint(void)
{
}


ContactPoint:: ~ContactPoint(void)
{
}

void ContactPoint::Resolve(double dt)
{
	ResolveVelocity(dt);
	ResolveInterpentration(dt);

	//game rule
	//particle[0]->wake();
	//particle[1]->wake();

	//Create event data
	Actor* a = particle[0]->GetOwner();
	Actor* b = particle[1]->GetOwner();

	EventData data(a, b);

	//Emit the event
	a->Collider()->OnCollision(data);
}

double ContactPoint::calculateSeperatingVelocity() const
{
	Vector3 relativeVelocity = particle[0]->GetOwner()->Transform()->GetVelocity();
	
	if (particle[1])
		relativeVelocity -= particle[1]->GetOwner()->Transform()->GetVelocity();

	return relativeVelocity.Dot(contactNormal);
}

void ContactPoint::ResolveVelocity(double dt)
{
	double separatingVelocity = calculateSeperatingVelocity();

	if (separatingVelocity > 0)
		return;

	double newSepVelocity = -separatingVelocity * restitution;

	//resting contact
	Vector3 accCausedVelocity = particle[0]->m_acceleration;
	if (particle[1])
		accCausedVelocity -= particle[1]->m_acceleration;
	double accCausedSepVelocity = accCausedVelocity.Dot(contactNormal) * dt;

	if (accCausedSepVelocity < 0)
	{
		newSepVelocity += restitution * accCausedSepVelocity;

		if (newSepVelocity < 0)
			newSepVelocity = 0;
	}
	// -resting contact

	double deltaVelocity = newSepVelocity - separatingVelocity;

	double totalInverseMass = particle[0]->m_inverseMass;
	if (particle[1])
		totalInverseMass += particle[1]->m_inverseMass;

	if (totalInverseMass <= 0)
		return;

	double impulse = deltaVelocity / totalInverseMass;

	Vector3 impulsePerIMass = contactNormal * (float)impulse;

	particle[0]->GetOwner()->Transform()->SetVelocity(particle[0]->GetOwner()->Transform()->GetVelocity() + impulsePerIMass * (float)particle[0]->m_inverseMass);

	if (particle[1])
		particle[1]->GetOwner()->Transform()->SetVelocity(particle[1]->GetOwner()->Transform()->GetVelocity() + impulsePerIMass * -(float)particle[1]->m_inverseMass);

}

void ContactPoint::ResolveInterpentration(double dt)
{
	if (penetration <= 0)
		return;

	double totalInverseMass = particle[0]->m_inverseMass;
	if (particle[1])
		totalInverseMass += particle[1]->m_inverseMass;

	if (totalInverseMass <= 0)
		return;

	Vector3 movePerIMass = contactNormal * (float)(penetration / totalInverseMass);

	particleMovement[0] = movePerIMass * (float)particle[0]->m_inverseMass;
	if (particle[1]){
		particleMovement[1] = movePerIMass * -(float)particle[1]->m_inverseMass;
	}
	else {
		particleMovement[1] = Vector3();
	}

	particle[0]->GetOwner()->Transform()->SetPosition(particle[0]->GetOwner()->Transform()->GetPosition() + particleMovement[0]);

	if (particle[1])
		particle[1]->GetOwner()->Transform()->SetPosition(particle[1]->GetOwner()->Transform()->GetPosition() + particleMovement[1]);

}

void ContactPoint::setBodyData(ParticleComponent* one, ParticleComponent* two, double rest, double fric)
{
	particle[0] = one;
	particle[1] = two;
	restitution = rest;
	friction = fric;
}