#pragma once
/*
	This file has some Memory Tools that are almost
	exclusively used for memory manipulation and 
	allocation assistance
*/

namespace MemoryTools
{

	void RawMemoryToChunkList(unsigned char* rawMem, size_t chunkSize, size_t blockSize, unsigned int chunkCount);

}