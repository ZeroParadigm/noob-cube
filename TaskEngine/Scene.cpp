#include "EngineCommonStd.h"
#include "Scene.h"

//Render devices
#include "D3D9RenderDevice.h"

//Windows to draw on
#include "RenderWindow.h"

//Standard system layout
void Scene::Init(MemoryDelegate* memory,
					eDeviceType type,
					float desiredDrawRate,
					bool calculateStats,
					HINSTANCE appInstance,
					const TCHAR* caption,
					long x, long y,
					long w, long h)
{
	m_memory = memory;
	m_window = new RenderWindow();

	switch (type)
	{
		case DEVICE_D3D9:
			m_device = _new D3D9RenderDevice();
			break;
	}

	m_window->Init(appInstance, caption, x, y, w, h);
	m_device->Init(memory, m_window, desiredDrawRate, calculateStats);

	m_window->AttatchDevice(m_device);

	m_window->Show();
}

void Scene::Shutdown()
{
	m_device->Shutdown();
	m_window->Hide();

	SAFE_DELETE(m_device);
	SAFE_DELETE(m_window);
}

void Scene::Update(double dt)
{
	m_device->Update(dt);
}

RenderWindow* Scene::GetWindow()
{
	return m_window;
}

IRenderDevice* Scene::GetDevice()
{
	return m_device;
}
