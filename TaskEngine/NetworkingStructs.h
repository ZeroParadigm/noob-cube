#pragma once

#include <string>


struct PacketData
{
	unsigned int packetID;
	TCHAR data[512];
};

struct ConnectionInfo
{
	std::string ip;
	std::string port;
};

struct DemoPacket
{
	unsigned int packetID;
	int data;

	DemoPacket()
	{
		packetID = 1;
		data = 0;
	}
};