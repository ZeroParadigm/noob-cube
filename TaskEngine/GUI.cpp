#include "EngineCommonStd.h"

#include "GUI.h"
#include "GraphicsUtility.h"

#include "WindowElement.h"
#include "StaticTextElement.h"
#include "StaticImageElement.h"
#include "ButtonElement.h"


GUI::GUI()
{
}

GUI::~GUI()
{
	for (auto element : m_GUIElements)
	{
		delete element;
		element = nullptr;
	}

	m_GUIElements.clear();
}

void GUI::Draw()
{
	g_d3dSprite->Begin(D3DXSPRITE_ALPHABLEND);

	for (BaseGUIElement* element : m_GUIElements)
	{
		if (element->IsVisible())
		{
			element->Draw();
		}
	}

	g_d3dSprite->Flush();
	g_d3dSprite->End();
}

WindowElement* GUI::CreateWindowElement()
{
	WindowElement* window = new WindowElement();
	m_GUIElements.push_back(window);
	return window;
}

WindowElement* GUI::CreateWindowElement(BaseGUIElement* parent)
{
	WindowElement* window = new WindowElement(parent);
	m_GUIElements.push_back(window);
	return window;
}

StaticTextElement* GUI::CreateStaticTextElement()
{
	StaticTextElement* text = new StaticTextElement();
	m_GUIElements.push_back(text);
	return text;
}

StaticTextElement* GUI::CreateStaticTextElement(BaseGUIElement* parent)
{
	StaticTextElement* text = new StaticTextElement(parent);
	m_GUIElements.push_back(text);
	return text;
}

StaticImageElement* GUI::CreateStaticImageElement()
{
	StaticImageElement* image = new StaticImageElement();
	m_GUIElements.push_back(image);
	return image;
}

StaticImageElement* GUI::CreateStaticImageElement(BaseGUIElement* parent)
{
	StaticImageElement* image = new StaticImageElement(parent);
	m_GUIElements.push_back(image);
	return image;
}

ButtonElement* GUI::CreateButtonElement()
{
	ButtonElement* button = new ButtonElement();
	m_GUIElements.push_back(button);
	return button;
}

ButtonElement* GUI::CreateButtonElement(BaseGUIElement* parent)
{
	ButtonElement* button = new ButtonElement(parent);
	m_GUIElements.push_back(button);
	return button;
}

BaseGUIElement* GUI::GetElement(std::wstring elementName)
{
	for (BaseGUIElement* element : m_GUIElements)
	{
		if (element->GetName() == elementName)
		{
			return element;
		}
	}

	return nullptr;
}