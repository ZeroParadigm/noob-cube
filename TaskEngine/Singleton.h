#pragma once

// These defines come with the singleton header. 
// Which should only be included to a definition 
// file when it's intended to be a singleton.

#define DEFINE_SINGLETON_ACCESS(_className_) Singleton<_className_>::Instance()

#define LOCK_TO_SINGLETON(_className_) \
	friend class Singleton<_className_>; \
	_className_(); \
	public:\
	~_className_(); \
	private:\


#define SINGLETON_CLASS(_className_) \
	class _className_ \
	{ \
	LOCK_TO_SINGLETON(_className_) \



template<class T>
class Singleton
{
public:
	static T& Instance()
	{
		static T instance;
		return instance;
	}

protected:

	Singleton();
	~Singleton();

private:

	Singleton(Singleton const&);
	Singleton& operator= (Singleton const&);
};