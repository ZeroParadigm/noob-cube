#pragma once

#include "BaseGUIElement.h"
#include "Sprite.h"


class ButtonElement : public BaseGUIElement
{
protected:

	std::wstring m_Text;
	D3DCOLOR m_TextColor;

	Sprite* m_Image;
	D3DCOLOR m_ColorMask;

public:

	ButtonElement();
	ButtonElement(BaseGUIElement* parent);
	~ButtonElement();

	void Draw() override;

public:

	void SetText(std::wstring text);
	void SetTextColor(D3DCOLOR color);

	void SetImage(Sprite* image);
	void SetColorMask(D3DCOLOR color);

	std::wstring GetText();
	D3DCOLOR GetTextColor();

	IDirect3DTexture9* GetImage();
	Sprite* GetImageObj();

	D3DCOLOR GetColorMask();

};