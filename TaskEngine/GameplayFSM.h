#pragma once

#include "GameState.h"

class MemoryDelegate;


class GameplayFSM
{
private:

	D3D9RenderDevice* graphics;
	MemoryDelegate*	memory;
	DirectInput*	input;
	Gamepad*		gamepad;
	WebSocketClient* network;

public:

	GameState* m_CurrentState;
	GameState* m_PreviousState;

public:

	GameplayFSM(D3D9RenderDevice* g, MemoryDelegate* mem, DirectInput* dinput, Gamepad* pad, WebSocketClient* net)
	{
		m_CurrentState = nullptr;
		m_PreviousState = nullptr;

		graphics = g;
		memory = mem;
		input = dinput;
		gamepad = pad;
		network = net;
	}
	~GameplayFSM()
	{
		if(m_CurrentState)
		{
			m_CurrentState->LeaveState();
			delete m_CurrentState;
		}
		if(m_PreviousState)
		{
			delete m_PreviousState;
		}
	}

	void TransitionState(GameState* newState)
	{
		if(m_PreviousState)
		{
			delete m_PreviousState;
		}

		if(m_CurrentState)
		{
			m_CurrentState->LeaveState();
		}
		m_PreviousState = m_CurrentState;

		newState->graphics = graphics;
		newState->memory = memory;
		newState->fsm = this;
		newState->input = input;
		newState->gamepad = gamepad;
		newState->network = network;

		m_CurrentState = newState;
		m_CurrentState->EnterState();
	}

	void UpdateState(float dt)
	{
		m_CurrentState->UpdateState(dt);
	}

};