#include "EngineCommonStd.h"
#include "BaseWindow.h"

#include "WindowController.h"
//===============================================

#pragma region Private Definitions

LRESULT CALLBACK BaseWindow::s_MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	BaseWindow *self;

	if (msg == WM_NCCREATE)
	{
		LPCREATESTRUCT lpcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
		self = reinterpret_cast<BaseWindow*>(lpcs->lpCreateParams);
		self->m_hwnd = hwnd;

		SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(self));
	}
	else
	{
		self = reinterpret_cast<BaseWindow*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
	}


	if (self)
	{
		return self->HandleMessage(msg, wParam, lParam);
	}
	else
	{
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
}

#pragma endregion

#pragma region Protected Definitions

void BaseWindow::Register()
{
	WNDCLASSEX wcx;

	wcx.cbSize = sizeof(WNDCLASSEX);
	wcx.style = 0;
	wcx.lpfnWndProc = BaseWindow::s_MsgProc;
	wcx.cbClsExtra = 0;
	wcx.cbWndExtra = 0;
	wcx.hInstance = m_appInstance;
	wcx.hIcon = NULL;
	wcx.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcx.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcx.lpszMenuName = NULL;
	wcx.lpszClassName = ClassName();
	wcx.hIconSm = NULL;


	////You need to explicitly assign the hIcon and hIconSm with the proper size icon if you expect it to be displayed correctly(otherwise, the image you supply will be automatically sized by the window manager, which is usually ugly).Assuming your icon resource defines the required image types(16x16, 24x24, 32x32, and even 48x48 if you�re game), you can explicitly load the correct size for any platform as follows :

	//wcex.hIcon = (HICON)::LoadImage(hResource,
	//	MAKEINTRESOURCE(IDR_APPLICATION),
	//	IMAGE_ICON,
	//	::GetSystemMetrics(SM_CXICON),
	//	::GetSystemMetrics(SM_CYICON),
	//	LR_DEFAULTCOLOR);

	//wcex.hIconSm = (HICON)::LoadImage(hResource,
	//	MAKEINTRESOURCE(IDR_APPLICATION),
	//	IMAGE_ICON,
	//	::GetSystemMetrics(SM_CXSMICON),
	//	::GetSystemMetrics(SM_CYSMICON),
	//	LR_DEFAULTCOLOR);


	if (!WinRegisterClass(&wcx))
	{
		DWORD error = GetLastError();
		if (error == ERROR_CLASS_ALREADY_EXISTS)
		{
			ENGINE_WARNING(L"Class already exists\n"
						   L"\tClass: " + std::wstring(ClassName()) + L" ");
		}
		else
		{
			ENGINE_ERROR(L"Failed to register window: " + m_caption + L"\n" +
						L"<Unknown Reason> \n" +
						L"Class: " + ClassName() + L"\n" +
						L"Closing Application.");

			PostQuitMessage(0);
		}
	}
}

void BaseWindow::WinCreateWindowEx(DWORD exStyle, LPCWSTR caption, DWORD style, int x, int y, int w, int h, HWND hwndParent, HMENU menu)
{
	m_caption	= caption;
	m_position.x = x; m_position.y = y;

	RECT R = { 0, 0, w, h };
	AdjustWindowRectEx(&R, style, (menu ? true : false), exStyle);
	m_hwnd = CreateWindowEx(exStyle, ClassName(), 
							caption, 
							style, 
							x, y, 
							R.right, R.bottom, 
							hwndParent, 
							menu, 
							m_appInstance, 
							this);


	GetClientRect(m_hwnd, &R);
	m_size.x = R.right; m_size.y = R.bottom;

	if (!m_hwnd)
	{
		DWORD error = GetLastError();
		ENGINE_ERROR(L"Failed to create window: " + m_caption + L"\n" +
					 L"Class: " + ClassName() + L"\n" + 
					 L"Closing Application.");

		PostQuitMessage(0);
	}
}

LRESULT BaseWindow::HandleMessage(UINT msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lres;

	switch (msg)
	{
	case WM_CLOSE:
		DestroyWindow(m_hwnd);
		UnregisterClass(ClassName(), NULL);
		return 0;

	case WM_NCDESTROY:
		lres = DefWindowProc(m_hwnd, msg, wParam, lParam);
		SetWindowLongPtr(m_hwnd, GWLP_USERDATA, 0);
		return lres;

	case WM_DESTROY:
		if (m_isMainWindow) PostQuitMessage(0);
		break;
	}

	return DefWindowProc(m_hwnd, msg, wParam, lParam);
}

#pragma endregion

//===============================================

#pragma region Public API

void BaseWindow::Hide()
{
	ShowWindow(m_hwnd, SW_HIDE);
}

void BaseWindow::Show(BOOL forceForeground /* = false */)
{
	ShowWindow(m_hwnd, SW_SHOW);

	if (forceForeground)
		SetForegroundWindow(m_hwnd);

	UpdateWindow(m_hwnd);
}

void BaseWindow::Minimize()
{
	ShowWindow(m_hwnd, SW_MINIMIZE);
}

void BaseWindow::Maximize(BOOL forceForeground /* = false */)
{
	ShowWindow(m_hwnd, SW_MAXIMIZE);

	if (forceForeground)
		SetForegroundWindow(m_hwnd);

	UpdateWindow(m_hwnd);
}

void BaseWindow::Restore()
{
	ShowWindow(m_hwnd, SW_RESTORE);
	UpdateWindow(m_hwnd);
}

void BaseWindow::Enable(BOOL enable)
{
	EnableWindow(m_hwnd, enable);
}

void BaseWindow::SetFullscreen(bool full)
{
	if (full)
	{
		if (m_fullscreen) return;

		RECT R = { 0, 0, 0, 0 };
		GetWindowRect(m_hwnd, &R);
		m_position.x = R.left;
		m_position.y = R.top;

		m_size.x = R.right;
		m_size.y = R.bottom;
		ScreenToClient(m_hwnd, &m_size);

		int width = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		SetWindowLongPtr(m_hwnd, GWL_STYLE, WS_POPUP);

		SetWindowPos(m_hwnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);

		m_fullscreen = true;
	}
	else
	{
		if (!m_fullscreen) return;

		RECT R = { 0, 0, m_size.x, m_size.y };
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, m_hasMenu);

		SetWindowLongPtr(m_hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		SetWindowPos(m_hwnd, HWND_TOP, m_position.x, m_position.y, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);

		m_fullscreen = false;
	}
}

void BaseWindow::SetCaption(const TCHAR* caption)
{
	m_caption = caption;

	SetWindowText(m_hwnd, caption);
}

void BaseWindow::SetPosition(long x, long y)
{
	m_position.x = x; m_position.y = y;

	SetWindowPos(m_hwnd, HWND_TOP, x, y, m_size.x, m_size.y, SWP_NOCOPYBITS);
}

void BaseWindow::SetSize(long w, long h)
{
	SetWindowPos(m_hwnd, HWND_TOP, m_position.x, m_position.y, w, h, SWP_NOCOPYBITS);

	RECT R;
	GetClientRect(m_hwnd, &R);
	m_size.x = R.right; m_size.y = R.bottom;
}

void BaseWindow::SetRect(long x, long y, long w, long h)
{
	m_position.x = x; m_position.y = y;

	SetWindowPos(m_hwnd, HWND_TOP, x, y, w, h, SWP_NOCOPYBITS);

	RECT R;
	GetClientRect(m_hwnd, &R);
	m_size.x = R.right; m_size.y = R.bottom;
}

#pragma endregion

//===============================================