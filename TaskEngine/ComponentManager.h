/**
 * ComponentManager
 *
 * Provides access to component lists and individual components
 */

#pragma once

//#include <map>
//#include <vector>
#include "Components.h"

class BaseComponent;

class ComponentManager
{
	friend class MemoryDelegate;

protected:

	std::map<int, BaseComponent*>	 m_ComponentMap;
	std::vector<BaseComponent*>		 m_ComponentList;

	std::vector<TransformComponent*> m_TransformComponents;
	std::vector<MeshComponent*>		 m_MeshComponents;
	std::vector<IRendererComponent*>		 m_RendererComponents;

	std::vector<IAIComponent*>		 m_AIComponents;
	std::vector<MovementComponent*>  m_MovementComponents;
	std::vector<IPhysicsComponent*>  m_PhysicsComponents;
	std::vector<BaseComponent*>	     m_ScriptComponents;

	std::vector<IGameplayComponent*>  m_GameplayComponents;


	std::vector<IColliderComponent*> m_ColliderComponents;
	std::vector<RigidBodyComponent*> m_RigidBodyComponents;

public:

	ComponentManager(void);
	~ComponentManager(void);

	/**
	 * CreateComponent
	 *
	 * Returns a blank component and registers it with the manager
	 *
	 * @param[in] - Component type
	 */
	BaseComponent* CreateComponent(Actor* owner, int type);

	/**
	 * GetComponent
	 *
	 * Returns a component with the requested id
	 *
	 * @param[in] - Id of the component
	 */
	BaseComponent* GetComponent(int id);


	//REMOVED TO FORCE ACCESS THROUGH MEMORY DELEGATE
	///**
	// * GetAllComponents
	// *
	// * Returns a list of all components in system
	// */
	//std::vector<BaseComponent*> GetAllComponents(void);

	///**
	// * GetTransforms
	// *
	// * Returns a list of all transform components
	// */
	//std::vector<TransformComponent*> GetTransforms(void);

	///**
	// * GetMeshes
	// *
	// * Returns a list of all mesh components
	// */
	//std::vector<MeshComponent*>		 GetMeshes(void);

	///**
	// * GetRenderers
	// *
	// * Returns a list of all renderers
	// */
	//std::vector<BaseComponent*>		 GetRenderers(void);

	///**
	// * GetAi
	// *
	// * Returns a list of all Ai components
	// */
	//std::vector<IAIComponent*>		 GetAi(void);

	///**
	// * GetPhysics
	// *
	// * Returns a list of all Physics components
	// */
	//std::vector<IPhysicsComponent*>  GetPhysics(void);

	///**
	// * GetScripts
	// *
	// * Returns a list of all script components
	// */
	//std::vector<BaseComponent*>		 GetScripts(void);

	///**
	// * GetGameplay
	// *
	// * Returns a list of all gameplay components
	// */
	//std::vector<GameplayComponent*>  GetGameplay(void);

private:

	/**
	 * Release all components
	 */
	void ClearComponents(void);
};
