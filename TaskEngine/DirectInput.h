#pragma once

#define DIRECTINPUT_VERSION 0x800

#include <dinput.h>

class DirectInput
{
private:

	//We need to store a handle to the window
	HWND m_hWindow;

	IDirectInput8*	m_DirectInput;

	IDirectInputDevice8*	m_Keyboard;
	char					m_KeyboardState[256];
	char					m_KeyboardLastState[256];
	char					m_KeyboardCurrentState[256];

	IDirectInputDevice8*	m_Mouse;
	DIMOUSESTATE2			m_MouseState;

	POINT					m_MousePos;

private:

	DirectInput(const DirectInput& rhs){}
	DirectInput& operator=(const DirectInput& rhs){}

public:

	DirectInput(HINSTANCE hAppInst, HWND hWnd, 
		DWORD keyboardCoopFlags, DWORD mouseCoopFlags);
	~DirectInput(void);

	void Poll(void);
	
	bool KeyDown(char key);
	bool KeyPressed(int key);
	
	bool MouseButtonDown(int button);

	float MouseDX(void);
	float MouseDY(void);
	float MouseDZ(void);

	POINT MousePosition(void);
};

extern DirectInput* gDInput;