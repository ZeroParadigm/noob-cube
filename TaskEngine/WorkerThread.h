#pragma once


class WorkerThread
{
private:
	
	std::thread							m_thread;
    std::condition_variable				m_queuePending;
    std::mutex							m_queueMutex;
	std::queue<std::function<void()>>	m_jobQueue;

    bool								m_wantExit;

private:

	void Entry();

	WorkerThread(const WorkerThread&){}
	WorkerThread& operator = (const WorkerThread&){}

public:

    WorkerThread();
    ~WorkerThread();

    void addJob(std::function<void()> job);
};





//
//  Example usage:
//

//#include <Windows.h>
//
//int main()
//{
//    {
//        WorkerThread t;
//        t.addJob( [] () { Sleep(2000); } );
//        t.addJob( [] () { std::cout << "printing 1." << std::endl; } );
//        t.addJob( [] () { std::cout << "printing 2." << std::endl; } );
//        t.addJob( [] () { Sleep(2000); } );
//        t.addJob( [] () { std::cout << "printing 3." << std::endl; } );
//        t.addJob( [] () { std::cout << "printing 4." << std::endl; } );
//
//        Sleep(3000);
//    } // <- thread exits here
//    return 0;
//}