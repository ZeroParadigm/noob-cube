#include "EngineCommonStd.h"

#include "GraphicsUtility.h"

//Global Defines for convenience
IDirect3DDevice9* g_d3dDevice = 0;
GraphicsSystem* d3dSystem = 0;
MemoryDelegate* g_Memory = 0;
ID3DXFont*		g_ExoRegular = 0;
ID3DXSprite*	g_d3dSprite = 0;
GUI*			g_GUI = 0;

//ID3DXMesh* g_BoxMesh = 0;

//Define Colors
//Color			g_White			( 1.0f, 1.0f, 1.0f, fOPAQUE );	
//Color			g_Black			( 0.0f, 0.0f, 0.0f, fOPAQUE );
//Color			g_Cyan			( 0.0f, 1.0f, 1.0f, fOPAQUE );	
//Color			g_Red			( 1.0f, 0.0f, 0.0f, fOPAQUE );
//Color			g_Green			( 0.0f, 1.0f, 0.0f, fOPAQUE );
//Color			g_Blue			( 0.0f, 0.0f, 1.0f, fOPAQUE );
//Color			g_Yellow		( 1.0f, 1.0f, 0.0f, fOPAQUE );	
//Color			g_Gray40		( 0.4f, 0.4f, 0.4f, fOPAQUE );
//Color			g_Gray25		( 0.25f, 0.25f, 0.25f, fOPAQUE );
//Color			g_Gray65		( 0.65f, 0.65f, 0.65f, fOPAQUE );
//Color			g_Transparent	( 1.0f, 0.0f, 1.0f, fTRANSPARENT );
//
////Define Geometry Defaults
//Vector3			g_Right			(1.0f, 0.0f, 0.0f);
//Vector3			g_Up			(0.0f, 1.0f, 0.0f);
//Vector3			g_Forward		(0.0f, 0.0f, 1.0f);
//
//Vector4			g_Up4			(g_Up.x, g_Up.y, g_Up.z, 0.0f);
//Vector4			g_Right4		(g_Right.x, g_Right.y, g_Right.z, 0.0f);
//Vector4			g_Forward4		(g_Forward.x, g_Forward.y, g_Forward.z, 0.0f);

//Define Transparancies
//const float		fOPAQUE			= 1.0f;
//const int		iOPAQUE			= 1; 
//const float		fTRANSPARENT	= 0.0f;
//const int		iTRANSPARENT	= 0;