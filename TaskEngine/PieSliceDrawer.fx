//=============================================================================
// PhongDirLtTex.fx by Frank Luna (C) 2004 All Rights Reserved.
//
// Phong directional light & texture.
//=============================================================================

struct Mtrl
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float4 emissive;
	float  specPower;
};

struct DirLight
{
	float4 ambient;
	float4 diffuse;
	float4 spec;
	float3 dirW;  
};

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;
uniform extern Mtrl     gMtrl;
uniform extern DirLight gLight;
uniform extern float3   gEyePosW;

uniform extern texture  gTex;
uniform extern texture	gSpec;

sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
    AddressV  = WRAP;
};

sampler SpecS = sampler_state
{
	Texture = <gSpec>;
	Filter   = MIN_MAG_MIP_LINEAR;
	AddressU  = WRAP;
    AddressV  = WRAP;
};
 
struct OutputVS
{
    float4 posH    : POSITION0;
    float3 normalW : TEXCOORD0;
    float3 toEyeW  : TEXCOORD1;
    float2 tex0    : TEXCOORD2;
};

OutputVS PhongDirLtTexVS(float3 posL : POSITION0, float3 normalL : NORMAL0, float2 tex0: TEXCOORD0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;
	
	// Transform normal to world space.
	outVS.normalW = mul(float4(normalL, 0.0f), gWorldInvTrans).xyz;
	
	// Transform vertex position to world space.
	float3 posW  = mul(float4(posL, 1.0f), gWorld).xyz;
	
	// Compute the unit vector from the vertex to the eye.
	outVS.toEyeW = gEyePosW - posW;
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	
	// Pass on texture coordinates to be interpolated in rasterization.
	outVS.tex0 = tex0;

	// Done--return the output.
    return outVS;
}

float4 PhongDirLtTexPS(float3 normalW : TEXCOORD0, float3 toEyeW  : TEXCOORD1, float2 tex0 : TEXCOORD2) : COLOR
{
	// Interpolated normals can become unnormal--so normalize.
	normalW = normalize(normalW);
	toEyeW  = normalize(toEyeW);
	
	// Light vector is opposite the direction of the light.
	float3 lightVecW = -gLight.dirW;
	
	// Compute the reflection vector.
	float3 r = reflect(-lightVecW, normalW);
	
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, toEyeW), 0.0f), gMtrl.specPower);
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(lightVecW, normalW), 0.0f);
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 spec = t * (gMtrl.spec*gLight.spec).rgb;

	float3 diffuse = s*(gMtrl.diffuse*gLight.diffuse).rgb;
	float3 ambient = gMtrl.ambient*gLight.ambient;
	
	// Get the texture color.
	float4 texColor = tex2D(TexS, tex0);
	//float4 specHighlight = tex2D(SpecS, tex0);
	
	// Combine the color from lighting with the texture color.
	float3 color = (ambient + diffuse)*texColor.rgb + (spec - tex2D(SpecS, tex0).rbg);
		
	// Sum all the terms together and copy over the diffuse alpha.
    return float4(color, gMtrl.diffuse.a*texColor.a);
}

technique PhongDirLtTexTech
{
    pass P0
    {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 PhongDirLtTexVS();
        pixelShader  = compile ps_2_0 PhongDirLtTexPS();
    }
}

/*
void main(void)
{
//-------------------------------------------------
// User values
//-------------------------------------------------

// Circle diameter.
float diameter = 400.0;

// Normalized slice sizes.
const int numSlices = 3;
float slices[numSlices];
slices[0] = 0.125;
slices[1] = 0.125;
slices[2] = 0.25;

// Slice colors.
vec3 colors[numSlices];
colors[0] = vec3(1,0,0);
colors[1] = vec3(0,1,0);
colors[2] = vec3(0,0,1);

//-------------------------------------------------
// Other code //
//-------------------------------------------------

// Define some convenient values.
vec2 diameterVector = vec2(diameter, diameter);
vec2 centerUV = vec2(0.5, 0.5);
vec2 uv = gl_FragCoord.xy / diameterVector;
vec2 pixelDirection = uv - centerUV;
float uvDistance = length(pixelDirection);
pixelDirection = normalize(pixelDirection);
float pi = 3.14159265359;
float pi2 = 2.0 * pi;
float radius = diameter / 2.0;

// If we're outside of the circle area, discard.
if(uvDistance > 0.5)
{
discard;
} // If


// Compute slice bisecting unit vectors.
float centerCounter = 0.0;
vec2 directions[numSlices];
for(int i = 0; i < numSlices; i++)
{
// Find the center normalized coordinate of the slice.
float sliceSize = slices[i];
float sliceCenter = centerCounter + (sliceSize / 2.0);

// Convert it into radians.
sliceCenter *= pi2;

// Find the bisecting unit vector of the slice.
vec2 centerDirection;
centerDirection.x = cos(sliceCenter);
centerDirection.y = sin(sliceCenter);
directions[i] = centerDirection;

// Increment the center counter.
centerCounter += sliceSize;
} // For

// Check which slice this pixel is in..
int chosenSlice = -1;
vec3 color = vec3(0,0,0);
for(int i = 0; i < numSlices; i++)
{
float slice = slices[i];
vec2 direction = directions[i];
float radianSlice = slice * pi;
float dotThreshold = cos(radianSlice);

float sliceDot = dot(direction, pixelDirection);
if(sliceDot >= dotThreshold)
{
color = colors[i];
} // If
} // For each slice..

gl_FragColor = vec4(color,1.0);
}
*/