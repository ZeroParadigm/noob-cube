#pragma once

void InitAllVertexDeclarations(LPDIRECT3DDEVICE9 gd3dDevice);
void DestroyAllVertexDeclarations();

struct VertexP
{
	VertexP() :pos(0.09f, 0.0f, 0.0f){}
	VertexP(float x, float y, float z) :pos(x, y, z){}
	VertexP(const D3DXVECTOR3& p) :pos(p){}

	D3DXVECTOR3 pos;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPN
{
	VertexPN() :pos(0.0f, 0.0f, 0.0f),
	normal(0.0f, 0.0f, 0.0f){}

	VertexPN(float x, float y, float z,
		float nx, float ny, float nz) :pos(x, y, z),
		normal(nx, ny, nz){}

	VertexPN(const D3DXVECTOR3& p,
		const D3DXVECTOR3& n) :pos(p),
		normal(n){}

	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPNT
{
	VertexPNT() :pos(0.0f, 0.0f, 0.0f),
	normal(0.0f, 0.0f, 0.0f),
	tex(0.0f, 0.0f){}

	VertexPNT(float x, float y, float z,
		float nx, float ny, float nz,
		float u, float v) :pos(x, y, z),
		normal(nx, ny, nz),
		tex(u, v){}

	VertexPNT(const D3DXVECTOR3& p,
		const D3DXVECTOR3& n,
		const D3DXVECTOR2& uv) :pos(p),
		normal(n),
		tex(uv){}

	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPNTC
{
	VertexPNTC() :pos(0.0f, 0.0f, 0.0f),
	normal(0.0f, 0.0f, 0.0f),
	tex(0.0f, 0.0f),
	color(0x00000000){}

	VertexPNTC(float x, float y, float z,
		float nx, float ny, float nz,
		float u, float v,
		D3DCOLOR c) :pos(x, y, z),
		normal(nx, ny, nz),
		tex(u, v),
		color(c){}

	VertexPNTC(const D3DXVECTOR3& p,
		const D3DXVECTOR3& n,
		const D3DXVECTOR2& uv,
		D3DCOLOR c) :pos(p),
		normal(n),
		tex(uv),
		color(c){}

	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex;
	D3DCOLOR	color;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexNormalMap
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 tangent;
	D3DXVECTOR3 binormal;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex0;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexSkyBox
{
	VertexSkyBox() :pos(0.0f, 0.0f, 0.0f),
	tex(0.0f, 0.0f, 0.0f){}

	VertexSkyBox(float x, float y, float z,
		float tx, float ty, float tz) :pos(x, y, z),
		tex(tx, ty, tz){}

	VertexSkyBox(const D3DXVECTOR3& p,
		const D3DXVECTOR3& t) :pos(p),
		tex(t){}

	D3DXVECTOR3 pos;
	D3DXVECTOR3 tex;

	static IDirect3DVertexDeclaration9* Decl;
};

struct VertexPP
{
	VertexPP()
	:pos(0.0f, 0.0f, 0.0f, 0.0f),
	texUV0(0.0f, 0.0f),
	texUV1(0.0f, 0.0f){}

	VertexPP(
		float x, float y, float z, float rhw,
		float tu0, float tv0,
		float tu1, float tv1)
		:pos(x, y, z, rhw),
		texUV0(tu0, tv0),
		texUV1(tu1, tv1){}

	VertexPP(
		const D3DXVECTOR4& p,
		const D3DXVECTOR2& tuv0,
		const D3DXVECTOR2& tuv1) 
		:pos(p),
		texUV0(tuv0),
		texUV1(tuv1){}

	D3DXVECTOR4 pos;
	D3DXVECTOR2 texUV0;	// Post-Process source
	D3DXVECTOR2 texUV1;	// Original scene

	static IDirect3DVertexDeclaration9* Decl;
};

//struct Particle
//{
//	D3DXVECTOR3 initialPos;
//	D3DXVECTOR3 initialVelocity;
//	float       initialSize; // In pixels.
//	float       initialTime;
//	float       lifeTime;
//	float       mass;
//	D3DCOLOR    initialColor;
//
//	static IDirect3DVertexDeclaration9* Decl;
//};