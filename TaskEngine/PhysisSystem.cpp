#include "EngineCommonStd.h"

#include "PhysicsSystem.h"
#include "ContactGenerator.h"

//#include "MemoryDelegate.h"

//==== Private System Functions ================================
//void PhysicsSystem::broadPhase()
//{
//
//}
//
//void PhysicsSystem::narrowPhase()
//{
//
//}
//==============================================================



//==== Protected System Functions ==============================
void PhysicsSystem::init(MemoryDelegate* memory)
{
	m_memory = memory;

	m_Generator = new ContactGenerator();
	m_Generator->Init();
}

void PhysicsSystem::shutdown()
{
	delete m_Generator;
}

void PhysicsSystem::update(float dt)
{
	m_Generator->SetColliders(m_memory->GetColliders());
	m_Generator->Generate(dt);
}
//==============================================================