#pragma once 

#include "IRendererComponent.h"
#include "Actor.h"
#include "Frustum.h"

using namespace DXMathLibrary;


/**
 * CameraComponent
 *
 * Provides perspective information for the camera
 */
class CameraComponent : public IRendererComponent
{
protected:

	Frustum		m_frustum;

	Matrix4		m_View;
	Matrix4		m_Proj;
	Matrix4		m_ViewProj;

	Vector3*	m_Position;
	Vector3		m_Forward;
	Vector3		m_Right;
	Vector3		m_Up;

	float		m_Fov;
	float		m_NearZ;
	float		m_FarZ;

	bool		m_dirty;

	virtual void BuildView();

	CameraComponent(void){}

public:

	CameraComponent(Actor* owner);
	~CameraComponent(void);

	void OnResetDevice(float w, float h);

	void SetLens(float FOV, float aspect, float nearZ, float farZ);

	virtual Matrix4 GetWorldMatrix();

	/**
	 * Update
	 *
	 * @param[in] float Time elapsed since last update
	 */
	void Update(float dt) override;

	/**
	 * GetPosition
	 * 
	 * Utility to get the camera position
	 */
	Vector3 GetPosition(void);

	/**
	 * GetForward
	 *
	 * Return the forward
	 */
	Vector3 GetForward(void);

	/**
	 * GetUp
	 *
	 * Utility to get the up vector
	 */
	Vector3 GetUp(void);

	/**
	 * GetRight
	 *
	 * Return the right
	 */
	Vector3 GetRight(void);

	Matrix4 Projection();
	Matrix4 View();
	Matrix4 ViewProjection();

	void LookAt(Vector3 target);

	/**
	 * Set the camera position and orientation
	 *
	 * @param[in] Vector3& Position of the camera
	 * @param[in] Vector3& Target for the camera
	 * @param[in] Vector3& Up vector of the camera
	 */
	void LookAt(Vector3 pos, Vector3 target, Vector3 up);

};