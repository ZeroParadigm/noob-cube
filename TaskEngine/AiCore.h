/**
 * AICore - System for managing autonomous objects
 *
 * Author - Jesse Dillon
 */

#pragma once


using namespace DXMathLibrary;

class MovementComponent;

class AiCore
{
	//System memory
	MemoryDelegate*			m_Memory;

public:

	AiCore(void);
	~AiCore(void);

	/**
	 * Initialize
	 *
	 * @memory Pointer to the system memory
	 * Initialize the AiCore
	 */
	void Initialize(MemoryDelegate* memory);

	/**
	 * Shutdown
	 *
	 * Shut down the AiCore
	 */
	void Shutdown(void);

	/**
	 * Update
	 *
	 * Update all AI components
	 *
	 * @dt Time elapsed
	 */
	void Update(float dt);

private:

	/**
	 * Local member for time elapsed
	 */
	float m_TimeElapsed;

	/**
	 * Local copy of the current mover
	 */
	MovementComponent* m_Agent;

	/**
	 * Calculate movement
	 *
	 * Deprecated - Use both LinearUpdate and AngularUpdate
	 *
	 * @dt Time elapsed
	 * @agent Movement component to be updated
	 */
	Vector3 CalculateMovement(float dt);

	/**
	 * Linear update
	 *
	 * Performs updates on linear velocity
	 *
	 * @agent Movement component to be updated
	 */
	Vector3 LinearUpdate(float dt);

	/**
	 * Angular update
	 *
	 * Performs updates on angular velocity
	 *
	 * @agent Movement component to be updated
	 */
	Vector3 AngularUpdate(float dt);

	/**
	 * Seek
	 *
	 * Performs the seek algorithm
	 *
	 * @param[in] Vector3 Seek target
	 */
	Vector3 Seek(Vector3 target);

	/**
	 * Flee
	 *
	 * Performs the flee algorithm
	 *
	 * @param[in] Vector3 Flee target
	 */
	Vector3 Flee(Vector3 target);

	/**
	 * Arrive
	 *
	 * Performs the arrive algorithm
	 *
	 * @param[in] Vector3 Arrive target
	 */
	Vector3 Arrive(Vector3 target);

	/**
	 * Pursue
	 *
	 * Performs the pursue algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Pursue(void);

	/**
	 * Evade
	 *
	 * Performs the evade algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Evade(void);

	/**
	 * Wander
	 *
	 * Performs the wander algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Wander(void);

	/**
	 * ObstacleAvoidance
	 *
	 * Performs obstacle avoidance algorithms
	 *
	 * @agent Movement component
	 */
	Vector3 ObstaceAvoidance(void);

	/**
	 * WallAvoidance
	 *
	 * Performs wall avoidance algorithms
	 *
	 * @agent Movement component
	 */
	Vector3 WallAvoidance(void);

	/**
	 * Interpose
	 *
	 * Performs the interpose algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Interpose(const Vector3& a, const Vector3& b);

	/**
	 * Interpose Point
	 *
	 * Performs an interposition of a point between this object and the origin
	 *
	 * Used for camera positioning
	 * @param Vector3 Target point
	 */
	Vector3 InterposePoint(void);

	/**
	 * Hide
	 *
	 * Performs the hide algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Hide(void);

	/**
	 * Separation
	 *
	 * Performs the separation algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Separation(void);

	/**
	 * Cohesion
	 *
	 * Perfroms the cohesion algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Cohesion(void);

	/**
	 * Alignment
	 *
	 * Performs the alignment algorithm
	 *
	 * @agent Movement component
	 */
	Vector3 Alignment(void);

	/**
	 * FollowPath
	 *
	 * Performs A* heuristic on path
	 *
	 * @agent Movement component
	 */
	Vector3 FollowPath(Vector3 target);

	/**
	 * Align
	 * 
	 * Performs 3D rotational alignment
	 *
	 * @param[in] Target orientation
	 */
	Vector3 Align(Quaternion target);

	/**
	 * CalculateOrientation
	 *
	 * Calculates the orientation change from a vector
	 */
	Quaternion CalculateOrientation(Vector3 vector);

	/**
	 * FacePoint
	 *
	 * Performs alignment to point
	 */
	Vector3 FacePoint(void);

	/**
	 * CamFacePoint
	 *
	 * Specialized alignment for camera
	 */
	Quaternion CamFacePoint(void);

	/**
	 * FaceForward
	 *
	 * Performs alignment to velocity
	 */
	Vector3 FaceForward(void);

	Quaternion PlayerFaceForward(void);

	/*
	 * AlignVector
	 *
	 * Aligns the object to a direction
	 */
	Quaternion AlignVector(void);

	/**
	* AccumulateForce
	*
	* Accumulates forces until a max force has been met
	*
	* @agent Movement component
	* @running Running total of accumulated force
	* @forceToAdd Force requested for addition
	*/
	bool AccumulateForce(Vector3& running, Vector3 forceToAdd);
};