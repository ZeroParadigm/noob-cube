#include "EngineCommonStd.h"

#include "ActorManager.h"
#include "Actor.h"
#include "BulletSystem.h"

ActorManager::ActorManager(void)
{
	m_Camera = nullptr;
	m_Player = nullptr;
	m_GameWorld = nullptr;
	m_Gun = nullptr;
}

ActorManager::~ActorManager(void)
{
	ClearActors();
}

Actor* ActorManager::CreateActor()
{
	Actor* newActor = new Actor();

	m_ActorList.push_back(newActor);
	m_ActorMap.insert(std::make_pair(newActor->Id(), newActor));

	return newActor;
}

std::vector<Actor*> ActorManager::GetActorList()
{
	return m_ActorList;
}

Actor* ActorManager::GetActor(int id)
{
	return m_ActorMap.find(id)->second;
}

void ActorManager::ClearActors()
{
	for(auto it = m_ActorList.begin(); it != m_ActorList.end(); ++it)
	{
		delete *it;
	}

	m_ActorList.clear();
	m_ActorMap.clear();

	//Gameplay specific
	if (m_Gun)
	{
		delete m_Gun;
		m_Gun = nullptr;
	}
}

Actor* ActorManager::GetCamera(void)
{
	return m_Camera;
}

Actor* ActorManager::GetPlayer(void)
{
	return m_Player;
}

Actor* ActorManager::GetWorld(void)
{
	return m_GameWorld;
}

BulletSystem* ActorManager::GetGun(void)
{
	return m_Gun;
}

void ActorManager::SetCamera(Actor* a)
{
	m_Camera = a;
}

void ActorManager::SetPlayer(Actor* a)
{
	m_Player = a;
}

void ActorManager::SetWorld(Actor* a)
{
	m_GameWorld	= a;
}

void ActorManager::SetGun(BulletSystem* gun)
{
	m_Gun = gun;
}