#include "EngineCommonStd.h"

#include "BaseGUIElement.h"
#include "Observable.h"

using namespace std;
using namespace DXMathLibrary;


BaseGUIElement::BaseGUIElement()
{
	m_Visible = true;

	SetID();
	m_Name = L"BaseGUIElement";

	m_Parent = 0;

	m_Observable = new Observable();
	m_Observable->Attach(EventObs);
}

BaseGUIElement::BaseGUIElement(BaseGUIElement* parent) : m_Parent(parent)
{
	m_Visible = true;

	SetID();
	m_Name = L"BaseGUIElement";

	m_Parent = 0;

	m_Observable = new Observable();
	m_Observable->Attach(EventObs);
}

BaseGUIElement::~BaseGUIElement()
{
	if (m_Observable)
	{
		delete m_Observable;
		m_Observable = nullptr;
	}

	m_Children.clear();
}

void BaseGUIElement::SetID()
{
	static unsigned int ID = 0;
	m_ID = ID;
	ID++;
}

void BaseGUIElement::SetVisibility(bool visible)
{
	m_Visible = visible;
}

void BaseGUIElement::SetName(wstring name)
{
	m_Name = name;
}

void BaseGUIElement::SetPosition(Vector2 position)
{
	m_Position = position;
}

void BaseGUIElement::SetWidth(unsigned int width)
{
	m_Width = width;
}

void BaseGUIElement::SetHeight(unsigned int height)
{
	m_Height = height;
}

void BaseGUIElement::SetParent(BaseGUIElement* parent)
{
	m_Parent = parent;
}

void BaseGUIElement::LeaveParent()
{
	if (m_Parent)
	{
		m_Parent->OrphanChild(this);
	}
}

void BaseGUIElement::AdoptChild(BaseGUIElement* child)
{
	child->SetParent(this);
	m_Children.insert(pair<wstring, BaseGUIElement*>(child->GetName(), child));
}

void BaseGUIElement::OrphanChild(BaseGUIElement* child)
{
	map<wstring, BaseGUIElement*>::iterator it;

	it = m_Children.find(child->GetName());

	if (it != m_Children.end())
	{
		it->second->m_Parent = 0;
		m_Children.erase(it);
	}
}

void BaseGUIElement::OrphanChildren()
{
	map<wstring, BaseGUIElement*>::iterator it;

	for (it = m_Children.begin(); it != m_Children.end(); it++)
	{
		it->second->m_Parent = 0;
	}

	m_Children.clear();
}

bool BaseGUIElement::IsVisible()
{
	if (HasParent())
	{
		if (m_Parent->IsVisible())
		{
			return m_Visible;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return m_Visible;
	}
}

bool BaseGUIElement::HasParent()
{
	if (m_Parent == 0 || m_Parent == nullptr)
	{
		return false;
	}
	else
	{
		return true;
	}
}

unsigned int BaseGUIElement::GetID()
{
	return m_ID;
}

wstring BaseGUIElement::GetName()
{
	return m_Name;
}

Vector2 BaseGUIElement::GetPosition()
{
	return m_Position;
}

unsigned int BaseGUIElement::GetWidth()
{
	return m_Width;
}

unsigned int BaseGUIElement::GetHeight()
{
	return m_Height;
}

Matrix BaseGUIElement::GetMatrix()
{
	Vector2 screenPosition = GetScreenPosition();
	Matrix elementMatrix;

	D3DXMatrixTransformation2D(&elementMatrix, NULL, 0.0f, NULL, NULL, 0.0f, &screenPosition);

	return elementMatrix;
}

RECT BaseGUIElement::GetScreenArea()
{
	RECT rectangle;
	Vector2 screenPosition = GetScreenPosition();

	rectangle.top = (LONG)screenPosition.y;
	rectangle.left = (LONG)screenPosition.x;
	rectangle.bottom = rectangle.top + m_Height;
	rectangle.right = rectangle.left + m_Width;

	return rectangle;
}

Vector2 BaseGUIElement::GetScreenPosition()
{
	Vector2 position;

	if (HasParent())
	{
		position = m_Parent->GetScreenPosition();
	}

	return position + m_Position;
}

BaseGUIElement* BaseGUIElement::GetParent()
{
	return m_Parent;
}

BaseGUIElement* BaseGUIElement::GetChild(wstring childName)
{
	map<wstring, BaseGUIElement*>::iterator it;

	it = m_Children.find(childName);

	if (it == m_Children.end())
	{
		return 0;
	}
	else
	{
		return it->second;
	}
}