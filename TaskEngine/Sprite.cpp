#include "EngineCommonStd.h"

#include "Sprite.h"


Sprite::Sprite()
{
}

Sprite::Sprite(std::wstring fileName, unsigned int width, unsigned int height)
	: Texture(fileName), m_Width(width), m_Height(height)
{
}

Sprite::~Sprite()
{
}

void Sprite::Load(IDirect3DDevice9* device)
{
	D3DXCreateTextureFromFileEx(device, m_FileName.c_str(), m_Width, m_Height,
		8, 0, D3DFMT_A16B16G16R16, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT,
		0, NULL, NULL, &m_Texture);
}

unsigned int Sprite::GetWidth()
{
	return m_Width;
}

unsigned int Sprite::GetHeight()
{
	return m_Height;
}