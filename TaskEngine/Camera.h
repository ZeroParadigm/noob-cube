#pragma once


enum CameraMode
{
	CONTROLLED, //Position controlled by external system
	FIXEDPOINT,	//Fix camera to a point
	DEBUG,		//Debug Camera
	FREEFORM,	//Mesh controlled Camera
};

class Camera {
public:

	DXMathLibrary::Matrix4	m_View;
	DXMathLibrary::Matrix4	m_Proj;
	DXMathLibrary::Matrix4	m_ViewProj;

	DXMathLibrary::Vector3	m_Position;
	DXMathLibrary::Vector3	m_Right;
	DXMathLibrary::Vector3	m_Up;
	DXMathLibrary::Vector3	m_Forward;
	DXMathLibrary::Vector3  m_FixedLookPoint;

	float					m_Fov;
	float					m_NearZ;
	float					m_FarZ;

	float					m_fixedPointTheta;
	float					m_fixedPointPhi;
	float					m_fixedPointDistance;

	float					m_Speed;

	CameraMode				m_CameraMode;

private:

	//Update Techniques
	void AdjustFixedCamera(float dt);
	void RotateFixedCamera(float dt);

	void AdjustDebugCamera(float dt);
	void RotateDebugCamera(float dt);

	//View Builders
	void BuildView();
	void BuildFixedView();

public:

	Camera();
	~Camera();

	void Update(float dt);
	void OnResetDevice(float w, float h);
	
	//Setters
	void SetLens(float FOV, float aspect, float nearZ, float farZ);
	void SetLookAt(DXMathLibrary::Vector3 &pos, DXMathLibrary::Vector3 &target, DXMathLibrary::Vector3 &up);
	void SetLookAtPoint(DXMathLibrary::Vector3 snapPoint);
	void SetMode(CameraMode mode);

	void ApplyInput(DXMathLibrary::Vector3, float pitch, float yaw, float speed);

	DXMathLibrary::Vector3& Position();

	DXMathLibrary::Matrix4 ViewProj();
};