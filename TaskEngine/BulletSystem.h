#pragma once

//#include <vector>
//#include "MathLibrary.h"

class ActorFactory;
class BulletComponent;


class BulletSystem
{
public:

	std::vector<BulletComponent*> m_Bullets;

public:

	BulletSystem();
	~BulletSystem();

	void Initialize(ActorFactory* factory);
	void Shutdown();

	BulletComponent* GetBullet();

	void ShootBullet(DXMathLibrary::Vector3 startPos, DXMathLibrary::Vector3 normal, float speed);

};