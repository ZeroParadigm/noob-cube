#include "EngineCommonStd.h"



namespace Utility
{
	GameTimer::GameTimer(void)
		: m_SecondsPerCount(0.0), m_DeltaTime(-1.0), m_BaseTime(0),
		  m_PausedTime(0), m_PrevTime(0), m_CurrTime(0), m_Stopped(false)
	{
		__int64 countsPerSec;
		QueryPerformanceCounter((LARGE_INTEGER*)&countsPerSec);
		m_SecondsPerCount = 1.0 / (double)countsPerSec;
	}

	GameTimer::~GameTimer(void)
	{
	}

	float GameTimer::GameTime(void) const
	{
		if (m_Stopped)
		{
			return (float)(((m_StopTime - m_PausedTime) - m_BaseTime) * m_SecondsPerCount);
		}

		else 
		{
			return (float)(((m_CurrTime - m_PausedTime) - m_BaseTime) * m_SecondsPerCount);
		}
	}

	float GameTimer::DeltaTime(void) const
	{
		return (float)m_DeltaTime;
	}

	void GameTimer::Reset(void)
	{
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

		m_BaseTime = currTime;
		m_PrevTime = currTime;
		m_StopTime = 0;
		m_Stopped = false;
	}

	void GameTimer::Start(void)
	{
		__int64 startTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

		if (m_Stopped)
		{
			m_PausedTime += (startTime - m_StopTime);

			m_PrevTime = startTime;
			m_StopTime = 0;
			m_Stopped = false;
		}
	}

	void GameTimer::Stop(void)
	{
		if (!m_Stopped)
		{
			__int64 currTime;
			QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

			m_StopTime = currTime;
			m_Stopped = true;
		}
	}

	void GameTimer::Tick(void)
	{
		if (m_Stopped)
		{
			m_DeltaTime = 0.0f;
			return;
		}

		//Get the time this frame
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
		m_CurrTime = currTime;

		m_DeltaTime = (m_CurrTime - m_PrevTime) * m_SecondsPerCount;

		m_PrevTime = m_CurrTime;

		if (m_DeltaTime < 0.0)
		{
			m_DeltaTime = 0.0;
		}
	}


	/**
	 * Deprecated clock
	 */

	Clock::Clock(void)
	{
		ResetClock();
	}

	Clock::~Clock(void)
	{
	}

	void Clock::Update(float dt)
	{
		m_TimeElapsed += dt;
	}

	void Clock::ResetClock(void)
	{
		m_TimeElapsed = 0.0f;
	}

	const float Clock::GetTime(void) const
	{
		return m_TimeElapsed;
	}


}