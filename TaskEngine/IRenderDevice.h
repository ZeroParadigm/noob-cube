#pragma once


class IRenderDevice
{
friend class RenderWindow;
private:

private:

	virtual bool IsContextStable(RenderWindow* targetWindow) = 0;

	virtual LRESULT WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) = 0;

protected:

	enum DeviceDrawPhase
	{
		DEVICE_IDLE = 0,
		DEVICE_CULLING,
		DEVICE_PRERENDERING,
		DEVICE_RENDERING,
		DEVICE_POSTRENDERING,
		DEVICE_PHASE_COUNT,
	};

	RenderWindow*	m_window;
	bool			m_active;

protected:
	
	virtual bool Render() = 0;

public:

	virtual HRESULT Init(MemoryDelegate* memory, RenderWindow* window, float desiredDrawRate, bool calculateStats) = 0;
	virtual void Shutdown() = 0;

	virtual void Update(double dt) = 0;

	virtual std::wstring DeviceName() = 0;
	virtual void SetBackgroundColor(BYTE r, BYTE g, BYTE b, BYTE a) = 0;

	//This generic line drawer allows for any outside algorithm to draw
	//	a wireframe to the screen if needed.
	//virtual void DrawLine(const Vec3& from, const Vec3& to, const Color& color) = 0;

	//Might use something similar. Need to implement glow first
	//virtual void CalcLighting(Lights *lights, int maximumLights) = 0;

	//I think these are deprecated functions that have to do with how
	//	things used to be drawn using the FVF pipeline. The transform
	//	component held with each actor helps define this for each object
	////virtual void VSetWorldTransform(const Mat4x4 *m) = 0;
	////virtual void VSetViewTransform(const Mat4x4 *m) = 0;
	////virtual void VSetProjectionTransform(const Mat4x4 *m) = 0;
};