#include "EngineCommonStd.h"

#include "WorldPolygon.h"
#include "GraphicsUtility.h"
#include "MeshComponent.h"

WorldPolygon::WorldPolygon()
{
	m_NumWalls = 0;
	m_HasWalls = false;

	m_ScaleX = 100.0f;
	m_ScaleY = 100.0f;
	m_ScaleZ = 100.0f;
}

WorldPolygon::~WorldPolygon() 
{
	DestroyWalls();
	ReleaseBuffers();
}

void WorldPolygon::CreatePlaneBuffer(MeshComponent* mesh)
{
	ReleaseBuffers();
	mesh->GetMesh()->UseMeshObj(false);

	IDirect3DVertexBuffer9** vertBuffer = &mesh->GetMesh()->m_VertexBuffer;
	IDirect3DIndexBuffer9** indexBuffer = &mesh->GetMesh()->m_IndexBuffer;

	// Create Vertex Buffer
	g_d3dDevice->CreateVertexBuffer(4 * sizeof(VertexPNT), D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, vertBuffer , 0);
	mesh->GetMesh()->m_NumVertices = 4;

	VertexPNT* v = 0;
	(*vertBuffer)->Lock(0, 0, (void**)&v, 0);

	v[0] = VertexPNT(-0.5f * m_ScaleX, 0.5f * m_ScaleY, -0.5f * m_ScaleZ,		0,0,-1,		0,0);
	v[1] = VertexPNT(0.5f * m_ScaleX, 0.5f * m_ScaleY, -0.5f * m_ScaleZ, 0, 0, -1, 1, 0);
	v[2] = VertexPNT(-0.5f * m_ScaleX, -0.5f * m_ScaleY, -0.5f * m_ScaleZ, 0, 0, -1, 0, 1);
	v[3] = VertexPNT(0.5f * m_ScaleX, -0.5f * m_ScaleY, -0.5f * m_ScaleZ, 0, 0, -1, 1, 1);

	(*vertBuffer)->Unlock();

	// Create Index Buffer
	g_d3dDevice->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, indexBuffer, 0);
	mesh->GetMesh()->m_NumTriangles = 2;

	WORD* i = 0;
	(*indexBuffer)->Lock(0, 0, (void**)&i, 0);

	// Upper Tri
	i[0] = 0;
	i[1] = 1;
	i[2] = 2;

	// Lower Tri
	i[3] = 1;
	i[4] = 3;
	i[5] = 2;

	(*indexBuffer)->Unlock();

	WorldPlane* face = new WorldPlane(Vector3(0.0f,0.0f,-1.0f), 0);

	//Points for wall generation
	Vector3 p0, p1, p2, p3;
	p0 = Vector3(-0.5f, 0.5f, 0);
	p1 = Vector3(0.5f, 0.5f, 0);
	p2 = Vector3(-0.5f, -0.5f, 0);
	p3 = Vector3(0.5f, -0.5f, 0);

	face->AddPoint(p0);
	face->AddPoint(p1);
	face->AddPoint(p2);
	face->AddPoint(p3);

	CreateWalls(p0, p1, p2, p3, m_ScaleX);

	m_Faces.push_back(face);
}

void WorldPolygon::CreatePyramidBuffer(MeshComponent* mesh)
{
	DestroyWalls();
	ReleaseBuffers();
	mesh->GetMesh()->Release();
}

void WorldPolygon::CreateCubeBuffer(MeshComponent* mesh)
{
	DestroyWalls();
	ReleaseBuffers();
	mesh->GetMesh()->UseMeshObj(false);

	IDirect3DVertexBuffer9** vertBuffer = &mesh->GetMesh()->m_VertexBuffer;
	IDirect3DIndexBuffer9** indexBuffer = &mesh->GetMesh()->m_IndexBuffer;

	// Create Vertex Buffer
	g_d3dDevice->CreateVertexBuffer(24 * sizeof(VertexPNT), D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, vertBuffer, 0);
	mesh->GetMesh()->m_NumVertices = 24;

	VertexPNT* v = 0;
	(*vertBuffer)->Lock(0, 0, (void**)&v, 0);

	// X +
	v[0] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		1,0,0,		0,0);
	v[1] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		1,0,0,		1,0);
	v[2] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,		1,0,0,		0,1);
	v[3] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		1,0,0,		1,1);

	// X -
	v[4] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		-1,0,0,		0,0);
	v[5] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		-1,0,0,		1,0);
	v[6] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		-1,0,0,		0,1);
	v[7] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,		-1,0,0,		1,1);

	// Y +
	v[8] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,1,0,		0,0);
	v[9] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,1,0,		1,0);
	v[10] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,1,0,		0,1);
	v[11] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,1,0,		1,1);

	// Y -
	v[12] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,	0,-1,0,		0,0);
	v[13] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,-1,0,		1,0);
	v[14] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,-1,0,		0,1);
	v[15] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,-1,0,		1,1);

	// Z +
	v[16] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,0,1,		0,0);
	v[17] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,0,1,		1,0);
	v[18] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,0,1,		0,1);
	v[19] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, 0.5f *m_ScaleZ,		0,0,1,		1,1);

	// Z -
	v[20] = VertexPNT(-0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,0,-1,		0,0);
	v[21] = VertexPNT(0.5f *m_ScaleX, 0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,0,-1,		1,0);
	v[22] = VertexPNT(-0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,	0,0,-1,		0,1);
	v[23] = VertexPNT(0.5f *m_ScaleX, -0.5f *m_ScaleY, -0.5f *m_ScaleZ,		0,0,-1,		1,1);

	(*vertBuffer)->Unlock();

	// Create Index Buffer
	g_d3dDevice->CreateIndexBuffer(36 * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, indexBuffer, 0);
	mesh->GetMesh()->m_NumTriangles = 12;

	WORD* i = 0;
	(*indexBuffer)->Lock(0, 0, (void**)&i, 0);

	// X + Upper
	i[0] = 0;
	i[1] = 1;
	i[2] = 2;

	// X + Lower
	i[3] = 1;
	i[4] = 3;
	i[5] = 2;

	// X - Upper
	i[6] = 4;
	i[7] = 5;
	i[8] = 6;

	// X - Lower
	i[9] = 5;
	i[10] = 7;
	i[11] = 6;

	// Y + Upper
	i[12] = 8;
	i[13] = 9;
	i[14] = 10;

	// Y + Lower
	i[15] = 9;
	i[16] = 11;
	i[17] = 10;

	// Y - Upper
	i[18] = 12;
	i[19] = 13;
	i[20] = 14;

	// Y - Lower
	i[21] = 13;
	i[22] = 15;
	i[23] = 14;

	// Z + Upper
	i[24] = 16;
	i[25] = 17;
	i[26] = 18;

	// Z + Lower
	i[27] = 17;
	i[28] = 19;
	i[29] = 18;

	// Z - Upper
	i[30] = 20;
	i[31] = 21;
	i[32] = 22;

	// Z - Lower
	i[33] = 21;
	i[34] = 23;
	i[35] = 22;

	(*indexBuffer)->Unlock();

	float X = 0.5f * m_ScaleX;
	float Y = 0.5f * m_ScaleY;
	float Z = 0.5f * m_ScaleZ;

	//=========
	// m_Face[0] = +X plane
	// m_Face[1] = -X plane
	// m_Face[2] = +Y plane
	// m_Face[3] = -Y plane
	// m_Face[4] = +Z plane
	// m_Face[5] = -Z plane
	//=========

	WorldPlane* face = new WorldPlane(Vector3(1.0f, 0.0f, 0.0f), X);
	m_Faces.push_back(face);

	face = new WorldPlane(Vec3(-1.0f, 0.0f, 0.0f), -X);
	m_Faces.push_back(face);

	face = new WorldPlane(Vec3(0.0f, 1.0f, 0.0f), Y);
	m_Faces.push_back(face);

	face = new WorldPlane(Vec3(0.0f, -1.0f, 0.0f), -Y);
	m_Faces.push_back(face);

	face = new WorldPlane(Vec3(0.0f, 0.0f, 1.0f), Z);
	m_Faces.push_back(face);

	face = new WorldPlane(Vec3(0.0f, 0.0f, -1.0f), -Z);
	m_Faces.push_back(face);

	face->AddPoint(Vector3(-X, Y, -Z));
	face->AddPoint(Vector3(X, Y, -Z));
	face->AddPoint(Vector3(-X, -Y, -Z));
	face->AddPoint(Vector3(X, -Y, -Z));
}

void WorldPolygon::ReleaseBuffers()
{
	for(WorldPlane* face : m_Faces)
	{
		WorldPlane* temp = face;
		delete temp;
		face = nullptr;
	}
	m_Faces.clear();
}

void WorldPolygon::CreateWalls(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3, float scale)
{
	//Adding scale into the equation	(Note: This is a hack to incorporate scale - JD)
	Vector3 p0 = v0 * scale;
	Vector3 p1 = v1 * scale;
	Vector3 p2 = v2 * scale;
	Vector3 p3 = v3 * scale;

	//*******************************
	//Create the walls for ai to use (Note: Can be optimized - JD)
	//*******************************
	Plane upper, lower, left, right;

	//Get reference points
	Vector3 ref0, ref1, tempA, tempB;
	tempA = p2 - p3;
	tempB = p1 - p3;
	ref0 = tempA.Cross(tempB) + p3;

	tempA = p1 - p0;
	tempB = p2 - p0;
	ref1 = tempA.Cross(tempB) + p0;

	//Create planes
	m_Walls.push_back(new Plane(p2, ref0, p3));	//Upper
	m_Walls.push_back(new Plane(p1, ref1, p0));	//Lower
	m_Walls.push_back(new Plane(p0, ref1, p2));	//Left
	m_Walls.push_back(new Plane(p3, ref0, p1));	//Right

	//Set wall values
	m_NumWalls = 4;
	m_HasWalls = true;
}

void WorldPolygon::DestroyWalls(void)
{
	m_HasWalls = false;
	m_NumWalls = 0;

	for (auto& wall : m_Walls)
	{
		Plane* temp = wall;
		wall = nullptr;
		delete temp;
	}

	m_Walls.clear();
}

bool WorldPolygon::Walls(std::vector<Plane*>* walls, int& numWalls)
{
	if (!m_HasWalls) return false;

	walls = &m_Walls;
	numWalls = m_NumWalls;

	return true;
}

std::vector<Plane*>* WorldPolygon::GetWalls(void)
{
	return &m_Walls;
}