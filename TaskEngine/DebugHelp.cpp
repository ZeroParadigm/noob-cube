#include "EngineCommonStd.h"
#include "DebugHelp.h"

//#include <sstream>
//#include <iomanip>
//#include <stdarg.h> 
//#include <stdio.h>
//
//#include <DbgHelp.h>
//
//#pragma comment (lib, "DbgHelp")

using std::wstring;

#pragma region Globals and Statics

#if defined(DEBUG_VERBOSE)
#pragma message("Debug will print to Output and File")
	const unsigned char ERRORFLAG_DEFAULT		= (LOGFLAG_WRITE_TO_OUTPUT | LOGFLAG_WRITE_TO_FILE);
	const unsigned char WARNINGFLAG_DEFAULT		= (LOGFLAG_WRITE_TO_OUTPUT | LOGFLAG_WRITE_TO_FILE);
	const unsigned char LOGFLAG_DEFAULT			= (LOGFLAG_WRITE_TO_OUTPUT | LOGFLAG_WRITE_TO_FILE);
#elif defined(_DEBUG)
#pragma message("Debug will print to Output")
	const unsigned char ERRORFLAG_DEFAULT = LOGFLAG_WRITE_TO_OUTPUT;
	const unsigned char WARNINGFLAG_DEFAULT = LOGFLAG_WRITE_TO_OUTPUT;
	const unsigned char LOGFLAG_DEFAULT = LOGFLAG_WRITE_TO_OUTPUT;
#elif defined(NDEBUG)
#pragma message("Debug will print to File")
	const unsigned char ERRORFLAG_DEFAULT		= LOGFLAG_WRITE_TO_FILE;
	const unsigned char WARNINGFLAG_DEFAULT		= LOGFLAG_WRITE_TO_FILE;
	const unsigned char LOGFLAG_DEFAULT			= LOGFLAG_WRITE_TO_FILE;
#else
#pragma message("Debug logging is disabled")
	const unsigned char ERRORFLAG_DEFAULT		= 0;
	const unsigned char WARNINGFLAG_DEFAULT		= 0;
	const unsigned char LOGFLAG_DEFAULT			= 0;
#endif

#define DbgMngr DebugManager::Instance()

#pragma endregion

#pragma region DebugManager class Declaration

class DebugManager
{
private:

	DebugManager()
	{
		ERRORLOG_FILENAME = L"engineFilure.log";
		Startup();
	}

private:

	std::wstring ERRORLOG_FILENAME;

	typedef std::map<std::wstring, unsigned char> Tags;
	typedef std::list<DebugHelp::DebugMessage*> DebugMessageList;

	Tags m_tags;
	DebugMessageList m_debugMessengers;

	CriticalSection m_tagCriticalSection;
	CriticalSection m_messengerCriticalSection;

private:

	void OutputBuffersToLogs(wstring finalErrorBuffer, unsigned char flags);
	void OutputToFile(wstring output);
	void GetOutputBuffer(wstring& outBuffer, wstring tag, wstring message, const TCHAR* funcname, const TCHAR* filename, int line);

public:

	enum ErrorDlgResult
	{
		DBGMRG_ERROR_ABORT,
		DBGMRG_ERROR_RETRY,
		DBGMRG_ERROR_IGNORE
	};

public:

	static DebugManager* Instance()
	{
		static DebugManager ptr;
		return &ptr;
	}
	~DebugManager()
	{
		Shutdown();
	}

	std::wstring GetLogfileName();

	void Startup();
	void Shutdown();

	//Logging
	void Log(std::wstring tag, wstring message, const TCHAR* funcname, const TCHAR* filename, int line);
	void SetDebugFlags(wstring tag, unsigned char flags);

	//Messages
	void AddDebugMessenger(DebugHelp::DebugMessage* messenger);
	ErrorDlgResult Error(wstring message, bool isFatal, const TCHAR* funcname, const TCHAR* filename, int line);
};
#pragma endregion

#pragma region DebugManager Definition

std::wstring DebugManager::GetLogfileName()
{
	return ERRORLOG_FILENAME;
}

void DebugManager::Startup()
{
	SetDebugFlags(L"ERROR", ERRORFLAG_DEFAULT);
	SetDebugFlags(L"WARNING", WARNINGFLAG_DEFAULT);
	SetDebugFlags(L"INFO", LOGFLAG_DEFAULT);
	SetDebugFlags(L"OUTPUT", LOGFLAG_WRITE_TO_OUTPUT);
	SetDebugFlags(L"FILE", LOGFLAG_WRITE_TO_FILE);
}

void DebugManager::Shutdown()
{
	m_messengerCriticalSection.Lock();

	for (auto messenger : m_debugMessengers)
		delete messenger;
	m_debugMessengers.clear();

	m_messengerCriticalSection.Unlock();
}

void DebugManager::Log(wstring tag, wstring message, const TCHAR* funcname, const TCHAR* filename, int line)
{
	m_tagCriticalSection.Lock();

	auto it = m_tags.find(tag);

	if (it != m_tags.end())
	{
		m_tagCriticalSection.Unlock();

		wstring buffer;
		GetOutputBuffer(buffer, tag, message, funcname, filename, line);
		OutputBuffersToLogs(buffer, it->second);
	}
	else
	{
		m_tagCriticalSection.Unlock();
	}
}

void DebugManager::SetDebugFlags(wstring tag, unsigned char flags)
{
	m_tagCriticalSection.Lock();
	if (flags != 0)
	{
		auto result = m_tags.find(tag);

		if (result == m_tags.end())
			m_tags.insert(std::make_pair(tag, flags));
		else
			result->second = flags;
	}
	else
	{
		m_tags.erase(tag);
	}
	m_tagCriticalSection.Unlock();
}

void DebugManager::AddDebugMessenger(DebugHelp::DebugMessage* messenger)
{
	m_messengerCriticalSection.Lock();
	m_debugMessengers.push_back(messenger);
	m_messengerCriticalSection.Unlock();
}

DebugManager::ErrorDlgResult DebugManager::Error(wstring message, bool isFatal, const TCHAR* funcname, const TCHAR* filename, int line)
{
	wstring tag = isFatal ? L"FATAL" : L"ERROR";

	wstring buffer;
	GetOutputBuffer(buffer, tag, message, funcname, filename, line);

	m_tagCriticalSection.Lock();
	auto it = m_tags.find(tag);
	if (it != m_tags.end())
		OutputBuffersToLogs(buffer, it->second);
	m_tagCriticalSection.Unlock();

	int result = MessageBox(NULL, buffer.c_str(), tag.c_str(), MB_ABORTRETRYIGNORE | (isFatal ? MB_ICONERROR : MB_ICONWARNING) | MB_DEFBUTTON3);

	switch (result)
	{
		case IDIGNORE: 
			return DebugManager::DBGMRG_ERROR_IGNORE;

		case IDABORT: 
			PostQuitMessage(0);
			return DebugManager::DBGMRG_ERROR_ABORT;

		case IDRETRY: 
			__debugbreak();
			return DebugManager::DBGMRG_ERROR_RETRY;

		default: 
			return DebugManager::DBGMRG_ERROR_RETRY;
	}
}

void DebugManager::OutputBuffersToLogs(wstring finalBuffer, unsigned char flags)
{
	//Print log to a file
	if ((flags & LOGFLAG_WRITE_TO_FILE) > 0)
		OutputToFile(finalBuffer);

	//if ((flags & LOGFLAG_DUMP_STACK_TO_FILE) > 0)
	//	OutputStackToFile();

	//Print log to output
	if ((flags & LOGFLAG_WRITE_TO_OUTPUT) > 0)
		OutputDebugStringW(finalBuffer.c_str());
}

void DebugManager::OutputToFile(wstring data)
{
	FILE* logFile = NULL;
	_wfopen_s(&logFile, ERRORLOG_FILENAME.c_str(), L"a+");
	if (!logFile) return;

	fwprintf_s(logFile, data.c_str());

	fclose(logFile);
}

void DebugManager::GetOutputBuffer(wstring& outputBuffer, wstring tag, wstring message, const TCHAR* funcname, const TCHAR* filename, int line)
{
	if (!tag.empty())
	{
		outputBuffer = L"[" + tag + L"] " + message;
	}
	else
	{
		outputBuffer = message;
	}

	if (funcname)
	{
		outputBuffer += L"\nFunction: ";
		outputBuffer += funcname;
	}

	if (filename)
	{
		outputBuffer += L"\nFile: ";
		outputBuffer += filename;
	}

	if (line != 0)
	{
		outputBuffer += L"\nLine: ";
		outputBuffer += std::to_wstring(line);
	}

	outputBuffer += L"\n";
}

#pragma endregion

namespace DebugHelp
{

#pragma region DebugMessage class Definition

	DebugMessage::DebugMessage()
	{
		DbgMngr->AddDebugMessenger(this);
		m_enabled = true;
	}

	void DebugMessage::Show(wstring message, bool fatal, const TCHAR* funcname, const TCHAR* filename, int line)
	{
		if (m_enabled)
		{
			if (DbgMngr->Error(message, fatal, funcname, filename, line) == DebugManager::DBGMRG_ERROR_IGNORE)
				m_enabled = false;
		}
	}

#pragma endregion

	std::wstring CrashFilename()
	{
		return DbgMngr->GetLogfileName();
	}

	void StartDebugger()
	{
		DbgMngr->Startup();
	}

	void StopDebugger()
	{
		DbgMngr->Shutdown();
	}

	void Log(wstring tag, wstring message, const TCHAR* funcName, const TCHAR* sourceFile, int lineNum)
	{
		DbgMngr->Log(tag, message, funcName, sourceFile, lineNum);
	}

	void Logf(wstring tag, const TCHAR* funcName, const TCHAR* sourceFile, int lineNum, wstring message, ...)
	{
		va_list marker;
		va_start(marker, message);
		TCHAR buf[1024];
		_vsnwprintf_s(buf, sizeof(buf), message.c_str(), marker);

		DbgMngr->Log(tag, buf, funcName, sourceFile, lineNum);
	}

	void SetDebugFlags(std::wstring tag, unsigned char flags)
	{
		DbgMngr->SetDebugFlags(tag, flags);
	}

	void EnableLeakDetection()
	{
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	}

	void SetBreakPointAlloc(long block)
	{
		if (block == 0) return;

		_CrtSetBreakAlloc(block);
	}
}