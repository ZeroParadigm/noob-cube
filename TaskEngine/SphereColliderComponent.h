#pragma once
#include "IColliderComponent.h"

class SphereColliderComponent : public IColliderComponent
{
private:

	float m_Radius;

	SphereColliderComponent(void) {}

public:
	
	SphereColliderComponent(Actor *owner);
	SphereColliderComponent(Actor *owner, float radius);
	~SphereColliderComponent(void);

	void SetRadius(float radius);
	float GetRadius();
};

typedef SphereColliderComponent SphereCC;