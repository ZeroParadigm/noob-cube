#pragma once
#include "BaseWindow.h"

class IRenderDevice;

class RenderWindow : public BaseWindow
{
private:

	IRenderDevice* m_deviceContext;

private:

protected:

	virtual LRESULT HandleMessage(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual LPCWSTR ClassName() { return _T("RenderWindow"); }

public:

	RenderWindow() : BaseWindow()
	{
		m_deviceContext = nullptr;
	}

	virtual int Init(HINSTANCE appInstance,
					 const TCHAR* caption,
					 long x, long y,
					 long w, long h);

	void AttatchDevice(IRenderDevice* device);
	void BreakContext();
};