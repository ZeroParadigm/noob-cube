#pragma once

//This geometry is driven by directX 9 math

//	Geometry is anything visible and used in the 
//	selected graphics device. This is what gameplay
//	and gameobject must use to properly interface
//	with graphics

//type defines
typedef D3DXCOLOR Color;

extern Color g_White;
extern Color g_Black;
extern Color g_Cyan;
extern Color g_Red;
extern Color g_Green;
extern Color g_Blue;
extern Color g_Yellow;
extern Color g_Gray40;
extern Color g_Gray25;
extern Color g_Gray65;
extern Color g_Transparent;

extern DXMathLibrary::Vector3 g_Up;
extern DXMathLibrary::Vector3 g_Right;
extern DXMathLibrary::Vector3 g_Forward;

extern DXMathLibrary::Vector4 g_Up4;
extern DXMathLibrary::Vector4 g_Right4;
extern DXMathLibrary::Vector4 g_Forward4;

extern const float fOPAQUE;
extern const int iOPAQUE;
extern const float fTRANSPARENT;
extern const int iTRANSPARENT;

class Material
{
private:

	D3DMATERIAL9 m_D3D;

public:

	Material()
	{
		ZeroMemory(&m_D3D, sizeof(D3DMATERIAL9));

		m_D3D.Diffuse = g_White;
		m_D3D.Ambient = Color(0.10f, 0.10f, 0.10f, 1.0f);
		m_D3D.Specular = g_White;
		m_D3D.Emissive = g_Black;
	}

	//Material Setters
	void			SetAmbient(const Color &color)						{ m_D3D.Ambient = color; }
	void			SetDiffuse(const Color &color)						{ m_D3D.Diffuse = color; }
	void			SetSpecular(const Color&color, const float power)	{ m_D3D.Specular = color; m_D3D.Power = power; }
	void			SetEmissive(const Color &color)						{ m_D3D.Emissive = color; }

	void			SetAlpha(const float alpha)							{ m_D3D.Diffuse.a = alpha; }

	//Material getters
	const Color		GetEmissive()										{ return m_D3D.Emissive; }
	float			GetAlpha() const									{ return m_D3D.Diffuse.a; }

	//Material checking
	bool			HasAlpha() const									{ return m_D3D.Diffuse.a != fOPAQUE; }

	void			UseMaterial9(LPDIRECT3DDEVICE9 device)
	{
		device->SetMaterial(&m_D3D);
	}

	D3DMATERIAL9	GetD3D9()
	{
		return m_D3D;
	}
};

//Lights
struct Light
{
	Color ambient;
	Color diffuse;
	Color specular;
};

//struct DirectionalLight : public Light
//{
//	Vector3 directionWorld;
//};

struct DirectionalLight
{
	Color		ambient;
	Color		diffuse;
	Color		specular;
	DXMathLibrary::Vector3		directionWorld;
};

struct PointLight : public Light
{
	DXMathLibrary::Vector3		posW;
	float		range;
};