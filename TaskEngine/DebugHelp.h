#pragma once

const unsigned char LOGFLAG_WRITE_TO_FILE = 1 << 0;
const unsigned char LOGFLAG_WRITE_TO_OUTPUT = 1 << 1; 

namespace DebugHelp
{
	class DebugMessage
	{
		bool m_enabled;
	public:
		DebugMessage();
		void Show(std::wstring message, bool fatal, const TCHAR* funcName, const TCHAR* filename, int line);
	};

	std::wstring CrashFilename();

	void StartDebugger();
	void StopDebugger();

	void Log(std::wstring tag, std::wstring message, const TCHAR* funcName, const TCHAR* sourceFile, int lineNum);
	void Logf(std::wstring tag, const TCHAR* funcName, const TCHAR* sourceFile, int lineNum, std::string message, ...);
	void SetDebugFlags(std::wstring tag, unsigned char flags);

	void EnableLeakDetection();
	void SetBreakPointAlloc(long block);
}

#define CRASH_FILENAME DebugHelp::CrashFilename();

#define ENGINE_FATAL(str)\
{\
	static DebugHelp::DebugMessage* messenger = DBG_NEW DebugHelp::DebugMessage; \
	messenger->Show(std::wstring((str)), true, __FUNCNAME__, __FILENAME__, __LINE__); \
}\

//#ifdef _DEBUG

#define ENGINE_ERROR(str)\
{\
	static DebugHelp::DebugMessage* messenger = DBG_NEW DebugHelp::DebugMessage;\
	messenger->Show(std::wstring((str)), false, __FUNCNAME__, __FILENAME__, __LINE__); \
}\

#define ENGINE_ASSERT(expr)\
{\
	if (!(expr))\
	{\
		static DebugHelp::DebugMessage* messenger = DBG_NEW DebugHelp::DebugMessage; \
		messenger->Show(std::wstring((L#expr)), false, __FUNCNAME__, __FILENAME__, __LINE__);\
	}\
}\

#define ENGINE_WARNING(msg)			DebugHelp::Log(L"WARNING", std::wstring((msg)), __FUNCNAME__, __FILENAME__, __LINE__); \

#define ENGINE_INFO(msg)			DebugHelp::Log(L"INFO", std::wstring((msg)), NULL, NULL, 0);\

#define LOG_TAG(tag, msg)			DebugHelp::Log(tag, std::wstring((msg)), __FUNCNAME__, __FILENAME__, __LINE__);
#define LOGF_TAG(tag, msg, ...)		DebugHelp::Logf(tag, __FUNCNAME__, __FILENAME__, __LINE__, msg, __VA_ARGS__);

#define LOG_TAG_N(tag, msg)			DebugHelp::Log(tag, std::wstring((msg)), NULL, NULL, 0);
#define LOGF_TAG_N(tag, msg, ...)	DebugHelp::Logf(tag, NULL, NULL, 0, msg, __VA_ARGS__);


#define LOG(msg)					LOG_TAG(L"INFO", msg)
#define LOGF(msg, ...)				LOGF_TAG(L"INFO", msg, ...)

#define OUTPUT(msg)					LOG_TAG_N(L"OUTPUT", msg)
#define OUTPUTF(msg, ...)			LOGF_TAG_N(L"OUTPUT", msg, ...)

#define FILE(msg)					LOG_TAG_N(L"FILE", msg)
#define FILEF(msg, ...)				LOGF_TAG_N(L"FILE", msg, ...)

#if defined(DEBUG) || defined(_DEBUG)
#ifndef V
#define V(x)           { hr = (x); if( FAILED(hr) ) { /*ENGINE_ERROR(L#x)*/ } }
#endif
#ifndef V_RETURN
#define V_RETURN(x)    { hr = (x); if( FAILED(hr) ) { /*ENGINE_ERROR(L#x)*/ return hr; } }
#endif
#else
#ifndef V
#define V(x)           { hr = (x); }
#endif
#ifndef V_RETURN
#define V_RETURN(x)    { hr = (x); if( FAILED(hr) ) { return hr; } }
#endif
#endif

//#else
//
//#define ENGINE_ERROR(str) do { (void)sizeof(str); } while(0) 
//#define ENGINE_WARNING(str) do { (void)sizeof(str); } while(0) 
//#define ENGINE_INFO(str) do { (void)sizeof(str); } while(0) 
//#define LOG(str) ENGINE_INFO(str)
//#define ENGINE_ASSERT(expr) do { (void)sizeof(expr); } while(0) 
//
//#endif