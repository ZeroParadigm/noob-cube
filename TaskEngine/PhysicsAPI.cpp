#include "EngineCommonStd.h"

#include "PhysicsAPI.h"
#include "PhysicsSystem.h"

PhysicsAPI::PhysicsAPI()
{
	core = new PhysicsSystem();
}

PhysicsAPI::~PhysicsAPI()
{
	SAFE_DELETE(core);
}

void PhysicsAPI::init(MemoryDelegate* memory)
{
	core->init(memory);
}

void PhysicsAPI::shutdown()
{
	core->shutdown();
}

void PhysicsAPI::update(float dt)
{
	core->update(dt);
}