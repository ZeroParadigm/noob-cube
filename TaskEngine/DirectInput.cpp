#include "EngineCommonStd.h"

#include "DirectInput.h"


//HR
//#if defined(DEBUG) | defined(_DEBUG)
//	#ifndef HR
//	#define HR(x)                                      \
//	{                                                  \
//		HRESULT hr = x;                                \
//		if(FAILED(hr))                                 \
//		{                                              \
//			DXTrace(__FILE__, __LINE__, hr, #x, TRUE); \
//		}                                              \
//	}
//	#endif
//
//#else
//	#ifndef HR
//	#define HR(x) x;
//	#endif
//#endif 


//Check for undefined value
template <class T>
bool UndefinedResult(T t)
{
	return t != t;
}
//***************************************************************

DirectInput* gDInput = 0;

DirectInput::DirectInput(HINSTANCE hApp, 
						 HWND      hWnd,
						 DWORD keyboardCoopFlags, 
						 DWORD mouseCoopFlags)
{
	m_hWindow = hWnd;

	ZeroMemory(m_KeyboardState, sizeof(m_KeyboardState));
	ZeroMemory(&m_MouseState, sizeof(m_MouseState));

	DirectInput8Create(hApp, DIRECTINPUT_VERSION, 
		IID_IDirectInput8, (void**)&m_DirectInput, 0);

	m_DirectInput->CreateDevice(GUID_SysKeyboard, &m_Keyboard, 0);
	m_Keyboard->SetDataFormat(&c_dfDIKeyboard);
	HRESULT hr = m_Keyboard->SetCooperativeLevel(hWnd, keyboardCoopFlags);
	hr = m_Keyboard->Acquire();
	if (hr != DI_OK)
	{
		//Log some info
		switch(hr)
		{
		case DIERR_INVALIDPARAM : 
			MessageBox(hWnd, L"Acquire FAILED : Invalid parameter", L"Keyboard", MB_OK );
			break;
		case DIERR_NOTINITIALIZED : 
			MessageBox(hWnd, L"Acquire FAILED : Not Initialized", L"Keyboard", MB_OK );
			break;
		case DIERR_OTHERAPPHASPRIO : 
			MessageBox(hWnd, L"Acquire FAILED : Access denied", L"Keyboard", MB_OK );
			break;
		default:
			MessageBox(hWnd, L"Unknown error", L"Keyboard", MB_OK );
		}
	}

	m_DirectInput->CreateDevice(GUID_SysMouse, &m_Mouse, 0);
	m_Mouse->SetDataFormat(&c_dfDIMouse2);
	hr = m_Mouse->SetCooperativeLevel(hWnd, mouseCoopFlags);
	if (hr != DI_OK)
	{
		//Log some info
		switch(hr)
		{
		case DIERR_INVALIDPARAM : 
			MessageBox(hWnd, L"Acquire FAILED : Invalid parameter", L"Mouse", MB_OK );
			break;
		case DIERR_NOTINITIALIZED : 
			MessageBox(hWnd, L"Acquire FAILED : Not Initialized", L"Mouse", MB_OK );
			break;
		case DIERR_OTHERAPPHASPRIO : 
			MessageBox(hWnd, L"Acquire FAILED : Access denied", L"Mouse", MB_OK );
			break;
		default:
			MessageBox(hWnd, L"Unknown error", L"Mouse", MB_OK );
		}
	}
	hr = m_Mouse->Acquire();
	if (hr != DI_OK)
	{
		//Log some info
		switch(hr)
		{
		case DIERR_INVALIDPARAM : 
			MessageBox(hWnd, L"Acquire FAILED : Invalid parameter", L"Mouse", MB_OK );
			break;
		case DIERR_NOTINITIALIZED : 
			MessageBox(hWnd, L"Acquire FAILED : Not Initialized", L"Mouse", MB_OK );
			break;
		case DIERR_OTHERAPPHASPRIO : 
			MessageBox(hWnd, L"Acquire FAILED : Access denied", L"Mouse", MB_OK );
			break;
		default:
			MessageBox(hWnd, L"Unknown error", L"Mouse", MB_OK );
		}
	}
}

DirectInput::~DirectInput()
{
	if (m_DirectInput)
	{
		m_DirectInput->Release();
	}

	m_Keyboard->Unacquire();
	m_Mouse->Unacquire();

	if (m_Keyboard)
	{
		m_Keyboard->Release();
	}
	
	if (m_Mouse)
	{
		m_Mouse->Release();
	}
}

void DirectInput::Poll()
{
	// Poll keyboard.
	HRESULT hr = m_Keyboard->GetDeviceState(sizeof(m_KeyboardState), (void**)&m_KeyboardState); 
	if( FAILED(hr) )
	{
		// Keyboard lost, zero out keyboard data structure.
		ZeroMemory(m_KeyboardState, sizeof(m_KeyboardState));

		 // Try to acquire for next time we poll.
		hr = m_Keyboard->Acquire();
	}

	// Poll mouse.
	hr = m_Mouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&m_MouseState); 
	if( FAILED(hr) )
	{
		// Mouse lost, zero out mouse data structure.
		ZeroMemory(&m_MouseState, sizeof(m_MouseState));

		// Try to acquire for next time we poll.
		hr = m_Mouse->Acquire(); 
	}

	POINT temp;
	GetCursorPos(&temp);
	ScreenToClient(m_hWindow, &temp);

	m_MousePos = temp;
}

bool DirectInput::KeyDown(char key)
{
	return (m_KeyboardState[key] & 0x80) != 0;
}

bool DirectInput::KeyPressed(int Key)
{
	bool relevantState;

	m_KeyboardCurrentState[Key] = ((m_KeyboardState[Key] & 0x80) != 0);
	
	relevantState = (m_KeyboardCurrentState[Key] != m_KeyboardLastState[Key]) && (m_KeyboardLastState[Key] == 0);
	
	m_KeyboardLastState[Key] = ((m_KeyboardState[Key] & 0x80) != 0);

	return relevantState;
}

bool DirectInput::MouseButtonDown(int button)
{
	return (m_MouseState.rgbButtons[button] & 0x80) != 0;
}

float DirectInput::MouseDX()
{
	return (float)m_MouseState.lX;
}

float DirectInput::MouseDY()
{
	return (float)m_MouseState.lY;
}

float DirectInput::MouseDZ()
{
	return (float)m_MouseState.lZ;
}

POINT DirectInput::MousePosition()
{
	return m_MousePos;
}