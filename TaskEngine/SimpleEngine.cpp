#include "EngineCommonStd.h"

#include "SimpleEngine.h"


#include "GraphicsAPI.h"
#include "PhysicsAPI.h"
#include "AiAPI.h"
#include "GameplaySystem.h"

#include "D3D9RenderDevice.h"
#include "RenderWindow.h"
#include "Scene.h"

#include "DirectInput.h"
#include "Gamepad.h"

#include "WebSocketClient.h"

#include "GraphicsUtility.h"


//==== Constructors ==============================
//Allocate Memory
SimpleEngine::SimpleEngine()
{
}
//Deallocate memory
SimpleEngine::~SimpleEngine()
{
}
//================================================



//==== Private API ===============================
void SimpleEngine::UpdateSystems(double dt)
{
	input->Poll();
	gamepad->Check();

	memory->GetDebugCamera()->Update((float)dt);

	network->Update((float)dt);
	gameplay->Update((float)dt);
	ai->Update((float)dt);
	physics->update((float)dt);
	//graphics->update((float)dt);//Old
	device->Update((float)dt);
	//scene->Update((float)dt);

	if (GetAsyncKeyState(VK_F1))
	{
		window->SetFullscreen(false);
	}
	if (GetAsyncKeyState(VK_F2))
	{
		window->SetFullscreen(true);
	}
	if (GetAsyncKeyState(VK_F3))
	{
		device->SetPostProcess(true);
	}
	if (GetAsyncKeyState(VK_F4))
	{
		device->SetPostProcess(false);
	}
	if (gDInput->KeyPressed(DIK_1))
		device->AddFilter(21);
	if (gDInput->KeyPressed(DIK_2))
		device->AddFilter(11);
	if (gDInput->KeyPressed(DIK_3))
		device->AddFilter(8);
	if (gDInput->KeyPressed(DIK_4))
		device->AddFilter(13);
	if (gDInput->KeyPressed(DIK_5))
		device->AddFilter(7);
	if (gDInput->KeyPressed(DIK_6))
		device->AddFilter(9);
	if (gDInput->KeyPressed(DIK_7))
		device->AddFilter(10);
	if (gDInput->KeyPressed(DIK_8))
		device->AddFilter(6);
}
//================================================



//==== Public API ================================
void SimpleEngine::Init(HINSTANCE appInstance, const TCHAR* appName)
{
	//Grab static instance ptr
	memory				= MemoryDelegate::instance();

	//Allocation Phase
	//scene				= _new Scene();
	device				= _new D3D9RenderDevice();
	window				= _new RenderWindow();


	//graphics			= _new GraphicsAPI();//Old

	physics				= _new PhysicsAPI();
	ai					= _new AiAPI();
	network				= _new WebSocketClient();
	gameplay			= _new GameplaySystem();
	gDInput = input		= _new DirectInput(appInstance, window->GetHWND(),
										DISCL_NONEXCLUSIVE | DISCL_BACKGROUND,
										DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	gamepad				= _new Gamepad(1);



	//Initalization Phase
	//scene->Init(memory, Scene::DEVICE_D3D9, 60, true, appInstance, appName, 50, 50, 800, 600);
	memory->ToggleDebugCamera();

	window->Init(appInstance, appName, 50, 50, 800, 600);
	device->Init(memory, window, 60, true);
	window->AttatchDevice(device);

	//graphics->init(memory, appInstance, appName, 1, true);

	physics->init(memory);
	ai->Init(memory);

	memory->init(device->GetDevice());

	network->Initialize();

	gameplay->Initialize(device, memory, input, gamepad, network);


	//Staging Phase
	window->Show();
	//window->SetFullscreen(true);
	window->SetMainWindow(true);
}

int SimpleEngine::Shutdown(int appCloseCode)
{
	//scene->Shutdown();
	//graphics->shutdown();//Old
	device->Shutdown();
	physics->shutdown();
	ai->Shutdown();
	network->Shutdown();
	gameplay->Shutdown();

	memory->shutdown();

	window->Hide();

	//SAFE_DELETE(scene);
	SAFE_DELETE(device);
	SAFE_DELETE(window);
	//SAFE_DELETE(graphics); // Old
	SAFE_DELETE(physics);
	SAFE_DELETE(ai);
	SAFE_DELETE(network);
	SAFE_DELETE(gameplay);
	SAFE_DELETE(input);
	SAFE_DELETE(gamepad);

	return appCloseCode;
}

int SimpleEngine::Run()
{
	MSG msg;
	msg.message = WM_NULL;

	__int64 countsPerSecond = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSecond);
	float secondsPerCount = 1.0f / (float)countsPerSecond;

	__int64 previousTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&previousTimeStamp);

	double DT = 0.0;

	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			__int64 currenTimeStamp = 0;
			QueryPerformanceCounter((LARGE_INTEGER*)&currenTimeStamp);
			m_engineUpTime += DT = (currenTimeStamp - previousTimeStamp) * secondsPerCount;
			previousTimeStamp = currenTimeStamp;

			UpdateSystems(DT);
		}
	}

	return Shutdown((int)msg.wParam);
}
//================================================