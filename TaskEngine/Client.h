#pragma once

//#define WIN32_LEAN_AND_MEAN
//
//#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <IPHlpApi.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma warning (disable : 4251)

#include "PacketReporter.h"


class Client
{
private:

	WSAData m_WSAData;

	SOCKET* m_ClientConnection;
	HANDLE* m_ClientThread;

	bool m_Connected;

protected:

	PacketReporter* reporter;

public:

	Client(PacketReporter* r);
	~Client();

	bool Initialize();

	bool Connect(std::string ipaddr, std::string port);
	
	bool IsConnected();

	bool Send(char* data, int size);

	bool Receive();

	void Disconnect();
};

void CreateReceiveThread(Client* c, HANDLE* h);

DWORD WINAPI RecvProc(LPVOID lpParam);