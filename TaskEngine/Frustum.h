#pragma once

class FPlane : public D3DXPLANE
{
public:
	inline void Normalize();

	// normal faces away from you if you send in verts in counter clockwise order....
	inline void Init(const Vector3 &p0, const Vector3 &p1, const Vector3 &p2);

	bool Inside(const Vector3 &point, const float radius) const;
	bool Inside(const Vector3 &point) const;
};



class Frustum
{
public:
	enum Side { Near, Far, Top, Right, Bottom, Left, NumPlanes };

	FPlane		m_Planes[NumPlanes];	// planes of the frusum in camera space
	Vector3		m_NearClip[4];			// verts of the near clip plane in camera space
	Vector3		m_FarClip[4];			// verts of the far clip plane in camera space

	float		m_Fov;					// field of view in radians
	float		m_Aspect;				// aspect ratio - width divided by height
	float		m_Near;					// near clipping distance
	float		m_Far;					// far clipping distance

public:
	Frustum();

	bool Inside(const Vector3 &point) const;
	bool Inside(const Vector3 &point, const float radius) const;

	const FPlane &Get(Side side)	{ return m_Planes[side]; }
	void SetFOV(float fov)			{ m_Fov = fov; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetAspect(float aspect)	{ m_Aspect = aspect; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetNear(float nearClip)	{ m_Near = nearClip; Init(m_Fov, m_Aspect, m_Near, m_Far); }
	void SetFar(float farClip)		{ m_Far = farClip; Init(m_Fov, m_Aspect, m_Near, m_Far); }

	void Init(const float fov, const float aspect, const float nearClip, const float farClip);

	void Render();
};