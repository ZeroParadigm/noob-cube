#pragma once
//#include "Geometry.h"
//#include "CriticalSection.h"

class Actor;

enum ComponentType
{
	COMPONENT_INVALID = 0,
	COMPONENT_TRANSFORM,
	COMPONENT_MESH,
	COMPONENT_RENDERER,
	COMPONENT_EFFECT,
	COMPONENT_CAMERA,
	COMPONENT_SPHERE,
	COMPONENT_BOX,
	COMPONENT_SKYBOX,
	COMPONENT_GUI_TEXT,
	COMPONENT_GUI_LAYER,
	COMPONENT_GUI_IMAGE,
	COMPONENT_MOVEMENT,
	COMPONENT_AI,
	COMPONENT_PARTICLEPHYSICS,
	COMPONENT_SCRIPT,
	COMPONENT_GAMEWORLD,
	COMPONENT_PLAYER,
	COMPONENT_ENEMY,
	COMPONENT_COUNT,
	COMPONENT_BULLET,
	COMPONENT_TRIGGERVOLUME,
	COMPONENT_EDGETRIGGERVOLUME
};

class BaseComponent
{
protected:

	CriticalSection m_CriticalSection;

protected:

	//These might need to be protected
	int				m_ID;
	int				m_Type;
	Actor*			m_Owner;

private:

	void SetId(void);

public:

	BaseComponent(void);
	BaseComponent(Actor *owner);
	virtual ~BaseComponent(void);

	int		GetID(void){return m_ID;}
	int		GetType(void){ return m_Type; }
	Actor*	GetOwner(void){return m_Owner;}

};

