#pragma once
//#include "MathLibrary.h"

class SphereColliderComponent;
class BoxColliderComponent;
class MeshColliderComponent;
class CylinderColliderComponent;
class CapsuleColliderComponent;

class ContactPoint;

class IntersectionTests
{
public:

	static bool SphereToSphere(SphereColliderComponent* A, SphereColliderComponent* B, ContactPoint &point);
	static bool SphereToAABB(SphereColliderComponent* sphere, BoxColliderComponent* box, ContactPoint &point);
	//static bool SphereToOBB(SphereColliderComponent* sphere, BoxColliderComponent* box);
	//static bool SphereToCylinder(SphereColliderComponent* sphere, CylinderColliderComponent* cylinder);
	//static bool SphereToCapsule(SphereColliderComponent* sphere, CapsuleColliderComponent* capsule);
	//static bool SphereToMesh(SphereColliderComponent* sphere, MeshColliderComponent* mesh);

	static bool AABBToAABB(BoxColliderComponent* A, BoxColliderComponent* B, ContactPoint &point);
	//static bool AABBToOBB(BoxColliderComponent* aabb, BoxColliderComponent* obb);
	//static bool AABBToMesh(BoxColliderComponent* aabb, MeshColliderComponent* mesh);
	//static bool AABBToCylinder(BoxColliderComponent* aabb, CylinderColliderComponent* cylinder);
	//static bool AABBToCapsule(BoxColliderComponent* aabb, CapsuleColliderComponent* capsule);
	
	//static bool OBBToOBB(BoxColliderComponent* A, BoxColliderComponent* B);
	//static bool OBBToMesh(BoxColliderComponent* obb, MeshColliderComponent* mesh);
	//static bool OBBToCylinder(BoxColliderComponent* obb, CylinderColliderComponent* cylinder);
	//static bool OBBToCapsule(BoxColliderComponent* obb, CapsuleColliderComponent* capsule);

	//static bool MeshToMesh(MeshColliderComponent* A, MeshColliderComponent* B);
	//static bool MeshToCylinder(MeshColliderComponent* mesh, CylinderColliderComponent* cylinder);
	//static bool MeshToCapsule(MeshColliderComponent* mesh, CapsuleColliderComponent* capsule);

	//static bool CylinderToCylinder(CylinderColliderComponent* A, CylinderColliderComponent* B);
	//static bool CylinderToCapsule(CylinderColliderComponent* cylinder, CapsuleColliderComponent* capsule);

	//static bool CapsuleToCapsule(CapsuleColliderComponent* A, CapsuleColliderComponent* B);
};