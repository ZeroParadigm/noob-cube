#pragma once

#include "easywsclient.hpp"
#ifdef _WIN32
#pragma comment( lib, "ws2_32" )
#include <WinSock2.h>
#endif
#include <string>
#include <functional>

class WebSocketClient
{
private:

	WSAData m_WSAData;
	std::string m_ServerAddress;

	easywsclient::WebSocket::pointer m_ws;

	std::function<void(const std::string&)> m_RecvHandler;

public:

	WebSocketClient();
	~WebSocketClient();

	void Initialize();
	void Shutdown();

	bool Connect();

	void Send();
	void SendPosition(float x, float y, float z);

	void Update(float dt);

	easywsclient::WebSocket::readyStateValues GetReadyState();

	void HandleMessage(std::function<void(const std::string& message)> handler);

};