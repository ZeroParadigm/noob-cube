#include "EngineCommonStd.h"

#include "Gamepad.h"

Gamepad* gGamepad = 0;

Gamepad::Gamepad(int numPlayers)
{
	m_Players = numPlayers - 1;

	Init();

	gGamepad = this;
}

Gamepad::~Gamepad(void)
{
	//TerminateThread(this->m_Thread, 0);
}

XINPUT_STATE Gamepad::GetState(void)
{
	ZeroMemory(&ControllerState, sizeof(XINPUT_STATE));

	XInputGetState(m_Players, &ControllerState);

	return ControllerState;
}

bool Gamepad::IsConnected(void)
{
	ZeroMemory(&ControllerState, sizeof(XINPUT_STATE));

	DWORD result = XInputGetState(m_Players, &ControllerState);

	if (result == ERROR_SUCCESS)
	{
		return true;
	}
	else 
	{
		return false;
	}
}

void Gamepad::Init(void)
{
	//wait = false;

	for(unsigned int i = 0; i <= m_Players; i++)
	{   
		current[i].InUse=false;    
		current[i].LAnol.x=0;
		current[i].LAnol.y=0;
		current[i].RAnol.x=0;
		current[i].RAnol.y=0;    
		current[i].LTrig=0;
		current[i].RTrig=0;    
		current[i].A=false;
		current[i].B=false;
		current[i].X=false;
		current[i].Y=false;    
		current[i].Start=false;
		current[i].Select=false;
		current[i].LShoulder=false;
		current[i].RShoulder=false;
		current[i].DPAD_UP=false;
		current[i].DPAD_DOWN=false;
		current[i].DPAD_LEFT=false;
		current[i].DPAD_RIGHT=false;
		current[i].LStick=false;
		current[i].RStick=false;

		previous[i].InUse=false;    
		previous[i].LAnol.x=0;
		previous[i].LAnol.y=0;
		previous[i].RAnol.x=0;
		previous[i].RAnol.y=0;    
		previous[i].LTrig=0;
		previous[i].RTrig=0;    
		previous[i].A=false;
		previous[i].B=false;
		previous[i].X=false;
		previous[i].Y=false;    
		previous[i].Start=false;
		previous[i].Select=false;
		previous[i].LShoulder=false;
		previous[i].RShoulder=false;
		previous[i].DPAD_UP=false;
		previous[i].DPAD_DOWN=false;
		previous[i].DPAD_LEFT=false;
		previous[i].DPAD_RIGHT=false;
		previous[i].LStick=false;
		previous[i].RStick=false;
	}
}

void Gamepad::Check(void)
{
	GetState(); 

	for(unsigned int i = 0; i <= m_Players; i++){

		if(IsConnected())    
		{            
			if(!current[i].InUse)            
			{                    
				current[i].InUse=true;                              
			}    

			//left trigger
			if(ControllerState.Gamepad.bLeftTrigger!= 0)            
			{             
				current[i].LTrig = ControllerState.Gamepad.bLeftTrigger / 255.0f;            
			}       

			//right trigger
			if(ControllerState.Gamepad.bRightTrigger!= 0)            
			{             
				current[i].RTrig=ControllerState.Gamepad.bRightTrigger / 255.0f;           
			}  

			//left analog stick
			if(ControllerState.Gamepad.sThumbLX!=current[i].LAnol.x)            
			{       
				current[i].LAnol.x=ControllerState.Gamepad.sThumbLX;            
			}            
			if(ControllerState.Gamepad.sThumbLY!=current[i].LAnol.y)            
			{     
				current[i].LAnol.y=ControllerState.Gamepad.sThumbLY;            
			}

			//right analog stick
			if(ControllerState.Gamepad.sThumbRX!=current[i].RAnol.x)            
			{           
				
				current[i].RAnol.x=ControllerState.Gamepad.sThumbRX;            
			}            
			if(ControllerState.Gamepad.sThumbRY!=current[i].RAnol.y)            
			{       
				
				current[i].RAnol.y=ControllerState.Gamepad.sThumbRY;            
			}

			//Left Shoulder
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
			{
				if(current[i].LShoulder == false)
				{
					current[i].LShoulder = true;
					
				} else {
					previous[i].LShoulder = true;
				}

			} else {
				if(current[i].LShoulder == true)
				{
					current[i].LShoulder=false;
					
				} else {
					previous[i].LShoulder = false;
				}
			}

			//Right Shoulder
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
			{
				if(current[i].RShoulder == false)
				{
					current[i].RShoulder = true;
					
				} else {
					previous[i].RShoulder = true;
				}

			} else {
				if(current[i].RShoulder == true)
				{
					current[i].RShoulder=false;
				} else {
					previous[i].RShoulder = false;
				}
			}

			//A
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				if(current[i].A == false){
					current[i].A=true;
					
				} else {
					previous[i].A = true;
				}

			}else{
			
				if(current[i].A == true){
					current[i].A=false;
				} else {
					previous[i].A = false;
				}
			}    

			//B
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{	
				if(current[i].B == false){
					current[i].B=true;
					
				} else {
					previous[i].B = true;
				}

			}else{
			
				if(current[i].B == true){
					current[i].B=false;
					
				} else {
					previous[i].B = false;
				}
			}   

			//X
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X)
			{
				if(current[i].X == false){
					current[i].X=true;
					
				} else {
					previous[i].X = true;
				}

			}else{
			
				if(current[i].X == true){
					current[i].X=false;
				} else {
					previous[i].X = false;
				}
			}  

			//Y
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y)
			{
				if(current[i].Y == false){
					current[i].Y=true;
					
				} else {
					previous[i].Y = true;
				}

			}else{
			
				if(current[i].Y == true){
					current[i].Y=false;
				} else {
					previous[i].Y = false;
				}
			}

			//DPAD UP
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP)
			{
				if(current[i].DPAD_UP == false){
					current[i].DPAD_UP=true;
					
				} else {
					previous[i].DPAD_UP = true;
				}

			}else{
			
				if(current[i].DPAD_UP == true){
					current[i].DPAD_UP=false;
				} else {
					previous[i].DPAD_UP = false;
				}
			}

			//DPAD DOWN
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN)
			{
				if(current[i].DPAD_DOWN == false){
					current[i].DPAD_DOWN=true;
					
				} else {
					previous[i].DPAD_DOWN = true;
				}

			}else{
			
				if(current[i].DPAD_DOWN == true){
					current[i].DPAD_DOWN=false;
				} else {
					previous[i].DPAD_DOWN = false;
				}
			}

			//DPAD LEFT
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT)
			{
				if(current[i].DPAD_LEFT == false){
					current[i].DPAD_LEFT=true;
					
				} else {
					previous[i].DPAD_LEFT = true;
				}
			}else{
			
				if(current[i].DPAD_LEFT == true){
					current[i].DPAD_LEFT=false;
				} else {
					previous[i].DPAD_LEFT = false;
				}
			}

			//DPAD RIGHT
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)
			{
				if(current[i].DPAD_RIGHT == false){
					current[i].DPAD_RIGHT=true;
					
				} else {
					previous[i].DPAD_RIGHT = true;
				}
			}else{
			
				if(current[i].DPAD_RIGHT == true){
					current[i].DPAD_RIGHT=false;
				} else {
					previous[i].DPAD_RIGHT = false;
				}
			}

			//Left Stick Press
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB)
			{
				if(current[i].LStick == false){
					current[i].LStick=true;
					
				} else {
					previous[i].LStick = true;
				}
			}else{
			
				if(current[i].LStick == true){
					current[i].LStick=false;
				} else {
					previous[i].LStick = false;
				}
			}

			//Left Stick Press
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB)
			{
				if(current[i].RStick == false){
					current[i].RStick=true;
					
				} else {
					previous[i].RStick = true;
				}
			}else{
			
				if(current[i].RStick == true){
					current[i].RStick=false;
				} else {
					previous[i].RStick = false;
				}
			}

			//Select (back)
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
			{
				if(current[i].Select == false)
				{
					current[i].Select=true;
					
				} else {
					previous[i].Select = true;
				}

			}else{
			
				if(current[i].Select == true)
				{
					current[i].Select=false;
				} else {
					previous[i].Select = false;
				}
			}

			//Start
			if(ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START)
			{
				if(current[i].Start == false)
				{
					current[i].Start=true;
					
				} else {
					previous[i].Start = true;
				}

			}else{
			
				if(current[i].Start == true)
				{
					current[i].Start=false;
				} else {
					previous[i].Start = false;
				}
			}
		}  
		else if(current[i].InUse)    
		{            
			current[i].InUse = false;            
			//StopChecking();
		}
	}
}
