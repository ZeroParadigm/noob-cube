#include "EngineCommonStd.h"
#include "DebugCamera.h"

#include "Gamepad.h"
#include "DirectInput.h"


Matrix4 DebugCamera::GetWorldMatrix()
{
	Matrix4 worldMatrix;

	//worldMatrix.BuildRotationQuat(m_debugOrientation);
	worldMatrix.SetPosition(m_debugPosition);

	return worldMatrix;
}

void DebugCamera::AdjustDebugCamera(float dt)
{
	//check speed modifier
	float speedSet = m_speed;
	if (gDInput->KeyDown(DIK_LSHIFT))
	{
		speedSet *= 5;
	}

	Vector3 dir;

	if (gDInput->KeyDown(DIK_W))  //move forward
	{
		dir += m_Forward;
	}

	if (gDInput->KeyDown(DIK_S)) //move backward
	{
		dir -= m_Forward;
	}

	if (gDInput->KeyDown(DIK_D))  //strafe right
	{
		dir += m_Right;
	}

	if (gDInput->KeyDown(DIK_A)) //strafe left
	{
		dir -= m_Right;
	}

	if (gDInput->KeyDown(DIK_SPACE)) //move up in the y direction
	{
		dir.y += 1;
	}

	if (gDInput->KeyDown(DIK_O))		//move down in y direction
	{
		dir.y -= 1;
	}

	D3DXVec3Normalize(&dir, &dir);
	m_debugPosition += dir * speedSet * dt;
}

void DebugCamera::RotateDebugCamera(float dt)
{
	if (gDInput->MouseButtonDown(1)) //if right mouse button are down
	{

		//grab angles
		float pitch = gDInput->MouseDY() * dt; //get the change in the angle for moving the mouse up and down
		float yAngle = gDInput->MouseDX() * dt; //get the change in the angle for moving the mouse left and right

		float sensitivity = 5.0f;

		//set sensativity
		pitch *= sensitivity;
		yAngle *= sensitivity;

		//rotate Up and Down
		D3DXMATRIX matRotAxis;
		D3DXMatrixRotationAxis(&matRotAxis, &m_Right, pitch);
		D3DXVec3TransformCoord(&m_Forward, &m_Forward, &matRotAxis);
		D3DXVec3TransformCoord(&m_Up, &m_Up, &matRotAxis);

		//rotate left and right
		D3DXMATRIX matRotYAxis;
		D3DXMatrixRotationY(&matRotYAxis, yAngle);
		D3DXVec3TransformCoord(&m_Right, &m_Right, &matRotYAxis);
		D3DXVec3TransformCoord(&m_Up, &m_Up, &matRotYAxis);
		D3DXVec3TransformCoord(&m_Forward, &m_Forward, &matRotYAxis);

	}
}

void DebugCamera::Update(float dt)
{
	AdjustDebugCamera(dt);
	RotateDebugCamera(dt);

	__super::Update(dt);
}

void DebugCamera::BuildView()
{
	//m_Forward = m_debugOrientation.Forward();
	//m_Up = m_debugOrientation.Up();
	//m_Right = m_debugOrientation.Right();

	m_Forward.Normalize();
	m_Up.Normalize();
	m_Right.Normalize();

	float x, y, z;

	x = -m_debugPosition.Dot(m_Right);
	y = -m_debugPosition.Dot(m_Up);
	z = -m_debugPosition.Dot(m_Forward);

	//Protect only the shared resource manipulation
	//ScopedCriticalSection critical_section(this->m_CriticalSection);

	m_View(0, 0) = m_Right.x;
	m_View(1, 0) = m_Right.y;
	m_View(2, 0) = m_Right.z;
	m_View(3, 0) = x;

	m_View(0, 1) = m_Up.x;
	m_View(1, 1) = m_Up.y;
	m_View(2, 1) = m_Up.z;
	m_View(3, 1) = y;

	m_View(0, 2) = m_Forward.x;
	m_View(1, 2) = m_Forward.y;
	m_View(2, 2) = m_Forward.z;
	m_View(3, 2) = z;

	m_View(0, 3) = 0.0f;
	m_View(1, 3) = 0.0f;
	m_View(2, 3) = 0.0f;
	m_View(3, 3) = 1.0f;
}