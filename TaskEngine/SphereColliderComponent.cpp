#include "EngineCommonStd.h"

#include "SphereColliderComponent.h"


SphereColliderComponent::SphereColliderComponent(Actor* owner) : IColliderComponent(owner)
{
	m_Radius = 1.0f;
}

SphereColliderComponent::SphereColliderComponent(Actor *owner, float radius) : IColliderComponent(owner)
{
	m_Radius = radius;
}

SphereColliderComponent::~SphereColliderComponent(void)
{

}

float SphereColliderComponent::GetRadius()
{
	return m_Radius;
}

void SphereColliderComponent::SetRadius(float radius)
{
	m_Radius = radius;
}