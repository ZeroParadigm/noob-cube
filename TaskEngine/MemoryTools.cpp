#include "EngineCommonStd.h"
#include "MemoryTools.h"

#pragma region Internal Helper Functions

// Inits the headers of each chunk in a linked list of raw memory
void InitLinkedListHeaders(unsigned char* start, unsigned char* end, size_t chunkSize, size_t headerSize)
{

}

#pragma endregion


//Exposed API
namespace MemoryTools
{
	void RawMemoryToChunkList(unsigned char* rawMem, size_t chunkSize, size_t blockSize)
	{
			//define how to setup linked list
		auto CreateLinkedList = [&](unsigned char* start, unsigned char* end)
		{
			unsigned char* current = start;

			while (current < end)
			{
				unsigned char* next = current + chunkSize;

				unsigned char** chunkHeader = (unsigned char**)current;
				chunkHeader[0] = (next < end ? next : NULL);

				current += chunkSize;
			}
		};

		//Use a thread for each 256 chunks in a block
		int threads = 1 / 256;
		WARNING_TODO(change 1 to number of chunks some how);

		if (threads >= 2)
		{
			std::list<std::thread> workers;
			for (int t = 0; t < threads; ++t)
			{
				workers.push_back(std::thread([&](){
					CreateLinkedList(0, 0);
				}));
			}

			//wait for workers to finish
			for (auto& worker : workers) worker.join();
		}
		else
		{

		}
	}
}