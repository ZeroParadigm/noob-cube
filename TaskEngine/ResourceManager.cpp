#include "EngineCommonStd.h"

#include "ResourceManager.h"
#include "Mesh.h"
#include "Sound.h"
#include "Texture.h"

#include "Mesh.h"
#include "Sound.h"
#include "Texture.h"
#include "Sprite.h"


ResourceManager::ResourceManager(IDirect3DDevice9* d3dDevice) : e_d3dDevice(d3dDevice)
{
	m_DefaultMesh = new Mesh(L"Assets/Meshes/Default.X");
	//m_DefaultSound = new Sound();
	m_DefaultTexture = new Texture(L"Assets/Textures/Default.jpg");

	LoadDefaults();
}

ResourceManager::~ResourceManager()
{
	ReleaseResources();

	m_DefaultMesh->Release();
	m_DefaultTexture->Release();
	
	SAFE_DELETE(m_DefaultMesh);
	SAFE_DELETE(m_DefaultTexture);
}

bool ResourceManager::LoadMesh(std::wstring fileName)
{
	std::map<std::wstring, Mesh*>::iterator it;

	it = m_Meshes.find(fileName);

	if(it != m_Meshes.end())
	{
		return true;
	}

	Mesh* mesh = new Mesh(fileName);

	try
	{
		mesh->Load(e_d3dDevice);
	}
	catch(int e)
	{
		mesh->Release();
		delete mesh;
		OutputDebugString(L"Could not load mesh through LoadMesh() function. Error:  " + e);
		return false;
	}
	m_Meshes.insert(std::pair<std::wstring, Mesh*>(fileName, mesh));

	return true;
}

bool ResourceManager::LoadModel(std::wstring folderName)
{
	Mesh* model = GetMesh(L"Assets/Meshes/" + folderName + L"/" + folderName + L".X");
	Texture* modelDIF = GetTexture(L"Assets/Meshes/" + folderName + L"/Diffuse/" + folderName + L"_DIF.jpg");

	model->SetTexture(modelDIF);

	return true;
}

bool ResourceManager::LoadSound(std::wstring fileName)
{

	return true;
}

bool ResourceManager::LoadTexture(std::wstring fileName)
{
	std::map<std::wstring, Texture*>::iterator it;

	it = m_Textures.find(fileName);

	if(it != m_Textures.end())
	{
		return true;
	}

	Texture* txtr = new Texture(fileName);

	try
	{
		txtr->Load(e_d3dDevice);
	}
	catch(int e)
	{
		txtr->Release();
		delete txtr;
		OutputDebugString(L"Could not Load Texture from file. ResourceManager.cpp ln 83 Error: " + e);
		return false;
	}
	m_Textures.insert(std::pair<std::wstring, Texture*>(fileName, txtr));

	return true;
}

bool ResourceManager::LoadSprite(std::wstring fileName, unsigned int width, unsigned int height)
{
	std::map<std::wstring, Sprite*>::iterator it;

	it = m_Sprites.find(fileName);

	if (it != m_Sprites.end())
	{
		return true;
	}

	Sprite* txtr = new Sprite(fileName, width, height);

	try
	{
		txtr->Load(e_d3dDevice);
	}
	catch (int e)
	{
		txtr->Release();
		delete txtr;
		OutputDebugString(L"Could not Load Sprite from file. ResourceManager.cpp ln 127 Error: " + e);
		return false;
	}
	m_Sprites.insert(std::pair<std::wstring, Sprite*>(fileName, txtr));

	return true;
}

Mesh* ResourceManager::GetMesh(std::wstring fileName)
{
	std::wstring checkString(fileName);
	Utility::Strings::ToUpper(checkString);

	if (checkString.compare(_T("DEFAULT")) == 0)
	{
		return m_DefaultMesh;
	}

	std::map<std::wstring, Mesh*>::iterator it;

	it = m_Meshes.find(fileName);

	if(it == m_Meshes.end())
	{
		if(LoadMesh(fileName))
		{
			it = m_Meshes.find(fileName);
			return it->second;
		}
		else
		{
			return m_DefaultMesh;
		}
	}
	else
	{
		return it->second;
	}
}

Mesh* ResourceManager::GetEmptyMesh(std::wstring name)
{
	std::map<std::wstring, Mesh*>::iterator it;

	it = m_Meshes.find(name);

	if (it == m_Meshes.end())
	{
		Mesh* empty = new Mesh(name);
		m_Meshes.insert(std::pair<std::wstring, Mesh*>(name, empty));
		return empty;
	}
	else
	{
		OutputDebugString(L"Could not create empty Mesh Object with that name. One already exists. == ResourceManager.cpp Ln 180");
		return nullptr;
	}
}

Mesh* ResourceManager::GetModel(std::wstring folderName)
{
	std::map<std::wstring, Mesh*>::iterator it;

	it = m_Meshes.find(folderName);

	if(it == m_Meshes.end())
	{
		if(LoadModel(folderName))
		{
			it = m_Meshes.find(L"Assets/Meshes/" + folderName + L"/" + folderName + L".X");
			return it->second;
		}
		else
		{
			return m_DefaultMesh;
		}
	}
	else
	{
		return it->second;
	}
}

//Sound* ResourceManager::GetSound(std::string fileName)
//{
//	std::map<std::string, Sound*>::iterator it;
//
//	it = m_Sounds.find(fileName);
//
//	if(it == m_Sounds.end())
//	{
//		if(LoadSound(fileName))
//		{
//			it = m_Sounds.find(fileName);
//			return it->second;
//		}
//		else
//		{
//			return m_DefaultSound;
//		}
//	}
//	else
//	{
//		return it->second;
//	}
//}

Texture* ResourceManager::GetTexture(std::wstring fileName)
{
	std::wstring checkString(fileName);
	Utility::Strings::ToUpper(checkString);

	if (checkString.compare(_T("DEFAULT")) == 0)
	{
		return m_DefaultTexture;
	}

	std::map<std::wstring, Texture*>::iterator it;

	it = m_Textures.find(fileName);

	if(it == m_Textures.end())
	{
		if(LoadTexture(fileName))
		{
			it = m_Textures.find(fileName);
			return it->second;
		}
		else
		{
			return m_DefaultTexture;
		}
	}
	else
	{
		return it->second;
	}
}

Sprite* ResourceManager::GetSprite(std::wstring fileName)
{
	std::map<std::wstring, Sprite*>::iterator it;

	it = m_Sprites.find(fileName);

	if (it == m_Sprites.end())
	{
		return nullptr;
	}
	else
	{
		return it->second;
	}
}

void ResourceManager::LoadDefaults()
{
	m_DefaultTexture->Load(e_d3dDevice);

	m_DefaultMesh->Load(e_d3dDevice);
	m_DefaultMesh->SetTexture(m_DefaultTexture);

	//m_DefaultSound->Load();
}

void ResourceManager::ReleaseResources()
{
	for(std::map<std::wstring, Mesh*>::iterator it = m_Meshes.begin(); it != m_Meshes.end(); ++it)
	{
		it->second->Release();
		delete it->second;
	}

	m_Meshes.clear();

	for(std::map<std::wstring, Texture*>::iterator it = m_Textures.begin(); it != m_Textures.end(); ++it)
	{
		it->second->Release();
		delete it->second;
	}

	m_Textures.clear();

	for (std::map<std::wstring, Sprite*>::iterator it = m_Sprites.begin(); it != m_Sprites.end(); ++it)
	{
		it->second->Release();
		delete it->second;
	}

	m_Sprites.clear();

	//for(std::map<std::wstring, Sound*>::iterator it = m_Sounds.begin(); it != m_Sounds.end(); ++it)
	//{
	//	it->second->Release();
	//	delete it->second;
	//}
	//
	//m_Sounds.clear();
}

Mesh* ResourceManager::GetDefaultMesh()
{
	return m_DefaultMesh;
}

//Sound* ResourceManager::GetDefaultSound()
//{
//	return m_DefaultSound;
//}

Texture* ResourceManager::GetDefaultTexture()
{
	return m_DefaultTexture;
}