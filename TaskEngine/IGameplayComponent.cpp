#include "EngineCommonStd.h"

#include "IGameplayComponent.h"
#include "Observable.h"

IGameplayComponent::IGameplayComponent() : BaseComponent()
{
	m_Observable = new Observable();
	
	/**
	 * Attach the observer
	 */
	m_Observable->Attach(EventObs);
}

IGameplayComponent::IGameplayComponent(Actor* owner) : BaseComponent(owner)
{

	m_Observable = new Observable();

	/**
	 * Attach the observer
	 */
	m_Observable->Attach(EventObs);
}

IGameplayComponent::~IGameplayComponent()
{
	if (m_Observable)
	{
		delete m_Observable;
		m_Observable = nullptr;
	}
}

void IGameplayComponent::Update(float dt)
{
}