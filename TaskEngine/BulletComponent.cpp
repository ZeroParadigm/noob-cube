#include "EngineCommonStd.h"

#include "BulletComponent.h"
#include "Actor.h"
#include "TransformComponent.h"
#include "MeshComponent.h"
#include "Observable.h"
#include "Enemy.h"
#include "IColliderComponent.h"

using namespace DXMathLibrary;


BulletComponent::BulletComponent() : IGameplayComponent()
{
	m_Type = COMPONENT_BULLET;
	m_Normal = Vector3(0.0f, 0.0f, 0.0f);
	m_Speed = 0.0f;
	m_TimeActive = 0.0f;
	m_Active = false;
}

BulletComponent::BulletComponent(Actor* owner) : IGameplayComponent(owner)
{
	m_Type = COMPONENT_BULLET;
	m_Normal = Vector3(0.0f, 0.0f, 0.0f);
	m_Speed = 0.0f;
	m_TimeActive = 0.0f;
	m_Active = false;
}

BulletComponent::~BulletComponent()
{
}

void BulletComponent::Init()
{
	m_Observable->Attach(EventObs);
	m_Observable->OnEvent(COLLISION, [&](const EventData& data){
		this->HandleCollisionEvent(data.actors.first, data.actors.second);
	});
	m_Observable->OnEvent(PLAYER_DIE, [&](const EventData& data){
		this->Deactivate();
	});
	m_Observable->OnEvent(GAME_OVER, [&](const EventData& data){
		this->Deactivate();
	});
}

void BulletComponent::Update(float dt)
{
	float maxTime = 5.0f;

	if (IsActive())
	{
		Vector3 pos = m_Owner->Transform()->GetPosition();

		pos += (m_Normal * m_Speed) * dt;

		m_Owner->Transform()->SetPosition(pos);

		m_TimeActive += dt;

		if (m_TimeActive >= maxTime)
		{
			Deactivate();
		}
	}
}

void BulletComponent::Activate(DXMathLibrary::Vector3 startPos, DXMathLibrary::Vector3 normal, float speed)
{
	
	m_Observable->Subscribe(COLLISION | GAME_OVER | PLAYER_DIE);

	m_Normal = normal;
	m_Speed = speed;
	m_Active = true;
	m_Owner->Mesh()->SetActive(true);
	m_Owner->Collider()->SetActive(true);

	m_Owner->Transform()->SetPosition(startPos);

	// Orient the bullet here if we are using a mesh and not spheres

}

void BulletComponent::Deactivate()
{
	m_Observable->Unsubscribe();

	m_TimeActive = 0.0f;

	m_Active = false;
	m_Owner->Mesh()->SetActive(false);
	m_Owner->Collider()->SetActive(false);
}

bool BulletComponent::IsActive()
{
	return m_Active;
}

void BulletComponent::HandleCollisionEvent(Actor* first, Actor* second)
{
	if (first == this->GetOwner() || second == this->GetOwner())
	{
		EnemyComponent* enemyA = dynamic_cast<EnemyComponent*>(first->Gameplay());
		EnemyComponent* enemyB = dynamic_cast<EnemyComponent*>(second->Gameplay());

		if (enemyA || enemyB)
		{
			this->Deactivate();
		}
	}
}