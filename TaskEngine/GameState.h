#pragma once

class D3D9RenderDevice;
class MemoryDelegate;
class GameplayFSM;
class DirectInput;
class Gamepad;
class WebSocketClient;


class GameState
{
public:

	MemoryDelegate*	memory;
	D3D9RenderDevice* graphics;
	GameplayFSM*	fsm;
	WebSocketClient* network;
	DirectInput*	input;
	Gamepad*		gamepad;

public:

	GameState(){}
	~GameState(){}

	virtual void EnterState() = 0;
	virtual void LeaveState() = 0;
	virtual void UpdateState(float dt) = 0;

};