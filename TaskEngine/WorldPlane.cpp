#include "EngineCommonStd.h"

#include "WorldPlane.h"

using namespace DXMathLibrary;

WorldPlane::WorldPlane()
{
	m_Point = Vector3(0.0f, 0.0f, 0.0f);
	m_Normal = Vector3(0.0f,0.0f,-1.0f);
	m_Offset = 0;
}

WorldPlane::WorldPlane(Vector3 normal, float offset) : m_Normal(normal), m_Offset(offset)
{
	m_Point = normal * (fabs(offset) + 5);		//Assuming the player radius is 5
	m_Normal.Normalize();
}

WorldPlane::~WorldPlane()
{
}

void WorldPlane::AddPoint(Vector3 p)
{
	m_Points.push_back(p);
}