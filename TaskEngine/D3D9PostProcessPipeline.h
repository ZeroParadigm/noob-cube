#pragma once
#include "IRenderPipeline.h"

#include "D3D9PostProcessFilter.h"
#include "D3D9RenderTargetChain.h"

#include "EngineFileMesh.h"


// RT_COUNT is the number of simultaneous render targets used in the pipeline.
#define RT_COUNT 3

#define PPCOUNT 18

class VertexPP;

class D3D9PostProcessPipeline : public IRenderPipeline
{
friend class D3D9RenderDevice;
private:

	MemoryDelegate*			m_memory;

	IDirect3DDevice9*		m_device;

	D3DSURFACE_DESC			m_backBufferSurfaceDesc;

	////////////////////////////////////////////////////////////

	ID3DXEffect*			m_effect;					// D3DX effect interface

	IDirect3DCubeTexture9*	m_envTex;					// Texture for environment mapping

	Matrix4					m_meshWorld;
	int						m_scene;					// Indicates the scene # to render

	D3DXHANDLE				m_TRenderScene;				// Handle to RenderScene technique
	D3DXHANDLE				m_TRenderEnvMapScene;		// Handle to RenderEnvMapScene technique
	D3DXHANDLE				m_TRenderNoLight;			// Handle to RenderNoLight technique
	D3DXHANDLE				m_TRenderSkyBox;			// Handle to RenderSkyBox technique

	EngineFileMesh			m_sceneMesh[2];				// Mesh objects in the scene
	EngineFileMesh			m_skyBoxMesh;				// Skybox mesh
	D3DFORMAT				m_TexFormat;				// Render target texture format
	IDirect3DTexture9*		m_SceneSave[RT_COUNT];		// To save original scene image before PostProcess
	D3D9RenderTargetChain	m_RTChain[RT_COUNT];		// Render target chain (4 used in sample)

	bool					m_enablePost;				// Whether or not to enable post-processing
	bool					m_pipelineCreated;

	int						m_passes;					// Number of passes required to render scene
	int						m_RTUsed;					// Number of simultaneous RT used to render scene
	int						m_filterCount;

	D3D9RenderTargetSet		m_RTTable[RT_COUNT];		// Table of which RT to use for all passes

	std::unordered_map<int, PP9Filter*> m_availableFilters;
	std::list<PP9FilterInstance> m_activeFilters;

private:

	void UpdateBackbufferDescription();

	HRESULT ApplyPostProcessFilter(D3D9PostProcessFilterInstance filterInst,
		IDirect3DVertexBuffer9* pVB,
		VertexPP* aQuad,
		float& fExtentX,
		float& fExtentY);
	HRESULT PostProcess();

	void AddFilter(int filterID);

public:

	D3D9PostProcessPipeline()
	{
		m_effect = nullptr;					
		m_envTex = nullptr;
			
		m_scene = 0;											

		m_enablePost = true;	
		m_pipelineCreated = false;

		m_passes = 0;					
		m_RTUsed = 0;
		m_filterCount = 0;

		D3D9RenderTargetSet		m_RTTable[RT_COUNT];
	}
	~D3D9PostProcessPipeline()
	{
		if (m_pipelineCreated) Destroy();
	}

	virtual Tstring PipelineName() { return _T("Post Process Pipeline"); }

	virtual void Render();

	virtual HRESULT Init(MemoryDelegate* memory, IDirect3DDevice9* device, IDirect3D9* d3d9);
	virtual void Destroy();

	virtual void OnLostDevice();
	virtual HRESULT OnResetDevice();
};