#include "EngineCommonStd.h"

#include "GameplaySystem.h"

//#include "MemoryDelegate.h"
#include "GameplayFSM.h"
#include "GameStates.h"


GameplaySystem::GameplaySystem()
{
}

GameplaySystem::~GameplaySystem()
{
}

void GameplaySystem::Initialize(D3D9RenderDevice* device, MemoryDelegate* mem, DirectInput* dinput, Gamepad* pad, WebSocketClient* net)
{
	memory = mem;
	fsm = new GameplayFSM(device, mem, dinput, pad, net);

	//fsm->TransitionState(new MainGameState());
	//fsm->TransitionState(new TriggerVolumeTestState());
	fsm->TransitionState(new NetworkingTestState());
}

void GameplaySystem::Shutdown()
{
	delete fsm;
}

void GameplaySystem::Update(float dt)
{
	fsm->UpdateState(dt);

	//Update all gameplay objects
	for (auto obj : memory->GetGameplay())
	{
		obj->Update(dt);
	}
}