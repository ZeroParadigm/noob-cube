#include "EngineCommonStd.h"

#include "PacketReporter.h"


PacketReporter::PacketReporter(){}
	
PacketReporter::~PacketReporter()
{
	ClearRegistry();
}
	
bool PacketReporter::CheckPacket(unsigned int ID)
{
	std::map<unsigned int, PacketAlert*>::iterator it;

	it = m_Registry.find(ID);

	if(it == m_Registry.end())
	{
		return false;
	}
	else
	{
		if(it->second->dirty)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

char* PacketReporter::GetLastPacket(unsigned int ID)
{
	std::map<unsigned int, PacketAlert*>::iterator it;

	it = m_Registry.find(ID);

	if(it == m_Registry.end())
	{
		// Not found in the map
		return 0;
	}
	else
	{
		// ID exists in the map, check if dirty
		if(it->second->dirty)
		{
			// Set dirty to false & return data
			it->second->dirty = false;
			return it->second->data;
		}
		else
		{
			return 0;
		}
	}
}

void PacketReporter::TrackPacket(PacketAlert* p)
{
	std::map<unsigned int, PacketAlert*>::iterator it;

	it = m_Registry.find(p->packetID);

	if(it == m_Registry.end())
	{
		// Not found in the map
		m_Registry.insert(std::pair<unsigned int, PacketAlert*>(p->packetID, p));
	}
	else
	{
		// ID exists in the map, delete old data and replace with new data
		PacketAlert* temp;
		temp = it->second;
		it->second = p;
		delete temp;
	}
}

void PacketReporter::ClearRegistry()
{
	std::map<unsigned int, PacketAlert*>::iterator it;
	for(it = m_Registry.begin(); it != m_Registry.end(); ++it)
	{
		delete it->second;
	}
	m_Registry.clear();
}