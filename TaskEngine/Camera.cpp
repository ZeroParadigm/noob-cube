#include "EngineCommonStd.h"

#include "Camera.h"

using namespace DXMathLibrary;

Camera::Camera()
{
	m_View				= Matrix4();
	m_View(0, 0) = 0.0f;
	m_View(1, 1) = 0.0f;
	m_View(2, 2) = 0.0f;
	m_Proj				= Matrix4();
	m_ViewProj			= Matrix4();

	m_Position			= Vector3(0.0f, 0.0f, 0.0f);
	m_Right				= Vector3(1.0f, 0.0f, 0.0f);
	m_Up				= Vector3(0.0f, 1.0f, 0.0f);
	m_Forward			= Vector3(0.0f, 0.0f, 1.0f);

	m_FixedLookPoint	= Vector3(0.0f, 0.0f, 0.0f);

	m_Speed				= 25.0f;

	m_CameraMode		= DEBUG;	//Default Camera mode is DEBUG

	SetLens(D3DX_PI / 4.0f, 1.0f, 1.0f, 20000.f);
}

Camera::~Camera()
{

}

//Private Functions
//===========================================
void Camera::AdjustFixedCamera(float dt)
{

}

void Camera::RotateFixedCamera(float dt)
{

}

void Camera::AdjustDebugCamera(float dt)
{

}

void Camera::RotateDebugCamera(float dt)
{

}

void Camera::BuildView()
{
	m_Forward.Normalize();

	m_Up = m_Forward.Cross(m_Right);
	m_Up.Normalize();

	m_Right = m_Up.Cross(m_Forward);
	m_Right.Normalize();

	float x = -m_Position.Dot(m_Right);
	float y = -m_Position.Dot(m_Up);
	float z = -m_Position.Dot(m_Forward);

	m_View(0, 0) = m_Right.x;
	m_View(1, 0) = m_Right.y;
	m_View(2, 0) = m_Right.z;
	m_View(3, 0) = x;

	m_View(0, 1) = m_Up.x;
	m_View(1, 1) = m_Up.y;
	m_View(2, 1) = m_Up.z;
	m_View(3, 1) = y;

	m_View(0, 2) = m_Forward.x;
	m_View(1, 2) = m_Forward.y;
	m_View(2, 2) = m_Forward.z;
	m_View(3, 2) = z;

	m_View(0, 3) = 0.0f;
	m_View(1, 3) = 0.0f;
	m_View(2, 3) = 0.0f;
	m_View(3, 3) = 1.0f;
}

void Camera::BuildFixedView()
{

}
//*******************************************

//Public Functions
//===========================================
void Camera::Update(float dt)
{
	switch(m_CameraMode)
	{
	case CONTROLLED:
		{
			//Do nothing
			
			break;
		}
		case FREEFORM:
		{
			//Do nothing
			break;
		}
		case FIXEDPOINT:
		{
			AdjustFixedCamera(dt);
			RotateFixedCamera(dt);
			break;
		}
		case DEBUG:
		{
			//AdjustDebugCamera(dt);
			//RotateDebugCamera(dt);
			break;
		}
	}

	BuildView();

	m_ViewProj = m_View * m_Proj;
}

void Camera::OnResetDevice(float w, float h)
{
	SetLens(m_Fov, w / h, m_NearZ, m_FarZ);
}

void Camera::SetLens(float FOV, float aspect, float nearZ, float farZ)
{
	m_Fov	= FOV;
	m_NearZ	= nearZ;
	m_FarZ	= farZ;

	//m_Frustum.Init(FOV, aspect, nearZ, farZ);

	D3DXMatrixPerspectiveFovLH(&m_Proj, FOV, aspect, nearZ, farZ);

	m_ViewProj = m_View * m_Proj;
}

void Camera::SetLookAt(Vector3 &pos, Vector3 &target, Vector3 &up)
{
	Vector3 F = target - pos;
	F.Normalize();

	Vector3 R = m_Up.Cross(F);
	R.Normalize();

	Vector3 U = F.Cross(R);
	U.Normalize();

	m_Position		= pos;
	m_Right			= R;
	m_Up			= U;
	m_Forward		= F;

	BuildView();

	m_ViewProj = m_View * m_Proj;
}

void Camera::SetLookAtPoint(Vector3 snapPoint)
{

}

void Camera::SetMode(CameraMode mode)
{
	m_CameraMode = mode;
}

void Camera::ApplyInput(Vector3 pos, float pitch, float yaw, float speed)
{
	//Up and Down
	Matrix4 RotateX;
	RotateX.BuildRotationAxis(m_Right, pitch);
	m_Forward.TransformCoord(RotateX);
	m_Up.TransformCoord(RotateX);

	//Left and right
	Matrix4 RotateY;
	RotateY.BuildRotationY(yaw);
	m_Right.TransformCoord(RotateY);
	m_Up.TransformCoord(RotateY);
	m_Forward.TransformCoord(RotateY);

	Vector3 dir;

	//Position Change
	if(pos.z > 0.0f) //Forward
		dir += m_Forward;

	if(pos.z < 0.0f) // backward
		dir -= m_Forward;

	if(pos.x > 0.0f) //strafe right
		dir += m_Right;

	if(pos.x < 0.0f) //strafe left
		dir -= m_Right;

	if(pos.y > 0.0f) //Up
		dir.y += pos.y;

	if(pos.y < 0.0f) //Down
		dir.y -= pos.y;

	dir.Normalize();

	m_Position += dir * speed;

	Update(0.0f);
}

//Frustum Camera::GetFrustum()
//{
//	return m_Frustum;
//}

Vector3& Camera::Position()
{
	return m_Position;
}

Matrix4 Camera::ViewProj()
{
	return m_ViewProj;
}
//*******************************************