#pragma once
#include "BaseComponent.h"

class IPhysicsComponent : public BaseComponent
{
private:

public:

	IPhysicsComponent(void) : BaseComponent() {}
	IPhysicsComponent(Actor* owner) : BaseComponent(owner) {}
	~IPhysicsComponent(void) {}

};