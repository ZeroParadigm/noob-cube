#include "EngineCommonStd.h"

#include "PlayerComponent.h"
#include "WorldPlane.h"
#include "Observable.h"
#include "Observer.h"
#include "Actor.h"
#include "MovementComponent.h"
#include "Gamepad.h"
#include "DirectInput.h"
#include "BulletSystem.h"
#include "Enemy.h"
#include "ScoreSystem.h"

PlayerComponent::PlayerComponent(void)
{
	m_Type = COMPONENT_PLAYER;
	m_CurrentFace = nullptr;
	m_Camera = nullptr;
	m_Gun = nullptr;
	m_ScoreBoard.SetGrowthRate(500.0f);
	m_Timer = 0.0f;
	m_SpawnTime = 1.0f;
	m_ShootTimer = 0.0f;
	m_FireRate = 0.25f;
	m_CanShoot = false;
	m_Active = false;
	m_Lives = 3;
	m_MovePointDistance = 10.0f;
}

PlayerComponent::PlayerComponent(Actor* owner) : IGameplayComponent(owner)
{
	m_Type = COMPONENT_PLAYER;
	m_CurrentFace = nullptr;
	m_Camera = nullptr;
	m_Gun = nullptr;
	m_ScoreBoard.SetGrowthRate(500.0f);
	m_Timer = 0.0f;
	m_SpawnTime = 1.0f;
	m_ShootTimer = 0.0f;
	m_FireRate = 0.25f;
	m_CanShoot = false;
	m_Active = false;
	m_Lives = 3;
	m_MovePointDistance = 10.0f;
}

PlayerComponent::~PlayerComponent(void)
{
	m_CurrentFace = nullptr;
	m_Camera = nullptr;
	m_Gun = nullptr;
}

void PlayerComponent::Init()
{
	//Register for collisions
	m_Observable->Attach(EventObs);
	m_Observable->OnEvent(COLLISION, [&](const EventData& data){
		this->HandleCollisionEvent(data.actors.first, data.actors.second);
	});
	m_Observable->OnEvent(GAME_OVER, [&](const EventData& data){
		//delete this;
	});
	m_Observable->OnEvent(ENEMY_DIE, [&](const EventData& data){
		this->AddScore(5000);
	});
}

void PlayerComponent::Update(float dt)
{
	m_Timer += dt;
	m_ScoreBoard.Update(dt);

	if (!CanShoot())
	{
		m_ShootTimer += dt;
		
		if (m_ShootTimer >= m_FireRate)
		{
			m_CanShoot = true;
		}
	}

	if (IsActive())
	{
		//Controller movement
		XINPUT_STATE gamepad = gGamepad->GetState();
		POINT ls, rs;
		ls.x = gamepad.Gamepad.sThumbLX;
		ls.y = gamepad.Gamepad.sThumbLY;
		Move(ls);

		rs.x = gamepad.Gamepad.sThumbRX;
		rs.y = gamepad.Gamepad.sThumbRY;
		Shoot(rs);

		//Keyboard movement
		ls.x = 0;
		ls.y = 0;

		if (gDInput->KeyDown(DIK_W))
		{
			ls.y = 50000;
		}
		if (gDInput->KeyDown(DIK_S))
		{
			ls.y = -50000;
		}
		if (gDInput->KeyDown(DIK_A))
		{
			ls.x = -50000;
		}
		if (gDInput->KeyDown(DIK_D))
		{
			ls.x = 50000;
		}
		if ((ls.x != 0) || (ls.y != 0))
		{
			Move(ls);
		}

		rs.x = 0;
		rs.y = 0;

		if (gDInput->KeyDown(DIK_I))
		{
			rs.y = 50000;
		}
		if (gDInput->KeyDown(DIK_K))
		{
			rs.y = -50000;
		}
		if (gDInput->KeyDown(DIK_J))
		{
			rs.x = -50000;
		}
		if (gDInput->KeyDown(DIK_L))
		{
			rs.x = 50000;
		}
		if ((rs.x != 0) || (rs.y != 0))
		{
			Shoot(rs);
		}

	}
	else
	{
		if (m_Timer >= m_SpawnTime)
		{
			Activate();
		}
	}
}

void PlayerComponent::Spawn(void)
{
	//Ensure the player is inactive
	m_ShootTimer = 0.0f;
	m_CanShoot = false;
	m_Active = false;

	//Set the current face
	ChangeFace(m_CurrentFace);

	//Set the timer
	m_Timer = 0.0f;
}

void PlayerComponent::Activate(void)
{
	m_Lives--;

	m_Observable->Subscribe(COLLISION | GAME_OVER);

	m_Active = true;
	m_CanShoot = true;

	((MovementComponent*)m_Owner->Ai())->SeekOn();
	((MovementComponent*)m_Owner->Ai())->FacePoint3DOn();
}

void PlayerComponent::Deactivate(void)
{
	m_Active = false;
	m_CanShoot = false;

	m_Observable->Unsubscribe();

	m_Timer = 0.0f;
}

void PlayerComponent::Die(void)
{
	if (m_Lives < 0)
	{
		EventData data(this->m_Owner, this->m_Owner);
		m_Observable->Notify(GAME_OVER, data);
	}
	else
	{
		EventData data(this->m_Owner, this->m_Owner);
		m_Observable->Notify(PLAYER_DIE, data);

		m_Lives--;
	}

	m_Timer = 0.0f;
}

void PlayerComponent::Shoot(POINT p)
{
	if (IsActive())
	{
		if (CanShoot())
		{
			if (labs(p.x) >= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE || labs(p.y) >= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
			{
				LONG x = 0, y = 0;
				if (labs(p.x) > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) x = p.x;
				if (labs(p.y) > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) y = p.y;

				//Fire bullets
				float bulletSpeed = 70.0f;

				Vector3 shootDir((float)x, (float)y, 0);
				shootDir = m_Camera->Transform()->GetOrientation().VectorTransform(shootDir);
				shootDir.Normalize();
				
				Vector3 shootConstrained = shootDir.ProjectVectorOnPlane(m_CurrentFace->m_Normal, shootDir);
				shootConstrained.Normalize();

				Vector3 position = m_Owner->Transform()->GetPosition();

				m_Gun->ShootBullet(m_Owner->Transform()->GetPosition(), shootConstrained, bulletSpeed);

				//Set orientation direction to shoot direction
				position += (10.0f* shootConstrained);

				((MovementComponent*)m_Owner->Ai())->SetTargetPoint3D(position);

				//Set can shoot to false
				m_CanShoot = false;
				m_ShootTimer = 0.0f;
			}
		}
	}
}

void PlayerComponent::Move(POINT p)
{
	if (IsActive())
	{
		Vector3 normal;
		if (m_CurrentFace)
		{
			//Get the face normal
			normal = m_CurrentFace->m_Normal;
		}
		else //In case there is no current face
		{
			normal = m_Owner->Transform()->GetOrientation().Up();
		}
		
		//Ensure the normal is normalized
		normal.Normalize();
		
		//Calculate a dead zone
		LONG x = 0, y = 0;
		if (XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE < labs(p.x)) x = p.x;
		if (XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE < labs(p.y)) y = p.y;

		//Create the movement vector
		Vector3 movement((float)x, (float)y, 0);
		movement.Normalize();

		//Transform it by the camera's orientation
		movement = m_Camera->Transform()->GetOrientation().VectorTransform(movement);

		//Add the movement to the player 
		movement += m_Owner->Transform()->GetPosition();

		//Set direction to plane
		Vector3 point = movement.ProjectPointOnPlane(m_CurrentFace->m_Normal, m_CurrentFace->m_Point, movement);

		//Set the movement point
		((MovementComponent*)m_Owner->Ai())->SetTargetSeek(point);
		((MovementComponent*)m_Owner->Ai())->SetTargetPoint3D(point);
	}
}

WorldPlane* PlayerComponent::GetPlane(void)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_CurrentFace;	
}

void PlayerComponent::ChangeFace(WorldPlane* face)
{
	if (m_CurrentFace)
	{
		Vector3 newVel = -m_CurrentFace->m_Normal;

		Vector3 currVel = m_Owner->Transform()->GetVelocity();
		float magnitude = currVel.LengthSq();

		currVel.Normalize();

		newVel += currVel;

		newVel.Normalize();
		newVel *= magnitude;

		m_Owner->Transform()->SetVelocity(newVel);
	}


	ScopedCriticalSection critical_section(this->m_CriticalSection);
	m_CurrentFace = face;
}

void PlayerComponent::SetCamera(Actor* camera)
{
	m_Camera = camera;
}

void PlayerComponent::AddScore(int points)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	m_ScoreBoard.AddPoints(points);
}

int PlayerComponent::GetScore()
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_ScoreBoard.GetCurrentScore();
}

int PlayerComponent::GetDisplayScore()
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_ScoreBoard.GetDisplayScore();
}

bool PlayerComponent::CanShoot(void)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_CanShoot;
}

bool PlayerComponent::IsActive(void)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_Active;
}

bool PlayerComponent::IsEdging(void)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	return m_Edging;
}

void PlayerComponent::SetEdging(bool edging)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	m_Edging = edging;
}

void PlayerComponent::CameraTransform(Quaternion q, Vector3& p)
{
	/**
	 * Flip the handedness of the orientation
	 */
	Quaternion rot;
	rot.BuildAxisAngle(Vector3(0, 1, 0), pi);

	q *= rot;

	p = q.VectorTransform(p);
}

void PlayerComponent::SetGun(BulletSystem* gun)
{
	m_Gun = gun;
}

void PlayerComponent::HandleCollisionEvent(Actor* first, Actor* second)
{
	if (first == this->GetOwner() || second == this->GetOwner())
	{
		EnemyComponent* enemyA = dynamic_cast<EnemyComponent*>(first->Gameplay());
		EnemyComponent* enemyB = dynamic_cast<EnemyComponent*>(second->Gameplay());

		if (enemyA || enemyB)
		{
			this->Die();
		}
	}
}