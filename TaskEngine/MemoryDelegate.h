#pragma once

#include "Components.h"

#include "ActorFactory.h"
#include "ActorManager.h"
#include "ComponentManager.h"
#include "ResourceManager.h"

#include "DebugCamera.h"

/**
 * Singleton
 *
 * Only Task Engine can delete this
 *
 * Subsystems should be created with a pointer to this heap
 */
class MemoryDelegate
{
friend class SimpleEngine;

protected:

	ActorManager*			m_ActorManager;
	ComponentManager*		m_ComponentManager;
	ResourceManager*		m_ResourceManager;

	ActorFactory*			m_ActorFactory;

	bool m_debugCameraActive;

private:

	MemoryDelegate()
	{
		m_debugCameraActive = false;
	}

protected:

	~MemoryDelegate()
	{
		delete m_DebugCamera;
	}

	//Singleton Pattern
	static MemoryDelegate* instance();

	void init(IDirect3DDevice9* d3dDevice);
	void shutdown();
	
	//ActorManager*		actorManager()		{ return m_ActorManager; }
	ComponentManager*	componentManager()	{ return m_ComponentManager; }
	ResourceManager*	resourceManager()	{ return m_ResourceManager; }

	DebugCamera* m_DebugCamera;

public:

	void ToggleDebugCamera()
	{
		if (!m_debugCameraActive)
		{
			if (m_DebugCamera == nullptr)
				m_DebugCamera = new DebugCamera(0.0f, 0.0f, 0.0f);

			m_debugCameraActive = true;
		}
		else
		{
			m_debugCameraActive = false;
		}
	}

	DebugCamera* GetDebugCamera()
	{
		return m_DebugCamera;
	}

	ActorManager*		actorManager()		{ return m_ActorManager; }//Moved for debugging

	/**
	 * Get the actor factory
	 */
	ActorFactory* actorFactory(void){return m_ActorFactory;}

	/**
	 * @name Actor access
	 *///@{

	/** 
	 * Get all actors
	 */
	std::vector<Actor*> GetActorList()
	{
		return m_ActorManager->m_ActorList;
	}

	/**
	 * Get an actor by id
	 * @param id Id of actor
	 */
	Actor* GetActor(int id)
	{
		return m_ActorManager->GetActor(id);
	}
	//@}


	/**
	 * @name Component access
	 *///@{

	/**
	 * Get all components
	 */
	std::vector<BaseComponent*> GetComponentList()
	{
		return m_ComponentManager->m_ComponentList;
	}

	/**
	 * Get component by Id
	 * @param int Id of the component
	 */
	BaseComponent* GetComponent(int id)
	{
		return m_ComponentManager->GetComponent(id);
	}

	/**
	 * Get all transforms
	 */
	std::vector<TransformComponent*> GetTransforms(void)
	{
		return m_ComponentManager->m_TransformComponents;
	}

	/**
	 * Get all meshes
	 */
	std::vector<MeshComponent*> GetMeshes(void)
	{
		return m_ComponentManager->m_MeshComponents;
	}

	/**
	 * Get all renderers
	 */
	std::vector<IRendererComponent*> GetRenderers(void)
	{
		return m_ComponentManager->m_RendererComponents;
	}

	/**
	 * Get all Ai
	 */
	std::vector<IAIComponent*> GetAi(void)
	{
		return m_ComponentManager->m_AIComponents;
	}

	/**
	 * Get all Movement ai
	 */
	std::vector<MovementComponent*> GetMovement(void)
	{
		return m_ComponentManager->m_MovementComponents;
	}

	/**
	 * Get all physics
	 */
	std::vector<IPhysicsComponent*> GetPhysics(void)
	{
		return m_ComponentManager->m_PhysicsComponents;
	}

	/**
	 * Get all scripts
	 */
	std::vector<BaseComponent*> GetScripts(void)
	{
		return m_ComponentManager->m_ScriptComponents;
	}

	/**
	 * Get all gameplay
	 */
	std::vector<IGameplayComponent*> GetGameplay(void)
	{
		return m_ComponentManager->m_GameplayComponents;
	}
	//@}

	/**
	 * @name Resource access
	 *///@{

	/**
	 * Get a mesh
	 * @param fileName File name of the mesh
	 */
	Mesh* GetMesh(std::wstring fileName)
	{
		return m_ResourceManager->GetMesh(fileName);
	}

	/**
	 * Get a texture
	 * @param fileName File name of the texture
	 */
	Texture* GetTexture(std::wstring fileName)
	{
		return m_ResourceManager->GetTexture(fileName);
	}

	Sprite* GetSprite(std::wstring fileName)
	{
		return m_ResourceManager->GetSprite(fileName);
	}

	bool LoadSprite(std::wstring fileName, unsigned int width, unsigned int height)
	{
		return m_ResourceManager->LoadSprite(fileName, width, height);
	}
	//@}

	/**
	 * @name Camera specific accessors
	 *///@{

	/**
	 * Get the camera
	 */
	CameraComponent* GetCamera(void)
	{

		return (CameraComponent*)m_ActorManager->m_Camera->Renderer();
	}

	void CameraLookAt(const Vector3& pos, const Vector3& target, const Vector3& up)
	{
		
	}

	/**
	 * Get the player
	 */
	Actor* GetPlayer(void)
	{
		return m_ActorManager->m_Player;
	}

	//@}


	std::vector<IColliderComponent*> GetColliders()
	{
		return m_ComponentManager->m_ColliderComponents;
	}

	std::vector<RigidBodyComponent*> GetRigidBodies()
	{
		return m_ComponentManager->m_RigidBodyComponents;
	}

	/**
	 * Actor factory access
	 *///@{

	/**
	 * Create the player
	 */
	Actor* CreatePlayer(void)
	{
		m_ActorManager->m_Player = m_ActorFactory->CreatePlayer();
		return m_ActorManager->m_Player;
	}

	/**
	 * Create an enemy
	 */
	Actor* CreateEnemy(void)
	{
		Actor* enemy = m_ActorFactory->CreateEnemy();

		if (m_ActorManager->GetPlayer())
		{
			((MovementComponent*)enemy->Ai())->SetAgentToPursue(m_ActorManager->GetPlayer()->Transform());
		}

		return enemy;
	}

	//@}

	Actor* GetGameWorld()
	{
		return m_ActorManager->GetWorld();
	}

	//Sound* GetSound(std::string fileName);
	//Texture* GetTexture(std::string fileName);
};