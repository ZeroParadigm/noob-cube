#include "EngineCommonStd.h"

#include "PoolManager.h"
#include "MemoryPool.h"

const static size_t CHUNK_HEADER_SIZE = (sizeof(unsigned char*));


//Takes pointer to memory and it's size
//Takes size of chunks

// chunkSize - size of a chunk in bytes
// blockSize - size of a block in bytes
// numChunks - convenient number of chunks to allocate = (blockSize / chunkSize)
auto AllocateBlockHelper = [](unsigned char* rawMem, size_t chunkSize, size_t blockSize, unsigned int numChunks)
{
	//define how to setup linked list
	auto CreateLinkedList = [&](unsigned char* start, unsigned char* end)
	{
		unsigned char* current = start;

		while (current < end)
		{
			unsigned char* next = current + chunkSize;

			unsigned char** chunkHeader = (unsigned char**)current;
			chunkHeader[0] = (next < end ? next : NULL);

			current += chunkSize;
		}
	};

	//Use a thread for each 256 chunks in a block
	int threads = numChunks / 256;

	if (threads >= 2)
	{
		std::list<std::thread> workers;
		for (int t = 0; t < threads; ++t)
		{
			workers.push_back(std::thread([&](){
				CreateLinkedList(0, 0);
			}));
		}

		//wait for workers to finish
		for (auto& worker : workers) worker.join();
	}
	else
	{

	}
};






MemoryPool::MemoryPool()
{
	Reset();
}

MemoryPool::~MemoryPool()
{
	Destroy();
}

#pragma region Public API

bool MemoryPool::Init(int ChunkSize, int InitialSize, bool canGrow, bool isManaged /*= false*/)
{
	if (m_rawMemoryPool)
		Destroy();

	m_chunkSize		= ChunkSize;	//Size in bytes that each chunk takes
	m_blockSize	= InitialSize;	//Used for initial size and growth amount each GrowMemoryPool() call

	m_canGrow		= canGrow;		//Determines if MemoryPool can grow
	m_isManaged		= isManaged;	//Manages self or is managed

	if (m_isManaged)
		m_manager = POOL_MANAGER.Attach(this);

	if (GrowMemoryPool())
		return true;

	ENGINE_WARNING(L"Failed to initialize memory MemoryPool");
	return false;
}

void MemoryPool::Destroy()
{
#ifdef _DEBUG
	if (m_numAllocs != 0)
	{
		unsigned long totalNumChunks = m_blockSize * m_blockCount;
		unsigned long wastedMem = (totalNumChunks - m_peakAlloc) * m_chunkSize;

		OUTPUT(L"***(" + std::to_wstring(m_numAllocs) + L")");
		OUTPUT(L"Destroying memory MemoryPool: [" + GetDebugPoolName() + L":" + std::to_wstring((unsigned long)m_chunkSize) + L"] = " + std::to_wstring(m_peakAlloc) + L"/" + std::to_wstring((unsigned long)totalNumChunks) + L" (" + std::to_wstring(wastedMem) + L" bytes wasted)\n");
	}
#endif

	for (unsigned int i = 0; i < m_blockCount; ++i)
	{
		free(m_rawMemoryPool[i]);
	}
	free(m_rawMemoryPool);

	Reset();
}

void* MemoryPool::Alloc(void)
{
	ScopedCriticalSection lock(m_cs);

	if (!m_head)
	{
		if (!m_canGrow)
		{
#ifdef _DEBUG
			OUTPUT(L"Allocation <" + GetDebugPoolName() + L">: FAIL - Not allowed to grow.");
#endif
			return nullptr;
		}

		if (!GrowMemoryPool())
		{
#ifdef _DEBUG
			OUTPUT(L"Allocation <" + GetDebugPoolName() + L">: FAIL - Could not grow MemoryPool.");
#endif
			return nullptr;
		}
	}

#ifdef _DEBUG
	++m_numAllocs;
	if (m_numAllocs > m_peakAlloc)
		m_peakAlloc = m_numAllocs;

	OUTPUT(L"Allocation <" + GetDebugPoolName() + L">: " + std::to_wstring(m_numAllocs) + L"/" + std::to_wstring(m_chunkCount));
#endif

	unsigned char* retPtr = m_head;

	m_head = GetNext(m_head);

	return (retPtr + CHUNK_HEADER_SIZE);
}

void MemoryPool::Free(void* mem)
{
	ScopedCriticalSection lock(m_cs);

	if (mem != nullptr)
	{
		unsigned char* block = ((unsigned char*)mem) - CHUNK_HEADER_SIZE;

		SetNext(block, m_head);
		m_head = block;

#ifdef _DEBUG
		--m_numAllocs;
		OUTPUT(L"Free <" + GetDebugPoolName() + L">: " + std::to_wstring(m_numAllocs) + L"/" + std::to_wstring(m_chunkCount));
		ENGINE_ASSERT(m_numAllocs >= 0);
#endif
	}
}


#ifdef _DEBUG

void MemoryPool::SetDebugPoolName(const TCHAR* poolName) 
{ 
	m_poolName = poolName; 
}
std::wstring MemoryPool::GetDebugPoolName() 
{ 
	return m_poolName; 
}

#else

void MemoryPool::SetDebugPoolName(const char* poolName) {}
std::wstring MemoryPool::GetDebugPoolName() { return L"<Debug Disabled>"; }

#endif

#pragma endregion

#pragma region Private API

void MemoryPool::Reset()
{
	m_rawMemoryPool = nullptr;
	m_head = nullptr;

	m_chunkSize = 0;
	m_chunkCount = 0;
	m_blockSize = 0;
	m_blockCount = 0;

	m_canGrow = true;

#ifdef _DEBUG
	m_peakAlloc = 0;
	m_numAllocs = 0;
#endif

}

bool MemoryPool::GrowMemoryPool()
{
#ifdef _DEBUG
	std::wstring output;

	output += L"Growing memory MemoryPool ";
	output += (m_canGrow) ? L"" : L"(LOCKED) ";
	output += L"<" + GetDebugPoolName() + L">...";
	OUTPUT(output);
	output = L"";
#endif

	size_t allocationSize = sizeof(unsigned char*)* (m_blockCount + 1);
	unsigned char** newMemoryPool = (unsigned char**)malloc(allocationSize);

	if (!newMemoryPool)
	{
#ifdef _DEBUG
		output += L"FAILED to grow memory MemoryPool<" + GetDebugPoolName() + L">";
		OUTPUT(output);
#endif
		ENGINE_WARNING(L"FAILED to grow memory MemoryPool<" + GetDebugPoolName() + L">");
		return false;
	}

	// copy exisiting memory pointers
	for (unsigned int i = 0; i < m_blockCount; ++i)
	{
		newMemoryPool[i] = m_rawMemoryPool[i];
	}

	newMemoryPool[m_blockCount] = AllocateNewBlock();

	if (m_head)
	{
		unsigned char* currentHead = m_head;
		unsigned char* next = GetNext(m_head);
		while (next)
		{
			currentHead = next;
			next = GetNext(next);
		}

		SetNext(currentHead, newMemoryPool[m_blockCount]);
	}
	else
	{
		m_head = newMemoryPool[m_blockCount];
	}

	if (m_rawMemoryPool)
		free(m_rawMemoryPool);

	m_rawMemoryPool = newMemoryPool;
	++m_blockCount;

#ifdef _DEBUG

	output += L"SUCCESS: ";
	output += L"(Growth Amount : " + std::to_wstring(m_blockSize) + L") -> ";
	output += L"[ChunkSize : " + std::to_wstring(m_chunkSize) + L"] X ";
	output += L"[ChunkCount : " + std::to_wstring(m_chunkCount) + L"] = ";
	output += L"[PoolSize : " + Utility::Strings::bytesToHumanFriendly(GetAllocatedSize()) + L"]\n";

	OUTPUT(output);
#endif

	if (m_isManaged) m_manager->Inform(m_poolID, NOTIFYPOOL_GROWTH, GetAllocatedSize());

	return true;
}

unsigned char* MemoryPool::AllocateNewBlock()
{
	size_t trueChunkSize = m_chunkSize + CHUNK_HEADER_SIZE;
	size_t trueBlockSize = trueChunkSize * m_blockSize;

	m_chunkCount += m_blockSize;

	unsigned char* newMem = (unsigned char*)malloc(trueBlockSize);
	if (!newMem)
	{
		ENGINE_WARNING(L"Failed to allocate block for MemoryPool<" + GetDebugPoolName() + L">");
		return NULL;
	}

	AllocateBlockHelper(newMem, trueChunkSize, trueBlockSize, m_blockSize);

	unsigned char* end = newMem + trueBlockSize;
	unsigned char* current = newMem;
	while (current < end)
	{
		unsigned char* next = current + trueChunkSize;

		unsigned char** ChunkHeader = (unsigned char**)current;
		ChunkHeader[0] = (next < end ? next : NULL);

		current += trueChunkSize;
	}

	return newMem;
}

unsigned char* MemoryPool::GetNext(unsigned char* block)
{
	unsigned char** ChunkHeader = (unsigned char**)block;
	return ChunkHeader[0];
}

void MemoryPool::SetNext(unsigned char* blockToChange, unsigned char* newNext)
{
	unsigned char** chucnkHeader = (unsigned char**)blockToChange;
	chucnkHeader[0] = newNext;
}

#pragma endregion