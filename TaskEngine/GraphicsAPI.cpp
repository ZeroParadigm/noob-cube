#include "EngineCommonStd.h"

#include "GraphicsAPI.h"
#include "GraphicsSystem.h"

#include "GraphicsUtility.h"

GraphicsAPI::GraphicsAPI()
{
	core = new GraphicsSystem();
}

GraphicsAPI::~GraphicsAPI()
{
	delete core;
}

void GraphicsAPI::init(MemoryDelegate* memory, HINSTANCE appInstance, const TCHAR* appCaption, int drawRate, bool stats)
{
	core->init(memory, appInstance, appCaption, drawRate, stats);
}

void GraphicsAPI::shutdown()
{
	core->shutdown();
}

void GraphicsAPI::update(float dt)
{
	core->update(dt);
}

bool GraphicsAPI::isDeviceLost()
{
	return core->isDeviceLost();
}

HWND GraphicsAPI::GetHandle()
{
	return core->getMainWnd();
}

IDirect3DDevice9* GraphicsAPI::GetDevice()
{
	return g_d3dDevice;
}