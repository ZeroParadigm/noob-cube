#include "EngineCommonStd.h"

#include "NetworkingAPI.h"
#include "NetworkingCore.h"

NetworkingAPI::NetworkingAPI()
{
	core = new NetworkingCore();
}

NetworkingAPI::~NetworkingAPI()
{
	delete core;
}

bool NetworkingAPI::Connect(std::string ipaddr, std::string port)
{
	return core->Connect(ipaddr, port);
}

bool NetworkingAPI::IsConnected()
{
	return core->IsConnected();
}

bool NetworkingAPI::Send(char* data, unsigned int size)
{
	return core->Send(data, size);
}

void NetworkingAPI::Disconnect()
{
	core->Disconnect();
}

bool NetworkingAPI::CheckPacket(unsigned int packetID)
{
	return core->CheckPacket(packetID);
}

char* NetworkingAPI::GetLastPacket(unsigned int packetID)
{
	return core->GetLastPacket(packetID);
}