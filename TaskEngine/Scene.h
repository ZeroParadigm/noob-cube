#pragma once


class MemoryDelegate;
class IRenderDevice;
class RenderWindow;
class Actor;

class Scene
{
public:

enum eDeviceType
{
	DEVICE_NULL = 0,
	DEVICE_D3D9,
	DEVICE_D3D911,
	DEVICE_COUNT,
};

private:

	MemoryDelegate* m_memory;
	RenderWindow*	m_window;
	IRenderDevice*	m_device;

	bool			m_active;
	bool			m_haveContext;

public:

	Scene()
	{
		m_window		= nullptr;
		m_device		= nullptr;
		m_memory		= nullptr;

		m_active		= false;
		m_haveContext	= false;
	}
	~Scene(){}

	//Standard system layout
	void Init(MemoryDelegate* memory, 
				eDeviceType type, 
				float desiredDrawRate, 
				bool calculateStats, 
				HINSTANCE appInstance, 
				const TCHAR* caption, 
				long x, long y, 
				long w, long h);

	void Shutdown();
	void Update(double dt);

	RenderWindow* GetWindow();
	IRenderDevice* GetDevice();
};