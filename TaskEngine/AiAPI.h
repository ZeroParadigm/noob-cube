/**
 * AiAPI - Defines the API used by the rest of the system
 *
 * Author - Jesse Dillon (2013)
 */

#pragma once

class AiCore;
class MemoryDelegate;

class AiAPI
{
private:

	AiCore*		m_Core;

public:

	AiAPI(void);
	~AiAPI(void);

	/**
	 * Init
	 * 
	 * Initialize the AI system
	 */
	void Init(MemoryDelegate* memory);

	/**
	 * Shutdown
	 *
	 * Shut down the AI system
	 */
	void Shutdown(void);


	/**
	 * Update - Update all registered AI components
	 *
	 * @dt - time elapsed since last update
	 */
	void Update(float dt);
};