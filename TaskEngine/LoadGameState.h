#pragma once

#include "GameState.h"

class PlayerComponent;


class LoadGameState : public GameState
{
private:

public:

	LoadGameState();
	~LoadGameState();

	void EnterState() override;
	void LeaveState() override;
	void UpdateState(float dt) override;

private:

	void CreateGUI();

};