#include "EngineCommonStd.h"

#include "CameraComponent.h"
#include "Actor.h"
#include "TransformComponent.h"

CameraComponent::CameraComponent(Actor* owner) : IRendererComponent(owner)
{
	if (owner)	m_Position = m_Owner->Transform()->GetPositonPtr();
	else	    m_Position = nullptr;

	m_Right		= Vector3(1.0f, 0.0f, 0.0f);
	m_Up		= Vector3(0.0f, 1.0f, 0.0f);
	m_Forward	= Vector3(0.0f, 0.0f, 1.0f);

	m_dirty		= true;

	SetLens(45.0f, 1.0f, 0.1f, 10000.0f);

	//BuildView(); - this should work itself out later in the code init and run phases
}

CameraComponent::~CameraComponent()
{

}

void CameraComponent::OnResetDevice(float w, float h)
{
	SetLens(m_Fov, w / h, m_NearZ, m_FarZ);
}

void CameraComponent::SetLens(float FOV, float aspect, float nearZ, float farZ)
{
	m_Fov = FOV;
	m_NearZ = nearZ;
	m_FarZ = farZ;

	m_frustum.Init(FOV, aspect, nearZ, farZ);

	D3DXMatrixPerspectiveFovLH(&m_Proj, FOV, aspect, nearZ, farZ);

	m_ViewProj = m_View * m_Proj;
}

Matrix4 CameraComponent::GetWorldMatrix()
{
	Matrix4 worldMatrix;

	worldMatrix.BuildRotationQuat(m_Owner->Transform()->GetOrientation());
	worldMatrix.SetPosition(m_Owner->Transform()->GetPosition());

	return worldMatrix;
}

void CameraComponent::Update(float dt)
{
	BuildView();

	m_ViewProj = m_View * m_Proj;
}

Vector3 CameraComponent::GetPosition(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return *m_Position;
}

Vector3 CameraComponent::GetForward(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Forward;
}

Vector3 CameraComponent::GetUp(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Up;
}

Vector3 CameraComponent::GetRight(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Right;
}

Matrix4 CameraComponent::Projection()
{
	return m_Proj;
}

Matrix4 CameraComponent::View()
{
	return m_View;
}

Matrix4 CameraComponent::ViewProjection()
{
	return m_ViewProj;
}

void CameraComponent::LookAt(Vector3 target)
{
	ScopedCriticalSection(this->m_CriticalSection);
	Vector3 F = target - *m_Position;
	F.Normalize();

	Vector3 R = m_Up.Cross(F);
	R.Normalize();

	Vector3 U = F.Cross(R);
	U.Normalize();

	m_Right			= R;
	m_Up			= U;
	m_Forward		= F;

	BuildView();

	m_ViewProj = m_View * m_Proj;
}

void CameraComponent::LookAt(Vector3 pos, Vector3 target, Vector3 up)		//TODO 
{
	ScopedCriticalSection(this->m_CriticalSection);
	Vector3 F = target - pos;
	F.Normalize();

	Vector3 R = m_Up.Cross(F);
	R.Normalize();

	Vector3 U = F.Cross(R);
	U.Normalize();

	*m_Position		= pos;
	m_Right			= R;
	m_Up			= U;
	m_Forward		= F;

	BuildView();

	m_ViewProj = m_View * m_Proj;
}

void CameraComponent::BuildView()
{
	TransformComponent* transform = m_Owner->Transform();
	
	Vector3 forward, up, right, pos;

	forward = transform->GetOrientation().Forward();
	up		= transform->GetOrientation().Up();
	right	= transform->GetOrientation().Right();

	forward.Normalize();
	up.Normalize();
	right.Normalize();

	float x, y, z;

	pos = transform->GetPosition();

	x = -pos.Dot(right);
	y = -pos.Dot(up);
	z = -pos.Dot(forward);

	//Protect only the shared resource manipulation
	ScopedCriticalSection critical_section(this->m_CriticalSection);

	//m_View = transform->GetOrientation().BuildMatrix();

	m_View(0, 0) = right.x;
	m_View(1, 0) = right.y;
	m_View(2, 0) = right.z;
	m_View(3, 0) = x;

	m_View(0, 1) = up.x;
	m_View(1, 1) = up.y;
	m_View(2, 1) = up.z;
	m_View(3, 1) = y;

	m_View(0, 2) = forward.x;
	m_View(1, 2) = forward.y;
	m_View(2, 2) = forward.z;
	m_View(3, 2) = z;

	m_View(0, 3) = 0.0f;
	m_View(1, 3) = 0.0f;
	m_View(2, 3) = 0.0f;
	m_View(3, 3) = 1.0f;

	//ScopedCriticalSection(this->m_CriticalSection);
	//m_Forward.Normalize();

	//m_Up = m_Forward.Cross(m_Right);
	//m_Up.Normalize();

	//m_Right = m_Up.Cross(m_Forward);
	//m_Right.Normalize();

	//float x = -m_Position->Dot(m_Right);
	//float y = -m_Position->Dot(m_Up);
	//float z = -m_Position->Dot(m_Forward);

	//m_View(0, 0) = m_Right.x;
	//m_View(1, 0) = m_Right.y;
	//m_View(2, 0) = m_Right.z;
	//m_View(3, 0) = x;

	//m_View(0, 1) = m_Up.x;
	//m_View(1, 1) = m_Up.y;
	//m_View(2, 1) = m_Up.z;
	//m_View(3, 1) = y;

	//m_View(0, 2) = m_Forward.x;
	//m_View(1, 2) = m_Forward.y;
	//m_View(2, 2) = m_Forward.z;
	//m_View(3, 2) = z;

	//m_View(0, 3) = 0.0f;
	//m_View(1, 3) = 0.0f;
	//m_View(2, 3) = 0.0f;
	//m_View(3, 3) = 1.0f;
}