#include "EngineCommonStd.h"

#include "ParticleComponent.h"

ParticleComponent::ParticleComponent(void)
{

}

ParticleComponent::ParticleComponent(Actor* owner) : IPhysicsComponent(owner)
{
	m_Type = COMPONENT_PARTICLEPHYSICS;

	m_linearDamping = 0.95;

	m_inverseMass = 1.0/10.0;
	m_mass = 10.0;

	m_motion = 0.0;
	m_wakeStatus = true;
	m_isAwake = true;
	m_canSleep = false;
}

ParticleComponent::~ParticleComponent(void)
{

}