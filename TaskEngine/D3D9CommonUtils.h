#pragma once

//typedef D3DXCOLOR Color;

enum RenderPass
{
	RenderPass_0,
	RenderPass_static = RenderPass_0,
	RenderPass_Actor,
	RenderPass_Sky,
	RenderPass_NotRendered,
	RenderPass_Last
};

enum NodeType
{
	NodeType_Root,
	NodeType_Camera,
	NodeType_SceneNode,
	NodeType_D3D9,
	NodeType_D3D9_Mesh,
	NodeType_D3D9_Particle
};

//extern Color g_White;
//extern Color g_Black;
//extern Color g_Cyan;
//extern Color g_Red;
//extern Color g_Green;
//extern Color g_Blue;
//extern Color g_Yellow;
//extern Color g_Gray40;
//extern Color g_Gray25;
//extern Color g_Gray65;
//extern Color g_Transparent;

//extern Vector3 g_Up;
//extern Vector3 g_Right;
//extern Vector3 g_Forward;
//
//extern Vector4 g_Up4;
//extern Vector4 g_Right4;
//extern Vector4 g_Forward4;

//extern const float fOPAQUE;
//extern const int iOPAQUE;
//extern const float fTRANSPARENT;
//extern const int iTRANSPARENT;



//Material
//class Material
//{
//private:
//
//	D3DMATERIAL9 m_D3D;
//
//public:
//
//	Material()
//	{
//		ZeroMemory(&m_D3D, sizeof(D3DMATERIAL9));
//
//		m_D3D.Diffuse = g_White;
//		m_D3D.Ambient = Color(0.10f, 0.10f, 0.10f, 1.0f);
//		m_D3D.Specular = g_White;
//		m_D3D.Emissive = g_Black;
//	}
//
//	//Material Setters
//	void			SetAmbient(const Color &color)						{ m_D3D.Ambient = color; }
//	void			SetDiffuse(const Color &color)						{ m_D3D.Diffuse = color; }
//	void			SetSpecular(const Color&color, const float power)	{ m_D3D.Specular = color; m_D3D.Power = power; }
//	void			SetEmissive(const Color &color)						{ m_D3D.Emissive = color; }
//
//	void			SetAlpha(const float alpha)							{ m_D3D.Diffuse.a = alpha; }
//
//	//Material getters
//	const Color		GetEmissive()										{ return m_D3D.Emissive; }
//	float			GetAlpha() const									{ return m_D3D.Diffuse.a; }
//
//	//Material checking
//	bool			HasAlpha() const									{ return m_D3D.Diffuse.a != fOPAQUE; }
//
//	void			UseMaterial9()
//	{
//		//g_d3dDevice->SetMaterial(&m_D3D);
//	}
//
//	D3DMATERIAL9	GetD3D9()
//	{
//		return m_D3D;
//	}
//};

//Lights
//struct Light
//{
//	Color ambient;
//	Color diffuse;
//	Color specular;
//};

//struct DirectionalLight : public Light
//{
//	Vector3 directionWorld;
//};

//struct DirectionalLight
//{
//	Color		ambient;
//	Color		diffuse;
//	Color		specular;
//	//Vector3		directionWorld;
//};
//
//struct PointLight : public Light
//{
//	//Vector3		posW;
//	float		range;
//};