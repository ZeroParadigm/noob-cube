#include "EngineCommonStd.h"

#include "MeshComponent.h"


MeshComponent::MeshComponent(Actor* owner)
{
	m_Owner = owner;
	m_Active = true;
}

MeshComponent::MeshComponent(Actor* owner, std::wstring name) : BaseComponent(owner)
{
	m_Owner = owner;
	m_Active = true;
	m_Mesh->m_fileName = name;
}

MeshComponent::~MeshComponent()
{

}

void MeshComponent::DrawSubset(int subset)
{
	m_Mesh->DrawSubset(subset);
}

void MeshComponent::SetMesh(Mesh* m)
{
	m_Mesh = m;
}

void MeshComponent::SetActive(bool active)
{
	m_Active = active;
}

std::wstring MeshComponent::GetMeshName()
{ 
	return m_Mesh->GetFileName(); 
}

Mesh* MeshComponent::GetMesh()
{ 
	return m_Mesh;
}

bool MeshComponent::IsActive()
{
	return m_Active;
}