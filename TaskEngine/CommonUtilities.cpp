#include "EngineCommonStd.h"
#include "CommonUtilities.h"

#include <io.h>
#include <fcntl.h>

namespace Utility
{

	void EnableConsole()
	{
		AllocConsole();

		HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
		int hCrt = _open_osfhandle((long)handle_out, _O_TEXT);
		FILE* hf_out = _fdopen(hCrt, "w");
		setvbuf(hf_out, NULL, _IONBF, 1);
		*stdout = *hf_out;

		HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
		hCrt = _open_osfhandle((long)handle_in, _O_TEXT);
		FILE* hf_in = _fdopen(hCrt, "r");
		setvbuf(hf_in, NULL, _IONBF, 128);
		*stdin = *hf_in;
	}

	//	Strings utilities
	//===============================================================

	std::wstring Strings::stringToWideString(const std::string& s)
	{
		if (s == "")
			return L"";

		int len;
		int slength = (int)s.length();
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		std::wstring r(len, L'\0');
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, &r[0], len);
		return r;
	}

	std::string Strings::wideStringToString(const std::wstring& s)
	{
		if (s == L"")
			return "";

		int len;
		int slength = (int)s.length();
		len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
		std::string r(len, '\0');
		WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, &r[0], len, 0, 0);
		return r;
	} 

	std::wstring Strings::bytesToHumanFriendly(unsigned int bytes)
	{
		float highestConversion = (float)bytes;
		int upCasts = 0;

		while (highestConversion >= 1024)
		{
			upCasts++;
			highestConversion /= 1024.0;
		}

		TCHAR c[10];
		if ((int)highestConversion == highestConversion)
		{
			_snwprintf_s(c, sizeof(c), L"%d", (int)highestConversion);
		}
		else
		{
			_snwprintf_s(c, sizeof(c), L"%.2f", highestConversion);
		}

		std::wstring output(c);

		switch (upCasts)
		{
		case 0: // Byte
			output += L"B";
			break;
		case 1: // Kilobyte
			output += L"KB";
			break;
		case 2: // Megabyte
			output += L"MB";
			break;
		case 3: // Gigabyte
			output += L"GB";
			break;
		case 4: // Terabyte
			output += L"TB";
			break;
		case 5: // Petabyte
			output += L"PB";
			break;
		default:
			break;
		}

		return output;
	}

	tstring Strings::ToUpper(tstring& s)
	{
		transform(s.begin(), s.end(), s.begin(), toupper);
		return s;
	}

	//===============================================================

	
	//	Filesystem utilities
	//===============================================================

	TCHAR* AssetSearchPath()
	{
		static TCHAR s_assetSearchPath[MAX_PATH] = { 0 };
		return s_assetSearchPath;

	}

	bool FindFileSearchTypicalDirs(TCHAR* searchPath, 
								   int sizeofSearchPath, 
								   const TCHAR* leaf,
								   TCHAR* exePath, 
								   TCHAR* exeName)
	{
		// Search directories:
		//      .\
		//      ..\
		//      ..\..\
		//      %EXE_DIR%\
		//      %EXE_DIR%\..\
		//      %EXE_DIR%\..\..\
		//      %EXE_DIR%\..\%EXE_NAME%
		//      %EXE_DIR%\..\..\%EXE_NAME%
		//      DXSDK media path

		// Search in .\  
		wcscpy_s(searchPath, sizeofSearchPath, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in ..\  
		swprintf_s(searchPath, sizeofSearchPath, L"..\\%s", leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in ..\..\ 
		swprintf_s(searchPath, sizeofSearchPath, L"..\\..\\%s", leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in ..\..\ 
		swprintf_s(searchPath, sizeofSearchPath, L"..\\..\\%s", leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in the %EXE_DIR%\ 
		swprintf_s(searchPath, sizeofSearchPath, L"%s\\%s", exePath, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in the %EXE_DIR%\..\ 
		swprintf_s(searchPath, sizeofSearchPath, L"%s\\..\\%s", exePath, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in the %EXE_DIR%\..\..\ 
		swprintf_s(searchPath, sizeofSearchPath, L"%s\\..\\..\\%s", exePath, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in "%EXE_DIR%\..\%EXE_NAME%\".  This matches the DirectX SDK layout
		swprintf_s(searchPath, sizeofSearchPath, L"%s\\..\\%s\\%s", exePath, exeName, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in "%EXE_DIR%\..\..\%EXE_NAME%\".  This matches the DirectX SDK layout
		swprintf_s(searchPath, sizeofSearchPath, L"%s\\..\\..\\%s\\%s", exePath, exeName, leaf);
		if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
			return true;

		// Search in media search dir 
		WCHAR* s_searchPath = AssetSearchPath();
		if (s_searchPath[0] != 0)
		{
			swprintf_s(searchPath, sizeofSearchPath, L"%s%s", s_searchPath, leaf);
			if (GetFileAttributes(searchPath) != 0xFFFFFFFF)
				return true;
		}

		return false;
	}

	bool FindFileSearchParentDirs(TCHAR* searchPath, 
								  int sizeofSearchPath, 
								  TCHAR* startAt,
								  TCHAR* leafName)
	{
		WCHAR fullPath[MAX_PATH] = { 0 };
		WCHAR fullFileName[MAX_PATH] = { 0 };
		WCHAR search[MAX_PATH] = { 0 };
		WCHAR* filePart = NULL;

		GetFullPathName(startAt, MAX_PATH, fullPath, &filePart);
		if (filePart == NULL)
			return false;

		while (filePart != NULL && *filePart != '\0')
		{
			swprintf_s(fullFileName, MAX_PATH, L"%s\\%s", fullPath, leafName);
			if (GetFileAttributes(fullFileName) != 0xFFFFFFFF)
			{
				wcscpy_s(searchPath, sizeofSearchPath, fullFileName);
				return true;
			}

			swprintf_s(search, MAX_PATH, L"%s\\..", fullPath);
			GetFullPathName(search, MAX_PATH, fullPath, &filePart);
		}

		return false;
	}

	HRESULT Filesystem::FindAssetFileFromCommonLocs(TCHAR* pathToFile,
		int sizeofPathToFile,
		const TCHAR* filename)
	{
		bool isFound;
		TCHAR searchFor[MAX_PATH];

		if (filename == NULL || filename[0] == 0 || pathToFile == NULL || sizeofPathToFile < 10)
			return E_INVALIDARG;

		//Get base executable file path
		TCHAR exePath[MAX_PATH] = { 0 };
		TCHAR exeName[MAX_PATH] = { 0 };
		TCHAR* lastSlash = NULL;

		GetModuleFileName(NULL, exePath, MAX_PATH);
		exePath[MAX_PATH - 1] = 0;
		lastSlash = _tcsrchr(exePath, _T('\\'));

		if (lastSlash)
		{
			_tcscpy_s(exeName, MAX_PATH, &lastSlash[1]);

			// Remove the exe name from the exe path
			*lastSlash = 0;

			// Remove the .exe from the exe name
			lastSlash = _tcsrchr(exeName, _T('.'));

			if (lastSlash)
				*lastSlash = 0;
		}

		// Typical directories:
		//      .\
				//      ..\
				//      ..\..\
				//      %EXE_DIR%\
				//      %EXE_DIR%\..\
				//      %EXE_DIR%\..\..\
				//      %EXE_DIR%\..\%EXE_NAME%
		//      %EXE_DIR%\..\..\%EXE_NAME%

		// basic directory search
		if (FindFileSearchTypicalDirs(pathToFile, sizeofPathToFile, filename, exePath, exeName))
			return S_OK;

		// basic directory search but look in 'assets'
		_stprintf_s(searchFor, MAX_PATH, _T("assets\\%s"), filename);
		if (FindFileSearchTypicalDirs(pathToFile, sizeofPathToFile, searchFor, exePath, exeName))
			return S_OK;

		TCHAR leafName[MAX_PATH] = { 0 };

		// search all parent directories starting at .\ for Filename
		_tcscpy_s(leafName, MAX_PATH, filename);
		if (FindFileSearchParentDirs(pathToFile, sizeofPathToFile, _T("."), leafName))
			return S_OK;

		// search all parent directories starting at exe's dir for Filename
		if (FindFileSearchParentDirs(pathToFile, sizeofPathToFile, exePath, leafName))
			return S_OK;

		// search all parent directories starting at .\ and using "assets\Filename"
		_stprintf_s(leafName, MAX_PATH, _T("assets\\%s"), filename);
		if (FindFileSearchParentDirs(pathToFile, sizeofPathToFile, _T("."), leafName))
			return S_OK;

		// search all parent directories starting at exe's dir for "assets\Filename"
		if (FindFileSearchParentDirs(pathToFile, sizeofPathToFile, exePath, leafName))
			return S_OK;

		// on failure, return the file as the path as well as error code
		_tcscpy_s(pathToFile, sizeofPathToFile, filename);

		return DXUTERR_MEDIANOTFOUND;
	}

	HRESULT Filesystem::SetAssetSearchPath(const TCHAR* path)
	{
		HRESULT hr = S_OK;

		TCHAR* s_searchPath = AssetSearchPath();

		hr = _tcscpy_s(s_searchPath, MAX_PATH, path);
		if (SUCCEEDED(hr))
		{
			size_t ch;
			ch = _tcslen(s_searchPath);
			if (SUCCEEDED(hr) && s_searchPath[ch - 1] != _T('\\'))
			{
				hr = _tcscat_s(s_searchPath, MAX_PATH, _T("\\"));
			}
		}

		return hr;
	}

	const TCHAR* Filesystem::GetAssetSearchPath()
	{
		return AssetSearchPath();
	}

	std::vector<tstring> Filesystem::GetDirectoryContents(tstring directory, tstring filter, std::vector<tstring> &contents)
	{
		WIN32_FIND_DATA ffd;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		tstring spec = directory + _T("\\") + filter;

		hFind = FindFirstFile(spec.c_str(), &ffd);

		do {
			if ((tstring(ffd.cFileName) != _T(".")) && (tstring(ffd.cFileName) != _T(".."))){
				if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
					//ignore files
					contents.push_back(directory + _T("\\") + tstring(ffd.cFileName));
				}
				else
				{
					contents.push_back(directory + _T("\\") + tstring(ffd.cFileName));
				}
			}
		} while (FindNextFile(hFind, &ffd) != 0);

		FindClose(hFind);

		return contents;
	}

	//===============================================================
}