#pragma once

class PhysicsSystem;
class MemoryDelegate;

class  PhysicsAPI
{
private:

	PhysicsSystem* core;

public:

	PhysicsAPI();
	~PhysicsAPI();

	void init(MemoryDelegate *memory);
	void shutdown();

	void update(float dt);
};