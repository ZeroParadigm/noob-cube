#pragma once

#include "NetworkingStructs.h"


struct PacketAlert
{
	unsigned int packetID;
	char data[512];
	unsigned int size;
	bool dirty;
};

class PacketReporter
{
	friend class Client;

private:

	std::map<unsigned int, PacketAlert*> m_Registry;

public:

	PacketReporter();
	~PacketReporter();

	bool CheckPacket(unsigned int ID);
	char* GetLastPacket(unsigned int ID);

protected:

	void TrackPacket(PacketAlert* p);

	void ClearRegistry();

};