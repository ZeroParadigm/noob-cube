#pragma once

#include "IGameplayComponent.h"


class BulletComponent : public IGameplayComponent
{
	friend class BulletSystem;

protected:

	DXMathLibrary::Vector3	m_Normal;

	float					m_Speed;

	float					m_TimeActive;

	bool					m_Active;

public:

	BulletComponent();
	BulletComponent(Actor* owner);
	~BulletComponent();

	void Init();

	void Update(float dt) override;

	void Activate(DXMathLibrary::Vector3 startPos, DXMathLibrary::Vector3 normal, float speed);

	void Deactivate();

	bool IsActive();

private:

	void HandleCollisionEvent(Actor* first, Actor* second);

};