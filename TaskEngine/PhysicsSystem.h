#pragma once
//#include "MemoryDelegate.h"

class ContactGenerator;

class PhysicsSystem
{
friend class PhysicsAPI;
private:

	MemoryDelegate *m_memory;

	double m_FixedDT;

	ContactGenerator* m_Generator;

protected:

	PhysicsSystem(){}
	~PhysicsSystem(){}

	void init(MemoryDelegate* memory);
	void shutdown();

	void update(float dt);

};