#pragma once

#define NUM_PARAMS 2
#define RT_COUNT 3

class D3D9PostProcessFilter
{
friend class D3D9PostProcessPipeline;
private:

	ID3DXEffect*	m_effect;
	D3DXHANDLE		m_PPTechnique;
	int				m_renderTargetChannel;
	D3DXHANDLE		m_TextureSource[4];
	D3DXHANDLE		m_TextureScene[4];
	bool			m_write[4];
	bool			m_isTemporal;
	bool			m_usesDT;
	//Needed?
	WCHAR			m_awszParamName[NUM_PARAMS][MAX_PATH]; // Names of changeable parameters
	WCHAR			m_awszParamDesc[NUM_PARAMS][MAX_PATH]; // Description of parameters

	D3DXHANDLE		m_Param[NUM_PARAMS];
	D3DXHANDLE		m_TemporalVariable;
	int				m_ParamSize[NUM_PARAMS];

	D3DXVECTOR4		m_paramDef[NUM_PARAMS];

public:

	D3D9PostProcessFilter()
	{
		m_effect = nullptr;
		m_PPTechnique = NULL;
		m_renderTargetChannel = 0;

		m_isTemporal = false;
		m_usesDT = false;

		ZeroMemory(m_TextureSource, sizeof(m_TextureSource));
		ZeroMemory(m_TextureScene, sizeof(m_TextureScene));
		ZeroMemory(m_write, sizeof(m_write));
		ZeroMemory(m_Param, sizeof(m_Param));
		ZeroMemory(m_awszParamName, sizeof(m_awszParamName));
		ZeroMemory(m_awszParamDesc, sizeof(m_awszParamDesc));
		ZeroMemory(m_ParamSize, sizeof(m_ParamSize));
		ZeroMemory(m_paramDef, sizeof(m_paramDef));
		ZeroMemory(&m_TemporalVariable, sizeof(m_TemporalVariable));
	}
	~D3D9PostProcessFilter()
	{
		Cleanup();
	}

	HRESULT Init(LPDIRECT3DDEVICE9 device, DWORD flags, const TCHAR* name)
	{
		HRESULT hr;
		
		LPD3DXBUFFER XLog = 0;
		hr = D3DXCreateEffectFromFile(device, name, NULL, NULL, flags, NULL, &m_effect, &XLog);
		if (XLog)
			MessageBox(0, (TCHAR*)XLog->GetBufferPointer(), 0, 0);
		if (FAILED(hr))
			return hr;

		m_PPTechnique = m_effect->GetTechniqueByName("PostProcess");

		// Get handles to effect texture objects
		m_TextureScene[0] = m_effect->GetParameterByName(NULL, "g_txSceneColor");
		m_TextureScene[1] = m_effect->GetParameterByName(NULL, "g_txSceneNormal");
		m_TextureScene[2] = m_effect->GetParameterByName(NULL, "g_txScenePosition");
		m_TextureScene[3] = m_effect->GetParameterByName(NULL, "g_txSceneVelocity");

		m_TextureSource[0] = m_effect->GetParameterByName(NULL, "g_txSrcColor");
		m_TextureSource[1] = m_effect->GetParameterByName(NULL, "g_txSrcNormal");
		m_TextureSource[2] = m_effect->GetParameterByName(NULL, "g_txSrcPosition");
		m_TextureSource[3] = m_effect->GetParameterByName(NULL, "g_txSrcVelocity");

		D3DXTECHNIQUE_DESC techdesc;
		if (FAILED(m_effect->GetTechniqueDesc(m_PPTechnique, &techdesc)))
			return D3DERR_INVALIDCALL;
		
		for (DWORD i = 0; i < techdesc.Passes; ++i)
		{
			D3DXPASS_DESC passdesc;
			if (SUCCEEDED(m_effect->GetPassDesc(m_effect->GetPass(m_PPTechnique, i), &passdesc)))
			{
				D3DXSEMANTIC sem[MAXD3DDECLLENGTH];
				UINT count;
				if (SUCCEEDED(D3DXGetShaderOutputSemantics(passdesc.pPixelShaderFunction, sem, &count)))
				{
					while (count--)
					{
						if (D3DDECLUSAGE_COLOR == sem[count].Usage &&
							RT_COUNT > sem[count].UsageIndex)
							m_write[count] = true;
					}
				}
			}
		}

		D3DXHANDLE anno;
		// Obtain temporal settings handles, if any.
		LPCSTR temporalVarName;
		anno = m_effect->GetAnnotationByName(m_PPTechnique, "TemporalVar");
		if (anno && SUCCEEDED(m_effect->GetString(anno, &temporalVarName)))
		{
			m_TemporalVariable = m_effect->GetParameterByName(NULL, temporalVarName);

			m_usesDT = (strcmp(temporalVarName, "FX_DT") == 0) ? true : false;

			m_isTemporal = true;
		}

		// obtain the render target channel
		anno = m_effect->GetAnnotationByName(m_PPTechnique, "nRenderTarget");
		if (anno)
			m_effect->GetInt(anno, &m_renderTargetChannel);

		// obtain the handles to the changeable paramaters, if any
		for (int i = 0; i < NUM_PARAMS; ++i)
		{
			char name[32];
			sprintf_s(name, 32, "Parameter%d", i);
			anno = m_effect->GetAnnotationByName(m_PPTechnique, name);

			const char* paramName;
			if (anno && SUCCEEDED(m_effect->GetString(anno, &paramName)))
			{
				m_Param[i] = m_effect->GetParameterByName(NULL, paramName);
				MultiByteToWideChar(CP_ACP, 0, paramName, -1, m_awszParamDesc[i], MAX_PATH);
			}

			// Get the parameter description
			sprintf_s(name, 32, "Parameter%dDesc", i);
			anno = m_effect->GetAnnotationByName(m_PPTechnique, name);
			if (anno && SUCCEEDED(m_effect->GetString(anno, &paramName)))
			{
				MultiByteToWideChar(CP_ACP, 0, paramName, -1, m_awszParamDesc[i], MAX_PATH);
			}

			// Get the parameter size
			sprintf_s(name, 32, "Parameter%dSize", i);
			anno = m_effect->GetAnnotationByName(m_PPTechnique, name);
			if (anno)
				m_effect->GetInt(anno, &m_ParamSize[i]);

			// Get the paramater default
			sprintf_s(name, 32, "Parameter%dDef", i);
			anno = m_effect->GetAnnotationByName(m_PPTechnique, name);
			if (anno)
				m_effect->GetVector(anno, &m_paramDef[i]);
		}

		return S_OK;
	}

	void Cleanup()
	{
		SAFE_RELEASE(m_effect);
	}

	HRESULT OnLostDevice()
	{
		ENGINE_ASSERT(m_effect);
		m_effect->OnLostDevice();

		return S_OK;
	}

	HRESULT OnResetDevice(DWORD width, DWORD height)
	{
		ENGINE_ASSERT(m_effect);
		m_effect->OnResetDevice();

		D3DXHANDLE paramToConvert;
		D3DXHANDLE annotation;
		UINT paramIndex = 0;

		// If a top-level parameter has the "ConvertPixelsToTexels" annotation,
		// do the conversion.
		while (NULL != (paramToConvert = m_effect->GetParameter(NULL, paramIndex++)))
		{
			if (NULL != (annotation = m_effect->GetAnnotationByName(paramToConvert, "ConvertPixelsToTexels")))
			{
				const char* source;
				m_effect->GetString(annotation, &source);
				D3DXHANDLE convertSource = m_effect->GetParameterByName(NULL, source);

				if (convertSource)
				{
					// Kernel source exists. Proceed.
					// Retrieve the kernel size
					D3DXPARAMETER_DESC desc;
					m_effect->GetParameterDesc(convertSource, &desc);

					// Each element has 2 floats
					DWORD cKernel = desc.Bytes / (2 * sizeof(float));

					D3DXVECTOR4* kernel = new D3DXVECTOR4[cKernel];
					if (!kernel)
						return E_OUTOFMEMORY;

					m_effect->GetVectorArray(convertSource, kernel, cKernel);

					// Convert
					for (DWORD i = 0; i < cKernel; ++i)
					{
						kernel[i].x = kernel[i].x / width;
						kernel[i].y = kernel[i].y / height;
					}

					// Copy back
					m_effect->SetVectorArray(paramToConvert, kernel, cKernel);

					delete[] kernel;
				}
			}
		}

		return S_OK;
	}
};

typedef D3D9PostProcessFilter PP9Filter;

class D3D9PostProcessFilterInstance
{
friend class D3D9PostProcessPipeline;
private:

	D3DXVECTOR4 m_Param[NUM_PARAMS];
	D3D9PostProcessFilter* m_filter;

public:

	D3D9PostProcessFilterInstance()
	{
		m_filter = nullptr;

		ZeroMemory(m_Param, sizeof(m_Param));
	}
};

typedef D3D9PostProcessFilterInstance PP9FilterInstance;