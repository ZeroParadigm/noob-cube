#pragma once

#include "GameState.h"

class PlayerComponent;

class WindowElement;
class StaticTextElement;
class StaticImageElement;
class ButtonElement;


class TriggerVolumeTestState : public GameState
{
private:

	std::default_random_engine generator;

	float m_TimeElapsed;

	Actor* gameworld;
	PlayerComponent* m_Player;

	WindowElement*		GUI_MainWindow;
	StaticTextElement* GUI_PlayerScore;

public:

	TriggerVolumeTestState();
	~TriggerVolumeTestState();

	void EnterState() override;
	void LeaveState() override;
	void UpdateState(float dt) override;

private:

	void CreateCamera(void);
	void CreateWorld(void);
	void CreatePlayer(void);

	void StartCamera(void);

	void CreateGUI();

	void CreateTestTriggerVolumes();
	void CreateWorldTriggerVolumes();

	bool EdgeTraversal(IGameplayComponent* comp, int face);
};