#pragma once

class IRenderPipeline
{
protected:

	virtual void Render() = 0;

	virtual HRESULT Init(MemoryDelegate* memory, IDirect3DDevice9* device, IDirect3D9* d3d9) = 0;
	virtual void Destroy() = 0;

	virtual std::wstring PipelineName() = 0;

	virtual void OnLostDevice() = 0;
	virtual HRESULT OnResetDevice() = 0;
};