#include "EngineCommonStd.h"

#include "GameWorldComponent.h"
#include "Actor.h"


GameWorldComponent::GameWorldComponent() : IGameplayComponent()
{
	m_World = new WorldPolygon();
}

GameWorldComponent::GameWorldComponent(Actor* owner) : IGameplayComponent(owner)
{
	m_World = new WorldPolygon();
}

GameWorldComponent::~GameWorldComponent()
{
	delete m_World;
}

void GameWorldComponent::Update(float dt)
{
}

void GameWorldComponent::MakePlane()
{
	m_World->CreatePlaneBuffer(m_Owner->Mesh());
}

void GameWorldComponent::MakePyramid()
{
	m_World->CreatePyramidBuffer(m_Owner->Mesh());
}

void GameWorldComponent::MakeCube()
{
	m_World->CreateCubeBuffer(m_Owner->Mesh());
}