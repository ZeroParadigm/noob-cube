/**
 * Observer - The observer accepts events and notifies subscribers 
 *
 * Author - Jesse Dillon
 */
#pragma once

//#include <vector>

#define EventObs Observer::Instance()

class Actor;
class Observable;

enum EVENT
{
	NONE           = 0x00,

	PLAYER_DIE     = 0x01,
	ENEMY_DIE      = 0x02,

	INPUT_LS       = 0x04,
	INPUT_RS       = 0x08,
	INPUT_LB       = 0x10,
	INPUT_RB       = 0x20,
	INPUT_LT       = 0x40,
	INPUT_RT       = 0x80,
	INPUT_A	       = 0x100,
	INPUT_B        = 0x200,
	INPUT_X        = 0x400,
	INPUT_Y		   = 0x800,
	INPUT_START    = 0x1000,
	INPUT_BACK     = 0x2000,
	INPUT_KEYPRESS = 0x4000,
	INPUT_MOUSE1   = 0x8000,
	INPUT_MOUSE2   = 0x10000,
	INPUT_MOUSE3   = 0x20000,

	GAME_START     = 0x40000,
	GAME_OVER      = 0x80000,

	COLLISION      = 0x100000,

	SELF_ACTIVATE  = 0x200000
};

typedef  std::pair<Actor*, Actor*> actor_pair;
typedef  std::pair<int, bool>      input_pair;

struct EventData
{
	actor_pair actors;
	input_pair input;

	EventData(Actor* a, Actor* b){actors.first = a; actors.second = b;}
	EventData(int Event, bool activity){input.first = Event; input.second = activity;}
};

class Observer
{
private:

	/**
	 * A list of all observables which have subscribed
	 */
	std::vector<Observable*>		m_Subscribers;

public:

	Observer(void);
	~Observer(void);

	static Observer* Instance(void);

	/**
	 * Notify
	 *
	 * Notify subscribers of an event
	 *
	 * @event type
	 * @event data
	 */
	void Notify(EVENT type, EventData data);

	/**
	 * Subscribe
	 *
	 * Subscribes an observable to this observer
	 *
	 * @events - Flags for the events to which the subscriber is interested
	 * @observable - Pointer to the subscribing observer
	 */
	void Subscribe(Observable* subscriber);

	/**
	 * Unsubscribe
	 *
	 * Unsubscribe an observable
	 *
	 * @param Observable* Pointer to the observable
	 */
	void Unsubscribe(Observable* subscriber);
};