#pragma once

class EngineFileMesh
{
public:
	TCHAR							m_name[512];
	ID3DXMesh*						m_mesh;

	IDirect3DVertexBuffer9*			m_VB;
	IDirect3DIndexBuffer9*			m_IB;
	IDirect3DVertexDeclaration9*	m_VertDecl;

	DWORD							m_numVerts;
	DWORD							m_numFaces;
	DWORD							m_bytesPerVertex;

	DWORD							m_numMaterials;
	D3DMATERIAL9*					m_materials;
	CHAR							(*m_strMaterials)[MAX_PATH];
	IDirect3DBaseTexture9**			m_textures;

	bool							m_useMaterials;

public:

	EngineFileMesh(const TCHAR* name = _T("EngineFile_Mesh"));
	virtual ~EngineFileMesh();

	HRESULT Release();

	// Initializing
	HRESULT RestoreDeviceObjects(IDirect3DDevice9* device);
	HRESULT InvalidateDeviceObjects();

	// Creation/destruction
	HRESULT Create(IDirect3DDevice9* device, const TCHAR* fileName);
	HRESULT Create(IDirect3DDevice9* device, LPD3DXFILEDATA fileData);
	HRESULT Create(IDirect3DDevice9* device,
					ID3DXMesh* toUse,
					D3DXMATERIAL* d3dxMaterials,
					DWORD materials);

	HRESULT CreateMaterials(const TCHAR* strPath,
							IDirect3DDevice9* device,
							D3DXMATERIAL* d3dxMaterials,
							DWORD numMaterials);

	//Rendering options
	void SetUseMeshMaterials(bool use)
	{
		m_useMaterials = use;
	}

	HRESULT SetFVF(IDirect3DDevice9* device, DWORD fvf);
	HRESULT SetVertexDecl(IDirect3DDevice9* device,
							const IDirect3DVertexDeclaration9* decl,
							bool autoComputeNormals = true,
							bool autoComputeTangents = true,
							bool splitVertexForOptimalTangents = false);

	ID3DXMesh* GetMesh()
	{
		return m_mesh;
	}

	HRESULT Render(IDirect3DDevice9* device, 
				   bool drawOpaqueSubsets = true,
				   bool drawAlphaSubsets = true);

	HRESULT Render(ID3DXEffect* effect,
					D3DXHANDLE texture = NULL,
					D3DXHANDLE diffuse = NULL,
					D3DXHANDLE ambient = NULL,
					D3DXHANDLE specular = NULL,
					D3DXHANDLE emissive = NULL,
					D3DXHANDLE power = NULL,
					bool drawOpaqueSubsets = true,
					bool drawAlphaSubsets = true);
};