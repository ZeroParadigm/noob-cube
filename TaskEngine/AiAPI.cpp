#include "EngineCommonStd.h"

#include "AiAPI.h"
#include "AiCore.h"
//#include "MemoryDelegate.h"

AiAPI::AiAPI(void)
{
	m_Core = new AiCore();
}

AiAPI::~AiAPI(void)
{
	delete m_Core;
}

void AiAPI::Init(MemoryDelegate* memory)
{
	m_Core->Initialize(memory);
}

void AiAPI::Shutdown(void)
{
	m_Core->Shutdown();
}

void AiAPI::Update(float dt)
{
	m_Core->Update(dt);
}

