#include "EngineCommonStd.h"

#include "Enemy.h"
#include "Observable.h"
#include "Actor.h"
//#include "MemoryDelegate.h"

EnemyComponent::EnemyComponent(void) : IGameplayComponent()
{
	m_Type = COMPONENT_ENEMY;
	m_Timer = 0.0f;
	m_SpawnTime = 1.0f;	
	m_Active = false;
	m_Dead = false;
}

EnemyComponent::EnemyComponent(Actor* owner) : IGameplayComponent(owner)
{
	m_Type = COMPONENT_ENEMY;
	m_Timer = 0.0f;
	m_SpawnTime = 1.0f;
	m_Active = false;
	m_Dead = false;
}

EnemyComponent::~EnemyComponent(void)
{
}

void EnemyComponent::Init()
{
	m_Observable->Attach(EventObs);
	m_Observable->OnEvent(COLLISION, [&](const EventData& data){
		this->HandleCollisionEvent(data.actors.first, data.actors.second);
	});
	m_Observable->OnEvent(PLAYER_DIE, [&](const EventData& data){
		this->Die();
	});
}

void EnemyComponent::Spawn(void)
{
	if (m_Active)
	{
		m_Active = false;
	}
	if (m_Dead)
	{
		m_Dead = false;
	}

	m_Timer = 0.0f;

	m_Owner->Collider()->SetActive(false);
}

void EnemyComponent::Die(void)
{
	m_Active = false;
	m_Dead = true;

	m_Owner->Mesh()->SetActive(false);
	m_Owner->Collider()->SetActive(false);

	EventData data(this->GetOwner(), this->GetOwner());
	m_Observable->Notify(ENEMY_DIE, data);
}

void EnemyComponent::Update(float dt)
{
	m_Timer += dt;

	if (!m_Active && !m_Dead)
	{
		if (m_Timer >= m_SpawnTime)
		{
			Activate();
		}
	}
}

void EnemyComponent::Activate(void)
{
	m_Active = true;
	m_Timer = 0.0f;
	m_Owner->Collider()->SetActive(true);

	/**
	 * Subscribe to events
	 */
	m_Observable->Subscribe(COLLISION | PLAYER_DIE);

	/**
	 * Seek the player
	 */
	((MovementComponent*)m_Owner->Ai())->PursueOn();
	((MovementComponent*)m_Owner->Ai())->FaceForward3DOn();
	
	/*
	 * Avoid walls
	 */
	((MovementComponent*)m_Owner->Ai())->WallAvoidanceOn();
}

void EnemyComponent::HandleCollisionEvent(Actor* first, Actor* second)
{
	if (first == this->GetOwner() || second == this->GetOwner())
	{
		PlayerComponent* playerA = dynamic_cast<PlayerComponent*>(first->Gameplay());
		PlayerComponent* playerB = dynamic_cast<PlayerComponent*>(second->Gameplay());

		if (playerA || playerB)
		{
			this->Die();
			return;
		}

		BulletComponent* bulletA = dynamic_cast<BulletComponent*>(first->Gameplay());
		BulletComponent* bulletB = dynamic_cast<BulletComponent*>(second->Gameplay());

		if (bulletA || bulletB)
		{
			this->Die();
			return;
		}
	}
}