#pragma once

#pragma message("Compiling Engine PCH")

/*
	Feature set defines
	comment / uncomment these defines to enable
	or disable specific feature of the engine
*/
//===============================================

#define DEPRECATED_IS_ERROR			//if not defined deprecations are treated as warnings.

#define USE_DIRECTX_9
//#define USE_DIRECTX_10
//#define USE_DIRECTX_11

//#define USE_OPENGL

#define USE_ENGINE_MATH
#define USE_ENGINE_GEOMETRY

//===============================================

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

//#define NOMINMAX		//uncomment to disable OS min max macros
#include <Windows.h>
#include <windowsx.h>


class ENGINE_noncopyable
{
private:
	ENGINE_noncopyable(const ENGINE_noncopyable& x);
	ENGINE_noncopyable& operator=(const ENGINE_noncopyable& x);
public:
	ENGINE_noncopyable() {};
};

//C files
//#include <stdlib.h>
//#include <malloc.h>
#include <tchar.h>
//#include <stdio.h>

#include <mmsystem.h>

//STL files
#include <atomic>
#include <thread>
#include <future>
#include <functional>
#include <memory>
#include <algorithm>
#include <iostream>
#include <random>
#include <chrono>
#include <sstream>
#include <fstream>



//STL Containers
#include <string>
#include <list>
#include <vector>
#include <queue>
#include <map>
#include <unordered_map>



//DirectX
#include <d3d9.h>
#include <d3dx9.h>
//#include <DxErr.h>



//Engine submodules
#include "Singleton.h"
#include "JSONParser.h"
#include "DebugHelp.h"
#include "CriticalSection.h"
#include "CommonUtilities.h"
#include "Clock.h"

#include "Geometry.h"



#ifdef USE_ENGINE_MATH
#if defined(USE_DIRECTX_9) || defined(USE_DIRECTX_10) || defined(USE_DIRECTX_11)
//#include "EngineDXMath.h"		// NYI - ultimately keep this
#include "MathLibrary.h"		// Use this for now
#elif defined(USE_OPENGL)
#include "EngineGLMath.h"		// NYI
#else
#include "EngineMath.h"			// Use custom math library
#endif
#endif


#ifdef USE_ENGINE_GEOMETRY
#if defined(USE_DIRECTX_9) || defined(USE_DIRECTX_10) || defined(USE_DIRECTX_11)
//#include "EngineDXGeometry.h"	// NYI - ultimately keep this
#include "EngineGeometry.h"
#elif defined(USE_OPENGL)
#include "EngineGLGeometry.h"	// NYI
#else
#include "EngineGeometry.h"		// Use custom math library for custom geometry
#endif
#endif


//Custom Includes
#include "MemoryDelegate.h"


/*
*	Below follows the Defines that are available
*	in this environment.
*/



//#undef NDEBUG		//uncommment to replicate deployment build define



#ifdef _DEBUG
#define _new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define DBG_NEW _new
#else
#define _new new
#define DBG_NEW new
#endif



#define SAFE_DELETE(x)			{if(x){ delete x; x=NULL;}}
#define SAFE_DELETE_ARRAY(x)	{if (x){ delete [] x; x=NULL; }}
#define SAFE_RELEASE(x)			{if(x){ (x)->Release(); (x)=NULL;}}



#define RADIANS_TO_DEGREES(r) ((r) * 57.295779513082325225835265587527f)
#define DEGREES_TO_RADIANS(d) ((d) * 0.01745329251994329444444444444444f)



//Ommiting this section since character 
// string encoding does not need to support
// multi types

#ifdef UNICODE
#define _tcssprintf wsprintf
#define tcsplitpath _wsplitpath
#define Tstring std::wstring
#else
#define _tcssprintf sprintf
#define tcsplitpath _splitpath
#define Tstring std::string
#endif



#ifdef DEPRECATED_IS_ERROR
#pragma warning(error : 4996)
#endif

//	DEPRECATED(void MyFunction(int var1)); -- applies warning in output on build
#define DEPRECATED(msg) __declspec(deprecated(msg))


#define __FILENAMEA__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define __FILENAMEW__ Utility::Strings::stringToWideString(__FILENAMEA__).c_str()

#define __FUNCNAMEA__ __FUNCTION__
#define __FUNCNAMEW__ Utility::Strings::stringToWideString(__FUNCTION__).c_str()

#if defined(UNICODE)
#define __FILENAME__ __FILENAMEW__
#define __FUNCNAME__ __FUNCNAMEW__
#else
#define __FILENAME__ __FILENAMEA__
#define __FUNCNAME__ __FUNCNAMEA__
#endif


//Wrap functionality with this macro  
//	to time its execution
#define TIME_THIS(x) Utility::time_call([&](){x})

//These will behave as Errors and Warnings
//	They will show up in the error list and
//	 affect realtime compilation
#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define __ERROR_LOC__ __FILE__ "("__STR1__(__LINE__)") : Error TODO: "
#define __WARNING_LOC__ __FILE__ "("__STR1__(__LINE__)") : Warning TODO: "
#define ERROR_TODO(x) __pragma(message(__ERROR_LOC__#x))
#define WARNING_TODO(x) __pragma(message(__WARNING_LOC__#x))

//ERROR_TODO(This is an injected Error)
//WARNING_TODO(This is an injected Warning)



//Memory Defines
#define KILOBYTE 1024
#define MEGABYTE KILOBYTE * 1024

#define MEMORY_LOGGING 1