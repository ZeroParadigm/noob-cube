#include "EngineCommonStd.h"

#include "Audio.h"
#include <fmod_errors.h>

#if defined(DEBUG) | defined(_DEBUG)
	void FMOD_ERRCHECK(FMOD_RESULT result)
	{
		if (result != FMOD_OK)
		{
			MessageBox(HWND_DESKTOP, (wchar_t*)FMOD_ErrorString(result), L"FMOD Error", MB_OK | MB_ICONEXCLAMATION);
		}
	}
#else
	void FMOD_ERRCHECK(FMOD_RESULT result)
	{
	}
#endif

Audio::Audio(void)
{
	m_MasterVolume = 100.0f;
	m_MusicVolume = 100.0f;
	m_SoundVolume = 100.0f;
}

Audio::~Audio(void)
{
}

void Audio::Initialize(void)
{
	FMOD_RESULT result;

	// Create the system
	result = FMOD::System_Create(&m_System);
	FMOD_ERRCHECK(result);

	// Check the version
	unsigned int version;
	result = m_System->getVersion(&version);
	FMOD_ERRCHECK(result);

	if (version < FMOD_VERSION)
	{
		MessageBox(HWND_DESKTOP, L"This system is using an old version of FMOD, please update FMOD.", L"FMOD Error", MB_OK);
	}
	
	// Get the number of sound cards and check the capabilities

	FMOD_CAPS caps;
	FMOD_SPEAKERMODE speakerMode;
	int numDrivers;
	result = m_System->getNumDrivers(&numDrivers);
	FMOD_ERRCHECK(result);

	// No sound card -> disable sound
	if (numDrivers == 0)
	{
		result = m_System->setOutput(FMOD_OUTPUTTYPE_NOSOUND);
		FMOD_ERRCHECK(result);
	}
	else
	{
		// Get the capabilities of the default card
		result = m_System->getDriverCaps(0, &caps, 0, &speakerMode);
		FMOD_ERRCHECK(result);

		// Set the speaker mode of the system to be the same as that of the sound card
		result = m_System->setSpeakerMode(speakerMode);
		FMOD_ERRCHECK(result);
	}

	// Increase buffer size if user has Acceleration slider set to off
	if (caps & FMOD_CAPS_HARDWARE_EMULATED)
	{
		result = m_System->setDSPBufferSize(1024, 10);
		FMOD_ERRCHECK(result);
	}

	// Get the name of the card
	char name[256];
	result = m_System->getDriverInfo(0, name, 256, 0);
	FMOD_ERRCHECK(result);

	// Set the software format if the device is a SigmaTel device
	if (name == "SigmaTel")
	{
		result = m_System->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR);
		FMOD_ERRCHECK(result);
	}

	// Initialize FMOD
	result = m_System->init(100, FMOD_INIT_NORMAL, 0);
	
	// If the selected speakermode isn't supported by this sound card, set the mode back to stereo
	if (result == FMOD_ERR_OUTPUT_CREATEBUFFER)
	{
		result = m_System->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
		FMOD_ERRCHECK(result);

		result = m_System->init(100, FMOD_INIT_NORMAL, 0);
		FMOD_ERRCHECK(result);
	}
	else
	{
		FMOD_ERRCHECK(result);
	}
}

void Audio::Shutdown(void)
{
	m_System->release();
}

void Audio::LoadSound(const char* fileName, bool looped)
{
	FMOD_RESULT result;

	FMOD::Sound* newSound;
	result = m_System->createSound(fileName, FMOD_DEFAULT, 0, &newSound);
	FMOD_ERRCHECK(result);

	if (!looped)
	{
		result = newSound->setMode(FMOD_LOOP_OFF | FMOD_2D);
	}
	else
	{
		result = newSound->setMode(FMOD_LOOP_NORMAL | FMOD_2D);
	}

	FMOD_ERRCHECK(result);

	m_Sounds.insert(std::make_pair(fileName, newSound));
}

void Audio::LoadMusic(const char* fileName, bool looped)
{
	FMOD_RESULT result;

	FMOD::Sound* tempSound;

	if (!looped)
	{
		result = m_System->createStream(fileName, FMOD_LOOP_OFF | FMOD_2D |
			FMOD_HARDWARE, 0, &tempSound);
	}
	else
	{
		result = m_System->createStream(fileName, FMOD_LOOP_NORMAL | FMOD_2D |
			FMOD_HARDWARE, 0, &tempSound);
	}

	FMOD_ERRCHECK(result);

	m_Sounds.insert(std::make_pair(fileName, tempSound));
}

void Audio::PlaySample(const char* fileName)
{
	FMOD_RESULT result;

	FMOD::Sound* tempSound;

	if (m_Sounds.find(fileName) == m_Sounds.end())
	{
		LoadSound(fileName, false);
	}

	tempSound = m_Sounds.find(fileName)->second;

	result = m_System->playSound(FMOD_CHANNEL_FREE, tempSound, 0,
		&m_SoundChannel);

	m_SoundChannel->setVolume(0.5f);
}

void Audio::PlayMusic(const char* fileName, bool looped)
{
	FMOD_RESULT result;

	auto tempSound = m_Sounds.find(fileName);

	if (tempSound == m_Sounds.end())
	{
		LoadMusic(fileName, true);
	}

	result = m_System->playSound(FMOD_CHANNEL_FREE, m_Sounds.at(fileName), 0, &m_MusicChannel);
}

void Audio::StopMusic(const char* fileName)
{
	FMOD_RESULT result;

	result = m_MusicChannel->stop();
}

void Audio::ReleaseSound(const char* fileName)
{
	auto result = m_Sounds.find(fileName);
	if (result != m_Sounds.end())
	{
		m_Sounds.find(fileName)->second->release();
		m_Sounds.erase(result);
	}
}
