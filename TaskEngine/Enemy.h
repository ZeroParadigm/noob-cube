/**
 * EnemyComponent - An EnemyComponent gameplay component
 *
 * Author - Jesse Dillon
 */

#pragma once 

#include "IGameplayComponent.h"

class Observable;
class Actor;

class EnemyComponent : public IGameplayComponent
{
private:

	float m_Timer;
	float m_SpawnTime;		//Number of seconds from spawn until active

	bool  m_Active;
	bool m_Dead;

public:

	EnemyComponent(void);
	EnemyComponent(Actor* owner);
	~EnemyComponent(void);

	void Init();

	void Spawn(void);
	void Die(void);

	void Update(float dt) override;

protected:

	void Activate(void);

private:

	void HandleCollisionEvent(Actor* first, Actor* second);

};