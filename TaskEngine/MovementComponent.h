/**
 * Movement component - Provides autonomous movement to an agent
 *
 * Author - Jesse Dillon
 */


#pragma once

#include "IAIComponent.h"
#include "TransformComponent.h"

using namespace DXMathLibrary;
using namespace std;

enum behavior_linear
{
	none_linear		= 0x00000,
	seek			= 0x00001,
	flee			= 0x00002,
	arrive			= 0x00004,
	wander			= 0x00010,
	cohesion		= 0x00020,
	separation		= 0x00040,
	interpose_point = 0x00080,
	obs_avoidance   = 0x00100,
	wall_avoidance  = 0x00200,
	follow_path     = 0x00400,
	pursue			= 0x00800,
	evade			= 0x01000,
	interpose		= 0x02000,
	hide			= 0x04000,
	flock			= 0x08000,
	offset_pursue   = 0x10000
};

enum behavior_angular
{
	none_angular	= 0x00000,
	align_3d		= 0x00001,
	face_forward_3d = 0x00002,
	face_point_3d	= 0x00004, 
	align_vector_3d = 0x00008
};

class MovementComponent : public IAIComponent
{
private:

	bool  m_Camera;		//Is this component the camera

	float m_MaxAngularSpeed;
	float m_MaxAngularAcceleration;
	float m_MaxSpeed;

	Vector3 m_MaxAcceleration;

	enum Deceleration
	{
		slow = 1,
		normal = 2,
		fast = 3
	};

	//Active behaviors
	int m_FlagsLinear;
	int m_FlagsAngular;

	/**
	 * Movement variables
	 */

	//Desired velocity
	Vector3 m_SteeringForce;

	//Targets 
	Vector3 m_TargetSeek;
	Vector3 m_TargetFlee;
	Vector3 m_TargetArrive;

	Vector3 m_TargetPosOrientation;
	Vector3 m_TargetHeadingOrientation;

	Quaternion m_TargetOrientation;
	Quaternion m_BaseOrientation;

	//Tracking agents
	TransformComponent*	    m_AgentEvade;
	TransformComponent*	    m_AgentPursue;
	TransformComponent*		m_AgentInterpose;

	//Current position on wander circle
	Vector3	    m_WanderTarget;
	Vector3     m_WanderProjection;

	//Wander values
	float	    m_WanderOffset;
	float		m_WanderRadius;
	float		m_WanderJitter;

	//Feelers
	std::vector<Vector3> m_Feelers;

	//Detection box length
	float		m_BoxLength;

	///@{
	/**
	 * Blending weights
	 */
	float m_WeightSeparation;
	float m_WeightCohesion;
	float m_WeightAlignment;
	float m_WeightWander;
	float m_WeightObstacleAvoidance;
	float m_WeightWallAvoidance;
	float m_WeightSeek;
	float m_WeightFlee;
	float m_WeightArrive;
	float m_WeightPursue;
	float m_WeightOffsetPursue;
	float m_WeightInterpose;
	float m_WeightHide;
	float m_WeightEvade;
	float m_WeightFollowPath;
	float m_WeightFacePoint3D;
	float m_WeightFaceForward3D;
	///@}

	/**
	 * Create feelers
	 *
	 * Generate a set of 5 feelers for 3D wall avoidance
	 */
	void CreateFeelers(void);

	/*
	 * Destroy feelers
	 *
	 * Clear the feelers
	 */
	void DestroyFeelers(void);

	MovementComponent(void) {}

public:


	MovementComponent(Actor* owner);
	~MovementComponent(void);

	/**
	 * Linear Velocity Update
	 */
	Vector3 LinearVelocity(void);
	Vector3 AngularVelocity(void);

	///@{
	/**
	 * Accessors
	 */

	float GetMaxRotation(void);
	float GetMaxRotationAcceleration(void);

	float GetMaxSpeed(void);
	Vector3 GetMaxAcceleration(void);

	bool  GetCameraMode(void);
	//@}

	///@{
	/**
	 * Setters
	 */

	void SetMaxRotation(float rot);
	void SetMaxRotationAcceleration(float rotAccel);

	void SetMaxSpeed(float speed);
	void SetMaxAcceleration(Vector3 accel);

	void SetCameraMode(bool cam);
	///@}


	///@{
	/**
	 * Movement variable accessors
	 */

	void SetTargetSeek(const Vector3 target);

	void SetTargetFlee(const Vector3 target);

	void SetTargetArrive(const Vector3 target);

	void SetAgentToEvade(TransformComponent* pursuer);

	void SetAgentToPursue(TransformComponent* evader);

	void SetAgentToInterpose(TransformComponent* interpose);

	TransformComponent* GetPursuer(void);

	TransformComponent* GetEvader(void);

	TransformComponent* GetInterposed(void);

	void SetTargetPoint3D(Vector3 p);
	void SetTargetHeading(Vector3 h);
	void SetTargetAlignment(Quaternion q);
	///@}


	///@{
	/**
	 * Wander settings
	 */

	void SetWanderOffset(float offset);
	void SetWanderRadius(float radius);
	void SetWanderJitter(float jitter);
	void SetWanderTarget(Vector3 target);
	///@}

	/** @name Behavior On Functions
	 *
	 */
	///@{
	/**
	 * Turn behavior on
	 */

	void SeekOn(void)				{ m_FlagsLinear |= seek; }
	void FleeOn(void)				{ m_FlagsLinear |= flee; }
	void ArriveOn(void)				{ m_FlagsLinear |= arrive; }
	void WanderOn(void)				{ m_FlagsLinear |= wander; }
	void CohesionOn(void)			{ m_FlagsLinear |= cohesion; }
	void SeparationOn(void)			{ m_FlagsLinear |= separation; }
	//void AlignmentOn(void)			{m_FlagsLinear |= alignment;}
	void ObstacleAvoidanceOn(void)  { m_FlagsLinear |= obs_avoidance; }
	void WallAvoidanceOn(void)		{ m_FlagsLinear |= wall_avoidance; }
	void InterposeOn(void)			{ m_FlagsLinear |= interpose; }
	void InterposePointOn(void)		{ m_FlagsLinear |= interpose_point; }
	void HideOn(void)				{ m_FlagsLinear |= hide; }
	void OffsetPursueOn(void)		{ m_FlagsLinear |= offset_pursue; }
	void FollowPathOn(void)			{ m_FlagsLinear |= follow_path; }
	void FlockingOn(void)			{ CohesionOn(); SeparationOn(); /*AlignmentOn();*/ }
	void PursueOn(void)				{ m_FlagsLinear |= pursue; }
	void EvadeOn(void)				{ m_FlagsLinear |= evade; }

	void Align3DOn(void)			{ m_FlagsAngular |= align_3d; }
	void FacePoint3DOn(void)		{ m_FlagsAngular |= face_point_3d; }
	void FaceForward3DOn(void)		{ m_FlagsAngular |= face_forward_3d; }
	void AlignVector3DOn(void)		{ m_FlagsAngular |= align_vector_3d; }
	///@}

	/** @name Behavior Off Functions
	 * 
	 */
	///@{
	/**
	 * Turn behavior off
	 */

	void SeekOff(void)				{if (IsOn(seek))			 m_FlagsLinear ^= seek;}
	void FleeOff(void)				{if (IsOn(flee))			 m_FlagsLinear ^= flee;}
	void ArriveOff(void)			{if (IsOn(arrive))		 m_FlagsLinear ^= arrive;}
	void WanderOff(void)			{if (IsOn(wander))		 m_FlagsLinear ^= wander;}
	void CohesionOff(void)			{if (IsOn(cohesion))		 m_FlagsLinear ^= cohesion;}
	void SeparationOff(void)		{if (IsOn(separation))	 m_FlagsLinear ^= separation;}
	//void AlignmentOff(void)			{if (IsOn(alignment))		 m_FlagsLinear ^= alignment;}
	void ObstacleAvoidanceOff(void) {if (IsOn(obs_avoidance))	 m_FlagsLinear ^= obs_avoidance;}
	void WallAvoidanceOff(void)		{if (IsOn(wall_avoidance)) m_FlagsLinear ^= wall_avoidance;}
	void InterposeOff(void)			{if (IsOn(interpose))		 m_FlagsLinear ^= interpose;}
	void InterposePointOff(void)    { if (IsOn(interpose_point)) m_FlagsLinear ^= interpose_point;}
	void HideOff(void)				{if (IsOn(hide))			 m_FlagsLinear ^= hide;}
	void OffsetPursueOff(void)		{if (IsOn(offset_pursue))  m_FlagsLinear ^= offset_pursue;}
	void FollowPathOff(void)		{if (IsOn(follow_path))    m_FlagsLinear ^= follow_path;}
	void FlockingOff(void)			{CohesionOff(); SeparationOff();/* AlignmentOff();*/}
	void PursueOff(void)			{if (IsOn(pursue)) m_FlagsLinear ^= pursue;}
	void EvadeOff(void)				{if (IsOn(evade)) m_FlagsLinear ^= evade;}

	void Align3DOff(void)			{ if (IsOn(align_3d))		 m_FlagsAngular ^= align_3d; }
	void FaceForward3DOff(void)		{ if (IsOn(face_forward_3d))m_FlagsAngular ^= face_forward_3d; }
	void FacePoint3DOff(void)		{ if (IsOn(face_point_3d))  m_FlagsAngular ^= face_point_3d; }
	void AlignVector3DOff(void)		{ if (IsOn(align_vector_3d)) m_FlagsAngular ^= align_vector_3d; }
	///@}

	/** @name Weight Setters
	 * 
	 *///@{

	/**
	 * Set behavior weight
	 */

	void SetWeightSeek(float w)				{m_WeightSeek = w;}
	void SetWeightFlee(float w)				{m_WeightFlee = w;}
	void SetWeightArrive(float w)			{m_WeightArrive = w;}
	void SetWeightWander(float w)			{m_WeightWander = w;}
	void SetWeightPursue(float w)			{m_WeightPursue = w;}
	void SetWeightEvade(float w)			{m_WeightEvade = w;}
	void SetWeightCohesion(float w)			{m_WeightCohesion = w;}
	void SetWeightSeparation(float w)		{m_WeightSeparation = w;}
	void SetWeightAlignment(float w)		{m_WeightAlignment = w;}
	void SetWeightObstacleAvoidance(float w){m_WeightObstacleAvoidance = w;}
	void SetWeightWallAvoidance(float w)	{m_WeightWallAvoidance = w;}
	void SetWeightInterpose(float w)		{m_WeightInterpose = w;}
	void SetWeightHide(float w)				{m_WeightHide = w;}
	void SetWeightOffsetPursue(float w)		{m_WeightOffsetPursue = w;}
	void SetWeightFollowPath(float w)		{m_WeightFollowPath = w;}
	///@}

	/** @name Weight Getters
	 * 
	 */
	///@{
	/**
	 * Get behavior weight
	 */

	float  GetWeightSeek(void)				{return m_WeightSeek;}
	float  GetWeightFlee(void)				{return m_WeightFlee;}
	float  GetWeightArrive(void)			{return m_WeightArrive;}
	float  GetWeightWander(void)			{return m_WeightWander;}
	float  GetWeightPursue(void)			{return m_WeightPursue;}
	float  GetWeightEvade(void)				{return m_WeightEvade;}
	float  GetWeightCohesion(void)			{return m_WeightCohesion;}
	float  GetWeightSeparation(void)		{return m_WeightSeparation;}
	float  GetWeightAlignment(void)			{return m_WeightAlignment;}
	float  GetWeightObstacleAvoidance(void) {return m_WeightObstacleAvoidance;}
	float  GetWeightWallAvoidance(void)		{return m_WeightWallAvoidance;}
	float  GetWeightInterpose(void)			{return m_WeightInterpose;}
	float  GetWeightHide(void)				{return m_WeightHide;}
	float  GetWeightOffsetPursue(void)		{return m_WeightOffsetPursue;}
	float  GetWeightFollowPath(void)		{return m_WeightFollowPath;}
	///@}

	/**
	 * Get behavior status
	 *
	 * @param[in] bt Behavior to check
	 */
	bool IsOn(behavior_linear flag){if(m_FlagsLinear & flag)return true; return false;}
	bool IsOn(behavior_angular flag){if(m_FlagsAngular & flag)return true; return false;}

	//Turn steering off
	void SteeringOff(void){ m_FlagsLinear = none_linear; m_FlagsAngular = none_angular;}

	/**
	 * Movement Getters
	 */

	//View distance
	float GetViewDistance(void){return m_BoxLength;}

	//Get target agents
	TransformComponent* GetPursuedAgent(void);
	TransformComponent* GetEvadingAgent(void);
	TransformComponent* GetInterposeAgent(void);

	//Get target positions
	Vector3 GetTargetSeek(void);
	Vector3 GetTargetFlee(void);
	Vector3 GetTargetArrive(void);

	//Get target orientation position
	Vector3 GetTargetPoint3D(void);

	//Get target vector align
	Vector3 GetTargetVectorAlign(void);

	//Get target alignment
	Quaternion GetTargetAlignment(void);

	//Get Wander values
	Vector3 GetWanderTarget(void);
	float GetWanderOffset(void);
	float GetWanderRadius(void);
	float GetWanderJitter(void);

	//Get feelers
	std::vector<Vector3>* GetFeelers(void);

	//Vision
	float   m_ViewDistance;

	//Offset
	Vector3 m_Offset;

	//Deceleration
	Deceleration m_Deceleration;

	///@{
	/**
	 * Utility functions for AiCore assistance
	 */
	///@}

	Vector3 GetPosition(void);
	Vector3 GetVelocity(void);

	Vector3 GetAngularVelocity(void);
	Quaternion GetOrientation(void);

	void SetVelocity(Vector3 v);
	void SetPosition(Vector3 p);
	void SetOrientation(Quaternion q);
	void SetAngularVelocity(Vector3 a);

	void SetDeceleration(int d);
};
