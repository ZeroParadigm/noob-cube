#include "EngineCommonStd.h"

#include "MeshPipeline.h"
#include "Actor.h"
//#include "Geometry.h"

MeshPipeline::MeshPipeline()
{
	buildFX();

	//Init default Light
	m_GlobalLight.ambient		= Color(1.0f, 1.0f, 1.0f, 1.0f);
	m_GlobalLight.diffuse		= Color(1.0f, 1.0f, 1.0f, 1.0f);
	m_GlobalLight.specular		= g_White;
	m_GlobalLight.directionWorld= Vector3(-0.577f, -0.577f, -0.577f);
	m_GlobalLight.directionWorld.Normalize();

	//Init default material
	m_Material.SetAmbient(g_White);
	m_Material.SetDiffuse(g_White);
	m_Material.SetSpecular(g_Black, 8.0f);
	
	onResetDevice();
}


MeshPipeline::~MeshPipeline()
{
	m_FX->Release();
}

void MeshPipeline::buildFX()
{
	LPD3DXBUFFER XLog = 0;
	D3DXCreateEffectFromFile(g_d3dDevice, L"Assets/Shaders/PhongDirLtTex.fx", 0, 0, 0, 0, &m_FX, &XLog);
	if(XLog)
		MessageBox(0, (TCHAR*)XLog->GetBufferPointer(), 0, 0);

	//technique
	m_tech			= m_FX->GetTechniqueByName("PhongDirLtTexTech");

	//transform and view matracies
	m_WVP			= m_FX->GetParameterByName(0, "gWVP");
	m_World			= m_FX->GetParameterByName(0, "gWorld");
	m_WorldInvTrans	= m_FX->GetParameterByName(0, "gWorldInvTrans");

	//camera eye
	m_EyePosW		= m_FX->GetParameterByName(0, "gEyePosW");

	//material handles
	m_Mtrl			= m_FX->GetParameterByName(0, "gMtrl");
	m_Light			= m_FX->GetParameterByName(0, "gLight");

	//texture handles
	m_Tex			= m_FX->GetParameterByName(0, "gTex");

	m_FX->SetTechnique(m_tech);
}

void MeshPipeline::SetLens(float FOV, float aspect, float nearZ, float farZ)
{
	m_FoV	= FOV;
	m_NearZ	= nearZ;
	m_FarZ	= farZ;

	D3DXMatrixPerspectiveFovLH(&m_Projection, FOV, aspect, nearZ, farZ);
}

void MeshPipeline::onResetDevice()
{
	D3DVIEWPORT9 viewport;
	g_d3dDevice->GetViewport(&viewport);

	SetLens(D3DX_PI / 4.0f, ((float)viewport.Width / (float)viewport.Height), 1.0f, 20000.f);

	m_FX->OnResetDevice();
}

void MeshPipeline::onLostDevice()
{
	m_FX->OnLostDevice();
}

void MeshPipeline::render()
{
	g_Memory->GetCamera()->Update(0.013f);
	Vector3 camPos = g_Memory->GetCamera()->GetPosition();
	std::vector<MeshComponent*> objects =  g_Memory->GetMeshes();

	g_d3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,  g_Blue, 1.0f, 0);
	g_d3dDevice->BeginScene();

	m_FX->SetValue(m_Light, &m_GlobalLight, sizeof(DirectionalLight));
	m_FX->SetValue(m_EyePosW, &camPos, sizeof(D3DXVECTOR3)); /// here

	UINT numPasses = 0;
	m_FX->Begin(&numPasses, 0);
	m_FX->BeginPass(0);

	Matrix4 W, WI;
	Matrix4 CameraVP = g_Memory->GetCamera()->View() * m_Projection;

	//iterate throuh mesh components and render
	for(auto object : objects)
	{
		if (object->IsActive())
		{
			W = object->GetOwner()->Transform()->GetWorldMatrix();

			m_FX->SetMatrix(m_World, &W);
			m_FX->SetMatrix(m_WVP, &(W * CameraVP));

			WI = W.Inverse();
			D3DXMatrixTranspose(&WI, &WI);

			m_FX->SetMatrix(m_WorldInvTrans, &WI);


			m_FX->SetTexture(m_Tex, object->GetMesh()->GetTexture());
			//m_FX->SetBool(m_FDif, (object->GetTexture()) ? false : true);

			m_FX->SetValue(m_Mtrl, &m_Material.GetD3D9(), sizeof(D3DMATERIAL9));
			m_FX->CommitChanges();

			object->DrawSubset(0);
		}
	}

	m_FX->EndPass();
	m_FX->End();

	g_GUI->Draw();

	g_d3dDevice->EndScene();
	g_d3dDevice->Present(0, 0, 0, 0);
}