#pragma once


class BaseWindow
{
private:

	void SetID()
	{
		static unsigned int nextID = 1;
		m_id = nextID;
		nextID++;
	}

	static LRESULT CALLBACK s_MsgProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

protected:

	unsigned int	m_id;
	HWND			m_hwnd;
	HINSTANCE		m_appInstance;
	POINT			m_position;
	POINT			m_size;

	std::wstring	m_caption;

	bool			m_fullscreen;
	bool			m_minOrMax;
	bool			m_hasMenu;
	bool			m_isMainWindow;

protected:

	void Register();
	//http://bit.ly/1dGC1uC
	void WinCreateWindowEx(DWORD dwExStyle,
						   LPCWSTR pszName,
						   DWORD dwStyle,
						   int x, int y,
						   int w, int h,
						   HWND hwndParent,
						   HMENU hmenu);

	virtual LRESULT HandleMessage(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual LPCWSTR ClassName() = 0;
	virtual BOOL WinRegisterClass(WNDCLASSEX *wcx)
	{
		return RegisterClassEx(wcx);
	}

public:

	BaseWindow()
	{
		SetID();				//get a unique ID
		m_fullscreen = false;	//default is NOT fullscreen
		m_minOrMax = false;		//default is NOT minimized or maximized
		m_hasMenu = false;		//default comes with NO window
		m_isMainWindow = false;	//NOT main window by default
	}

	~BaseWindow(){}

	//Initalizes all window paramaters and returns a pointer
	virtual int Init(HINSTANCE appInstance, 
					 const TCHAR* caption, 
					 long x, long y, 
					 long w, long h) = 0;

	virtual void Hide();
	virtual void Show(BOOL forceForeground = false);

	virtual void Minimize();
	virtual void Maximize(BOOL forceForeground = false);
	virtual void Restore();

	virtual void Enable(BOOL enable);

	virtual void SetFullscreen(bool full);

	void SetCaption(const TCHAR* caption);
	void SetPosition(long x, long y);
	void SetSize(long w, long h);
	void SetRect(long x, long y, long w, long h);

	void SetMainWindow(bool main) { m_isMainWindow = main; }

	int GetID()					{ return m_id; }
	HWND GetHWND()				{ return m_hwnd; }

	std::wstring GetCaption()	{ return m_caption; }
	POINT GetPosition()			{ return m_position; }
	POINT GetSize()				{ return m_size; }
	long GetWidth()				{ RECT rect; GetClientRect(m_hwnd, &rect); return rect.right; }
	long GetHeight()			{ RECT rect; GetClientRect(m_hwnd, &rect); return rect.bottom; }
	//RECT GetRect()				{ return RECT{ m_position.x, m_position.y, m_size.x, m_size.y }; }

};