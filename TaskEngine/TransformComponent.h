#pragma once
#include "BaseComponent.h"

using namespace DXMathLibrary;

class TransformComponent : public BaseComponent
{
private:

	//Transformation
	Vector3			m_Position;
	Vector3			m_Scale;

	Quaternion		m_Orientation;

	//Velocities
	Vector3			m_Velocity;
	Vector3			m_AngularVel;

	TransformComponent(){}

public:

	TransformComponent(Actor* owner);
	~TransformComponent(void);


	/**
	 * Set the world position of the transform
	 */
	void SetPosition(Vector3 position, bool delta = false);

	/**
	 * Set the scale of the transform
	 */
	void SetScale(Vector3 scale, bool delta = false);

	/**
	 * Set the orientation of the transform
	 */
	void SetOrientation(Quaternion orientation, bool delta = false);

	/**
	 * Set velocity
	 */
	void SetVelocity(Vector3 vel, bool delta = false);

	/**
	 * Set angular velocity
	 */
	void SetAngularVelocity(Vector3 ang, bool delta = false);

	/**
	 * Get the position
	 */
	Vector3 GetPosition();

	Vector3* GetPositonPtr();

	/**
	 * Get the scale
	 */
	Vector3 GetScale();

	/**
	 * Get the orientation
	 */
	Quaternion GetOrientation();

	/**
	 * Get velocity
	 */
	Vector3 GetVelocity(void);

	/**
	 * Get angular velocity
	 */
	Vector3 GetAngularVelocity(void);

	/**
	 *	Get World Matrix
	 */
	Matrix4 GetWorldMatrix();

};

