#include "EngineCommonStd.h"

#include "ScoreSystem.h"


ScoreSystem::ScoreSystem()
{
	m_CurrentScore = 0;
	m_DisplayScore = 0.0f;

	m_GrowthRate = 5.0f;
}

ScoreSystem::~ScoreSystem()
{
}

void ScoreSystem::Update(float dt)
{
	if (m_DisplayScore < m_CurrentScore)
	{
		m_DisplayScore += m_GrowthRate * dt;

		if (m_DisplayScore > m_CurrentScore)
		{
			m_DisplayScore = m_CurrentScore;
		}
	}
}

void ScoreSystem::AddPoints(int points)
{
	m_CurrentScore += points;
}

void ScoreSystem::SetGrowthRate(float pointsPerSecond)
{
	m_GrowthRate = pointsPerSecond;
}

int ScoreSystem::GetCurrentScore()
{
	return m_CurrentScore;
}

int ScoreSystem::GetDisplayScore()
{
	return (int)m_DisplayScore;
}