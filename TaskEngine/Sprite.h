#pragma once

#include "Texture.h"


class Sprite : public Texture
{
public:

	unsigned int m_Width;
	unsigned int m_Height;

public:

	Sprite();
	Sprite(std::wstring fileName, unsigned int width, unsigned int height);
	~Sprite();

public:

	void Load(IDirect3DDevice9* device) override;

	unsigned int GetWidth();
	unsigned int GetHeight();

};