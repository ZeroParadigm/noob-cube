#pragma once

#include "Components.h"

class Actor;

class ActorManager
{
	friend class MemoryDelegate;

protected:

	std::vector<Actor*>   m_ActorList;
	
	std::map<int, Actor*> m_ActorMap;

	Actor*	m_Camera;
	Actor*  m_Player;
	Actor*  m_GameWorld;

	BulletSystem*	m_Gun;

public:

	ActorManager(void);
	~ActorManager(void);

	/**
	 * Returns a blank actor
	 */
	Actor* CreateActor(void);

	/**
	 * Returns a list of all actors
	 */
	std::vector<Actor*> GetActorList();

	/**
	 * Return an actor
	 * @param[in] id Id of the requested actor
	 */
	Actor* GetActor(int id);

	/**
	 * Get the camera
	 */
	Actor* GetCamera(void);

	/**
	 * Get the player
	 */
	Actor* GetPlayer(void);

	/**
	 * Get the Game world
	 */
	Actor* GetWorld(void);

	/**
	 * Get the Gun object
	 */
	BulletSystem* GetGun(void);

	/**
	 * Set the camera
	 */
	void SetCamera(Actor* a);

	/**
	 * Set the player
	 */
	void SetPlayer(Actor* a);

	/**
	 * Set the Game world
	 */
	void SetWorld(Actor* a);

	/**
	 * Set the Gun
	 */
	void SetGun(BulletSystem* gun);

private:

	/**
	 * Release all actors
	 */
	void ClearActors();

};

