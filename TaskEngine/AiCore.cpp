#include "EngineCommonStd.h"
#include "AiCore.h"
#include "AiUtility.h"
//#include "MemoryDelegate.h"
//#include "Actor.h"
//#include "MathLibrary.h"
//#include "Geometry.h"
//#include "TransformComponent.h"

using namespace DXMathLibrary;

#define origin Vector3(0, 0, 0)

///**
//* Utility funciton for debugging
//**/
//template <class T>
//bool Undefined(T a)
//{
//	return a != a;
//}

AiCore::AiCore(void)
{
	m_Memory = nullptr;
	
	m_TimeElapsed = 0.0f;
}

AiCore::~AiCore(void)
{
}

void AiCore::Initialize(MemoryDelegate* memory)
{
	m_Memory = memory;
}

void AiCore::Shutdown(void)
{
	m_Memory = nullptr;
}

void AiCore::Update(float dt)
{
	//Update all movement
	for (auto ai : m_Memory->GetMovement())
	{
		m_Agent = ai;

		//Linear velocity
		
		Vector3 desired = LinearUpdate(dt);

		Vector3 acceleration = desired; // Currently assuming mass of 1 // * (float)(1.0 / m_Agent->m_Owner->Physics()->GetMass());

		Vector3 velocity = m_Agent->GetVelocity();	//Where 0.9 is velocity damping

		velocity += acceleration * dt;

		float maxSpeed = m_Agent->GetMaxSpeed();

		if (velocity.LengthSq() > maxSpeed * maxSpeed)
		{
			velocity.Normalize();
			velocity *= maxSpeed;
		}

		Vector3 position = m_Agent->GetPosition();

		position += velocity * dt;

		m_Agent->SetVelocity(velocity);
		m_Agent->SetPosition(position);

		//Angular velocity

		desired = AngularUpdate(dt);

		acceleration = desired; //Currently assuming mass of 1 // * (float)(1.0/m_Agent->m_Owner->Physics()->GetMass());

		velocity = m_Agent->GetAngularVelocity();

		velocity += acceleration * dt;

		maxSpeed = m_Agent->GetMaxRotation();

		if (velocity.LengthSq() > maxSpeed * maxSpeed)
		{
			velocity.Normalize();
			velocity *= maxSpeed;
		}

		if (!m_Agent->GetCameraMode())
		{
			//Quaternion orientation = m_Agent->GetOrientation();

			//orientation += Quaternion(desired.x, desired.y, desired.z, 0) * dt;

			//m_Agent->SetAngularVelocity(velocity);
			//m_Agent->SetOrientation(orientation);

			//Quaternion orientationA = PlayerFaceForward();

			//orientationA.Normalize();

			//Quaternion final;
			//final.Slerp(orientation, orientationA, 0.4f);

			//m_Agent->SetAngularVelocity(velocity);
			//m_Agent->SetOrientation(final);
		}
		else
		{
			Quaternion orientationA = m_Agent->GetOrientation();
			Quaternion orientationB = orientationA;

			//orientationA += Quaternion(desired.x, desired.y, desired.z, 0) * dt;		//Changed for camera debug JD 1/26

			orientationA = CamFacePoint();

			orientationA.Normalize();

			Quaternion final;
			final.Slerp(orientationB, orientationA, 0.8f);

			m_Agent->SetAngularVelocity(velocity);
			m_Agent->SetOrientation(final);
		}
	}
}

Vector3 AiCore::CalculateMovement(float dt)
{
	return Vector3();
}

Vector3 AiCore::LinearUpdate(float dt)
{
	//Create a steering force to use
	Vector3 desired = origin;

	//Create new force
	Vector3 force;

	if (m_Agent->IsOn(wall_avoidance))
	{
		force = WallAvoidance() * m_Agent->GetWeightWallAvoidance();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(obs_avoidance))
	{
		//TODO add obstacles
	}

	if (m_Agent->IsOn(evade))
	{
		force = Evade() * m_Agent->GetWeightEvade();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(flee))
	{
		force = Flee(m_Agent->GetTargetFlee()) * m_Agent->GetWeightFlee();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(wander))
	{
		force = Wander() * m_Agent->GetWeightWander();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(seek))
	{
		force = Seek(m_Agent->GetTargetSeek()) * m_Agent->GetWeightSeek();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(arrive))
	{
		force = Arrive(m_Agent->GetTargetArrive()) * m_Agent->GetWeightArrive();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(pursue))
	{
		force = Pursue() * m_Agent->GetWeightPursue();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(interpose_point))
	{
		force = InterposePoint() * m_Agent->GetWeightInterpose();

		if (!AccumulateForce(desired, force))
		{
			return desired;
		}
	}

	if (m_Agent->IsOn(separation) || m_Agent->IsOn(cohesion))
	{
		//Tag agents in range

		if (m_Agent->IsOn(separation))
		{
			force = Separation() * m_Agent->GetWeightSeparation();

			if (!AccumulateForce(desired, force))
			{
				return desired;
			}
		}

		if (m_Agent->IsOn(cohesion))
		{
			force = Cohesion() * m_Agent->GetWeightCohesion();

			if (!AccumulateForce(desired, force))
			{
				return desired;
			}
		}
	}

	return desired;

}

Vector3 AiCore::AngularUpdate(float dt)
{
	Vector3 desired = origin;

	if (m_Agent->IsOn(face_forward_3d))
	{
		desired = FaceForward();

		return desired;
	}
	else if (m_Agent->IsOn(face_point_3d))
	{
		desired = FacePoint();

		return desired;
	}
	else
	{
		return desired;
	}
}

Vector3 AiCore::Seek(Vector3 target)
{
	Vector3 desiredVelocity = target - m_Agent->GetPosition(); //m_Agent->GetOwner()->Transform()->GetPosition();

	desiredVelocity.Normalize();
	desiredVelocity *= m_Agent->GetMaxSpeed();

	return desiredVelocity - m_Agent->GetVelocity();
}

Vector3 AiCore::Flee(Vector3 target)
{
	Vector3 desiredVelocity = target - m_Agent->GetTargetFlee();

	desiredVelocity.Normalize();
	desiredVelocity *= m_Agent->GetMaxSpeed();

	return desiredVelocity - m_Agent->GetVelocity();
}

Vector3 AiCore::Arrive(Vector3 target)
{
	Vector3 toTarget = target - m_Agent->GetPosition();

	float distance = toTarget.Length();

	if (distance > _epsilon)
	{
		//Tune deceleration
		float decelTuning = 0.3f;

		//Calculate speed required to reach destination
		float speed = distance / ((float)m_Agent->m_Deceleration * decelTuning);

		//Clip speed
		float agentSpeed = m_Agent->GetMaxSpeed();
		speed = min(agentSpeed, speed);

		Vector3 desiredVelocity = toTarget * (speed / distance);

		return desiredVelocity - m_Agent->GetVelocity();
	}

	return origin;
}

Vector3 AiCore::Pursue(void)
{
	TransformComponent* evader = m_Agent->GetEvader();

	Vector3 targetPos;

	if (evader)
	{
	Vector3 toTarget = evader->GetPosition() - m_Agent->GetOwner()->Transform()->GetPosition();

	float lookAheadTime = toTarget.Length() / (m_Agent->GetMaxSpeed() + evader->GetVelocity().Length());

	targetPos = evader->GetPosition() + evader->GetVelocity();
	}

	return Arrive(targetPos);
}

/**
 *  Algorithms to be added as needed - JD
 */
Vector3 AiCore::Evade(void)
{
	return origin;
}

Vector3 AiCore::Wander(void)
{
	return origin;
}

Vector3 AiCore::ObstaceAvoidance(void)
{
	return origin;
}

Vector3 AiCore::WallAvoidance(void)
{
	//Get wall data
	std::vector<Plane*>* walls = nullptr;
	walls = ((GameWorldComponent*)m_Memory->GetGameWorld()->Gameplay())->GetPolygon()->GetWalls();  //So gross, we need to rethink this - JD

	if (walls->size() <= 0) return origin;

	//Set up the algorithm
	std::vector<Vector3>* feelers = m_Agent->GetFeelers();

	float ipDistance = 0.0f;
	float closestIp  = FLT_MAX;

	Plane* nearwall = nullptr;
	Vector3 steering, point, closest, position, transformed;

	steering = origin;
	Quaternion orientation = m_Agent->GetOrientation();
	position = m_Agent->GetPosition(); 

	//For each feeler
	for (auto& feeler : *feelers)
	{
		//Transform the feeler
		transformed = TransformQuaternion(feeler, orientation);
		transformed += position;

		//For each wall
		for (auto& wall : *walls)
		{
			if (wall->LineIntersect(position, transformed, point, ipDistance))
			{
				//Test for closest point
				if (ipDistance < closestIp)
				{
					closestIp = ipDistance;
					nearwall = wall;
					closest = point;
				}
			}
		}

		//If we have an intersection calculate the steering force
		if (nearwall)
		{
			//Calculate the penetration
			Vector3 overshoot = transformed - closest;
			steering = nearwall->m_Normal * overshoot.Length();
		}
	}

	return steering;
}

Vector3 AiCore::Interpose(const Vector3& a, const Vector3& b)
{
	return origin;
}

Vector3 AiCore::InterposePoint(void)
{
	float distance = 175.0f;  //Distance from origin

	//Grab a local copy to use
	TransformComponent* interposed = m_Agent->GetInterposed();

	//Interpose the point (We are not using predictive movement
	Vector3 target = interposed->GetPosition() - origin;
	target.Normalize();
	target *= distance;

	//Move toward this target
	return Arrive(target);
}

Vector3 AiCore::Hide(void)
{
	return origin;
}

Vector3 AiCore::Separation(void)
{
	return origin;
}

Vector3 AiCore::Cohesion(void)
{
	return origin;
}

Vector3 AiCore::Alignment(void)
{
	return origin;
}

Vector3 AiCore::FollowPath(Vector3 target)
{
	return origin;
}

Vector3 AiCore::Align(Quaternion target)
{
	Vector3 result;
	Quaternion q, sInv;

	sInv = m_Agent->GetOrientation();
	sInv.Invert();

	q = sInv * target;

	//===========================================
	// Split the quaternion into axis and angle
	//===========================================

	//Angle
	if (q.w > 1.0f || q.w < -1.0f)
	{
		q.w /= fabs(q.w);
	}

	float theta = 2 * acosf(q.w);

	//if (theta > pi)
	//{
	//	theta = MapToPi(theta);
	//}

	//Axis
	Vector3 a = Vector3((1 / (sinf(theta / 2)))*q.x, (1 / (sinf(theta / 2)))*q.y, (1 / (sinf(theta / 2)))*q.z);

	a.Normalize();

	if (a != a)	//Check for undefined result from floating point errors
	{
		a = Vector3(0, 0, 0);
	}
	
	//if (theta > pi)
	//{
	//	a = -a;
	//}

	//===========================================
	//         Find the rotation angle
	//===========================================

	float rotation = theta;		//Rotation means angular velocity for this equation

	//if (theta > pi)
	//{
	//	a *= -1.0f;
	//}

	float rotationDirection = MapToPi(rotation);
	
	float rotationSize = fabs(rotationDirection);

	if (rotationDirection < 0.0f)
	{
		rotationDirection = rotationDirection;
	}

	//if (rotationSize > pi)
	//{
	//	rotationSize = rotationSize - pi;
	//}

	//Bail out if no change
	if (rotationSize < pi / 32)		// pi / 32 reduces jitter incurred from small values
	{
		rotationSize = 0.0f;
	}

	float targetRotation;
	float slowRadius = pi / 16;

	//Rotate at full speed
	if (rotationSize > slowRadius)
	{
		targetRotation = m_Agent->GetMaxRotation();	 //May need to use rotation acceleration here
	}
	//Rotate slowly
	else
	{
		targetRotation = m_Agent->GetMaxRotation() * rotationSize / slowRadius;
	}

	//Calculate final target rotation
	if (fabs(rotationSize) > _epsilon)
	{
		targetRotation *= rotationDirection / rotationSize;//rotationSize;	//Possible fix 2:57am 1/21/14 jd
	}
	else
	{
		targetRotation = 0.0f;
	}

	targetRotation /= (float)0.1;

	//Clamp angular acceleration
	float acceleration = abs(targetRotation);
	float maxAcceleration = m_Agent->GetMaxRotationAcceleration();
	if (acceleration > maxAcceleration)
	{
		targetRotation /= acceleration;
		targetRotation *= maxAcceleration;
	}


	//Package up target rotation and the axis of rotation
	result = Vector3(a.x*targetRotation, a.y*targetRotation, a.z*targetRotation);

	return result;
}

Quaternion AiCore::CalculateOrientation(Vector3 vector)
{
	Vector3 BaseZ; //This will eventually turn into a forward vector using the current world face
	Quaternion baseO(0.0f, 0.0f, 0.0f, 1.0f);

	//if (m_Agent->GetCameraMode())
	//{
	//	baseO = m_Agent->GetOrientation();
	//}

	//Get the base vector along the Z axis
	BaseZ = TransformQuaternion(Vector3(0, 0, 1), baseO);

	if (BaseZ == vector)
	{
		return baseO;
	}

	Vector3 check = (-1.0) * vector;

	if (BaseZ.Equal(check))
	{
		return -baseO;
	}

	//Find the minimum rotation from base to target using vector product
	Vector3 change;

	Vector3 normal = vector;
	normal.Normalize();
	
	float angle = acosf(BaseZ.Dot(normal));

	//if (angle > pi / 2)
	//{
	//	change = -BaseZ.Cross(vector);
	//}
	//else
	//{
		change = BaseZ.Cross(vector);
	//}

	change.Normalize();

	Quaternion align(sinf(angle / 2)*change.x, sinf(angle / 2)*change.y,
					  sinf(angle / 2)*change.z, cosf(angle / 2));

	return align;
}

/**
 * To be added for alpha build - JD
 */
Vector3 AiCore::FacePoint(void)
{
	//Get the direction in which we will point
	Vector3 direction = m_Agent->GetTargetPoint3D() - m_Agent->GetPosition();

	direction.Normalize();

	//If zero direction make no change
	if (direction.LengthSq() < _epsilon)
	{
		return Vector3(0.0f, 0.0f, 0.0f);
	}

	Quaternion q = CalculateOrientation(direction);

	return Align(q);
}

Quaternion AiCore::CamFacePoint(void)
{
	//Get the direction in which we will point
	Vector3 direction = m_Agent->GetTargetPoint3D() - m_Agent->GetPosition();

	direction.Normalize();

	//If zero direction make no change
	if (direction.LengthSq() < _epsilon)
	{
		return m_Agent->GetOrientation();
	}

	Quaternion q = CalculateOrientation(direction);

	return q;
}

Vector3 AiCore::FaceForward(void)
{
	//Get the velocity vector 
	Vector3 direction = m_Agent->GetVelocity();

	direction.Normalize();

	//If zero direction make no change
	if (direction.LengthSq() < _epsilon)
	{
		return Vector3(0.0f, 0.0f, 0.0f);
	}

	Quaternion q = CalculateOrientation(direction);

	return Align(q);
}

Quaternion AiCore::PlayerFaceForward(void)
{
	//Get the velocity vector 
	Vector3 direction = m_Agent->GetVelocity();

	direction.Normalize();

	//If zero direction make no change
	if (direction.LengthSq() < _epsilon)
	{
		return m_Agent->GetOrientation();
	}

	Quaternion q = CalculateOrientation(direction);

	return q;
}

Quaternion AiCore::AlignVector(void)
{
	Vector3 direction = m_Agent->GetTargetVectorAlign();
	direction.Normalize();

	//If zero direction make no change
	if (direction.LengthSq() < _epsilon)
	{
		return m_Agent->GetOrientation();
	}

	Quaternion q = CalculateOrientation(direction);

	return q;
}

bool AiCore::AccumulateForce(Vector3& running, Vector3 forceToAdd)
{
	//Calculate the current magnitude
	float magnitude = running.Length();

	//Calculate available
	float magRemaining = m_Agent->GetMaxAcceleration().z - magnitude;

	if (magRemaining <= 0)
	{
		return false;
	}

	double magToAdd = forceToAdd.Length();

	if (magToAdd < magRemaining)
	{
		running += forceToAdd;
	}
	else
	{
		forceToAdd.Normalize();
		running += forceToAdd * magRemaining;
	}

	return true;
}
