#include "EngineCommonStd.h"

#include "ContactGenerator.h"
#include "ContactPoint.h"

#include "IntersectionTests.h"
#include "ClosestPointAlgorithms.h"


#include "Observable.h"


using namespace std;
using namespace DXMathLibrary;

ContactGenerator::ContactGenerator()
{
	m_MaxContacts = 250;

	m_Observable = new Observable();
	m_Observable->Attach(EventObs);
}

ContactGenerator::~ContactGenerator()
{
	m_Contacts.clear();

	if (m_Observable)
	{
		delete m_Observable;
		m_Observable = nullptr;
	}
}

void ContactGenerator::Init()
{
	m_Contacts.reserve(m_MaxContacts);
}

void ContactGenerator::Generate(double dt)
{
	//Update contact registry (cables, rods)

	//Broad Phase

	//Narrow Phase

	/*
	* Since the physics engine detection phase does not need to be
	* heavily effeciant the only thing it needs to do is literal
	* detection between boxes(obb or AABB) and spheres
	*/

	ContactPoint point;

	for (auto i = m_colliders.begin(); i != m_colliders.end(); i++)
	{
		if ((*i)->IsActive())
		{
			for (auto j = i + 1; j != m_colliders.end(); j++)
			{
				if ((*j)->IsActive())
				{
					if (SphereCC* sphereA = dynamic_cast<SphereCC*>(*i))
					{
						if (SphereCC* sphereB = dynamic_cast<SphereCC*>(*j))
						{
							if (IntersectionTests::SphereToSphere(sphereA, sphereB, point))
							{
								m_Contacts.push_back(point);

								EventData data(sphereA->GetOwner(), sphereB->GetOwner());
								m_Observable->Notify(COLLISION, data);
							}

						}
						else if (BoxCC* box = dynamic_cast<BoxCC*>(*j))
						{
							if (IntersectionTests::SphereToAABB(sphereA, box, point))
							{
								m_Contacts.push_back(point);

								EventData data(sphereA->GetOwner(), box->GetOwner());
								m_Observable->Notify(COLLISION, data);
							}
						}
					}
					else if (BoxCC* boxA = dynamic_cast<BoxCC*>(*i))
					{
						if (BoxCC* boxB = dynamic_cast<BoxCC*>(*j))
						{
							if (IntersectionTests::AABBToAABB(boxA, boxB, point))
							{
								m_Contacts.push_back(point);

								EventData data(boxA->GetOwner(), boxB->GetOwner());
								m_Observable->Notify(COLLISION, data);
							}
						}
					}
				}
			}
		}
	}
			/*IColliderComponent* objOne;
			IColliderComponent* objTwo;

			if((*i)->GetType() == SPHERE_COLLIDER || (*j)->GetType() == SPHERE_COLLIDER)
			{
				if((*i)->GetType() == SPHERE_COLLIDER)
				{
					objOne = (*i);
					objTwo = (*j);
				}
				else
				{
					objOne = (*j);
					objTwo = (*i);
				}

				switch(objTwo->GetType())
				{
					case SPHERE_COLLIDER:
						if(IntersectionTests::SphereToSphere((SphereCC*)objOne, (SphereCC*)objTwo))
							
					break;
						
					case BOX_COLLIDER:
						if (IntersectionTests::SphereToAABB((SphereCC*)objOne, (BoxCC*)objTwo))
					break;
				}
			}

			if((*i)->GetType() == BOX_COLLIDER || (*j)->GetType() == BOX_COLLIDER)
			{
				if((*i)->GetType() == BOX_COLLIDER)
				{
					objOne = (*i);
					objTwo = (*j);
				} 
				else 
				{
					objOne = (*j);
					objTwo = (*i);
				}

				if(objTwo->GetType() == BOX_COLLIDER)
				{
					BoxToBox((BoxCC*)objOne, (BoxCC*)objTwo);
				}
			}
		}
	}*/
}

void ContactGenerator::SetColliders(vector<IColliderComponent*> colliders)
{
	m_colliders = colliders;
}

//
//void ContactGenerator::SphereToSphere(SphereCC* a, SphereCC* b)
//{
//	if (m_Contacts.size() == m_MaxContacts) return;
//
//	Vector3 aPos = a->GetOwner()->Transform()->GetPosition();
//	Vector3 bPos = b->GetOwner()->Transform()->GetPosition();
//
//	Vector3 midline = aPos - bPos;
//	float size = midline.LengthSq();
//
//	if (size <= 0.0f || size >= a->GetRadius() + b->GetRadius()) return;
//
//	ContactPoint point;
//	point.contactNormal = midline * (1.0 / size);
//	point.contactPoint = aPos + midline * 0.5;
//	point.penetration = (a->GetRadius() + b->GetRadius() - size);
//	point.setBodyData(a, b, 0.95, 0.0);
//	m_Contacts.push_back(point);
//}
//
//void ContactGenerator::SphereToBox(SphereCC* sphere, BoxCC* box)
//{
//	if (m_Contacts.size == m_MaxContacts) return;
//
//	Vector3 spherePos = sphere->GetOwner()->Transform()->GetPosition();
//	Vector3 boxPos = box->GetOwner()->Transform()->GetPosition();
//
//	Vector3 q;
//	ClosestPointAABB(spherePos, b, q);
//
//	Vector3 midline = spherePos - q;
//	double size = midline.LengthSq();
//
//	if (size <= sphere->GetRadius())
//	{
//		ContactPoint point;
//		point.contactNormal = midline * (1.0 / size);
//		point.contactPoint = q;
//		point.penetration = sphere->GetRadius() - size;
//		point.setBodyData(sphere, box, 0.95, 0.0);
//		m_Contacts.push_back(point);
//	}
//}
//
//void ContactGenerator::BoxToBox(BoxCC* a, BoxCC* b)
//{
//
//}