#pragma once
#include "BaseComponent.h"
#include "Mesh.h"

//Simple Implementation
//Needs:
// - Material
// - Texture
class MeshComponent : public BaseComponent
{
private:

	MeshComponent(){}

	Mesh* m_Mesh;

	bool m_Active;

public:


	MeshComponent(Actor* owner);
	MeshComponent(Actor* owner, std::wstring name);
	~MeshComponent(void);

	void DrawSubset(int subset);

	void SetMesh(Mesh* m);

	void SetActive(bool active);

	std::wstring GetMeshName();
	Mesh* GetMesh();

	bool IsActive();
};