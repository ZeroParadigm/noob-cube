#pragma once

//#include <string>
//#include <vector>

class BaseGUIElement;
class WindowElement;
class StaticTextElement;
class StaticImageElement;
class ButtonElement;


class GUI
{
public:

	std::vector<BaseGUIElement*> m_GUIElements;

public:

	GUI();
	~GUI();

	void Draw();

public:

	WindowElement* CreateWindowElement();
	WindowElement* CreateWindowElement(BaseGUIElement* parent);
	StaticTextElement* CreateStaticTextElement();
	StaticTextElement* CreateStaticTextElement(BaseGUIElement* parent);
	StaticImageElement* CreateStaticImageElement();
	StaticImageElement* CreateStaticImageElement(BaseGUIElement* parent);
	ButtonElement* CreateButtonElement();
	ButtonElement* CreateButtonElement(BaseGUIElement* parent);

	BaseGUIElement* GetElement(std::wstring elementName);
};