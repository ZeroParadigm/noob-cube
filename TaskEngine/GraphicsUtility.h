#pragma once
//#include "Geometry.h"
//
//#include <string>
//#include <map>
//#include <vector>
//#include <list>
//#include <functional>

#include "Camera.h"
#include "Vertex.h"

//Memory Delegate is a Main Engine System
//#include "../SimpleEngine/MemoryDelegate.h"

using namespace DXMathLibrary;

//type defines
//typedef D3DXCOLOR Color;
typedef std::pair<std::string, LPDIRECT3DTEXTURE9> MeshMap;

//Enumerations
enum ComponentOptions
{
	ComponentOption_Render	= 1,
	ComponentOption_Undf2	= 2,
	ComponentOption_Undf4	= 4,
	ComponentOption_Undf8	= 8,

	ComponentOption_Diffuse = 16,
	ComponentOption_Undf32	= 32,
	ComponentOption_Undf64	= 64,
	ComponentOption_Undf128	= 128,

	ComponentOption_NumOptions
};

enum RenderPass
{
	RenderPass_0,
	RenderPass_static = RenderPass_0,
	RenderPass_Actor,
	RenderPass_Sky,
	RenderPass_NotRendered,
	RenderPass_Last
};

enum NodeType
{
	NodeType_Root,
	NodeType_Camera,
	NodeType_SceneNode,
	NodeType_D3D9,
	NodeType_D3D9_Mesh,
	NodeType_D3D9_Particle
}; 

class GraphicsSystem;
class GUI;

//Externs 
extern IDirect3DDevice9 *g_d3dDevice;
extern GraphicsSystem *d3dSystem;
extern MemoryDelegate *g_Memory;
extern ID3DXFont*	g_ExoRegular;
extern ID3DXSprite* g_d3dSprite;
extern GUI*			g_GUI;

//extern ID3DXMesh *g_BoxMesh;

//extern Color g_White;
//extern Color g_Black;
//extern Color g_Cyan;
//extern Color g_Red;
//extern Color g_Green;
//extern Color g_Blue;
//extern Color g_Yellow;
//extern Color g_Gray40;
//extern Color g_Gray25;
//extern Color g_Gray65;
//extern Color g_Transparent;
//
//extern Vector3 g_Up;
//extern Vector3 g_Right;
//extern Vector3 g_Forward;
//
//extern Vector4 g_Up4;
//extern Vector4 g_Right4;
//extern Vector4 g_Forward4;
//
//extern const float fOPAQUE;
//extern const int iOPAQUE;
//extern const float fTRANSPARENT;
//extern const int iTRANSPARENT;

struct Transform
{
	Vector3 Position;
	Vector3 Scale;
	Quaternion Orientation;
};


////Resources
//template <class type>
//class Resource
//{
//friend class MemoryManager;
//private:
//
//	type	m_resource;
//	int		m_refCount;
//
//	void	Release()	{ m_resource->ReleaseCOM(); }
//
//	Resource(type newResource) : m_resource(newResource), m_refCount(0) { }
//	~Resource() { }
//
//public:
//	
//	void AddRef()		{ m_refCount++; }
//	void RemoveRef()	{ m_refCount--; }
//
//	type Get()			{ return m_resource; }
//
//};

//Material
//class Material
//{
//private:
//
//	D3DMATERIAL9 m_D3D;
//
//public:
//
//	Material()
//	{
//		ZeroMemory(&m_D3D, sizeof(D3DMATERIAL9));
//
//		m_D3D.Diffuse	= g_White;
//		m_D3D.Ambient	= Color(0.10f, 0.10f, 0.10f, 1.0f);
//		m_D3D.Specular	= g_White;
//		m_D3D.Emissive	= g_Black;
//	}
//
//	//Material Setters
//	void			SetAmbient(const Color &color)						{ m_D3D.Ambient = color; }
//	void			SetDiffuse(const Color &color)						{ m_D3D.Diffuse = color; }
//	void			SetSpecular(const Color&color, const float power)	{ m_D3D.Specular = color; m_D3D.Power = power; }
//	void			SetEmissive(const Color &color)						{ m_D3D.Emissive = color; }
//
//	void			SetAlpha(const float alpha)							{ m_D3D.Diffuse.a = alpha; }
//
//	//Material getters
//	const Color		GetEmissive()										{ return m_D3D.Emissive; }
//	float			GetAlpha() const									{ return m_D3D.Diffuse.a; }
//
//	//Material checking
//	bool			HasAlpha() const									{ return m_D3D.Diffuse.a != fOPAQUE; }
//
//	void			UseMaterial9()
//	{
//		g_d3dDevice->SetMaterial(&m_D3D);
//	}
//
//	D3DMATERIAL9	GetD3D9()
//	{
//		return m_D3D;
//	}
//};
//
////Lights
//struct Light
//{
//	Color ambient;
//	Color diffuse;
//	Color specular;
//};
//
////struct DirectionalLight : public Light
////{
////	Vector3 directionWorld;
////};
//
//struct DirectionalLight
//{
//	Color		ambient;
//	Color		diffuse;
//	Color		specular;
//	Vector3		directionWorld;
//};
//
//struct PointLight : public Light
//{
//	Vector3		posW;
//	float		range;
//};

struct GraphicSchematic
{
	std::string		Identifier;
	std::string		FolderName;
	//std::string	ShaderName;
	ID3DXMesh	   *Mesh;
	MeshMap			Maps[5];
	//DWORD			DetailFlags;
	float			Radius;
};