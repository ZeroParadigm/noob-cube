#pragma once
//#include "MathLibrary.h"
#include "ContactPoint.h"

class SphereColliderComponent;
class BoxColliderComponent;
class MeshColliderComponent;
class CylinderColliderComponent;
class CapsuleColliderComponent;

class ClosestPointAlgorithms
{
public:

	static void ClosestPointAABB(DXMathLibrary::Vector3 position, BoxColliderComponent* aabb, ContactPoint &point);
	void ClosestPointOBB(DXMathLibrary::Vector3 position, BoxColliderComponent* obb, DXMathLibrary::Vector3 &point);
	void ClosestPointMesh(DXMathLibrary::Vector3 position, MeshColliderComponent* mesh, DXMathLibrary::Vector3 &point);
	void ClosestPointCylinder(DXMathLibrary::Vector3 position, CylinderColliderComponent* cylinder, DXMathLibrary::Vector3 &point);
	void ClosestPointCapsule(DXMathLibrary::Vector3 position, CapsuleColliderComponent* capsule, DXMathLibrary::Vector3 &point);
	

	DXMathLibrary::Vector3 ClosestPointAABB(DXMathLibrary::Vector3 position, BoxColliderComponent* aabb);
	DXMathLibrary::Vector3 ClosestPointOBB(DXMathLibrary::Vector3 position, BoxColliderComponent* obb);
	DXMathLibrary::Vector3 ClosestPointMesh(DXMathLibrary::Vector3 position, MeshColliderComponent* mesh);
	DXMathLibrary::Vector3 ClosestPointCylinder(DXMathLibrary::Vector3 position, CylinderColliderComponent* cylinder);
	DXMathLibrary::Vector3 ClosestPointCapsule(DXMathLibrary::Vector3 position, CapsuleColliderComponent* capsule);
};