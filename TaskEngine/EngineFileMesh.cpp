#include "EngineCommonStd.h"
#include "EngineFileMesh.h"




EngineFileMesh::EngineFileMesh(const TCHAR* name /*= _T("EngineFile_Mesh")*/)
{
	wcscpy_s(m_name, 512, name);
	m_mesh = NULL;
	m_materials = NULL;
	m_textures = NULL;
	m_useMaterials = true;
	m_VB = NULL;
	m_IB = NULL;
	m_VertDecl = NULL;
	m_strMaterials = NULL;
	m_numMaterials = 0;
	m_numVerts = 0;
	m_numFaces = 0;
	m_bytesPerVertex = 0;
}

EngineFileMesh::~EngineFileMesh()
{
	Release();
}

HRESULT EngineFileMesh::Release()
{
	InvalidateDeviceObjects();

	for (unsigned int i = 0; i < m_numMaterials; ++i)
		SAFE_RELEASE(m_textures[i]);
	SAFE_DELETE_ARRAY(m_textures);
	SAFE_DELETE_ARRAY(m_materials);
	SAFE_DELETE_ARRAY(m_strMaterials);

	SAFE_RELEASE(m_mesh);

	m_numMaterials = 0L;

	return S_OK;
}

HRESULT EngineFileMesh::RestoreDeviceObjects(IDirect3DDevice9* device)
{
	return S_OK;
}

HRESULT EngineFileMesh::InvalidateDeviceObjects()
{
	SAFE_RELEASE(m_IB);
	SAFE_RELEASE(m_VB);
	
	return S_OK;
}

HRESULT EngineFileMesh::Create(IDirect3DDevice9* device, const TCHAR* fileName)
{
	TCHAR strPath[MAX_PATH];
	LPD3DXBUFFER AdjacenyBuffer = NULL;
	LPD3DXBUFFER MaterialBuffer = NULL;

	HRESULT hr;

	Release();

	Utility::Filesystem::FindAssetFileFromCommonLocs(strPath, sizeof(strPath) / sizeof(TCHAR), fileName);

	// Load the mesh
	hr = D3DXLoadMeshFromX(strPath, D3DXMESH_MANAGED, device,
						   &AdjacenyBuffer, &MaterialBuffer, NULL,
						   &m_numMaterials, &m_mesh);

	if (FAILED(hr)) return hr;

	// Optimize mesh for performance
	hr = m_mesh->OptimizeInplace(
		D3DXMESHOPT_COMPACT | D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE,
		(DWORD*)AdjacenyBuffer->GetBufferPointer(), NULL, NULL, NULL);

	if (FAILED(hr))
	{
		SAFE_RELEASE(AdjacenyBuffer);
		SAFE_RELEASE(MaterialBuffer);
		return hr;
	}

	TCHAR* lastBackSlash = wcsrchr(strPath, _T('\\'));
	if (lastBackSlash)
		*(lastBackSlash + 1) = L'\0';
	else
		*strPath = L'\0';

	D3DXMATERIAL* d3dMaterials = (D3DXMATERIAL*)MaterialBuffer->GetBufferPointer();
	hr = CreateMaterials(strPath, device, d3dMaterials, m_numMaterials);

	SAFE_RELEASE(AdjacenyBuffer);
	SAFE_RELEASE(MaterialBuffer);

	// Make material and texture information easier to access by
	// pulling it out now and caching
	D3DVERTEXELEMENT9 decl[MAX_FVF_DECL_SIZE];
	m_numVerts = m_mesh->GetNumVertices();
	m_numFaces = m_mesh->GetNumFaces();
	m_bytesPerVertex = m_mesh->GetNumBytesPerVertex();
	m_mesh->GetIndexBuffer(&m_IB);
	m_mesh->GetVertexBuffer(&m_VB);
	m_mesh->GetDeclaration(decl);
	device->CreateVertexDeclaration(decl, &m_VertDecl);

	//return hr;
	return S_OK;
}

HRESULT EngineFileMesh::Create(IDirect3DDevice9* device, LPD3DXFILEDATA fileData)
{
	return E_FAIL;
}

HRESULT EngineFileMesh::Create(IDirect3DDevice9* device,
								ID3DXMesh* toUse,
								D3DXMATERIAL* d3dxMaterials,
								DWORD materials)
{
	return E_FAIL;
}

HRESULT EngineFileMesh::CreateMaterials(const TCHAR* strPath,
										IDirect3DDevice9* device,
										D3DXMATERIAL* d3dxMaterials,
										DWORD numMaterials)
{
	m_numMaterials = numMaterials;
	if (d3dxMaterials && m_numMaterials > 0)
	{
		m_materials = new D3DMATERIAL9[m_numMaterials];
		if (m_materials == NULL)
			return E_OUTOFMEMORY;
		m_textures = new LPDIRECT3DBASETEXTURE9[m_numMaterials];
		if (m_textures == NULL)
			return E_OUTOFMEMORY;
		m_strMaterials = new char[m_numMaterials][MAX_PATH];
		if (m_strMaterials == NULL)
			return E_OUTOFMEMORY;

		for (DWORD i = 0; i < m_numMaterials; i++)
		{
			m_materials[i] = d3dxMaterials[i].MatD3D;
			m_textures[i] = NULL;

			if (d3dxMaterials[i].pTextureFilename)
			{
				strcpy_s(m_strMaterials[i], MAX_PATH, d3dxMaterials[i].pTextureFilename);

				TCHAR strTexture[MAX_PATH];
				TCHAR strTexTemp[MAX_PATH];
				D3DXIMAGE_INFO imgInfo;

				MultiByteToWideChar(CP_ACP, 0, d3dxMaterials[i].pTextureFilename, -1, strTexTemp, MAX_PATH);
				strTexTemp[MAX_PATH - 1] = 0;

				_tcscpy_s(strTexture, MAX_PATH, strPath);
				_tcscat_s(strTexture, MAX_PATH, strTexTemp);

				if (FAILED(D3DXGetImageInfoFromFile(strTexture, &imgInfo)))
				{
					if (FAILED(Utility::Filesystem::FindAssetFileFromCommonLocs(strTexture, MAX_PATH, strTexTemp)))
						continue;

					D3DXGetImageInfoFromFile(strTexture, &imgInfo);
				}

				switch (imgInfo.ResourceType)
				{
					case D3DRTYPE_TEXTURE:
					{
						IDirect3DTexture9* tex;
						if (SUCCEEDED(D3DXCreateTextureFromFile(device, strTexture, &tex)))
						{
							tex->QueryInterface(IID_IDirect3DBaseTexture9, (LPVOID*)&m_textures[i]);
							tex->Release();
						}
						break;
					}
					case D3DRTYPE_CUBETEXTURE:
					{
						IDirect3DCubeTexture9* tex;
						if (SUCCEEDED(D3DXCreateCubeTextureFromFile(device, strTexture, &tex)))
						{
							tex->QueryInterface(IID_IDirect3DBaseTexture9, (LPVOID*)&m_textures[i]);
							tex->Release();
						}
						break;
					}
					case D3DRTYPE_VOLUMETEXTURE:
					{
						IDirect3DVolumeTexture9* tex;
						if (SUCCEEDED(D3DXCreateVolumeTextureFromFile(device, strTexture, &tex)))
						{
							tex->QueryInterface(IID_IDirect3DBaseTexture9, (LPVOID*)&m_textures[i]);
							tex->Release();
						}
						break;
					}
				}
			}
		}
	}
	return E_FAIL;
}

HRESULT EngineFileMesh::SetFVF(IDirect3DDevice9* device, DWORD fvf)
{
	WARNING_TODO("THIS");
	/*ERROR_TODO("THIS");*/
	return E_FAIL;
}

HRESULT EngineFileMesh::SetVertexDecl(IDirect3DDevice9* device,
										const IDirect3DVertexDeclaration9* decl,
										bool autoComputeNormals /*= true*/,
										bool autoComputeTangents /*= true*/,
										bool splitVertexForOptimalTangents /*= false*/)
{
	WARNING_TODO("THIS");
	/*ERROR_TODO("THIS");*/
	return E_FAIL;
}

HRESULT EngineFileMesh::Render(IDirect3DDevice9* device,
								bool drawOpaqueSubsets /*= true*/,
								bool drawAlphaSubsets /*= true*/)
{
	WARNING_TODO("THIS");
	/*ERROR_TODO("THIS");*/
	return E_FAIL;
}

HRESULT EngineFileMesh::Render(ID3DXEffect* effect,
								D3DXHANDLE texture /*= NULL*/,
								D3DXHANDLE diffuse /*= NULL*/,
								D3DXHANDLE ambient /*= NULL*/,
								D3DXHANDLE specular /*= NULL*/,
								D3DXHANDLE emissive /*= NULL*/,
								D3DXHANDLE power /*= NULL*/,
								bool drawOpaqueSubsets/* = true*/,
								bool drawAlphaSubsets /*= true*/)
{
	WARNING_TODO("THIS");
	/*ERROR_TODO("THIS");*/
	return E_FAIL;
}











