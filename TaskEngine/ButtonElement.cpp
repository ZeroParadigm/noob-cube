#include "EngineCommonStd.h"

#include "ButtonElement.h"
#include "GraphicsUtility.h"


ButtonElement::ButtonElement() : BaseGUIElement()
{
	m_Name = L"ButtonElement" + GetID();

	m_Text = L"";
	m_TextColor = D3DCOLOR_RGBA(0, 0, 0, 255);

	m_Image = nullptr;
	m_ColorMask = D3DCOLOR_RGBA(255, 255, 255, 255);
}

ButtonElement::ButtonElement(BaseGUIElement* parent) : BaseGUIElement(parent)
{
	m_Name = L"ButtonElement" + GetID();

	m_Text = L"";
	m_TextColor = D3DCOLOR_RGBA(0, 0, 0, 255);

	m_Image = nullptr;
	m_ColorMask = D3DCOLOR_RGBA(255, 255, 255, 255);
}

ButtonElement::~ButtonElement()
{
}

void ButtonElement::Draw()
{
	RECT drawArea = GetScreenArea();

	if (m_Image != nullptr)
	{
		g_d3dSprite->SetTransform(&GetMatrix());

		g_d3dSprite->Draw(GetImage(), &drawArea, NULL, NULL, m_ColorMask);
	}

	g_ExoRegular->DrawText(0, m_Text.c_str(), -1, &drawArea, DT_CENTER | DT_VCENTER, m_TextColor);
}

void ButtonElement::SetText(std::wstring text)
{
	m_Text = text;
}

void ButtonElement::SetTextColor(D3DCOLOR color)
{
	m_TextColor = color;
}

void ButtonElement::SetImage(Sprite* image)
{
	m_Image = image;
}

void ButtonElement::SetColorMask(D3DCOLOR color)
{
	m_ColorMask = color;
}

std::wstring ButtonElement::GetText()
{
	return m_Text;
}

D3DCOLOR ButtonElement::GetTextColor()
{
	return m_TextColor;
}

IDirect3DTexture9* ButtonElement::GetImage()
{
	return m_Image->GetTexture();
}

Sprite* ButtonElement::GetImageObj()
{
	return m_Image;
}

D3DCOLOR ButtonElement::GetColorMask()
{
	return m_ColorMask;
}