#pragma once

#include "Client.h"


	/**	NetworkingCore
	 *	
	 *	The NetworkingCore class. This class is designed to allow access to key functions this core uses in the creation of an online application.
	 *	The Core handles the receiving of packets with a Client class, and using a Reporter system the Core keeps track of the data for when the user needs it.
	 *	Packets MUST use an unsigned int as the packetID and it MUST be the first variable in the packet, I.E. the first 4 bytes.
	 */
	class NetworkingCore
	{
	private:

		Client* m_Client;
		PacketReporter* m_PacketReporter;

	public:

		NetworkingCore();
		~NetworkingCore();


		/**	
		 *	Connects to a socket using TCP/IP.
		 *	
		 *	std::string ipaddr	-	The IP address of the socket you are trying to connect to.
		 *	std::string port	-	The port of the socket you are trying to connect to.
		 *	
		 *	Returns true if successful, false if not successful.
		 */
		bool Connect(std::string ipaddr, std::string port);

		bool IsConnected();

		/**	
		 *	Sends a chunk of data over the connected socket.
		 *	
		 *	TCHAR* data			-	A pointer to the start of the data.
		 *	unsigned int size	-	The size of the data being sent.
		 *	
		 *	Returns true if successful, false if not successful.
		 */
		bool Send(char* data, unsigned int size);


		/**	
		 *	Disconnects the client from any connection currently active.
		 */
		void Disconnect();


		/**	
		 *	Checks if a packet has been received after the most recent CheckPacket(ID) call.
		 *	
		 *	unsigned int ID		-	The ID of the packet you are looking to check.
		 *	
		 *	Returns true if a packet has been received and not been checked since.
		 *	Returns false when called and the packet requested has not changed or is not found.
		 */
		bool CheckPacket(unsigned int ID);


		/**	
		 *	Checks if a packet has been received and updated, then returns a pointer to the packet.
		 *	
		 *	unsigned int ID		-	The ID of the packet you are looking to check and receive.
		 *
		 *	Returns a pointer to the saved packet if the packet has not recently been accessed.
		 *	Returns 0 otherwise.
		 */
		char* GetLastPacket(unsigned int ID);
	};