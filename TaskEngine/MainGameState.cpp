#include "EngineCommonStd.h"

#include "MainGameState.h"
#include "GameStates.h"
//#include "MemoryDelegate.h"
#include "GraphicsAPI.h"

#include "PlayerComponent.h"
#include "GraphicsUtility.h"
#include "GUI.h"
#include "WindowElement.h"
#include "StaticTextElement.h"
#include "StaticImageElement.h"
#include "ButtonElement.h"


MainGameState::MainGameState()
{
	generator.seed((unsigned int)std::chrono::high_resolution_clock::now().time_since_epoch().count());

	m_TimeElapsed = 0.0f;
	m_TimeSinceLastWave = 0.0f;

	m_SecondsBetweenWaves = 5.0f;

	m_WaveNum = 0;
}

MainGameState::~MainGameState()
{
}

void MainGameState::EnterState()
{
	m_TimeElapsed = 0.0f;

	CreateCamera();
	CreateWorld();
	CreatePlayer();
	CreateGUI();

	StartCamera();
	
	SpawnWave(m_WaveNum);
}

void MainGameState::LeaveState()
{
	//Clear memory
}

void MainGameState::UpdateState(float dt)
{
	m_TimeElapsed += dt;
	m_TimeSinceLastWave += dt;

	// Display Score
	
	// Display lives

	// Display Wave number

	// Check if player is dead
	if (m_Player->IsActive())
	{
		// IF player isn't dead then check time since last wave spawn
		if (m_TimeSinceLastWave >= m_SecondsBetweenWaves)
		{
			// If time is ok, spawn wave
			SpawnWave(m_WaveNum);
		}
	}
	else
	{
		// If player is dead then wait for player to respawn
	}

}

void MainGameState::CreateCamera(void)
{
	//Create the camera
	Actor* camera = memory->actorFactory()->CreateCamera();

	//Set up the camera
	Quaternion q(0, 0, 0, 1);
	q.BuildRotYawPitchRoll(0, 0, 0);
	
	((MovementComponent*)camera->Ai())->SteeringOff();
	camera->Transform()->SetPosition(Vector3(0.0f, 0.0f, -175.0f));
	camera->Transform()->SetOrientation(q);

	((CameraComponent*)camera->Renderer())->LookAt(Vector3(0,0,0));
}

void MainGameState::CreateWorld(void)
{
	//Create the game world
	Actor* gameworld = memory->actorFactory()->CreateGameWorld();
	//q.BuildRotYawPitchRoll(pi, 0, 0)
	((GameWorldComponent*)gameworld->Gameplay())->MakePlane();
}

void MainGameState::CreatePlayer(void)
{
	//Create the player
	Actor* player = memory->CreatePlayer();
	player->Transform()->SetScale(Vector3(0.1f, 0.1f, 0.1f));
	player->Transform()->SetPosition(Vector3(0, 0, -60));

	m_Player = ((PlayerComponent*)player->Gameplay());
		
	m_Player->ChangeFace(((GameWorldComponent*)memory->GetGameWorld()->Gameplay())->GetPolygon()->m_Faces[0]);
}

void MainGameState::CreateEnemy(void)
{
	Actor* enemy = memory->CreateEnemy();
	enemy->Transform()->SetScale(Vector3(0.1f, 0.1f, 0.1f));

	std::uniform_real_distribution<float> distribution(-1.0f, 1.0f);

	Vector3 rand;
	rand.x = distribution(generator) * 50;
	rand.y = distribution(generator) * 50;
	rand.z = -60;

	enemy->Transform()->SetPosition(rand);
}

void MainGameState::StartCamera(void)
{
	CameraComponent* camera = memory->GetCamera();
	MovementComponent* cam = (MovementComponent*)camera->GetOwner()->Ai();
	Actor* player = memory->GetPlayer();

	cam->SteeringOff();

	if (player)
	{
		//Set interpose method
		cam->SetAgentToInterpose(player->Transform());

		//Turn on interpose point
		cam->InterposePointOn();

		//Set align to point
		cam->SetTargetPoint3D(Vector3(0.0f, 0.0f, 0.0f));

		//Turn face point 3D on
		cam->FacePoint3DOn();
	}
}

void MainGameState::SpawnWave(int n)
{
	m_WaveNum++;
	m_TimeSinceLastWave = 0.0f;
	GUI_WaveNumber->SetText(L"Wave: " + to_wstring(m_WaveNum));

	for (int i = 0 ; i < n; i++)	
	{
		CreateEnemy();
	}
}

void MainGameState::CreateGUI()
{
	GUI_MainWindow = g_GUI->CreateWindowElement();
	GUI_WaveNumber = g_GUI->CreateStaticTextElement(GUI_MainWindow);
	GUI_TestImage1 = g_GUI->CreateStaticImageElement(GUI_MainWindow);
	GUI_TestImage2 = g_GUI->CreateStaticImageElement(GUI_MainWindow);

	GUI_WaveNumber->SetText(L"Wave: " + to_wstring(m_WaveNum));
	GUI_WaveNumber->SetWidth(100);
	GUI_WaveNumber->SetHeight(20);

	//memory->LoadSprite(L"Assets/Textures/pony1.png", 312, 312);
	//GUI_TestImage1->SetImage(memory->GetSprite(L"Assets/Textures/pony1.png"));
	//GUI_TestImage1->SetPosition(Vec2(300, 300));

	//memory->LoadSprite(L"Assets/Textures/pony2.png", 390, 341);
	//GUI_TestImage2->SetImage(memory->GetSprite(L"Assets/Textures/pony2.png"));
	//GUI_TestImage2->SetPosition(Vec2(300, 0));
}