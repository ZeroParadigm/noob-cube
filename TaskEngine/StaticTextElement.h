#pragma once

#include "BaseGUIElement.h"


class StaticTextElement : public BaseGUIElement
{
protected:

	std::wstring		m_Text;
	D3DCOLOR			m_TextColor;

public:

	StaticTextElement();
	StaticTextElement(BaseGUIElement* parent);
	~StaticTextElement();

	void Draw() override;

public:

	void SetText(std::wstring text);
	void SetTextColor(D3DCOLOR color);

	std::wstring GetText();
	D3DCOLOR GetTextColor();

};