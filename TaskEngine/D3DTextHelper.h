#pragma once


class D3DTextHelper
{
private:

	struct TextArea
	{
		POINT		m_pos;
		D3DXCOLOR	m_color;
		int			m_lineHeight;
		TCHAR*		m_text;
	};

private:

	ID3DXFont* m_font9;
	ID3DXSprite* m_sprite9;
	D3DXCOLOR m_color;
	POINT m_point;
	int m_lineHeight;

	std::map<TCHAR*, TextArea> m_textAreas;

public:

	D3DTextHelper();
	~D3DTextHelper();

	void Init(ID3DXFont* font, ID3DXSprite* sprite, int lineHeight = 15);

	void SetInsertionPoint(int x, int y)
	{
		m_point.x = x; m_point.y = y;
	}
	void SetForegroundColor(D3DXCOLOR color)
	{
		m_color = color;
	}

	void PlaceTextArea(int x, int y, int lineHeight, D3DXCOLOR color, const TCHAR* str, ...);

	void Begin();
	HRESULT DrawFormattedTextLine(const TCHAR* strMsg, ...);
	HRESULT DrawTextLine(const TCHAR* strMsg);
	HRESULT DrawFormattedTextLine(RECT& rc, DWORD dwFlags, const TCHAR* strMsg, ...);
	HRESULT DrawTextLine(RECT& rc, DWORD dwFlags, const TCHAR* strMsg);
	void End();
};
