/**
 * Clock - This class will provide timing functionality for the application
 *
 * Author - Jesse Dillon (Based on Frank Luna GameTimer class
 *			Introduction to 3D Game Programming with DirectX 11
 */

#pragma once

namespace Utility
{

	/**
	 * High performance Game Clock
	 */
	class GameTimer
	{
	private:

		double m_SecondsPerCount;
		double m_DeltaTime;

		__int64 m_BaseTime;
		__int64 m_PausedTime;
		__int64 m_StopTime;
		__int64 m_PrevTime;
		__int64 m_CurrTime;

		bool m_Stopped;

	public:

		GameTimer(void);

		~GameTimer(void);

		float GameTime(void) const;
		float DeltaTime(void) const;

		void Reset(void);
		void Start(void);
		void Stop(void);
		void Tick(void);
	};

	/**
	 * Lightweight Core Clock
	 */

	class Clock	
	{
		float m_TimeElapsed;

	public:

		Clock(void);

		~Clock(void);

		void Update(float dt);

		const float GetTime(void) const;

		void ResetClock(void);
	};


}