#pragma once

#include "Components.h"

class Actor;
class ActorManager;
class ComponentManager;
class ResourceManager;

struct ActorTemplate
{
	Vector3 position;
	Vector4 orientation;
	Vector3 scale;
	std::string meshName;

};

class ActorFactory
{
	friend class MemoryDelegate;
	friend class SimpleEngine;

protected:

	ActorManager*		m_ActorManager;
	ResourceManager*	m_ResourceManager;
	ComponentManager*	m_ComponentManager;

	ActorTemplate		m_CurrentActorTemplate;

public:

	ActorFactory(void);
	ActorFactory(ActorManager* actorMngr, ComponentManager* componentMngr, ResourceManager* resourceMngr);
	~ActorFactory(void);

protected:

	void LoadFile(const TCHAR* file);

	// Set the CurrentActorTemplate to the input template
	void SetActorTemplate(ActorTemplate templaet);
	// Create an actor based on the CurrentActorTemplate
	Actor* AssembleActor();

	// Create and return a blank actor
	Actor* CreateBlankActor();
	// Create and return a blank component of input type
	BaseComponent* CreateBlankComponent(Actor* owner, ComponentType type);

public:

	// Debugging functions

	Actor* CreateBasicMeshActor(float x, float y, float z);
	Actor* CreateBasicModelActor(float x, float y, float z, std::wstring folderName);
	Actor* CreateBasicSphereActor();
	Actor* CreateBasicScriptActor();
	Actor* CreateBasicAIActor();

	Actor* CreateCamera();
	Actor* CreateGameWorld();
	Actor* CreatePlayer();
	Actor* CreateEnemy();
	void CreateGun();
	BulletComponent* CreateBullet();

	Actor* CreateSphereTriggerVolume();
	Actor* CreateBoxTriggerVolume();

	Actor* CreateDebugPlayer();

	TriggerVolumeComponent* CreateWorldTriggerVolume(DXMathLibrary::Vector3 position, DXMathLibrary::Vector3 halfExtents);
};

