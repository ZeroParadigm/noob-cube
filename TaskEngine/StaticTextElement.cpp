#include "EngineCommonStd.h"

#include "StaticTextElement.h"
#include "GraphicsUtility.h"


StaticTextElement::StaticTextElement() : BaseGUIElement()
{
	m_Name = L"StaticTextElement" + GetID();

	m_Text = L"";
	m_TextColor = D3DCOLOR_RGBA(0, 0, 0, 255);
}

StaticTextElement::StaticTextElement(BaseGUIElement* parent) : BaseGUIElement(parent)
{
	m_Name = L"StaticTextElement" + GetID();

	m_Text = L"";
	m_TextColor = D3DCOLOR_RGBA(0, 0, 0, 255);
}

StaticTextElement::~StaticTextElement()
{
}

void StaticTextElement::Draw()
{
	RECT drawArea = GetScreenArea();

	g_ExoRegular->DrawText(0, m_Text.c_str(), -1, &drawArea, DT_CENTER | DT_VCENTER, m_TextColor);
}

void StaticTextElement::SetText(std::wstring text)
{
	m_Text = text;
}

void StaticTextElement::SetTextColor(D3DCOLOR color)
{
	m_TextColor = color;
}

std::wstring StaticTextElement::GetText()
{
	return m_Text;
}

D3DCOLOR StaticTextElement::GetTextColor()
{
	return m_TextColor;
}