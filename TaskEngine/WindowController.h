#pragma once
#include "Singleton.h"

//Types of windows implemented
#include "RenderWindow.h"

#define WINDOWS DEFINE_SINGLETON_ACCESS(WindowController)

class IRenderDevice;
class BaseWindow;

SINGLETON_CLASS(WindowController)
private:

	std::map<int, BaseWindow*> m_windows;

	BaseWindow*	m_mainWindow;

	std::wstring m_appCaption;

	HINSTANCE m_appInstance;

public:

	void Init(HINSTANCE appInstance, const TCHAR* appCaption);
	void Cleanup();

	void SetMain(BaseWindow* window);
	void SetMain(int id);

	void SuspendWindow(int id);

	BaseWindow* CreateAppRenderWindow(const TCHAR* windowName,
									  DWORD options,
									  IRenderDevice* pipeline,
									  int x, int y,
							  		  int w, int h);
};