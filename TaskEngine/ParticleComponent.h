#pragma once
#include "IPhysicsComponent.h"

class ParticleComponent : public IPhysicsComponent
{
public:

	DXMathLibrary::Vector3 m_acceleration;
	DXMathLibrary::Vector3 m_lastFrameAcceleration;

	DXMathLibrary::Vector3 m_forceAccumaltor;
	DXMathLibrary::Vector3 m_torqueAccumaltor;

	double	m_linearDamping;

	double	m_inverseMass;
	double	m_mass;

	double	m_motion;
	bool	m_wakeStatus;
	bool	m_isAwake;
	bool	m_canSleep;

	ParticleComponent();

public:

	ParticleComponent(Actor *owner);
	~ParticleComponent();
};