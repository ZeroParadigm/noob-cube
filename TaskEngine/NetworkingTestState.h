#pragma once

#include "GameState.h"
#include "easywsclient.hpp"

class PlayerComponent;

class WindowElement;
class StaticTextElement;
class StaticImageElement;
class ButtonElement;

class WebSocketClient;

class NetworkingTestState : public GameState
{
private:

	std::default_random_engine generator;

	float m_TimeElapsed;

	Actor* gameworld;
	PlayerComponent* m_Player;

	WindowElement*		GUI_MainWindow;
	StaticTextElement* GUI_PlayerScore;

	float m_PacketCooldown;
	float m_PacketTimer;

public:

	NetworkingTestState();
	~NetworkingTestState();

	void EnterState() override;
	void LeaveState() override;
	void UpdateState(float dt) override;

private:

	void CreateCamera(void);
	void CreateWorld(void);
	void CreatePlayer(void);

	void StartCamera(void);

	void CreateGUI();

	void CreateTestTriggerVolumes();
	void CreateWorldTriggerVolumes();


	bool EdgeTraversal(IGameplayComponent* comp, int face);
};