#pragma once



class GraphicsSystem;
class MemoryDelegate;

class GraphicsAPI
{
private:

	GraphicsSystem *core;

public:

	GraphicsAPI();
	~GraphicsAPI();

	void init(MemoryDelegate* memory, HINSTANCE appInstance, const TCHAR* appCaption, int drawRate, bool stats);
	void shutdown();

	void update(float dt);

	bool isDeviceLost();

	HWND GetHandle();

	IDirect3DDevice9* GetDevice();

};