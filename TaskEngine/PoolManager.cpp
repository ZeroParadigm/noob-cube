#include "EngineCommonStd.h"
#include "PoolManager.h"
#include "MemoryPool.h"

#include <thread>

#include <iostream>
using namespace std;

#pragma region Private API

PoolManager::PoolManager()
{
	m_ManagedPoolSize		= 0;

	m_memChecksumFreq		= 30.0f; //Default checksum is 30 seconds
	m_TESinceLastChecksum	= 0.0f;

	m_defrag				= false;
	m_checksumActive		= false;
	m_logging				= MEMORY_LOGGING ? true : false;
}

PoolManager::~PoolManager()
{
	ReleaseAllPools();
}

PoolManager* PoolManager::Attach(MemoryPool* poolToManage)
{
	//Assign a unique ID to the MemoryPool for enumerated recognition
	static int uniquePoolID = 1;
	poolToManage->m_poolID = uniquePoolID;
	m_managedPool.insert(make_pair(uniquePoolID, poolToManage));
	uniquePoolID++;

	m_ManagedPoolSize += poolToManage->GetAllocatedSize();

	return this;
}

void PoolManager::Inform(int id, int message, unsigned int package)
{
	MemoryPool* MemoryPool = m_managedPool.at(id);

	if (!MemoryPool) return;

	switch (message)
	{
	case NOTIFYPOOL_GROWTH:
		m_ManagedPoolSize += package;
		break;
	default:
		break;
	}
}

void PoolManager::PerformChecksum()
{
	OUTPUT(L"Conducting memory checksum.");

	Sleep(6000);
	
	OUTPUT(L"Finished memory checksum.");
	m_TESinceLastChecksum = 0.0f;
	m_checksumActive = false;
}

#pragma endregion

#pragma region Public API

#pragma region Utility Functions

void PoolManager::Update(float dt, bool force /*= false*/)
{
	m_TESinceLastChecksum = m_TESinceLastChecksum + dt;

	if (((m_TESinceLastChecksum >= m_memChecksumFreq) || force) && !m_checksumActive)
	{
		m_checksumActive = true;

		std::thread(&PoolManager::PerformChecksum, std::ref(POOL_MANAGER)).detach();
	}   
}

int PoolManager::ActivePoolCount()				{ return (int)m_registerdPools.size(); }
MemoryPool* PoolManager::GetPool(std::wstring name)	{ return m_registerdPools.at(name); }

int PoolManager::GetPoolAllocSpace(std::wstring name)
{
	return 0;
}

int PoolManager::GetTotalAllocSpace()
{
	return 0;
}

void PoolManager::SetChecksumFrequency(float value)	{ m_memChecksumFreq = value; }
void PoolManager::SetDefragging(bool value)			{ m_defrag = value; }
void PoolManager::SetLogging(bool value)			{ m_logging = value; }

#pragma endregion

#pragma region Registration Calls

void PoolManager::RegisterPool(std::wstring name, MemoryPool* MemoryPool)
{
	m_registerdPools.insert(make_pair(name, MemoryPool));
}

void PoolManager::UnRegisterPool(std::wstring name)
{
	m_registerdPools.erase(name);
}

void PoolManager::UnRegisterAllPools()
{
	m_registerdPools.clear();
}

#pragma endregion

#pragma region De-Allocation Calls

void PoolManager::DestroyAllPools()
{
	for (auto MemoryPool : m_registerdPools)
		MemoryPool.second->Destroy();
}

void PoolManager::DestroyPool(std::wstring poolName)
{
	m_registerdPools.at(poolName)->Destroy();
}

#pragma endregion

#pragma region Destructive Calls

void PoolManager::ReleasePool(std::wstring name)
{
	DestroyPool(name);
	UnRegisterPool(name);
}

void PoolManager::ReleaseAllPools()
{
	DestroyAllPools();
	UnRegisterAllPools();
}

#pragma endregion

#pragma region Logging

void PoolManager::Output()
{
	if (!m_logging) return;
}

#pragma endregion

#pragma endregion