#include "EngineCommonStd.h"

#include "IntersectionTests.h"
#include "ClosestPointAlgorithms.h"

#include "Components.h"

using namespace DXMathLibrary;

bool IntersectionTests::SphereToSphere(SphereColliderComponent* A, SphereColliderComponent* B, ContactPoint &point)
{
	Vector3 aPos = A->GetOwner()->Transform()->GetPosition();
	Vector3 bPos = B->GetOwner()->Transform()->GetPosition();

	Vector3 midline = aPos - bPos;
	float size = midline.Length();

	//float radii = (A->GetRadius() + B->GetRadius()) * (A->GetRadius() + B->GetRadius());
	float radii = A->GetRadius() + B->GetRadius();

	if ((size <= 0.0f) || (size >= radii))
	{
		return false;
	}
	else
	{
		point.contactNormal = midline * (1.0f / size);
		point.contactPoint = aPos - midline * 0.5f;
		point.penetration = (A->GetRadius() + B->GetRadius() - size);
		point.setBodyData(A->GetOwner()->Particle(), B->GetOwner()->Particle(), 0.95, 0.0);

		return true;
	}
}

bool IntersectionTests::SphereToAABB(SphereColliderComponent* sphere, BoxColliderComponent* box, ContactPoint &point)
{
	Vector3 spherePos = sphere->GetOwner()->Transform()->GetPosition();

	ClosestPointAlgorithms::ClosestPointAABB(spherePos, box, point);

	Vector3 midline = spherePos - point.contactPoint;
	float size = midline.LengthSq();

	if (size <= sphere->GetRadius() * sphere->GetRadius())
	{
		size = midline.Length();
		point.contactNormal = midline * (1.0f / size);
		point.penetration = sphere->GetRadius() - size;
		point.setBodyData(sphere->GetOwner()->Particle(), box->GetOwner()->Particle(), 0.95, 0.0);

		return true;
	}

	return false;
}

bool IntersectionTests::AABBToAABB(BoxColliderComponent* A, BoxColliderComponent* B, ContactPoint &point)
{
	auto aMinMax = A->GetAABB();
	auto bMinMax = B->GetAABB();

	Vector3 aMin = aMinMax.first;
	Vector3 aMax = aMinMax.second;

	Vector3 bMin = bMinMax.first;
	Vector3 bMax = bMinMax.second;

	if (aMax.x > bMin.x && aMin.x < bMax.x)
	if (aMax.z > bMin.z && aMin.z < bMax.z)
	if (aMax.y > bMin.y && aMin.y < bMax.y)
	{
		Vector3 aPos = A->GetOwner()->Transform()->GetPosition();
		ClosestPointAlgorithms::ClosestPointAABB(aPos, B, point);

		ContactPoint otherPoint;
		Vector3 bPos = B->GetOwner()->Transform()->GetPosition();
		ClosestPointAlgorithms::ClosestPointAABB(bPos, A, otherPoint);

		Vector3 midline = point.contactPoint - otherPoint.contactPoint;
		float size = midline.LengthSq();

		point.contactNormal = midline * (1.0f / size);
		point.penetration = size;
		point.setBodyData(A->GetOwner()->Particle(), B->GetOwner()->Particle(), 0.95, 0.0);
		
		return true;
	}

	return false;
}