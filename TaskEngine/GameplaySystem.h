#pragma once

//class GraphicsAPI;
class D3D9RenderDevice;
class MemoryDelegate;
class GameplayFSM;
class DirectInput;
class Gamepad;
class WebSocketClient;


class GameplaySystem
{
public:

	//memory
	MemoryDelegate* memory;
	//FSM
	GameplayFSM*	fsm;
	//Gameworld

public:

	GameplaySystem();
	~GameplaySystem();

	void Initialize(D3D9RenderDevice* graphics, MemoryDelegate* mem, DirectInput* dinput, Gamepad* pad, WebSocketClient* net);
	void Shutdown();

	void Update(float dt);

};