#pragma once

#include "BaseGUIElement.h"
#include "Sprite.h"


class StaticImageElement : public BaseGUIElement
{
protected:

	Sprite* m_Image;
	D3DCOLOR m_ColorMask;

public:

	StaticImageElement();
	StaticImageElement(BaseGUIElement* parent);
	~StaticImageElement();

	void Draw() override;

public:

	void SetImage(Sprite* image);
	void SetColorMask(D3DCOLOR color);

	IDirect3DTexture9* GetImage();
	Sprite* GetImageObj();

	D3DCOLOR GetColorMask();

};