#pragma once

//#include <vector>
//#include "Geometry.h"

class WorldPlane
{
public:

	DXMathLibrary::Vector3	m_Normal;
	DXMathLibrary::Vector3  m_Point;
	float	m_Offset;

	// The vert positions that make up the plane
	std::vector<DXMathLibrary::Vector3> m_Points;

public:

	WorldPlane();
	WorldPlane(DXMathLibrary::Vector3 normal, float offset);
	~WorldPlane();

	void AddPoint(DXMathLibrary::Vector3 p);

};