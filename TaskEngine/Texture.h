#pragma once

//#include <d3dx9.h>
//#include <string>


class Texture
{
public:

	std::wstring			m_FileName;

	IDirect3DTexture9*	m_Texture;

public:

	Texture();
	Texture(std::wstring fileName);
	~Texture();

	virtual void Load(IDirect3DDevice9* device);
	void Release();

	std::wstring GetFileName(){ return m_FileName; }
	IDirect3DTexture9* GetTexture(){ return m_Texture; }

};