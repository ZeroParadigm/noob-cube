#pragma once


class MeshPipeline;
class EffectsPipeline;
class MemoryDelegate;

class GraphicsSystem
{
friend class GraphicsAPI;
private:

	std::wstring			m_windowCaption;
	bool					m_stats;
	bool					m_drawing;
	float					m_drawRate;
	BOOL					m_systemPaused;
	D3DDEVTYPE				m_devType;
	DWORD					m_RequestedVP;
	HINSTANCE				m_appInst;
	HWND					m_mainWnd;
	IDirect3D9*				m_d3dObject;
	D3DPRESENT_PARAMETERS	m_d3dPP;

	//MemoryDelegate*			m_Memory;
	MeshPipeline*			m_meshPipe;
	EffectsPipeline*		m_effectsPipe;

	//std::function<void (LPARAM)> m_InputHandler; //<OPTIONAL>

private:

	void initDirect3D();
	void initMainWindow();

	void updateFrameStats(float dt);

protected:

	GraphicsSystem(){}
	~GraphicsSystem(){}
	
	void init(MemoryDelegate* memory, HINSTANCE appInstance, const TCHAR* appCaption, int drawRate, bool stats);
	void shutdown();
	void update(float dt);

	void onLostDevice();
	void onResetDevice();

	bool isDeviceLost();
	void setFullScreen(BOOL enable);

	HINSTANCE getAppInst();
	HWND getMainWnd();

public:

	LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);

};