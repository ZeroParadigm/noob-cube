#include "EngineCommonStd.h"

#include "BaseComponent.h"


BaseComponent::BaseComponent(void)
{
	SetId();
	m_Owner = nullptr;
}

BaseComponent::BaseComponent(Actor* owner)
{
	SetId();
	m_Owner	= owner;
}

BaseComponent::~BaseComponent(void)
{
}

void BaseComponent::SetId(void)
{
	static int nextID;
	m_ID = nextID;
	nextID++;
}