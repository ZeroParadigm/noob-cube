#include "EngineCommonStd.h"

#include "MovementComponent.h"	
#include "TransformComponent.h"
#include "Actor.h"

#define origin Vector3(0, 0, 0)

using namespace DXMathLibrary;

//This is a temporary note

MovementComponent::MovementComponent(Actor* owner) : IAIComponent(owner)
{
	m_Camera = false;

	m_MaxSpeed = 150.0f;
	m_MaxAcceleration = Vector3(0, 0, 75);

	m_MaxAngularSpeed = pi / 2;
	m_MaxAngularAcceleration = pi / 2;

	m_Deceleration = slow;

	m_SteeringForce = origin;

	m_TargetSeek = origin;
	m_TargetFlee = origin;
	m_TargetArrive = origin;

	m_TargetPosOrientation = origin;
	m_TargetHeadingOrientation = origin;

	m_TargetOrientation = Quaternion(0, 0, 0, 1);
	m_BaseOrientation = Quaternion(0, 0, 0, 1);

	m_AgentEvade = nullptr;
	m_AgentPursue = nullptr;
	m_AgentInterpose = nullptr;

	m_WanderTarget = origin;
	m_WanderProjection = origin;

	m_WanderOffset = 2.0f;
	m_WanderRadius = 1.2f;
	m_WanderJitter = 80.0f;

	m_WeightSeek = 1.0f;
	m_WeightFlee = 1.0f;
	m_WeightWander = 1.0f;
	m_WeightArrive = 1.0f;
	m_WeightPursue = 1.0f;
	m_WeightEvade = 0.1f;
	m_WeightOffsetPursue = 1.0f;
	m_WeightInterpose = 1.0f;
	m_WeightHide = 1.0f;
	m_WeightSeparation = 1.0f;
	m_WeightAlignment = 1.0f;
	m_WeightCohesion = 1.0f;
	m_WeightFollowPath = 0.5f;

	m_ViewDistance = 10.0f;
	m_BoxLength = 10.0f;

	CreateFeelers();

	SteeringOff();
}
MovementComponent::~MovementComponent(void)
{
	m_AgentEvade = nullptr;
	m_AgentPursue = nullptr;

	DestroyFeelers();
}

float MovementComponent::GetMaxRotation(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_MaxAngularSpeed;
}
float MovementComponent::GetMaxRotationAcceleration(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_MaxAngularAcceleration;
}

float MovementComponent::GetMaxSpeed(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_MaxSpeed;
}
Vector3 MovementComponent::GetMaxAcceleration(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_MaxAcceleration;
}

bool MovementComponent::GetCameraMode(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Camera;
}

void MovementComponent::SetMaxRotation(float rot)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_MaxAngularSpeed = rot;
}
void MovementComponent::SetMaxRotationAcceleration(float rotAccel)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_MaxAngularAcceleration = rotAccel;
}

void MovementComponent::SetMaxSpeed(float speed)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_MaxSpeed = speed;
}
void MovementComponent::SetMaxAcceleration(Vector3 accel)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_MaxAcceleration = accel;
}

void MovementComponent::SetCameraMode(bool cam)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_Camera = cam;
}

void MovementComponent::SetTargetSeek(const Vector3 target)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetSeek = target;
}

void MovementComponent::SetTargetFlee(const Vector3 target)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetFlee = target;
}

void MovementComponent::SetTargetArrive(const Vector3 target)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetArrive = target;
}

void MovementComponent::SetAgentToEvade(TransformComponent* pursuer)
{
	if (pursuer)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		m_AgentPursue = pursuer;
	}
}

void MovementComponent::SetAgentToPursue(TransformComponent* evader)
{
	if (evader)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		m_AgentEvade = evader;
	}
}

void MovementComponent::SetAgentToInterpose(TransformComponent* interposed)
{
	if (interposed)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		m_AgentInterpose = interposed;
	}
}

TransformComponent* MovementComponent::GetPursuer(void)
{
	if (m_AgentPursue)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		return m_AgentPursue;
	}
	return nullptr;
}

TransformComponent* MovementComponent::GetEvader(void)
{
	if (m_AgentEvade)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		return m_AgentEvade;
	}
	return nullptr;
}

TransformComponent* MovementComponent::GetInterposed(void)
{
	if (m_AgentInterpose)
	{
		ScopedCriticalSection(this->m_CriticalSection);
		return m_AgentInterpose;
	}
	return nullptr;
}

void MovementComponent::SetTargetPoint3D(Vector3 p)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetPosOrientation = p;
}

void MovementComponent::SetTargetHeading(Vector3 h)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetHeadingOrientation = h;
}

void MovementComponent::SetTargetAlignment(Quaternion q)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_TargetOrientation = q;
}

void MovementComponent::SetWanderOffset(float offset)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_WanderOffset = offset;
}
void MovementComponent::SetWanderRadius(float radius)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_WanderRadius = radius;
}
void MovementComponent::SetWanderJitter(float jitter)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_WanderJitter = jitter;
}
void MovementComponent::SetWanderTarget(Vector3 target)
{
	ScopedCriticalSection(this->m_CriticalSection);
	m_WanderTarget = target;
}

Vector3 MovementComponent::GetTargetSeek(void)
{
	return m_TargetSeek;
}
Vector3 MovementComponent::GetTargetFlee(void)
{
	return m_TargetFlee;
}

Vector3 MovementComponent::GetTargetArrive(void)
{
	return m_TargetArrive;
}

Vector3 MovementComponent::GetTargetPoint3D(void)
{
	return m_TargetPosOrientation;
}

Vector3 MovementComponent::GetTargetVectorAlign(void)
{
	return m_TargetHeadingOrientation;
}

Quaternion MovementComponent::GetTargetAlignment(void)
{
	return Quaternion();
}

Vector3 MovementComponent::GetWanderTarget(void)
{
	return m_WanderTarget;
}
float MovementComponent::GetWanderOffset(void)
{
	return m_WanderOffset;
}
float MovementComponent::GetWanderRadius(void)
{
	return m_WanderRadius;
}
float MovementComponent::GetWanderJitter(void)
{
	return m_WanderJitter;
}

void MovementComponent::CreateFeelers(void)
{
	Vector3 feelers[5];

	feelers[0] = Vector3(0, 0, 1);	//Forward
	feelers[1] = Vector3(1, 0, 1);	//Right
	feelers[2] = Vector3(-1, 0, 1); //Left
	feelers[3] = Vector3(0, 1, 1);  //Top
	feelers[4] = Vector3(0, -1, 1); //Bottom

	for (int i = 0; i < 5; i++)
	{
		feelers[i].Normalize();
		feelers[i] *= m_ViewDistance;
		m_Feelers.push_back(feelers[i]);
	}
}

void MovementComponent::DestroyFeelers(void)
{
	m_Feelers.clear();
}

std::vector<Vector3>* MovementComponent::GetFeelers(void)
{
	return &m_Feelers;
}

Vector3 MovementComponent::GetPosition(void)
{
	return m_Owner->Transform()->GetPosition();
	//return origin;
}
Vector3 MovementComponent::GetVelocity(void)
{
	return m_Owner->Transform()->GetVelocity();
	//return origin;
}

Vector3 MovementComponent::GetAngularVelocity(void)
{
	return m_Owner->Transform()->GetAngularVelocity();
	//return origin;
}
Quaternion MovementComponent::GetOrientation(void)
{
	return m_Owner->Transform()->GetOrientation();
	//return Quaternion(0, 0, 0, 1);
}

void MovementComponent::SetVelocity(Vector3 v)
{
	m_Owner->Transform()->SetVelocity(v);
}

void MovementComponent::SetPosition(Vector3 p)
{
	m_Owner->Transform()->SetPosition(p);
}

void MovementComponent::SetOrientation(Quaternion q)
{
	m_Owner->Transform()->SetOrientation(q);
}

void MovementComponent::SetAngularVelocity(Vector3 a)
{
	m_Owner->Transform()->SetAngularVelocity(a);
}

void MovementComponent::SetDeceleration(int d)
{
	switch (d)
	{
	case 1:
		m_Deceleration = slow;
		break;
	case 2:
		m_Deceleration = normal;
		break;
	case 3:
		m_Deceleration = fast;
		break;
	default:
		m_Deceleration = normal;
		break;
	}
}