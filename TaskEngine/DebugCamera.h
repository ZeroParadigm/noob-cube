#pragma once
#include "CameraComponent.h"


//This is a very open and easy to manipulate debug camera
//	it can operate detatched from an Actor / Transform Component
class DebugCamera : public CameraComponent
{
public:
	
	Vector3		m_debugPosition;
	Quaternion	m_debugOrientation;

	float		m_speed;

	bool		m_renderFrustum;

	DebugCamera(float x, float y, float z) : CameraComponent(nullptr)
	{
		m_debugPosition = Vector3(x, y, z);

		m_renderFrustum = false;

		m_speed = 25.0f;
	}
	~DebugCamera()
	{

	}

	virtual Matrix4 GetWorldMatrix();

	void AdjustDebugCamera(float dt);
	void RotateDebugCamera(float dt);

	void Update(float dt);

	void BuildView();
};