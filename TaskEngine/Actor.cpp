#include "EngineCommonStd.h"

#include "Actor.h"



Actor::Actor(void)
{
	SetId();
}

Actor::~Actor(void)
{
}

void Actor::SetId(void)
{
	static int nextID = 0;
	m_Id = nextID;
	nextID++;
}