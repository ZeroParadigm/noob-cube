#include "EngineCommonStd.h"
#include "WindowController.h"


WindowController::WindowController()
{

}

WindowController::~WindowController()
{
	Cleanup();
}

void WindowController::Init(HINSTANCE appInstance, const TCHAR* appCaption)
{
	m_appInstance	= appInstance;
	m_appCaption	= appCaption;
}

void WindowController::Cleanup()
{
	for (auto window : m_windows)
		delete window.second;
	m_windows.clear();
}

void WindowController::SetMain(BaseWindow* window)
{
	if (m_mainWindow != nullptr)
	{
		if (m_mainWindow == window)
			return;

		if (m_mainWindow)
			m_mainWindow->SetMainWindow(false);
	}

	m_mainWindow = window;
	m_mainWindow->SetMainWindow(true);
}

void WindowController::SetMain(int id)
{
	BaseWindow* canidate = m_windows.at(id);

	if (m_mainWindow != nullptr)
	{
		if (m_mainWindow == canidate)
			return;

		if (m_mainWindow)
			m_mainWindow->SetMainWindow(false);
	}

	m_mainWindow = canidate;
	m_mainWindow->SetMainWindow(true);
}

BaseWindow* WindowController::CreateAppRenderWindow(const TCHAR* windowName,
													DWORD options,
													IRenderDevice* device,
													int x, int y,
													int w, int h)
{
	//Allocate memory for window
	RenderWindow* win = new RenderWindow();

	//Init the window internals. returns the unique id to this window
	int id = win->Init(m_appInstance,
					   (std::wstring(m_appCaption) + L" - " + std::wstring(windowName)).c_str(),
					   x, y, 
					   w, h);

	//Attatch a graphics rendering device interface (DX9, 11, OpenGL)
	win->AttatchDevice(device);

	//Add window to the window pool
	m_windows.insert(std::make_pair(id, win));

	//If this is the first window than make it the main window
	if (m_mainWindow == nullptr) SetMain(win);

	return win;
}