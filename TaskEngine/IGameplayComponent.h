#pragma once

#include "BaseComponent.h"

class Observable;

class IGameplayComponent : public BaseComponent
{
private:


protected:

	Observable*	m_Observable;

public:

	IGameplayComponent();
	IGameplayComponent(Actor* owner);
	~IGameplayComponent();

	virtual void Update(float dt);
};