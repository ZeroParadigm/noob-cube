#include "EngineCommonStd.h"
#include "MemoryDelegate.h"


MemoryDelegate* MemoryDelegate::instance()
{
	static MemoryDelegate ptr;
	return &ptr;
}

//Memory Delegate Private API (SimpleEngine Accessable ONLY)

void MemoryDelegate::init(IDirect3DDevice9* d3dDevice)
{
	m_ActorManager = new ActorManager();
	m_ComponentManager = new ComponentManager();
	m_ResourceManager = new ResourceManager(d3dDevice);

	m_ActorFactory = new ActorFactory(m_ActorManager, m_ComponentManager, m_ResourceManager);
}

void MemoryDelegate::shutdown()
{
	delete m_ActorFactory;

	delete m_ComponentManager;
	delete m_ActorManager;
	delete m_ResourceManager;
}

//Memory Delegate Public API

//std::vector<Actor*>* MemoryDelegate::GetActorList()
//{
//	return m_ActorManager->GetActorList();
//}
//
//std::vector<Actor*>* MemoryDelegate::GetActorList(std::string componentType)
//{
//	if(componentType == "TRANSFORM")
//	{
//		return m_ActorManager->GetActorList(TRANSFORM);
//	}
//	else if(componentType == "MESH")
//	{
//		return m_ActorManager->GetActorList(MESH);
//	}
//
//	return 0;
//}
//
//std::vector<BaseComponent*>* MemoryDelegate::GetComponentList()
//{
//	return m_ComponentManager->GetComponentList();
//}
//
//std::vector<BaseComponent*>* MemoryDelegate::GetComponentList(std::string componentType)
//{
//	if(componentType == "TRANSFORM")
//	{
//		return m_ComponentManager->GetComponentList(TRANSFORM);
//	}
//	else if(componentType == "MESH")
//	{
//		return m_ComponentManager->GetComponentList(MESH);
//	}
//
//	return 0;
//}
//
//Mesh* MemoryDelegate::GetMesh(std::string fileName)
//{
//	return m_ResourceManager->GetMesh(fileName);
//}