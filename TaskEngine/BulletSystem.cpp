#include "EngineCommonStd.h"

#include "BulletSystem.h"
#include "BulletComponent.h"
#include "ActorFactory.h"

using namespace DXMathLibrary;

BulletSystem::BulletSystem()
{
}

BulletSystem::~BulletSystem()
{
}

void BulletSystem::Initialize(ActorFactory* factory)
{
	int maxBullets = 100;

	for (int x = 0; x < maxBullets; ++x)
	{
		m_Bullets.push_back(factory->CreateBullet());
	}
}

void BulletSystem::Shutdown()
{
	for (BulletComponent* b : m_Bullets)
	{
		BulletComponent* temp = b;
		b = nullptr;
		delete temp;
	}
	m_Bullets.clear();
}

BulletComponent* BulletSystem::GetBullet()
{
	for (BulletComponent* b : m_Bullets)
	{
		if (!b->IsActive())
		{
			return b;
		}
	}
	
	return nullptr;
}

void BulletSystem::ShootBullet(Vector3 startPos, Vector3 normal, float speed)
{
	BulletComponent* bullet = GetBullet();

	bullet->Activate(startPos, normal, speed);
}