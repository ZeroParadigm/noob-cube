#pragma once


class Observable;


class BaseGUIElement
{
protected:

	bool							m_Visible;
	unsigned int					m_ID;

	std::wstring					m_Name;

	DXMathLibrary::Vector2			m_Position;

	unsigned int					m_Width;
	unsigned int					m_Height;

	BaseGUIElement*					m_Parent;
	std::map<std::wstring, BaseGUIElement*>	m_Children;

	Observable*						m_Observable;

public:

	BaseGUIElement();
	BaseGUIElement(BaseGUIElement* parent);
	virtual ~BaseGUIElement();

	virtual void Draw(){}

public:

	void							SetVisibility(bool visible);
	void							SetName(std::wstring name);

	void							SetPosition(DXMathLibrary::Vector2 position);

	void							SetWidth(unsigned int width);
	void							SetHeight(unsigned int height);

	void							SetParent(BaseGUIElement* parent);
	void							LeaveParent();

	void							AdoptChild(BaseGUIElement* child);
	void							OrphanChild(BaseGUIElement* child);
	void							OrphanChildren();

	bool							IsVisible();
	bool							HasParent();
	unsigned int					GetID();
	
	std::wstring					GetName();

	DXMathLibrary::Vector2			GetPosition();

	unsigned int					GetWidth();
	unsigned int					GetHeight();

	DXMathLibrary::Matrix			GetMatrix();
	RECT							GetScreenArea();
	DXMathLibrary::Vector2			GetScreenPosition();

	BaseGUIElement*					GetParent();
	BaseGUIElement*					GetChild(std::wstring childName);

private:

	void SetID();

};