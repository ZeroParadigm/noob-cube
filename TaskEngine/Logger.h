//#pragma once
//#include <DbgHelp.h>
//#include <string>
//#include <Windows.h>
//#include <sstream>
//#include <unordered_map>
//
//#include <stdio.h>
//#include <stdarg.h> 
//#include <iomanip>
//#include <crtdbg.h>
//
//#define LOGGER_ENABLE 1
//
///*
// When looking for errors Try - Catch statements
// will need to be used in potential problem areas.
//
//	try
//	{
//		//Buggy Code
//	}
//	catch(...)
//	{
//		LOGGER->DumpToFile();
//	}
//*/
//
//
//enum eDebugLogType
//{
//	DBG_NULL = 0,
//	DBG_GENERIC,
//	DBG_ALLOCATION,
//	DBG_COUNT,
//};
//
//enum eLoggerSettings
//{
//	LOGGER_OFF = 0x0,
//	LOGGER_LOGGING = 0x1,
//	LOGGER_ALOC_LOGGING = 0x2,
//	LOGGER_OUTPUT_DUMP = 0x4,
//	LOGGER_FILE_DUMP = 0x10,
//	LOGGER_OUTPUT = 0x20,
//
//	LOGGER_LIGHT = LOGGER_FILE_DUMP | LOGGER_OUTPUT,
//	LOGGER_MEDIUM = LOGGER_LIGHT | LOGGER_LOGGING,			//DEFAULT
//	LOGGER_HIGH = LOGGER_MEDIUM | LOGGER_ALOC_LOGGING | LOGGER_OUTPUT_DUMP,
//};
//
//#if(LOGGER_ENABLE)
//
//#if defined(_DEBUG)
//#define ENGINE_NEW new(_NORMAL_BLOCK,__FILE__,__LINE__)
//#else
//#define ENGINE_NEW new
//#endif
//
//#define LOGGER Logging::Instance()
//
//#define LOG_NEW(x) LOGGER->LogAllocation(new x, __FILE__, __LINE__)
//#define LOG(log) LOGGER->Log(log, __FILE__, __LINE__)
//
//
//struct logdef
//{
//	eDebugLogType type;
//	int line;
//	int id;
//	std::string file;
//	std::string message;
//	double time;
//
//	logdef()
//	{
//		static int nextID = 0;
//		id = nextID;
//		nextID++;
//	}
//};
//
//class Logging
//{
//private:
//
//	std::unordered_map<int, logdef> m_logs;
//	//Concurrency::concurrent_unordered_map<int, logdef> m_tLogs;
//	time_t m_start;
//
//	DWORD m_LoggerSettings;
//
//private:
//
//	Logging()
//	{
//		m_start = time(0);
//
//		m_LoggerSettings = LOGGER_MEDIUM;
//	}
//
//	inline void Getfilename(std::string &filename)
//	{
//		TCHAR output[255];
//		TCHAR ext[255];
//
//		_splitpath_s(filename.c_str(), NULL, 0, NULL, 0, &output[0], 255, &ext[0], 255);
//		strcat_s(output, ext);
//
//		filename = std::string(output);
//	}
//
//	void GetStack(std::string &stack)
//	{
//		std::ostringstream output;
//
//		SymSetOptions( SYMOPT_DEFERRED_LOADS | SYMOPT_INCLUDE_32BIT_MODULES | SYMOPT_UNDNAME | SYMOPT_LOAD_LINES );
//		if (!SymInitialize(GetCurrentProcess(), "http://msdl.microsoft.com/download/symbols", TRUE )) return;
// 
//		PVOID addrs[ 25 ] = { 0 };
//		USHORT frames = CaptureStackBackTrace( 3, 25, addrs, NULL );
//
//		for (USHORT i = 0; i < frames; i++) 
//		{
//			ULONG64 buffer[ (sizeof( SYMBOL_INFO ) + 1024 + sizeof( ULONG64 ) - 1) / sizeof( ULONG64 ) ] = { 0 };
//
//			SYMBOL_INFO *info = (SYMBOL_INFO *)buffer;
//			IMAGEHLP_LINE64 line;
//
//			info->SizeOfStruct = sizeof( SYMBOL_INFO );
//			info->MaxNameLen = 1024;
// 
//			DWORD64 displacement = 0;
//			DWORD  dwDisplacement = 0;
//
//			if (SymFromAddr(GetCurrentProcess(), (DWORD64)addrs[i], &displacement, info ) &&
//				SymGetLineFromAddr64(GetCurrentProcess(), (DWORD64)addrs[i], &dwDisplacement, &line)) 
//			{
//				std::string filename(line.FileName);
//				Getfilename(filename);
//				output << std::left << "# " << std::left << i << " " << std::setw(20) << std::left << std::string(info->Name) << " " << std::setw(18) << filename << " " << std::setw(8) << "Line: " << line.LineNumber << std::endl;
//			}
//		}
//
//		stack = output.str();
//
//		SymCleanup(GetCurrentProcess());
//	}
//
//	void GenerateDump(std::string &dump)
//	{
//		std::ostringstream output;
//
//		for (auto log : m_logs)
//		{
//			Getfilename(log.second.file);
//			output << "< >ID: " << log.second.id << " File: " << log.second.file << " Line: " << log.second.line << " Message: " << log.second.message << std::endl;
//		}
//
//		output << std::endl << "Stack trace " << std::setfill('-') << std::setw(45) << "-" << std::endl << std::endl;
//		output << std::setfill(' ');
//
//		std::string stack;
//		GetStack(stack);
//
//		output << stack << std::endl;
//
//		dump = output.str();
//	}
//
//public:
//	static Logging* Instance()
//	{
//		static Logging ptr;
//		return &ptr;
//	}
//	~Logging(){}
//
//	void LeakTracking(bool state)
//	{
//#if defined(_DEBUG)
//		if (state)
//		{
//			_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//		}
//		else
//		{
//			_CrtSetDbgFlag(0);
//		}
//#else
//#endif
//	}
//
//	void InjectBreakPointAtBlock(long block)
//	{
//#if defined(_DEBUG)
//		_CrtSetBreakAlloc(block);
//#else
//#endif
//	}
//
//	void SetLoggerSettings(DWORD settings)
//	{
//		m_LoggerSettings = settings;
//	}
//
//	void DumpToFile()
//	{
//		if (!(m_LoggerSettings & LOGGER_FILE_DUMP)) return;
//		std::string dump;
//		GenerateDump(dump);
//
//		FILE *file = NULL;
//		_wfopen_s(&file, std::wstring(L"Dumpfile.dump").c_str(), L"w,ccs=UTF-8");
//		fwprintf_s(file, std::wstring(dump.begin(), dump.end()).c_str());
//		fclose(file);
//	}
//
//	void DumpToOutput()
//	{
//		if (!(m_LoggerSettings & LOGGER_OUTPUT_DUMP)) return;
//
//		std::string dump;
//		GenerateDump(dump);
//
//		OutputDebugStringA(dump.c_str());
//	}
//
//	void DebugOutput(TCHAR* formatString, ...)
//	{
//		if (!(m_LoggerSettings & LOGGER_OUTPUT)) return;
//
//		va_list marker;
//		va_start(marker, formatString);
//		TCHAR buf[1024];
//		_vsnprintf_s(buf, sizeof(buf), formatString, marker);
//		OutputDebugStringA("\n");
//		OutputDebugStringA(buf);
//	}
//
//	void Log(TCHAR* message, TCHAR* file, int line)
//	{
//		if (!(m_LoggerSettings & LOGGER_LOGGING)) return;
//
//		logdef log;
//
//		log.message = std::string(message);
//		log.file = std::string(file);
//		log.line = line;
//		log.type = DBG_GENERIC;
//		log.time = difftime( time(0), m_start);
//
//		m_logs.insert(std::make_pair(log.id, log));
//	}
//
//	template<class T>
//	T LogAllocation(T alloc, TCHAR* file, int line)
//	{
//		if (!(m_LoggerSettings & LOGGER_ALOC_LOGGING)) return alloc;
//
//		logdef log;
//
//		log.message = "Memory Allocation";
//		log.file = std::string(file);
//		log.line = line;
//		log.type = DBG_ALLOCATION;
//		log.time = difftime(time(0), m_start);
//
//		m_logs.insert(std::make_pair(log.id, log));
//
//		return alloc;
//	}
//};
//#else
//
//#define ENGINE_NEW new
//#define LOGGER Logging::Instance()
//#define LOG_NEW(x)(new x)
//#define LOG(log)(log)
//
//
//class Logging
//{
//private:
//
//	Logging(){}
//
//public:
//	static Logging* Instance()
//	{
//		static Logging ptr;
//		return &ptr;
//	}
//	~Logging(){}
//
//	void LeakTracking(bool state){}
//	void InjectBreakPoint(long block){}
//	void SetLoggerSettings(DWORD settings){}
//	void DumpToFile(){}
//	void DumpToOutput(){}
//	void DebugOutput(TCHAR* formatString, ...){}
//	void Log(TCHAR* message, TCHAR* file, int line){}
//	template<class T> 
//	T LogAllocation(T alloc, TCHAR* file, int line){}
//};
//#endif