#include "EngineCommonStd.h"

#include "ComponentManager.h"

using namespace std;

ComponentManager::ComponentManager(void)
{
}

ComponentManager::~ComponentManager(void)
{
	ClearComponents();
}

BaseComponent* ComponentManager::CreateComponent(Actor* owner, int type)
{
	BaseComponent* component = 0;

	switch(type)
	{
	case COMPONENT_TRANSFORM:
		{
			component = new TransformComponent(owner);
			owner->m_Transform = (TransformComponent*)component;
			m_TransformComponents.push_back((TransformComponent*)component);
			break;
		}
	case COMPONENT_MESH:
		{
			component = new MeshComponent(owner);
			owner->m_Mesh = (MeshComponent*)component;
			m_MeshComponents.push_back((MeshComponent*)component);
			break;
		}
	case COMPONENT_RENDERER:
		{
			//m_RendererComponents.push_back(component);
			break;
		}
	case COMPONENT_SPHERE:
		{
			component = new SphereColliderComponent(owner);
			owner->m_Collider = (SphereColliderComponent*)component;
			m_ColliderComponents.push_back((SphereColliderComponent*)component);
			break;
		}
	case COMPONENT_BOX:
		{
			component = new BoxColliderComponent(owner);
			owner->m_Collider = (BoxColliderComponent*)component;
			m_ColliderComponents.push_back((BoxColliderComponent*)component);
			break;
		}
	case COMPONENT_EFFECT:
		{
			break;
		}
	case COMPONENT_CAMERA:
		{
			component = new CameraComponent(owner);
			owner->m_Renderer = (CameraComponent*)owner;
			m_RendererComponents.push_back((CameraComponent*)component);
			break;
		}
	case COMPONENT_SKYBOX:
		{
			break;
		}
	case COMPONENT_GUI_TEXT:
		{
			break;
		}
	case COMPONENT_GUI_LAYER:
		{
			break;
		}
	case COMPONENT_GUI_IMAGE:
		{
			break;
		}
	case COMPONENT_MOVEMENT:
		{
			component = new MovementComponent(owner);
			owner->m_Ai = (MovementComponent*)component;
			m_AIComponents.push_back((IAIComponent*)component);
			m_MovementComponents.push_back((MovementComponent*)component);
			break;
		}
	case COMPONENT_PARTICLEPHYSICS:
		{
			component = new ParticleComponent(owner);
			owner->m_Particle = (ParticleComponent*)component;
			m_PhysicsComponents.push_back((ParticleComponent*)component);
			break;
		}
	case COMPONENT_SCRIPT:
		{
			//component = new ScriptComponent();
			//m_ScriptComponents.push_back(component);
			break;
		}
	case COMPONENT_GAMEWORLD:
		{
			component = new GameWorldComponent(owner);
			owner->m_Gameplay = (GameWorldComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	case COMPONENT_PLAYER:
		{
			component = new PlayerComponent(owner);
			owner->m_Gameplay = (PlayerComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	case COMPONENT_ENEMY:
		{
			component = new EnemyComponent(owner);
			owner->m_Gameplay = (EnemyComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	case COMPONENT_BULLET:
		{
			component = new BulletComponent(owner);
			owner->m_Gameplay = (BulletComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	case COMPONENT_TRIGGERVOLUME:
		{
			component = new TriggerVolumeComponent(owner);
			owner->m_Gameplay = (TriggerVolumeComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	case COMPONENT_EDGETRIGGERVOLUME:
		{
			component = new EdgeTriggerVolume(owner);
			owner->m_Gameplay = (TriggerVolumeComponent*)component;
			m_GameplayComponents.push_back((IGameplayComponent*)component);
			break;
		}
	default:
		{
			return 0;
			break;
		}
		break;
	}

	m_ComponentList.push_back(component);

	return component;
}

BaseComponent* ComponentManager::GetComponent(int id)
{
	std::map<int, BaseComponent*>::iterator component = m_ComponentMap.find(id);

	if (component != m_ComponentMap.end())
	{
		return component->second;
	}
	
	return nullptr;
}

//Removed to force access through memory delegate

//std::vector<BaseComponent*> ComponentManager::GetAllComponents(void)
//{
//	return m_ComponentList;
//}
//
//std::vector<TransformComponent*> ComponentManager::GetTransforms(void)
//{
//	return m_TransformComponents;
//}
//
//std::vector<MeshComponent*> ComponentManager::GetMeshes(void)
//{
//	return m_MeshComponents;
//}
//
//std::vector<BaseComponent*> ComponentManager::GetRenderers(void)
//{
//	return m_RendererComponents;
//}
//
//std::vector<IAIComponent*> ComponentManager::GetAi(void)
//{
//	return m_AIComponents;
//}
//
//std::vector<IPhysicsComponent*> ComponentManager::GetPhysics(void)
//{
//	return m_PhysicsComponents;
//}
//
//std::vector<BaseComponent*> ComponentManager::GetScripts(void)
//{
//	return m_ScriptComponents;
//}
//
//std::vector<GameplayComponent*> ComponentManager::GetGameplay(void)
//{
//	return m_GameplayComponents;
//}

void ComponentManager::ClearComponents(void)
{
	for (auto comp : m_ComponentList)
	{
		BaseComponent* temp = comp;
		comp = nullptr;
		if (temp)
		{
			delete temp;
		}
	}

	m_ComponentList.clear();
	m_ComponentMap.clear();
}

//vector<IColliderComponent*> ComponentManager::GetColliders()
//{
//	return m_ColliderComponents;
//}
//
//vector<RigidBodyComponent*> ComponentManager::GetRigidBodies()
//{
//	return m_RigidBodyComponents;
//}
