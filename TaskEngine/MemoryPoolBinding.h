#pragma once

class Pool;
class PoolManager;


//Binds Declaration to memory pool allocation and deallocation operators
#define ENGINE_MEMORYPOOL_DECL(__defaultNumChunks__)\
	public: \
		static Pool* pool; \
		static void InitMemoryPool(unsigned int numChunks = __defaultNumChunks__, const TCHAR* debugName = 0); \
		static void DestroyMemoryPool(void); \
		static void* operator new(size_t size); \
		static void operator delete(void* ptr); \
		static void* operator new[](size_t size); \
		static void operator delete[](void* ptr); \
	private:\
//======================================================================


#define ENGINE_MEMORYPOOL_CORE_DEF(_className_)\
	Pool* _className_::pool = NULL; \
	void _className_::DestroyMemoryPool(void) \
	{ \
		ENGINE_ASSERT(pool != NULL); \
		SAFE_DELETE(pool); \
	} \
	void* _className_::operator new(size_t size) \
	{ \
		ENGINE_ASSERT(pool); \
		void* pMem = pool->Alloc(); \
		return pMem; \
	} \
	void _className_::operator delete(void* pPtr) \
	{ \
		ENGINE_ASSERT(pool); \
		pool->Free(pPtr); \
	} \
	void* _className_::operator new[](size_t size) \
	{ \
		ENGINE_ASSERT(pool); \
		void* pMem = pool->Alloc(); \
		return pMem; \
	} \
	void _className_::operator delete[](void* pPtr) \
	{ \
		ENGINE_ASSERT(pool); \
		pool->Free(pPtr); \
	} \
//======================================================================

//Binds class to an unmanaged memory pool
#define ENGINE_MEMORYPOOL_UNMANAGED(_className_)\
	void _className_::InitMemoryPool(unsigned int numChunks, const TCHAR* debugName) \
	{ \
		if (pool != NULL) \
		{ \
			ENGINE_ERROR(L"pool is not NULL.  All data will be destroyed.  (Ignorable)"); \
			SAFE_DELETE(pool); \
		} \
		pool = DBG_NEW Pool; \
		if (debugName) \
			pool->SetDebugPoolName(debugName); \
		else \
			pool->SetDebugPoolName(L#_className_); \
		pool->Init(sizeof(_className_), numChunks, true); \
	} \
	ENGINE_MEMORYPOOL_CORE_DEF(_className_)\

//======================================================================


//Binds class to a managed memory pool (EXPERIMENTAL)
#define ENGINE_MEMORYPOOL_MANAGED(_className_)\
	void _className_::InitMemoryPool(unsigned int numChunks, const TCHAR* debugName) \
	{ \
		if (pool != NULL) \
		{ \
			ENGINE_ERROR(L"pool is not NULL.  All data will be destroyed.  (Ignorable)"); \
			SAFE_DELETE(pool); \
		} \
		pool = DBG_NEW Pool; \
		if (debugName) \
			pool->SetDebugPoolName(debugName); \
		else \
			pool->SetDebugPoolName(L#_className_); \
		pool->Init(sizeof(_className_), numChunks, true, true); \
	} \
	ENGINE_MEMORYPOOL_CORE_DEF(_className_)\

//======================================================================


//Static memory pool binding (managed vs unmanaged depends on the above definition used
#define ENGINE_MEMORYPOOL_AUTOINIT_CUSTOMNAME(_className_, _numChunks_, _debugName_) \
	class _className_ ## _AutoInitializedMemoryPool \
	{ \
	public: \
		_className_ ## _AutoInitializedMemoryPool(void); \
		~_className_ ## _AutoInitializedMemoryPool(void); \
	}; \
	_className_ ## _AutoInitializedMemoryPool::_className_ ## _AutoInitializedMemoryPool(void) \
	{ \
		_className_::InitMemoryPool(_numChunks_, _debugName_); \
	} \
	_className_ ## _AutoInitializedMemoryPool::~_className_ ## _AutoInitializedMemoryPool(void) \
	{ \
	_className_::DestroyMemoryPool(); \
	} \
	static _className_ ## _AutoInitializedMemoryPool s_ ## _className_ ## _AutoInitializedMemoryPool; \

#define ENGINE_MEMORYPOOL_AUTOINIT(_className_, _numChunks_) ENGINE_MEMORYPOOL_AUTOINIT_CUSTOMNAME(_className_, _numChunks_, L#_className_)
//======================================================================

#pragma region Explanations

/*
	The above macros are a bit advanced and do some really cool compile time unpacking
	with the careful use of preprocessor defines.

	preprocessor defines recieve one logical pass before code compilation. Therefore,
	it is not possible to have preprocessor logic to determine if a preprocessor define
	becomes available EG:

	#define SOME_MACRO(_someValue_) \
	#define USING_FOO \					<-- ERROR
	#ifdef USING_FOO \					<--	"
	#define MY_VALUE _someValue_ \		<--	"
	#elif \								<--	"
	#undef SOME_OTHER_MACRO \			<--	"
	#endif \							<--	"

	each # define causes a compiler error. Basically alerting you to the fact that this
	logic will never be hit. If it were to magically exisit than the logc checks in both the
	preprocessing phase and compilation phases would produce errors.

	Design around it.


	Furthermore, there are two potentially confusing things above, # and ##

	Both of these are unique and powerful on their own they are used to manipulate Tokens. Firstly:
	# Is used to explicitly change a Token into a string (const TCHAR*) This allows the preprocessor to
		treat it as a string before it is determined to be converted to one.

		Example: 
			#define FORMAT_NUMBER(_myNumber_) printf(#_myNumber_" is string %d is number", _myNumber_)

			FORMAT_NUMBER(10) -> printf("10 is string %d is number", 10)

	## Is ued to paste Tokens together. This is helpful for creating single compound tokens during the
		preprocessing phase.

		Example:
			#define DYNAMIC_CLASS(_className_) class _className_##_MyDynamicClass\

			DYNAMIC_CLASS(MyClass) - > class MyClass_MyDynamicClass



	Now, let's see this in practice with some code unpacking!

	using the ' \ ' allows you to add multiple lines to a macro definition. This can sometimes cause 
	end of file errors. So, try not to define macros like this at the end of a file.
	This allows you to write whole classes, static functions... well anything really by just using
	macros. Though, there is no real need to since c++ supports things like templates and typedefs.
	The above code is possibly one of the coolest examples of where it can be used very effectively.

	Let's examine the macro:

	ENGINE_MEMORYPOOL_AUTOINIT(_className_, _numChunks_)

	This macro is creating a polymorphic class that is than statically delcared and envokes functionality
	declared in the specified ' _className_ ' Token

	Let's unpack this like the preprocessor would:


			ENGINE_MEMORYPOOL_AUTOINIT(MyClass, 50) 

					V Envokes V

			ENGINE_MEMORYPOOL_AUTOINIT_CUSTOMNAME(MyClass, 50, L#MyClass)

					V Envokes V

			ENGINE_MEMORYPOOL_AUTOINIT_CUSTOMNAME(MyClass, 50, L#MyClass)

					V Execute # V

			ENGINE_MEMORYPOOL_AUTOINIT_CUSTOMNAME(MyClass, 50, L"MyClass")

					V Write V

			class MyClass ## _AutoInitializedMemoryPool 
			{ 
			public: 
				MyClass ## _AutoInitializedMemoryPool(void); 
				~MyClass ## _AutoInitializedMemoryPool(void); 
			}; 
			MyClass ## _AutoInitializedMemoryPool::MyClass ## _AutoInitializedMemoryPool(void) 
			{ 
				MyClass::InitMemoryPool(50, L"MyClass"); 
			} 
			MyClass ## _AutoInitializedMemoryPool::~MyClass ## _AutoInitializedMemoryPool(void) 
			{ 
				MyClass::DestroyMemoryPool(); 
			} 
			static MyClass ## _AutoInitializedMemoryPool s_ ## MyClass ## _AutoInitializedMemoryPool; 

					V Execute ## V 

			class MyClass_AutoInitializedMemoryPool
			{
			public:
				MyClass_AutoInitializedMemoryPool(void);
				~MyClass_AutoInitializedMemoryPool(void);
			};
			MyClass_AutoInitializedMemoryPool::MyClass_AutoInitializedMemoryPool(void)
			{
				MyClass::InitMemoryPool(50, L"MyClass");
			}
			MyClass_AutoInitializedMemoryPool::~MyClass_AutoInitializedMemoryPool(void)
			{
				MyClass::DestroyMemoryPool();
			}
			static MyClass_AutoInitializedMemoryPool s_MyClass_AutoInitializedMemoryPool;

			V Adds to Source and moves to compilation V

	I hope the Memory Pool Binding macros help to demonstrate the useability of these Macro
	Techniques. Also checkout the Singleton template class for a similar implementation that
	requires less Macro defines.

	References:
	Generic Tutorial -	http://www.cprogramming.com/tutorial/cpreprocessor.html
	MSDN -				http://msdn.microsoft.com/en-us/library/y4skk93w.aspx
	Literature -		http://www.amazon.com/gp/product/1133776574/ref=olp_product_details?ie=UTF8&me=&seller=

*/

#pragma endregion