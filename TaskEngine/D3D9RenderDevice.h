#pragma once
#include "IRenderDevice.h"

#include "D3D9PostProcessPipeline.h"


//Device Context - can have a 2D and 3D pipeline
class D3D9RenderDevice : public IRenderDevice
{
private:

	//struct SavedBufferStates
	//{
	//	UINT fullscreenHeight;
	//	UINT fullscreenWidth;
	//	UINT windowedHeight;
	//	UINT windowedWidth;
	//};

	struct D3D9DeviceSettings
	{
		UINT adapterOrdinal;
		D3DDEVTYPE deviceType;
		D3DFORMAT adapterFormat;
		DWORD behaviorFlags;
		D3DPRESENT_PARAMETERS pp;
	};

private:

	MemoryDelegate*			m_memory;

	IDirect3D9*				m_d3d;
	IDirect3DDevice9*		m_device;

	ID3DXFont*				m_Font;
	ID3DXSprite*			m_Sprite;

	//SavedBufferStates		m_savedBuffers;

	////D3DSURFACE_DESC			m_backBufferSurfaceDesc;

	D3D9DeviceSettings		m_settings;

	//DWORD					m_savedWindowStyle;

	DeviceDrawPhase			m_phase;

	float					m_eventDT;
	float					m_drawRate;

	bool					m_deviceCreated;
	bool					m_isTopMostWindowed;
	bool					m_active;
	bool					m_paused;
	bool					m_minimized;
	bool					m_maximized;
	bool					m_minimizedFullScreen;
	bool					m_inSizeMove;
	bool					m_windowed;

	bool					m_deviceLost;

	bool					m_stats;


	////These need to be implemented////

					//Gives direct access to the scene, UI and particle emitters

	D3D9PostProcessPipeline* m_post;
	//GeometryPipeline*		m_geometryPipeline;		//Draw geometry to buffer
	//EffectsPipeline*		m_effectsPipeline;		//Draw particles to buffer

	//PostProcessPipeline*	m_postProcessing;		//Take geometry buffer and apply post processing

	//TextPipeline*			m_textPipeline;			//Draw text
	//SpritePipeline*		m_spritePipeline;		//Draw sprites to the screen
	//LinePipeline*			m_linePipeline;			//Draw lines
	////////////////////////////////////

	///// Exclusive Variables /////

	//ID3DXEffect*			m_effect;					// D3DX effect interface

	//IDirect3DCubeTexture9*	m_envTex;					// Texture for environment mapping

	////D3DXMATRIXA16			m_meshWorld;				// World matrix (xlate and scale) for the mesh
	//Matrix4					m_meshWorld;
	//int						m_scene;					// Indicates the scene # to render

	//PP9Filter				m_PostProcess[PPCOUNT];		// Effect object for PostProcesses

	//D3DXHANDLE				m_TRenderScene;				// Handle to RenderScene technique
	//D3DXHANDLE				m_TRenderEnvMapScene;		// Handle to RenderEnvMapScene technique
	//D3DXHANDLE				m_TRenderNoLight;			// Handle to RenderNoLight technique
	//D3DXHANDLE				m_TRenderSkyBox;			// Handle to RenderSkyBox technique

	//EngineFileMesh			m_sceneMesh[2];				// Mesh objects in the scene
	//EngineFileMesh			m_skyBoxMesh;				// Skybox mesh
	//D3DFORMAT				m_TexFormat;				// Render target texture format
	//IDirect3DTexture9*		m_SceneSave[RT_COUNT];		// To save original scene image before PostProcess
	//D3D9RenderTargetChain	m_RTChain[RT_COUNT];		// Render target chain (4 used in sample)

	//bool					m_enablePost;				// Whether or not to enable post-processing

	//int						m_passes;					// Number of passes required to render scene
	//int						m_RTUsed;					// Number of simultaneous RT used to render scene

	//D3D9RenderTargetSet		m_RTTable[RT_COUNT];		// Table of which RT to use for all passes

	///////////////////////////////

private:

	/*std::unordered_map<std::wstring, PP9Filter*> m_availableFilters;*/
	/*std::list<PP9FilterInstance> m_activeFilters;*/

private:

	virtual bool IsContextStable(RenderWindow* targetWindow);
	virtual LRESULT WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:

	//void UpdateBackbufferDescription();

	void CheckForWindowSizeChange();
	void CheckForWindowMonitorChange();

	HRESULT ChangeDevice(D3D9DeviceSettings newSettings, IDirect3DDevice9* newDevice, bool forceRecreate, bool clipWindowToSginleAdapter);

	HRESULT RestEnvironment();
	HRESULT InitEnvironment();

	bool IsDeviceLost();
	void OnLostDevice();
	HRESULT OnResetDevice();

	void UpdateDeviceStats(double dt);
	void UpdateRenderers(double dt);

protected:

	virtual bool Render();

	// smashing it all in here //
	//HRESULT ApplyPostProcessFilter(D3D9PostProcessFilterInstance filterInst,
	//								IDirect3DVertexBuffer9* pVB, 
	//								VertexPP* aQuad, 
	//								float& fExtentX, 
	//								float& fExtentY);
	//HRESULT PostProcess();

	//void AddFilter(std::wstring filterName);
	/////////////////////////////

public:
	D3D9RenderDevice()
	{
		m_phase					= DEVICE_IDLE;

		m_window				= nullptr;
		m_d3d					= nullptr;
		m_device				= nullptr;

		m_Font					= nullptr;
		m_Sprite				= nullptr;

		m_eventDT				= 0.0f;
		m_drawRate				= 0.0f;

		m_deviceCreated			= false;
		m_isTopMostWindowed		= false;
		m_active				= false;
		m_stats					= false;
		m_paused				= false;
		m_minimized				= false;
		m_maximized				= false;
		m_minimizedFullScreen	= false;
		m_inSizeMove			= false;
		m_deviceLost			= false;
		m_windowed				= true;

		///// Exclusive setup /////

		//m_effect = nullptr;					
		//m_envTex = nullptr;

		////m_meshWorld;				
		//m_scene = 0;											

		//m_enablePost = true;				

		//m_passes = 0;					
		//m_RTUsed = 0;					

		//D3D9RenderTargetSet		m_RTTable[RT_COUNT];

		///////////////////////////

		//ZeroMemory(&m_d3dPP, sizeof(m_d3dPP));
	}
	~D3D9RenderDevice()
	{
		if (m_deviceCreated) Shutdown();
	}

	virtual Tstring DeviceName() { return _T("D3D9 Device"); }

	virtual HRESULT Init(MemoryDelegate* memory, RenderWindow* window, float desiredDrawRate, bool calculateStats);
	
	virtual void Shutdown();

	virtual void Update(double dt);

	virtual void SetBackgroundColor(BYTE r, BYTE g, BYTE b, BYTE a) { }

	//Pipeline Features API

	void ToggleFullscreen();

	void SetPostProcess(bool enable);
	void AddFilter(int filterID);

	IDirect3DDevice9* GetDevice() { return m_device; }

	HRESULT TakeScreenShot(const TCHAR* filename, _D3DXIMAGE_FILEFORMAT frmt = D3DXIFF_BMP);

	HRESULT DrawFormattedTextLine(RECT& rc, DWORD dwFlags, const WCHAR* strMsg, ...);
	HRESULT DrawFormattedTextLine(const WCHAR* strMsg, ...);

	HRESULT DrawTextLine(RECT& rc, DWORD dwFlags, const WCHAR* strMsg);
	HRESULT DrawTextLine(const WCHAR* strMsg);





};