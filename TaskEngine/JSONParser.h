#pragma once
#include <string>
#include <fstream>
#include <map>
#include <vector>

namespace Utility
{
	enum ERROR_MESSAGES{
		GOOD = 0,
		BAD_FILE = 1,
		NOT_FOUND = 2,
	};

	enum PARSE_MESSAGE{
		RUNNING = 10,
		ARRAY_OBJECT = 11,
		NEW_OBJECT = 12,
	};

	struct JSONObject;

	struct JSONArray{
		std::string							m_name;
		std::vector<JSONObject>				m_values;
	};

	struct JSONObject{
		std::string							m_name;
		std::map<std::string, std::string>	m_variables;
		std::map<std::string, JSONObject>	m_objects;
		std::map<std::string, JSONArray>	m_arrays;
	};

	/**
	A simple JSON parser
	that supports recursive data structures
	*/
	class JSONParser
	{
	private:

		void Tab();
		void ReadFile();

		bool ParseFile();
		bool ParseObject(std::string::iterator it1, std::string::iterator it2, PARSE_MESSAGE operation, JSONObject &object);
		bool ParseVariable(std::string::iterator &it1, std::string::iterator it2, JSONObject &object);
		bool ParseArray(std::string::iterator &it1, std::string::iterator it2, JSONObject &object);

		int Parse();

	private:

		std::ofstream	m_outputStream;
		std::ifstream	m_inputStream;

		int				m_tabCount;

		std::string		m_inputString;

		JSONObject		m_JSON;

	public:

		JSONParser();
		~JSONParser();

		//===== Input Functions ===============================
		int OpenFile(std::string filename);
		JSONObject GetParsedObject();
		//=====================================================

		//===== Output Functions ==============================
		void CreateNewFile(std::string filename);
		void CloseFile();

		void CreateObject(std::string name = "NULL");
		void CloseObject();
		void CreateVariable(std::string name, std::string value);
		void CreateArray(std::string name);
		void CloseArray();
		//=====================================================
	};
}