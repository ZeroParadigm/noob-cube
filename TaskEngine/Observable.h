/**
 * Observable - Observables attach to the observer to send events, and subscribe to the observer to recieve events
 *
 * Author - Jesse Dillon
 */

#pragma once

#include "Observer.h"


class Observable
{
private:

	Observer* m_Observer;

	std::map<EVENT, std::function<void (const EventData&)>> m_EventHandlers;

protected:
	
	int	  m_Events;

public:

	Observable(void);
	~Observable(void);

	/**
	 * Attach
	 *
	 * Attach this observable to an Observer
	 *
	 * @observer - A pointer to the Observer
	 */
	void Attach(Observer* observer);

	/**
	 * Detach
	 *
	 * Detach this observable from an Observer
	 *
	 * @observer - A pointer to the Observer
	 */
	void Detach(Observer* observer);

	/**
	 * Notify
	 *
	 * Notifies the observer of the change
	 *
	 * @param
	 */
	void Notify(EVENT type, EventData data);

	/**
	 * Subscribe
	 *
	 * Subscribes this observable to Events 
	 *
	 * @events - The event(s) this observable is interested in
	 */
	void Subscribe(int events);

	/**
	 * Unsubscribe
	 *
	 * Unsubscribe this observable
	 */
	void Unsubscribe(void);

	/**
	 * OnEvent 
	 *
	 * Assign a function to occur when an event is triggered
	 *
	 * @type - The event type
	 * @func - The function to be executed
	 */
	void OnEvent(EVENT type, std::function<void (const EventData&)>);

	/**
	* ResetEvents
	*
	* Removes all events from this observable
	*/
	void ResetEvents();

protected:

	/**
	 * HandleEvent
	 *
	 * Handle an event
	 *
	 * @type - The event type
	 * @data - The event data
	 */
	void HandleEvent(EVENT type, const EventData& data);

protected:

	friend class Observer;
};