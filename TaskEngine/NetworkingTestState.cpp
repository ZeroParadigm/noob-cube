#include "EngineCommonStd.h"

#include "NetworkingTestState.h"
#include "GameStates.h"
#include "GraphicsAPI.h"

#include "PlayerComponent.h"
#include "WorldPolygon.h"
#include "GraphicsUtility.h"
#include "GUI.h"
#include "WindowElement.h"
#include "StaticTextElement.h"
#include "StaticImageElement.h"
#include "ButtonElement.h"
#include "Observable.h"
#include "Gamepad.h"
#include "JSON.h"

#include "WebSocketClient.h"


NetworkingTestState::NetworkingTestState()
{
	generator.seed((unsigned int)std::chrono::high_resolution_clock::now().time_since_epoch().count());

	m_TimeElapsed = 0.0f;

	m_PacketCooldown = 0.5f;
	m_PacketTimer = 0.0f;
}

NetworkingTestState::~NetworkingTestState()
{
}

void NetworkingTestState::EnterState()
{
	m_TimeElapsed = 0.0f;

	CreateCamera();
	CreateWorld();
	CreatePlayer();
	CreateWorldTriggerVolumes();
	CreateGUI();
	//CreateWorldTriggerVolumes();

	StartCamera();

	network->HandleMessage([&](const std::string& message){
		std::wstring wstr(message.begin(), message.end());

		JSONValue* val = JSON::Parse(wstr.c_str());

		if (val)
		{
			if (val->Child(L"packetId")->AsString() == L"3")
			{
				val->IsObject();
			}
		}
		// scale pos by 
		// 1 / 0.35
	});
}

void NetworkingTestState::LeaveState()
{
	//Clear memory
}

void NetworkingTestState::UpdateState(float dt)
{
	m_TimeElapsed += dt;
	m_PacketTimer += dt;

	GUI_PlayerScore->SetText(L"Score: " + to_wstring(m_Player->GetDisplayScore()));

	XINPUT_STATE controller = gamepad->GetState();

	if (controller.Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		m_Player->AddScore(5);
	}

	if (network->GetReadyState() != easywsclient::WebSocket::CLOSED)
	{
		network->Update(dt);

		if (m_PacketTimer >= m_PacketCooldown)
		{
			m_PacketTimer = 0.0f;

			Vec3 playerPos = m_Player->GetOwner()->Transform()->GetPosition();

			network->SendPosition(playerPos.x, playerPos.y, playerPos.z);
		}
	}
}

void NetworkingTestState::CreateCamera(void)
{
	//Create the camera
	Actor* camera = memory->actorFactory()->CreateCamera();

	//Set up the camera
	Quaternion q(0, 0, 0, 1);
	q.BuildRotYawPitchRoll(0, 0, 0);

	((MovementComponent*)camera->Ai())->SteeringOff();
	camera->Transform()->SetPosition(Vector3(0.0f, 0.0f, -175.0f));
	camera->Transform()->SetOrientation(q);

	((CameraComponent*)camera->Renderer())->LookAt(Vector3(0, 0, 0));
}

void NetworkingTestState::CreateWorld(void)
{
	//Create the game world
	gameworld = memory->actorFactory()->CreateGameWorld();

	((GameWorldComponent*)gameworld->Gameplay())->MakeCube();

	//CreateWorldTriggerVolumes();  Being called earlier
}

void NetworkingTestState::CreatePlayer(void)
{
	//Create the player
	Actor* player = memory->actorFactory()->CreateDebugPlayer();
	//player->Transform()->SetScale(Vector3(10,10,10));
	player->Transform()->SetPosition(Vector3(0, 0, -60));

	m_Player = ((PlayerComponent*)player->Gameplay());

	m_Player->ChangeFace(((GameWorldComponent*)memory->GetGameWorld()->Gameplay())->GetPolygon()->m_Faces[5]);
}

void NetworkingTestState::StartCamera(void)
{
	CameraComponent* camera = memory->GetCamera();
	MovementComponent* cam = (MovementComponent*)camera->GetOwner()->Ai();
	Actor* player = memory->GetPlayer();

	cam->SteeringOff();

	if (player)
	{
		//Set interpose method
		cam->SetAgentToInterpose(player->Transform());

		//Turn on interpose point
		cam->InterposePointOn();

		//Set align to point
		cam->SetTargetPoint3D(Vector3(0.0f, 0.0f, 0.0f));

		//Turn face point 3D on
		cam->FacePoint3DOn();
	}
}

void NetworkingTestState::CreateGUI()
{
	GUI_MainWindow = g_GUI->CreateWindowElement();
	GUI_PlayerScore = g_GUI->CreateStaticTextElement(GUI_MainWindow);

	GUI_PlayerScore->SetText(L"Score: 0");
	GUI_PlayerScore->SetWidth(300);
	GUI_PlayerScore->SetHeight(20);
}

void NetworkingTestState::CreateTestTriggerVolumes()
{
	Actor* tv = memory->actorFactory()->CreateBoxTriggerVolume();

	TransformComponent* tvTransform = tv->Transform();
	tvTransform->SetPosition(Vec3(20, 0, -60));

	TriggerVolumeComponent* tvTrigger = (TriggerVolumeComponent*)tv->Gameplay();
	tvTrigger->SetHalfExtents(Vec3(5, 5, 5));
	tvTrigger->SetTriggerType(TRIGGER_ONENTER);

	tvTrigger->SetTriggerEvent([&](const EventData& data){
		PlayerComponent* player = dynamic_cast<PlayerComponent*>(data.actors.first->Gameplay());

		if (player)
		{
			player->AddScore(1);
		}
		else
		{
			player = dynamic_cast<PlayerComponent*>(data.actors.second->Gameplay());

			if (player)
			{
				player->AddScore(1);
			}
		}
	});

	tvTrigger->Activate();
}

bool NetworkingTestState::EdgeTraversal(IGameplayComponent* comp, int face)
{
	WorldPolygon* world = ((GameWorldComponent*)memory->GetGameWorld()->Gameplay())->GetPolygon();
	WorldPlane* plane = world->m_Faces[face];
	int type = comp->GetType();

	if (type == COMPONENT_PLAYER)	//If we have a dynamic object
	{

		//Get the objects collision radius
		float radius = ((SphereColliderComponent*)comp->GetOwner()->Collider())->GetRadius();

		switch (comp->GetType()){
		case COMPONENT_ENEMY:
		{
								//Handle enemy
								return true;
		}
		case COMPONENT_BULLET:
		{
								 //Handle bullet
								 ((BulletComponent*)comp)->Deactivate();
								 return true;
		}
		case COMPONENT_PLAYER:
		{
								 //Handle player
								 PlayerComponent* player = static_cast<PlayerComponent*>(comp);
								 if (player->GetPlane() != plane)
								 {
									 player->ChangeFace(plane);		//Change active face

									 //player->GetOwner()->Transform()->SetPosition(Vec3(-plane->m_Normal * (radius+1)), true);	//Fix penetration
								 }
								 return true;
		}
		}
	}

	return false;
}

void NetworkingTestState::CreateWorldTriggerVolumes()
{
	ActorFactory* factory = memory->actorFactory();

	float X = (0.5f * ((GameWorldComponent*)gameworld->Gameplay())->m_World->m_ScaleX) + 13;  //Volumes spaced out 13 units from cube exterior
	float Y = (0.5f * ((GameWorldComponent*)gameworld->Gameplay())->m_World->m_ScaleY) + 13;
	float Z = (0.5f * ((GameWorldComponent*)gameworld->Gameplay())->m_World->m_ScaleZ) + 13;

	float thickness = 1.0f;	//Half extents for volume thickness
	float span = X - 1.5f;	//Volumes create a shell around the gameworld

	TriggerVolumeComponent* trigger;

	/*
	* +X Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(X, 0, 0), Vec3(thickness, span, span));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 0)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 0);
	});
	trigger->Activate();

	/*
	* -X Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(-X, 0, 0), Vec3(thickness, span, span));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 1)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 1);
	});
	trigger->Activate();

	/*
	* +Y Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(0, Y, 0), Vec3(span, thickness, span));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 2)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 2);
	});
	trigger->Activate();

	/*
	* -Y Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(0, -Y, 0), Vec3(span, thickness, span));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 3)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 3);
	});
	trigger->Activate();

	/*
	* +Z Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(0, 0, Z), Vec3(span, span, thickness));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 4)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 4);
	});
	trigger->Activate();

	/*
	* -Z Face Trigger
	*/
	trigger = factory->CreateWorldTriggerVolume(Vec3(0, 0, -Z), Vec3(span, span, thickness));
	trigger->SetTriggerEvent([&](const EventData& data){

		IGameplayComponent* comp = dynamic_cast<IGameplayComponent*>(data.actors.first->Gameplay());		//Check the first actor

		if (!comp) return;	//Return if comp is invalid

		if (NetworkingTestState::EdgeTraversal(comp, 5)) return;		//The edge traversal has been handled

		comp = dynamic_cast<IGameplayComponent*>(data.actors.second->Gameplay());		//Check the second actor

		if (!comp) return;	//Return if comp is invalid

		NetworkingTestState::EdgeTraversal(comp, 5);
	});
	trigger->Activate();
}