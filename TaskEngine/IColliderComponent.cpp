#include "EngineCommonStd.h"

#include "IColliderComponent.h"
#include "Observable.h"

IColliderComponent::IColliderComponent(void) : BaseComponent()
{
	m_Observable = new Observable();

	/**
	* Attach the observer
	*/
	m_Observable->Attach(EventObs);

	m_Active = true;
}

IColliderComponent::IColliderComponent(Actor* owner) : BaseComponent(owner)
{
	m_Observable = new Observable();

	/**
	* Attach the observer
	*/
	m_Observable->Attach(EventObs);

	m_Active = true;
}

IColliderComponent::~IColliderComponent(void)
{
	if (m_Observable)
	{
		delete m_Observable;
		m_Observable = nullptr;
	}
}

void IColliderComponent::OnCollision(EventData e)
{
	if (m_Observable)
	{
		m_Observable->Notify(COLLISION, e);
	}
}

void IColliderComponent::SetActive(bool active)
{
	ScopedCriticalSection crit(m_CriticalSection);
	m_Active = active;
}

bool IColliderComponent::IsActive()
{
	ScopedCriticalSection crit(m_CriticalSection);
	return m_Active;
}