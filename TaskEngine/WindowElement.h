#pragma once

#include "BaseGUIElement.h"


class WindowElement : public BaseGUIElement
{
protected:

	

public:

	WindowElement();
	WindowElement(BaseGUIElement* parent);
	~WindowElement();

	void Draw() override;

public:

	

};