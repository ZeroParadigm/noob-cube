#include "EngineCommonStd.h"

#include "TransformComponent.h"

using namespace DXMathLibrary;

TransformComponent::TransformComponent(Actor* owner) : BaseComponent(owner)
{
	m_Position = Vector3(0.0f, 0.0f, 0.0f);
	m_Orientation.Identity();
	m_Scale = Vector3(1.0f, 1.0f, 1.0f);
}

TransformComponent::~TransformComponent(void)
{

}

/**
 * Set the world position of the transform
 */
void TransformComponent::SetPosition(Vector3 position, bool delta)
{
	ScopedCriticalSection(this->m_CriticalSection);

	if (delta)
	{
		m_Position += position;
	}
	else
	{
		m_Position = position;
	}
}

/**
 * Set the scale of the transform
 */
void TransformComponent::SetScale(Vector3 scale, bool delta)
{
	ScopedCriticalSection(this->m_CriticalSection);

	if (delta)
	{
		m_Scale += scale;
	}
	else
	{
		m_Scale = scale;
	}
}

/**
 * Set the orientation of the transform
 */
void TransformComponent::SetOrientation(Quaternion orientation, bool delta)
{
	ScopedCriticalSection(this->m_CriticalSection);

	if (delta)
	{
		m_Orientation += orientation;
	}
	else
	{
		m_Orientation = orientation;
	}
}

/**
 * Set velocity
 */
void TransformComponent::SetVelocity(Vector3 vel, bool delta)
{
	ScopedCriticalSection(this->m_CriticalSection);

	if (delta)
	{
		m_Velocity += vel;
	}
	else
	{
		m_Velocity = vel;
	}
}

/**
 * Set angular velocity
 */
void TransformComponent::SetAngularVelocity(Vector3 ang, bool delta)
{
	ScopedCriticalSection(this->m_CriticalSection);

	if (delta)
	{
		m_AngularVel += ang;
	}
	else
	{
		m_AngularVel = ang;
	}
}

/**
 * Get the position
 */
Vector3 TransformComponent::GetPosition()
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Position;
}

Vector3* TransformComponent::GetPositonPtr()
{
	ScopedCriticalSection(this->m_CriticalSection);
	return &m_Position;
}

/**
 * Get the scale
 */
Vector3 TransformComponent::GetScale()
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Scale;
}

/**
 * Get the orientation
 */
Quaternion TransformComponent::GetOrientation()
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Orientation;
}

/**
 * Get velocity
 */
Vector3 TransformComponent::GetVelocity(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_Velocity;
}

/**
 * Get angular velocity
 */
Vector3 TransformComponent::GetAngularVelocity(void)
{
	ScopedCriticalSection(this->m_CriticalSection);
	return m_AngularVel;
}

/**
 *	Get World Matrix
 */
Matrix4 TransformComponent::GetWorldMatrix()
{
	ScopedCriticalSection(this->m_CriticalSection);
	Matrix4 translate, rotate, scale, mat;

	translate.BuildTranslation(m_Position);
	rotate.BuildRotationQuat(m_Orientation);
	scale.BuildScale(m_Scale);
	mat = scale * rotate * translate;

	return mat;
}