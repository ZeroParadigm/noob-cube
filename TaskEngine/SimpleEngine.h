#pragma once


class GraphicsAPI;
class PhysicsAPI;
class AiAPI;
class GameplaySystem;
class MemoryDelegate;
class DirectInput;
class Gamepad;

class D3D9RenderDevice;
class RenderWindow;
class WebSocketClient;

class Scene;

class SimpleEngine 
{
private:

	MemoryDelegate*		memory;
	GraphicsAPI*		graphics;
	PhysicsAPI*			physics;
	AiAPI*				ai;
	GameplaySystem*		gameplay;
	WebSocketClient*	network;

	Scene*				scene;

	D3D9RenderDevice*	device;
	RenderWindow*		window;

	DirectInput*		input;
	Gamepad*			gamepad;



private:

	long double			m_engineUpTime;

private:

	void UpdateSystems(double dt);

public:

	SimpleEngine();
	~SimpleEngine();
	
	void Init(HINSTANCE appInstance, const TCHAR* appName);
	int Shutdown(int appCloseCode);
	int Run();
};