#include "EngineCommonStd.h"

#include "GraphicsSystem.h"
//#include <Windows.h>
//#include <windowsx.h>

#include "GraphicsUtility.h"

#include "MeshPipeline.h"
//#include "../SimpleEngine/MemoryDelegate.h"




#include "Actor.h"

//Static Window Functions
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if( d3dSystem != 0 )
		return d3dSystem->msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}



//==== Private System Functions ================================
void GraphicsSystem::initDirect3D()
{
	m_d3dObject = Direct3DCreate9(D3D_SDK_VERSION);
	if(!m_d3dObject)
	{
		MessageBox(0, L"Direct3DCreate9 FAILED", 0, 0);
		PostQuitMessage(0);
	}


	D3DDISPLAYMODE mode;
	m_d3dObject->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	m_d3dObject->CheckDeviceType(D3DADAPTER_DEFAULT, m_devType, mode.Format, mode.Format, true);
	m_d3dObject->CheckDeviceType(D3DADAPTER_DEFAULT, m_devType, D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false);

	D3DCAPS9 caps;
	m_d3dObject->GetDeviceCaps(D3DADAPTER_DEFAULT, m_devType, &caps);

	DWORD devBehaviorFlags = 0;
	if( caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT )
		devBehaviorFlags |= m_RequestedVP;
	else
		devBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;


	if( caps.DevCaps & D3DDEVCAPS_PUREDEVICE &&
		devBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING)
		devBehaviorFlags |= D3DCREATE_PUREDEVICE;


	//m_d3dPP.BackBufferWidth            = 800; 
	//m_d3dPP.BackBufferHeight           = 600;
	m_d3dPP.BackBufferFormat           = D3DFMT_UNKNOWN;
	m_d3dPP.BackBufferCount            = 1;
	m_d3dPP.MultiSampleType            = D3DMULTISAMPLE_NONE;
	m_d3dPP.MultiSampleQuality         = 0;
	m_d3dPP.SwapEffect                 = D3DSWAPEFFECT_DISCARD; 
	m_d3dPP.hDeviceWindow              = m_mainWnd;
	m_d3dPP.Windowed                   = true;
	m_d3dPP.EnableAutoDepthStencil     = true; 
	m_d3dPP.AutoDepthStencilFormat     = D3DFMT_D24S8;
	m_d3dPP.Flags                      = 0;
	m_d3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	m_d3dPP.PresentationInterval       = D3DPRESENT_INTERVAL_IMMEDIATE;




	m_d3dObject->CreateDevice(
		D3DADAPTER_DEFAULT, 
		m_devType,           
		m_mainWnd,          
		devBehaviorFlags,   
		&m_d3dPP,            
		&g_d3dDevice);

	AddFontResourceEx(L"Assets/Fonts/Exo/Exo-Regular.otf", FR_PRIVATE, 0);
	D3DXCreateFont(g_d3dDevice,			//d3d device
		24,								//height
		0,								//width
		FW_NORMAL,						//weight
		1,								//mip levels
		FALSE,							//italics
		DEFAULT_CHARSET,				//character set
		OUT_DEFAULT_PRECIS,				//precision
		DEFAULT_QUALITY,				//quality
		DEFAULT_PITCH | FF_DONTCARE,	//pitch
		L"Exo-Regular",					//font
		&g_ExoRegular);					//font object

	D3DXCreateSprite(g_d3dDevice, &g_d3dSprite);
}

void GraphicsSystem::initMainWindow()
{
	WNDCLASS wc;

	ZeroMemory(&wc, sizeof(WNDCLASS));

	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = MainWndProc; 
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_appInst;
	wc.hIcon         = 0; //LoadIcon(mhAppInst, MAKEINTRESOURCE(IDI_PROJECT_SOL_ICON2));
	wc.hCursor       = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszClassName = L"D3DWndClassName";

	if(!RegisterClass(&wc))
	{
		MessageBox(0, L"RegisterClass FAILED", 0, 0);
		PostQuitMessage(0);
	}

	// Default to a window with a client area rectangle of 1280x960.
	RECT R = {0, 0, 960, 720};
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	m_mainWnd = CreateWindow(L"D3DWndClassName", m_windowCaption.c_str(), 
		WS_OVERLAPPEDWINDOW, 50, 50, R.right, R.bottom, 
		0, 0, m_appInst, 0); 

	if(!m_mainWnd)
	{
		MessageBox(0, L"CreateWindow FAILED", 0, 0);
		PostQuitMessage(0);
	}

	ShowWindow(m_mainWnd, SW_SHOW);
	UpdateWindow(m_mainWnd);

	d3dSystem = this;
}

#include "Logger.h"

void GraphicsSystem::updateFrameStats(float dt)
{
	static int numFrames = 0;
	static float timeElapsed = 0.0f;

	if(!m_stats) return;

	numFrames++;
	timeElapsed += dt;

	if(timeElapsed >= 1.0f)
	{
		float fps = (float)numFrames;	
		float mspf = 1.0f / fps;
		
		std::wostringstream outs;
		outs.precision(8);
		outs << m_windowCaption << L"    "
			 << L"FPS: " << fps << L"    "
			 << L"Frame Time: " << mspf;

		SetWindowText(m_mainWnd, outs.str().c_str());

		timeElapsed = 0.0f;
		numFrames = 0;
	}
}
//==============================================================



//==== Protected System Functions ==============================
void GraphicsSystem::init(MemoryDelegate* memory, HINSTANCE appInstance, const TCHAR* appCaption, int drawRate, bool stats)
{

	g_Memory			= memory;

	m_windowCaption		= appCaption;

	m_drawRate			= 1.0f / (float)drawRate;

	m_drawing			= true;

	m_stats				= stats;

	m_appInst			= appInstance;
	m_devType			= D3DDEVTYPE_HAL;
	m_RequestedVP		= D3DCREATE_HARDWARE_VERTEXPROCESSING;

	m_mainWnd			= 0;
	m_d3dObject			= 0;

	ZeroMemory(&m_d3dPP, sizeof(m_d3dPP));

	initMainWindow();
	initDirect3D();

	//Init Pipelines
	m_meshPipe = new MeshPipeline();
	g_GUI = new GUI();

	//InitAllVertexDeclarations(g_d3dDevice);
}

void GraphicsSystem::shutdown()
{
	m_d3dObject->Release();
	g_d3dDevice->Release();
	g_ExoRegular->Release();
	g_d3dSprite->Release();

	//DestroyAllVertexDeclarations();

	delete m_meshPipe;
	//delete m_effectsPipe;
	delete g_GUI;
}

void GraphicsSystem::update(float dt)
{
	static float timeElapsed = 0.0f;

	timeElapsed += dt;

	if((timeElapsed >= m_drawRate) || (m_drawRate == 1.0f))
	{
		m_drawing = true;

		//Set the camera 
		//m_meshPipe->m_ActiveCamera->SetLookAt(g_Memory->GetCamera()->GetOwner()->Transform()->GetPosition(), g_Memory->GetCamera()->GetTarget(), g_Memory->GetCamera()->GetOwner()->Transform()->GetOrientation().Up());
		


		//tell pipeline to draw<><>
		m_meshPipe->render();

		updateFrameStats(timeElapsed);

		timeElapsed = 0.0f;
		m_drawing = false;
	}
}

void GraphicsSystem::onLostDevice()
{
	g_ExoRegular->OnLostDevice();
	g_d3dSprite->OnLostDevice();
	
	m_meshPipe->onLostDevice();
}

void GraphicsSystem::onResetDevice()
{
	g_ExoRegular->OnResetDevice();
	g_d3dSprite->OnResetDevice();

	m_meshPipe->onResetDevice();
}

bool GraphicsSystem::isDeviceLost()
{
	HRESULT hr = g_d3dDevice->TestCooperativeLevel();

	if( hr == D3DERR_DEVICELOST )
	{
		Sleep(20);
		return true;
	}

	else if( hr == D3DERR_DRIVERINTERNALERROR )
	{
		MessageBox(0, L"Internal Driver Error...Exiting", 0, 0);
		PostQuitMessage(0);
		return true;
	}

	else if( hr == D3DERR_DEVICENOTRESET )
	{
		onLostDevice();
		g_d3dDevice->Reset(&m_d3dPP);
		onResetDevice();
		return false;
	}
	else
		return false;
}

void GraphicsSystem::setFullScreen(BOOL enable)
{
	if(enable)
	{
		if(!m_d3dPP.Windowed) 
			return;

		int width  = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		m_d3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		m_d3dPP.BackBufferWidth  = width;
		m_d3dPP.BackBufferHeight = height;
		m_d3dPP.Windowed         = false;

		SetWindowLongPtr(m_mainWnd, GWL_STYLE, WS_POPUP);

		SetWindowPos(m_mainWnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);	
	}
	else
	{
		if(m_d3dPP.Windowed) 
			return;

		RECT R = {0, 0, 800, 600};
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		m_d3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
		m_d3dPP.BackBufferWidth  = 800;
		m_d3dPP.BackBufferHeight = 600;
		m_d3dPP.Windowed         = true;

		SetWindowLongPtr(m_mainWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		SetWindowPos(m_mainWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}


	onLostDevice();
	g_d3dDevice->Reset(&m_d3dPP);
	onResetDevice();
}

//==============================================================



//==== Public System Functions =================================
LRESULT GraphicsSystem::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	static bool minOrMaxed = false;

	RECT clientRect = {0, 0, 0, 0};
	switch( msg )
	{

	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE )
			m_systemPaused = true;
		else
			m_systemPaused = false;
		return 0;

	case WM_SIZE:
		if( g_d3dDevice )
		{
			m_d3dPP.BackBufferWidth  = LOWORD(lParam);
			m_d3dPP.BackBufferHeight = HIWORD(lParam);

			if( wParam == SIZE_MINIMIZED )
			{
				m_systemPaused = true;
				minOrMaxed = true;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				m_systemPaused = false;
				minOrMaxed = true;
				onLostDevice();
				g_d3dDevice->Reset(&m_d3dPP);
				onResetDevice();
			}

			else if( wParam == SIZE_RESTORED )
			{
				m_systemPaused = false;

				if( minOrMaxed && m_d3dPP.Windowed )
				{
					onLostDevice();
					g_d3dDevice->Reset(&m_d3dPP);
					onResetDevice();
				}
				else
				{

				}
				minOrMaxed = false;
			}
		}
		return 0;



	case WM_EXITSIZEMOVE:
		GetClientRect(m_mainWnd, &clientRect);
		m_d3dPP.BackBufferWidth  = clientRect.right;
		m_d3dPP.BackBufferHeight = clientRect.bottom;

		onLostDevice();
		g_d3dDevice->Reset(&m_d3dPP);
		onResetDevice();

		return 0;


	case WM_CLOSE:
		DestroyWindow(m_mainWnd);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_INPUT:
		//m_InputHandler(lParam); //<OPTIONAL>
		return 0;
		
		//Built in fullscreen / exit
	case WM_KEYDOWN:
		if( wParam == VK_ESCAPE )
			setFullScreen(false);
		else if( wParam == VK_F1 )
			setFullScreen(true);
		return 0;

	}

	return DefWindowProc(m_mainWnd, msg, wParam, lParam);
}
//==============================================================



HWND GraphicsSystem::getMainWnd()
{
	return m_mainWnd;
}

//void GraphicsSystem::SetMainWindowInputHandler(std::function<void (LPARAM)> handler)
//{
//	m_InputHandler = handler;
//}