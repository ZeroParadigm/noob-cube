#pragma once

#define RT_COUNT 3

class D3D9RenderTargetChain
{
public:

	int m_next;
	bool m_firstRender;
	LPDIRECT3DTEXTURE9 m_renderTarget[2];

public:

	D3D9RenderTargetChain()
	{
		m_next = 0;
		m_firstRender = true;

		ZeroMemory(m_renderTarget, sizeof(m_renderTarget));
	}
	~D3D9RenderTargetChain()
	{
		Cleanup();
	}

	void Init(LPDIRECT3DTEXTURE9* renderTextre)
	{
		for (int i = 0; i < 2; ++i)
		{
			m_renderTarget[i] = renderTextre[i];
			m_renderTarget[i]->AddRef();
		}
	}

	void Cleanup()
	{
		SAFE_RELEASE(m_renderTarget[0]);
		SAFE_RELEASE(m_renderTarget[1]);
	}

	void Flip()
	{
		m_next = 1 - m_next;
	}

	LPDIRECT3DTEXTURE9 GetPreviousTarget()
	{
		return m_renderTarget[1 - m_next];
	}

	LPDIRECT3DTEXTURE9 GetPreviousSource()
	{
		return m_renderTarget[m_next];
	}

	LPDIRECT3DTEXTURE9 GetNextTarget()
	{
		return m_renderTarget[m_next];
	}

	LPDIRECT3DTEXTURE9 GetNextSource()
	{
		return m_renderTarget[1 - m_next];
	}
};

class D3D9RenderTargetSet
{
public:
	IDirect3DSurface9* RenderTarget[RT_COUNT];
};