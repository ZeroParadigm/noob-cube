#pragma once
#include <string>
#include <vector>
#include <tchar.h>

#ifdef UNICODE
#define tstring std::wstring
#else
#define tstring std::string
#endif

#define DXUTERR_NODIRECT3D              MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0901)
#define DXUTERR_NOCOMPATIBLEDEVICES     MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0902)
#define DXUTERR_MEDIANOTFOUND           MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0903)
#define DXUTERR_NONZEROREFCOUNT         MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0904)
#define DXUTERR_CREATINGDEVICE          MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0905)
#define DXUTERR_RESETTINGDEVICE         MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0906)
#define DXUTERR_CREATINGDEVICEOBJECTS   MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0907)
#define DXUTERR_RESETTINGDEVICEOBJECTS  MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x0908)
#define DXUTERR_DEVICEREMOVED           MAKE_HRESULT(SEVERITY_ERROR, FACILITY_ITF, 0x090A)

namespace Utility
{

	template <class Function>
	static __int64 time_call(Function&& f)
	{
		__int64 begin = GetTickCount();
		f();
		return GetTickCount() - begin;
	}

	static void EnableConsole();

	class Strings
	{
	public:
		 
		static std::wstring stringToWideString(const std::string& s);
		static std::string wideStringToString(const std::wstring& s);
		//static const TCHAR* stringToWideStringChar(const TCHAR* s);
		//static const TCHAR* wideStringToStringChar(const TCHAR* s);

		static std::wstring bytesToHumanFriendly(unsigned int bytes);

		static tstring ToUpper(tstring& s);
	};

	class Filesystem
	{
	public:

		static HRESULT FindAssetFileFromCommonLocs(TCHAR* pathToFile, int what, const TCHAR* filename);
		static HRESULT SetAssetSearchPath(const TCHAR* path);
		static const TCHAR* GetAssetSearchPath();
		static std::vector<tstring> GetDirectoryContents(tstring directory, tstring filter, std::vector<tstring> &contents = std::vector<tstring>());
	};


}

//class CDXUTTimer
//{
//public:
//	CDXUTTimer();
//
//	void            Reset(); // resets the timer
//	void            Start(); // starts the timer
//	void            Stop();  // stop (or pause) the timer
//	void            Advance(); // advance the timer by 0.1 seconds
//	double          GetAbsoluteTime(); // get the absolute system time
//	double          GetTime(); // get the current time
//	float           GetElapsedTime(); // get the time that elapsed between Get*ElapsedTime() calls
//	void            GetTimeValues(double* pfTime, double* pfAbsoluteTime, float* pfElapsedTime); // get all time values at once
//	bool            IsStopped(); // returns true if timer stopped
//
//	// Limit the current thread to one processor (the current one). This ensures that timing code runs
//	// on only one processor, and will not suffer any ill effects from power management.
//	void            LimitThreadAffinityToCurrentProc();
//
//protected:
//	LARGE_INTEGER   GetAdjustedCurrentTime();
//
//	bool m_bUsingQPF;
//	bool m_bTimerStopped;
//	LONGLONG m_llQPFTicksPerSec;
//
//	LONGLONG m_llStopTime;
//	LONGLONG m_llLastElapsedTime;
//	LONGLONG m_llBaseTime;
//};
//
//CDXUTTimer*                 WINAPI DXUTGetGlobalTimer();