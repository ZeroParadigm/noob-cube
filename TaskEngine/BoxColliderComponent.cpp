#include "EngineCommonStd.h"

#include "BoxColliderComponent.h"
#include "TransformComponent.h"

#include "Actor.h"

using namespace DXMathLibrary;

BoxColliderComponent::BoxColliderComponent(Actor* owner) : IColliderComponent(owner)
{
	m_halfLengths = Vector3(1.0f, 1.0f, 1.0f);
	m_Type = COMPONENT_BOX;
}

BoxColliderComponent::~BoxColliderComponent()
{

}

std::pair<Vector3, Vector3> BoxColliderComponent::GetAABB()
{
	Vector3 position = m_Owner->Transform()->GetPosition();

	return std::make_pair(position - m_halfLengths, position + m_halfLengths);
}

void BoxColliderComponent::SetHalfLengths(Vector3 halfLengths)
{
	m_halfLengths = halfLengths;
}

Vector3 BoxColliderComponent::GetHalfLengths()
{
	return m_halfLengths;
}