#include "EngineCommonStd.h"

#include "Client.h"
#include "PacketReporter.h"


Client::Client(PacketReporter* r) : reporter(r)
{
	m_ClientConnection = new SOCKET();
	*m_ClientConnection = INVALID_SOCKET;

	m_ClientThread = new HANDLE();

	m_Connected = false;

	Initialize();
}

Client::~Client()
{
	Disconnect();

	delete m_ClientConnection;
	delete m_ClientThread;
}

bool Client::Initialize()
{
	int iResult;

	iResult = WSAStartup(MAKEWORD(2,2), &m_WSAData);
	if(iResult != 0)
	{
		printf("WSAStartup Failed!");
		return false;
	}
	return true;
}

bool Client::Connect(std::string ipaddr, std::string port)
{
	int iResult;
	addrinfo *result = NULL, *ptr = NULL, hints;

	ZeroMemory( &hints, sizeof(hints) );
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(ipaddr.c_str(), port.c_str(), &hints, &result);
	if (iResult != 0)
	{
		printf("getaddrinfo() Failed! Error: %d\n", WSAGetLastError());
		WSACleanup();
		Initialize();
		m_Connected = false;
		return false;
	}

	// Try to connect to an address until there are no more available
	for(ptr = result; ptr != NULL; ptr = ptr->ai_next)
	{
		// Create the socket 
		*m_ClientConnection = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if(*m_ClientConnection == INVALID_SOCKET)
		{
			printf("socket() Failed! Error: %d\n", WSAGetLastError());
			freeaddrinfo(result);
			WSACleanup();
			Initialize();
			m_Connected = false;
			return false;
		}

		// Connect to server.
		iResult = connect( *m_ClientConnection, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			printf("connect() Failed! Error: %d. Attempting again...\n", WSAGetLastError());
			closesocket(*m_ClientConnection);
			continue; 
		}
		break;
	}

	//free up the result information
	freeaddrinfo(result);

	CreateReceiveThread(this, m_ClientThread);

	m_Connected = true;
	return true;
}

bool Client::IsConnected()
{
	return m_Connected;
}

bool Client::Send(char* data, int size)
{
	int iResult;

	iResult = send(*m_ClientConnection, data, size, 0);

	if (iResult == SOCKET_ERROR) {
		printf("send() Failed! Error: %d\n", WSAGetLastError());
		Disconnect();
		Initialize();
		return false;
	}

	return true;
}

bool Client::Receive()
{
	int iResult, recvbuflen = 512;
	char recvbuf[512];

	iResult = recv(*m_ClientConnection, recvbuf, recvbuflen, 0);

	if (iResult > 0)
	{
		//Assume first 4 bytes is an unsigned int
		unsigned int packetID = *reinterpret_cast<unsigned int*>(recvbuf);

		PacketAlert* alert = new PacketAlert();
		alert->packetID = packetID;
		memcpy(alert->data, recvbuf, recvbuflen);
		alert->size = iResult;
		alert->dirty = true;

		reporter->TrackPacket(alert);

		return true;
	}
	else if (iResult == 0)
	{
		Disconnect();
		Initialize();

		return false;
	}
	else if(iResult < 0)
	{
		printf("recv() Failed! Error: %d\n", WSAGetLastError());
		Disconnect();
		Initialize();

		return false;
	}

	return false;
}

void Client::Disconnect()
{
	int iResult;

	iResult = shutdown(*m_ClientConnection, SD_BOTH);

	if (iResult == SOCKET_ERROR)
	{
		printf("shutdown() Failed! Error: %d\n", WSAGetLastError());
	}


	TerminateThread(*m_ClientThread, 0);
	closesocket(*m_ClientConnection);
	m_Connected = false;
	reporter->ClearRegistry();
	WSACleanup();
	Initialize();
}

void CreateReceiveThread(Client* c, HANDLE* h)
{
	*h = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) RecvProc, c, 0, NULL);
}

DWORD WINAPI RecvProc(LPVOID lpParam)
{
	Client* c = static_cast<Client*>(lpParam);

	for(;;)
	{
		if(!c->Receive())
		{
			return FALSE;
		}
	}
		
	return TRUE;
}