#pragma once

#include "Texture.h"


class Mesh
{
public:

	std::wstring	m_fileName;
	ID3DXMesh*	m_Mesh;
	Texture*	m_Texture;

	bool m_UsingMesh;

	IDirect3DVertexBuffer9* m_VertexBuffer;
	IDirect3DIndexBuffer9*	m_IndexBuffer;
	unsigned int m_NumVertices;
	unsigned int m_NumTriangles;

public:

	Mesh();
	Mesh(std::wstring file);
	~Mesh();

	void Load(IDirect3DDevice9* device);
	void Release();

	void DrawSubset(int subset);

	void SetTexture(Texture* txtr)
	{
		m_Texture = txtr;
	}
	void SetMesh(ID3DXMesh* mesh)
	{
		m_Mesh = mesh;
	}

	void UseMeshObj(bool yes)
	{
		m_UsingMesh = yes;
	}

	bool IsUsingMesh()
	{
		return m_UsingMesh;
	}

	std::wstring GetFileName(){ return m_fileName; }
	ID3DXMesh* GetMesh(){ return m_Mesh; }
	IDirect3DTexture9* GetTexture(){ return m_Texture->GetTexture(); }
	Texture* GetTextureObj(){ return m_Texture; }

	IDirect3DVertexBuffer9* VertBuffer()
	{
		return m_VertexBuffer;
	}
	IDirect3DIndexBuffer9* IndexBuffer()
	{
		return m_IndexBuffer;
	}
};