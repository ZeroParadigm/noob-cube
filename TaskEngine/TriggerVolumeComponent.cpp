#include "EngineCommonStd.h"
#include "TriggerVolumeComponent.h"
#include "Observable.h"
#include "TransformComponent.h"
#include "BoxColliderComponent.h"
#include "WorldPlane.h"


TriggerVolumeComponent::TriggerVolumeComponent()
{
	m_Type = COMPONENT_TRIGGERVOLUME;
	this->SetTriggerEvent([&](const EventData& data){});

	m_TriggerType = TRIGGER_WHILETOUCHING;
	m_Triggered = false;
	m_Cooldown = 1.25f;
}

TriggerVolumeComponent::TriggerVolumeComponent(Actor* owner) : IGameplayComponent(owner)
{
	m_Type = COMPONENT_TRIGGERVOLUME;

	this->SetTriggerEvent([&](const EventData& data){});

	m_TriggerType = TRIGGER_WHILETOUCHING;
	m_Triggered = false;
	m_Cooldown = 1.25f;
}

TriggerVolumeComponent::~TriggerVolumeComponent()
{
}

void TriggerVolumeComponent::Update(float dt)
{
	if (m_TriggerType == TRIGGER_ONENTER)
	{
		if (m_Triggered)
		{
			m_Timer += dt;
			
			if (m_Timer >= m_Cooldown)
			{
				m_Triggered = false;
			}
		}
	}
}

void TriggerVolumeComponent::Init()
{
	m_Observable->Attach(EventObs);

	m_Observable->OnEvent(COLLISION, [&](const EventData& data){
		this->HandleEvent(data);
	});
}

void TriggerVolumeComponent::Activate()
{
	m_Observable->Subscribe(COLLISION);
}

void TriggerVolumeComponent::Deactivate()
{
	m_Observable->Unsubscribe();
}

void TriggerVolumeComponent::HandleEvent(const EventData& data)
{
	if (data.actors.first->Gameplay()->GetType() == data.actors.second->Gameplay()->GetType()) return;

	if (data.actors.first == GetOwner() || data.actors.second == GetOwner())
	{
		if (m_TriggerType == TRIGGER_ONENTER)
		{
			if (!m_Triggered)
			{
				m_Triggered = true;
				m_Timer = 0.0f;
				m_TriggerEvent(data);
			}
			else
			{
				m_Triggered = true;
				m_Timer = 0.0f;
			}
		}
		else if (m_TriggerType == TRIGGER_WHILETOUCHING)
		{
			m_TriggerEvent(data);
		}
	}
}

void TriggerVolumeComponent::SetTriggerEvent(function<void(const EventData&)> eventHandler)
{
	m_TriggerEvent = eventHandler;
}

void TriggerVolumeComponent::SetHalfExtents(Vector3 halfExtents)
{
	m_HalfExtents = halfExtents;
	m_Owner->Transform()->SetScale(m_HalfExtents);
	((BoxCC*)m_Owner->Collider())->SetHalfLengths(m_HalfExtents);
}

Vector3 TriggerVolumeComponent::GetHalfExtents()
{
	return m_HalfExtents;
}

void TriggerVolumeComponent::SetTriggerType(TriggerType type)
{
	m_TriggerType = type;
}

TriggerType TriggerVolumeComponent::GetTriggerType()
{
	return m_TriggerType;
}

void TriggerVolumeComponent::SetTriggerCooldown(float t)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	m_Cooldown = t;
}

/**
 * EdgeTriggerVolume functions
 */
EdgeTriggerVolume::EdgeTriggerVolume(void) : TriggerVolumeComponent()
{
	m_PlaneA = nullptr;
	m_PlaneB = nullptr;
}

EdgeTriggerVolume::EdgeTriggerVolume(Actor* owner) : TriggerVolumeComponent(owner)
{
	m_PlaneA = nullptr;
	m_PlaneB = nullptr;
}

EdgeTriggerVolume::~EdgeTriggerVolume(void)
{
	m_PlaneA = nullptr;
	m_PlaneB = nullptr;
}

void EdgeTriggerVolume::SetWorldPlanes(WorldPlane* a, WorldPlane* b)
{
	ScopedCriticalSection critical_section(this->m_CriticalSection);
	m_PlaneA = a;
	m_PlaneB = b;
}

void EdgeTriggerVolume::Init(void)
{

	m_Observable->Attach(EventObs);

	m_Observable->Subscribe(COLLISION);

	SetTriggerEvent([&](const EventData& data){
		Actor* a = data.actors.first;
		Actor* b = data.actors.second;

		if (a->Gameplay()->GetType() == b->Gameplay()->GetType()) return;


		if (EnemyComponent* enemy = dynamic_cast<EnemyComponent*>(a->Gameplay()))
		{
			//Check edging
		}
		if (EnemyComponent* enemy = dynamic_cast<EnemyComponent*>(b->Gameplay()))
		{
			//Check edging
		}

		if (PlayerComponent* player = dynamic_cast<PlayerComponent*>(a->Gameplay()))
		{
			if (player->IsEdging())
			{
				(player->GetPlane() == m_PlaneA) ? player->ChangeFace(m_PlaneB) : player->ChangeFace(m_PlaneA);
				player->SetEdging(false);
			}
			else
			{
				player->SetEdging(true);
			}

			return;
		}
		if (PlayerComponent* player = dynamic_cast<PlayerComponent*>(b->Gameplay()))
		{
			if (player->IsEdging())
			{
				(player->GetPlane() == m_PlaneA) ? player->ChangeFace(m_PlaneB) : player->ChangeFace(m_PlaneA);
				player->SetEdging(false);
			}
			else
			{
				player->SetEdging(true);
			}

			return;
		}
	});

	m_Observable->OnEvent(COLLISION, [&](const EventData& data){
		//this->HandleEvent(data);
		this->EdgeTesting(data);
	});
}

void EdgeTriggerVolume::EdgeTesting(const EventData& data)
{
	Actor* a = data.actors.first;
	Actor* b = data.actors.second;

	if (a->Gameplay()->GetType() == b->Gameplay()->GetType()) return;


	if (EnemyComponent* enemy = dynamic_cast<EnemyComponent*>(a->Gameplay()))
	{
		//Check edging
	}
	if (EnemyComponent* enemy = dynamic_cast<EnemyComponent*>(b->Gameplay()))
	{
		//Check edging
	}

	if (PlayerComponent* player = dynamic_cast<PlayerComponent*>(a->Gameplay()))
	{
		if (player->IsEdging())
		{
			(player->GetPlane() == m_PlaneA) ? player->ChangeFace(m_PlaneB) : player->ChangeFace(m_PlaneA);
			player->SetEdging(false);
		}
		else
		{
			player->SetEdging(true);
		}

		return;
	}
	if (PlayerComponent* player = dynamic_cast<PlayerComponent*>(b->Gameplay()))
	{
		if (player->IsEdging())
		{
			(player->GetPlane() == m_PlaneA) ? player->ChangeFace(m_PlaneB) : player->ChangeFace(m_PlaneA);
			player->SetEdging(false);
		}
		else
		{
			player->SetEdging(true);
		}

		return;
	}
}