#include "EngineCommonStd.h"
#include "WorkerThread.h"

using namespace std;

WorkerThread::WorkerThread()
{
	m_wantExit = false;
    //thread = std::unique_ptr<std::thread>( new std::thread( std::bind(&WorkerThread::Entry, this) ) );
	m_thread = thread(bind(&WorkerThread::Entry, this));
	m_thread.detach();
}

WorkerThread::~WorkerThread()
{
    lock_guard<mutex> lock(m_queueMutex);
    m_wantExit = true;
    m_queuePending.notify_one();

    m_thread.join();
}

void WorkerThread::Entry()
{
	std::function<void()> job;

    while(true)
    {
        std::unique_lock<std::mutex> lock(m_queueMutex);
        m_queuePending.wait( lock, [&] () { return m_wantExit || !m_jobQueue.empty(); } );

        if(m_wantExit)
            return;

        job = m_jobQueue.front();
        m_jobQueue.pop();


        job();
    }
}

//template <class Function>
//void WorkerThread::addJob(Function&& job)
//{
//	//std::lock_guard<std::mutex> lock(m_queueMutex);
//	//m_jobQueue.push(job);
//    //m_queuePending.notify_one();
//}

void WorkerThread::addJob(std::function<void()> job)
{
	std::lock_guard<std::mutex> lock(m_queueMutex);
	m_jobQueue.push(job);
    m_queuePending.notify_one();
}