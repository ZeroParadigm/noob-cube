#include "EngineCommonStd.h"
#include "D3D9RenderDevice.h"
#include "D3D9Vertex.h"

#include "RenderWindow.h"

#include "GraphicsUtility.h"

#include "GUI.h"
#include "MemoryDelegate.h"


#pragma region Window Context Procedures

bool D3D9RenderDevice::IsContextStable(RenderWindow* targetWindow)
{
	std::wstring status;

	status += L"Performing Context stability check:\n";
	status += L"\tHWND Check: "; status += (targetWindow->GetHWND() == m_window->GetHWND()) ? L"PASS\n" : L"-FAIL-\n";
	status += L"\tSDK Check: "; status += (m_d3d) ? L"PASS\n" : L"-FAIL-\n";
	status += L"\tDevice Check: "; status += (m_device) ? L"PASS\n" : L"-FAIL-\n";
	status += L"\tFont Check: "; status += (m_Font) ? L"PASS\n" : L"-FAIL-\n";
	status += L"\tSprite Check: "; status += (m_Sprite) ? L"PASS" : L"-FAIL-";

	if (!m_d3d ||
		!m_device ||
		!m_Font ||
		!m_Sprite ||
		!(targetWindow->GetHWND() == m_window->GetHWND()))
	{
		ENGINE_WARNING(status);
		return false;
	}

	OUTPUT(status);

	return true;
}

LRESULT D3D9RenderDevice::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
		case WM_PAINT:
		{
						 if (m_paused && m_deviceCreated)
						 {
							 HRESULT hr;
							 if (m_device)
							 {
								 Render();
							 }
							 hr = m_device->Present(NULL, NULL, NULL, NULL);
							 if (hr == D3DERR_DEVICELOST)
							 {
								 m_deviceLost = true;
							 }
							 else if (hr == D3DERR_DRIVERINTERNALERROR)
							 {
								 m_deviceLost = true;
							 }
						 }
						 break;
		}
			

		case WM_SIZE:
			if (wParam == SIZE_MINIMIZED)
			{
				m_paused = true;
				m_minimized = true;
				m_maximized = false;
			}
			else
			{
				RECT clientRect;
				GetClientRect(hwnd, &clientRect);
				if (clientRect.top == 0 && clientRect.bottom == 0)
				{

				}
				else if (wParam == SIZE_MAXIMIZED)
				{
					if (m_minimized)
						m_paused = false;
					m_minimized = false;
					m_maximized = true;
					CheckForWindowSizeChange();
					CheckForWindowMonitorChange();
				}
				else if (wParam == SIZE_RESTORED)
				{
					if (m_maximized)
					{
						m_maximized = false;
						CheckForWindowSizeChange();
						CheckForWindowMonitorChange();
					}
					else if (m_minimized)
					{
						m_paused = false;
						m_minimized = false;
						CheckForWindowSizeChange();
						CheckForWindowMonitorChange();
					}
					else if (m_inSizeMove)
					{
						//This is caught to prevent trouble.
						//	it will be handled in another msg
					}
					else
					{
						CheckForWindowSizeChange();
						CheckForWindowMonitorChange();
					}
				}
			}
			break;

		case WM_ENTERSIZEMOVE:
			m_paused = true;
			m_inSizeMove = true;
			break;

		case WM_EXITSIZEMOVE:
			m_paused = false;
			CheckForWindowSizeChange();
			CheckForWindowMonitorChange();
			m_inSizeMove = false;
			break;

		case WM_MOUSEMOVE:
			if (!m_paused && !m_windowed)
			{
				if (m_device)
				{
					POINT cursorPt;
					GetCursorPos(&cursorPt);
					m_device->SetCursorPosition(cursorPt.x, cursorPt.y, 0);
				}
			}
			break;

		case WM_ACTIVATEAPP:
			if (wParam == TRUE && !m_active)
			{
				m_active = true;

				//XInput

				if (m_minimizedFullScreen)
				{
					m_paused = false;
					m_minimizedFullScreen = false;
				}
			}
			else if (wParam == FALSE && m_active)
			{
				m_active = false;

				//XInput

				if (!m_windowed)
				{
					ClipCursor(NULL);
					m_paused = true;
					m_minimizedFullScreen = true;
				}
			}
			break;

		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_PAUSE:
				m_paused = !m_paused;
			}
			break;
		}
	}
	return 0;
}

#pragma endregion


#pragma region Private Definitions

void D3D9RenderDevice::CheckForWindowSizeChange()
{
	if (!m_device || !m_windowed)
		return;

	D3D9DeviceSettings settings = m_settings;
	RECT currentClient;
	GetClientRect(m_window->GetHWND(), &currentClient);

	if (((UINT)currentClient.right != settings.pp.BackBufferWidth) ||
		((UINT)currentClient.bottom != settings.pp.BackBufferHeight))
	{
		settings.pp.BackBufferWidth = 0;
		settings.pp.BackBufferHeight = 0;

		ChangeDevice(settings, nullptr, false, false);
	}
}

void D3D9RenderDevice::CheckForWindowMonitorChange()
{
	//NYI
}

HRESULT D3D9RenderDevice::ChangeDevice(D3D9DeviceSettings newSettings, IDirect3DDevice9* newDevice, bool forceRecreate, bool clipWindowToSginleAdapter)
{
	HRESULT hr;

	bool bKeepCurrentWindowSize = false;
	if (newSettings.pp.BackBufferWidth == 0 &&
		newSettings.pp.BackBufferHeight == 0)
		bKeepCurrentWindowSize = true;

	// Carry over the requested new settings
	m_settings = newSettings;

	if (FAILED(hr = RestEnvironment()))
	{
		if (hr == D3DERR_DEVICELOST)
		{
			m_deviceLost = true;
		}
		else if (hr == DXUTERR_RESETTINGDEVICEOBJECTS || hr == DXUTERR_MEDIANOTFOUND)
		{
			ENGINE_ERROR(L"Something bad happen in recreating device resources.");
		}
		else
		{
			ENGINE_WARNING(L"Device was lost for an unkonwn reason. Recreation NYI.");
		}
	}

	bool needToResize = false;

	// Now that we know the client rect, compare it against the back buffer size
	// to see if the client rect is already the right size
	if (m_window->GetWidth() != newSettings.pp.BackBufferWidth ||
		m_window->GetHeight() != newSettings.pp.BackBufferHeight)
	{
		needToResize = true;
	}

	m_paused = false;

	return S_OK;
}

HRESULT D3D9RenderDevice::RestEnvironment()
{
	HRESULT hr;

	ENGINE_ASSERT(m_device != NULL);

	OnLostDevice();

	// Reset device with new settings
	if (FAILED(hr = m_device->Reset(&m_settings.pp)))
	{
		if (hr == D3DERR_DEVICELOST)
			return D3DERR_DEVICELOST;
		else
			return DXUTERR_RESETTINGDEVICE;
	}

	if (FAILED(hr = OnResetDevice()))
	{
		if (hr != DXUTERR_MEDIANOTFOUND)
			hr = DXUTERR_RESETTINGDEVICEOBJECTS;

		OnLostDevice();
		return hr;
	}

	return S_OK;
}

HRESULT D3D9RenderDevice::InitEnvironment()
{
	//Create D3D Object
	m_d3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (!m_d3d)
	{
		ENGINE_FATAL(L"Direct3D 9 not supported. Closing.");
		PostQuitMessage(0);
	}

	//Setup default device settings
	m_settings.adapterOrdinal = D3DADAPTER_DEFAULT;
	m_settings.deviceType = D3DDEVTYPE_HAL;

	//Grab display modes
	D3DDISPLAYMODE mode;
	m_d3d->GetAdapterDisplayMode(m_settings.adapterOrdinal, &mode);

	m_settings.adapterFormat = mode.Format;

	//Grab device capabilities
	D3DCAPS9 caps;
	m_d3d->GetDeviceCaps(m_settings.adapterOrdinal, m_settings.deviceType, &caps);

	//Define behavior based on capabilities
	m_settings.behaviorFlags = 0;
	if (caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		m_settings.behaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		m_settings.behaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	if ((caps.DevCaps & D3DDEVCAPS_PUREDEVICE) && (m_settings.behaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING))
		m_settings.behaviorFlags |= D3DCREATE_PUREDEVICE;

	//Define Presentation Paramaters
	m_settings.pp.BackBufferWidth = m_window->GetWidth();
	m_settings.pp.BackBufferHeight = m_window->GetHeight();
	m_settings.pp.BackBufferFormat = m_settings.adapterFormat;
	m_settings.pp.BackBufferCount = 1;
	m_settings.pp.MultiSampleType = D3DMULTISAMPLE_NONE;
	m_settings.pp.MultiSampleQuality = 0;
	m_settings.pp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_settings.pp.hDeviceWindow = m_window->GetHWND();
	m_settings.pp.Windowed = true;
	m_settings.pp.EnableAutoDepthStencil = true;
	m_settings.pp.AutoDepthStencilFormat = D3DFMT_D24X8;
	m_settings.pp.Flags = D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
	m_settings.pp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	m_settings.pp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	//Create the Device
	m_d3d->CreateDevice(
		m_settings.adapterOrdinal,
		m_settings.deviceType,
		m_window->GetHWND(),
		m_settings.behaviorFlags,
		&m_settings.pp,
		&m_device);
	ENGINE_ASSERT(m_device);

	// Assign the global device pointer //
	g_d3dDevice = m_device;  /////////////
	//////////////////////////////////////

	//Create Font Device
	AddFontResourceEx(L"Assets/Fonts/Exo/Exo-Regular.otf", FR_PRIVATE, 0);
	D3DXCreateFont(m_device,			//d3d device
		24,								//height
		0,								//width
		FW_NORMAL,						//weight
		1,								//mip levels
		FALSE,							//italics
		DEFAULT_CHARSET,				//character set
		OUT_DEFAULT_PRECIS,				//precision
		DEFAULT_QUALITY,				//quality
		DEFAULT_PITCH | FF_DONTCARE,	//pitch
		L"Exo-Regular",					//font
		&m_Font);						//font object
	ENGINE_ASSERT(m_Font);

	//D3DXCreateFont(m_device,
	//	15,
	//	0,
	//	FW_BOLD,
	//	1,
	//	FALSE,
	//	DEFAULT_CHARSET,
	//	OUT_DEFAULT_PRECIS,
	//	DEFAULT_QUALITY,
	//	DEFAULT_PITCH | FF_DONTCARE,
	//	L"Arial",
	//	&m_Font);

	//Create Sprite Device
	D3DXCreateSprite(m_device, &m_Sprite);
	ENGINE_ASSERT(m_Sprite);

	InitAllVertexDeclarations(m_device);

	m_deviceCreated = true;

	return S_OK;
}

bool D3D9RenderDevice::IsDeviceLost()
{
	HRESULT hr = m_device->TestCooperativeLevel();

	if (hr == D3DERR_DEVICELOST)
	{
		Sleep(20);
		return true;
	}

	else if (hr == D3DERR_DRIVERINTERNALERROR)
	{
		MessageBox(0, L"Internal Driver Error...Exiting", 0, 0);
		PostQuitMessage(0);
		return true;
	}

	else if (hr == D3DERR_DEVICENOTRESET)
	{
		OnLostDevice();
		m_device->Reset(&m_settings.pp);
		OnResetDevice();
		return false;
	}
	else
		return false;
}

void D3D9RenderDevice::OnLostDevice()
{
	// Release utility pipelines
	if (m_Font) m_Font->OnLostDevice();
	SAFE_RELEASE(m_Sprite);

	// Release pipelines
	m_post->OnLostDevice();
}

HRESULT D3D9RenderDevice::OnResetDevice()
{
	HRESULT hr;

	// Reset utility pipelines
	if (m_Font) V_RETURN(m_Font->OnResetDevice());
	V_RETURN(D3DXCreateSprite(m_device, &m_Sprite));

	// Reset cameras
	auto cam = m_memory->GetDebugCamera();
	cam->OnResetDevice(m_settings.pp.BackBufferWidth, m_settings.pp.BackBufferHeight);

	// Reset pipelines
	m_post->OnResetDevice();

	return S_OK;
}

void D3D9RenderDevice::UpdateDeviceStats(double dt)
{
	//static int numFrames = 0;
	//static float timeElapsed = 0.0f;

	//if (!m_stats) return;

	//numFrames++;
	//timeElapsed += dt;

	//if (timeElapsed >= 1.0f)
	//{
	//	float fps = (float)numFrames;
	//	float mspf = 1.0f / fps;

	//	std::wostringstream outs;
	//	outs.precision(8);
	//	outs << m_windowCaption << L"    "
	//		<< L"FPS: " << fps << L"    "
	//		<< L"Frame Time: " << mspf;

	//	SetWindowText(m_mainWnd, outs.str().c_str());

	//	timeElapsed = 0.0f;
	//	numFrames = 0;
	//}
}

void D3D9RenderDevice::UpdateRenderers(double dt)
{
	//Get the list of renderer components
	auto comps = m_memory->GetRenderers();

	for (auto comp : comps)
	{
		if (comp)
		{
			comp->Update(dt);
		}
	}
}

#pragma endregion


#pragma region Protected Definitions

bool D3D9RenderDevice::Render()
{
	// Grab debug camera
	auto cam = m_memory->GetDebugCamera();

	HRESULT hr;

	// Clear the render target and the zbuffer
	V(m_device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_ARGB(0, 255, 255, 255), 1.0f, 0));

	// Render post processing pipline
	m_post->Render();

	return true;
}

#pragma endregion


#pragma region Standard Pipeline API

HRESULT D3D9RenderDevice::Init(MemoryDelegate* memory, RenderWindow* window, float desiredDrawRate, bool calculateStats)
{
	m_memory	= memory;
	m_window	= window;

	m_drawRate	= 1.0f / (float)desiredDrawRate;
	m_stats		= calculateStats;

	//Setup DirectX environment
	if (FAILED(InitEnvironment()))
		return E_FAIL;

	//Allocate utility pipelines
	g_GUI = _new GUI();

	// Init utility pipelines

	// Allocate pipelines
	//m_meshPipe = _new MeshPipeline();
	m_post = _new D3D9PostProcessPipeline();

	// Init pipelines
	if (FAILED(m_post->Init(memory, m_device, m_d3d)))
		return E_FAIL;

	OnResetDevice();

	m_active = true;

	return S_OK;
}

void D3D9RenderDevice::Shutdown()
{
	if (m_device)
	{
		OnLostDevice();

		m_window->BreakContext();

		m_post->Destroy();

		SAFE_RELEASE(m_Font);
		SAFE_RELEASE(m_d3d);
		SAFE_RELEASE(m_device);
		SAFE_RELEASE(m_Font);
		SAFE_RELEASE(m_Sprite);

		SAFE_DELETE(g_GUI);
		SAFE_DELETE(m_post);

		DestroyAllVertexDeclarations();

		m_deviceCreated = false;
	}
}

void D3D9RenderDevice::Update(double dt)
{
	HRESULT hr;

	if (m_deviceLost || m_paused || !m_active)
		Sleep(50);


	if (m_deviceLost && !m_paused)
	{
		if (FAILED(hr = m_device->TestCooperativeLevel()))
		{
			if (hr == D3DERR_DEVICELOST)
			{
				return;
			}

			if (FAILED(hr = RestEnvironment()))
			{
				if (hr == D3DERR_DEVICELOST)
				{
					return;
				}
				else if (hr == DXUTERR_RESETTINGDEVICEOBJECTS ||
						 hr == DXUTERR_MEDIANOTFOUND)
				{
					ENGINE_WARNING(L"Something bad happen. Device shutting down.");
					Shutdown();
					return;
				}
				else
				{
					ENGINE_WARNING(L"Something bad happen. Device needs to be recreated. NYI");
					Shutdown();
					return;
				}
			}
		}

		m_deviceLost = false;
	}

	// Update stats
	UpdateDeviceStats(dt);

	// Update renderer components
	UpdateRenderers(dt);

	// Render pipelines
	Render();

	// Render utility pipelines
	////m_text->Render();
	////m_lines->Render();

	// Present the final surface
	if (FAILED(hr = m_device->Present(NULL, NULL, NULL, NULL)))
	{
		if (hr == D3DERR_DEVICELOST)
		{
			m_deviceLost = true;
		}
		else if (hr == D3DERR_DRIVERINTERNALERROR)
		{
			m_deviceLost = true;
		}
	}
}

#pragma endregion


#pragma region Pipeline Feature set API

void D3D9RenderDevice::SetPostProcess(bool enable)
{
	m_post->m_enablePost = enable;
}

void D3D9RenderDevice::AddFilter(int filterID)
{
	m_post->AddFilter(filterID);
}

HRESULT D3D9RenderDevice::TakeScreenShot(const TCHAR* filename, _D3DXIMAGE_FILEFORMAT frmt /*= D3DXIFF_BMP*/)
{
	HRESULT hr = S_OK;
	if (!m_device)
		return E_FAIL;

	IDirect3DSurface9* backBuffer = NULL;
	V_RETURN(m_device->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuffer));

	hr = D3DXSaveSurfaceToFile(filename, frmt, backBuffer, NULL, NULL);
	SAFE_RELEASE(backBuffer);

	return hr;
}

#pragma endregion
