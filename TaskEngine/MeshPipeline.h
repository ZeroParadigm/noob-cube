#pragma once
#include "GraphicsUtility.h"
#include "GUI.h"

class MeshPipeline
{
private:
	friend class GraphicsSystem;

	float	m_FoV;
	float	m_NearZ;
	float	m_FarZ;
	Matrix4	m_Projection;

	PointLight			m_PointLight;
	DirectionalLight	m_GlobalLight;
	Material			m_Material;

	//D3D9SkyBox *SkyBox;
		
	//Effect Handles
	ID3DXEffect *m_FX;

	D3DXHANDLE m_tech;

	D3DXHANDLE m_WVP;
	D3DXHANDLE m_World;
	D3DXHANDLE m_WorldInvTrans;

	D3DXHANDLE m_EyePosW;

	D3DXHANDLE m_Mtrl;
	D3DXHANDLE m_Light;

	D3DXHANDLE m_Tex;

private:

	void buildFX();

public:

	MeshPipeline();
	~MeshPipeline();

	void onResetDevice();
	void onLostDevice();

	void render();

	void SetLens(float FOV, float aspect, float nearZ, float farZ);
};

